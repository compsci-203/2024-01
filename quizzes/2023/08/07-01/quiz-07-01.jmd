---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 07 (01)
author: "Instructor Xing Shi Cai"
date: 2023-10-12
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} \Large{} $\text{\emoji{bomb}}$ Exam Guidelines}]
\large{}
\begin{itemize}
    \item[\emoji{ab}] To write a proper proof, it is necessary to provide both equations and explanatory statements.
    \item[\emoji{robot}] When writing your name and short mathematical answers,
    use clear and distinct letters to facilitate AI recognition.
    \item[\emoji{pencil}] Avoid using pencil as its visibility diminishes upon scanning.
\end{itemize}
\end{tcolorbox}

\pagebreak{}

# Series (0.5pt)

A ball is dropped from a height of 4 ft above the ground, and upon each bounce off the ground the
ball bounces straight up to a height equal to 65\% of its previous height. Find the theoretical total
distance the ball could travel if it could bounce indefinitely.

```julia

short_answer(L"\text{Total distance (in ft)}", L"\frac{132}{7}")

answer = L"""
Solution:

1. **Initial Height**: The ball is initially dropped from a height of $h_0 = 4$ ft.
2. **Bounce Efficiency**: The ball reaches a height of $0.65 \times h_0$ on
   the first bounce, $0.65 \times h_1$ on the second, and so on.
3. **General Formula for Bounce Heights**: 
    $$
    h_n = 0.65^n \times h_0
    $$
4. **Total Distance Formula**: The total distance $D$ the ball travels can be represented as:
    $$
    D = h_0 + 2 \sum_{n=1}^{\infty} 0.65^n \times h_0
    $$
5. **Calculation**: Treating 0.65 as a rational number, the summation yields a total distance of $\frac{132}{7}$ ft, or approximately 18.8571 ft.
"""

print_in_solution(answer)

```

# Convergent Test (0.5pt)

Is the series $\sum_{n=1}^{\infty} \frac{1}{4n^2 - 1}$ convergent?
If converge, find its sum. Otherwise answer "diverge"

```julia

short_answer(L"\sum_{n=1}^{\infty} \frac{1}{4n^2 - 1}", L"\frac{1}{2}")

solution = L"""
Solution

Upon decomposing, we get:
\begin{equation*}
    \frac{1}{4n^2-1}=\frac{1}{2(2n-1)}-\frac{1}{2(2n+1)}
\end{equation*}
So the series converges by the telescoping series test and the limit
is $\frac{1}{2}$.
"""

print_in_solution(solution)

```

# Alternating Series (0.5pt)

Consider the series $\sum_{n=1}^{\infty} \frac{(-1)^n}{2 n -1}$.

Check the box before each true statement below.

```julia
print_in_quiz("""
* [ ] The series is conditional convergence.
* [ ] The series is absolute convergence.
""")
print_in_solution("""
* [x] The series is conditional convergence.
* [ ] The series is absolute convergence.
""")

```

# Power Series (0.5pt)

What is the interval of convergence of $\sum_{n=1}^{\infty} \frac{n x^n}{2(n+1)}$?

```julia

short_answer(L"\text{Interval of Convergence}", L"(-1, 1)")

```

# Taylor Series (1pt)

Let $f(x) = \sqrt{1 + x^2}$.
Write out the first three nonzero terms in the Taylor’s series for 
$f(x)$ about $0$.

```julia

x = Sym("x")
taylor_series = series(sqrt(x^2+1), x, 0, 6)

short_answer(L"f(x)", taylor_series)

solution = L"""
Solution:

To find the Taylor series of $\sqrt{1+x^2}$, we can start with the Taylor
series expansion of $\sqrt{1+y}$ centered at $y=0$:
\begin{equation*}
    \sqrt{1+y} = 1 + \frac{y}{2} - \frac{y^2}{8} + O(y^3)
\end{equation*}

Next, we substitute $y = x^2$
\begin{equation*}
    \sqrt{1+x^2} = 1 + \frac{x^2}{2} - \frac{x^4}{8} + O(x^6)
\end{equation*}

Thus, we have approximated $\sqrt{1+x^2}$ with terms up to $x^4$. Note that
obtaining the term $O(x^6)$ is not necessary for full marks.
"""

print_in_solution(solution)

```

`j print_in_quiz(L"\pagebreak{}")`

`j print_in_quiz(L"\centering --- This page is left empty --- \pagebreak{}")`

\begin{center}
--- This is the end of the quiz. ---
\end{center}

![The End](./bunny.jpg){width=400px}
