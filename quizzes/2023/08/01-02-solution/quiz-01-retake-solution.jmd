---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 01 Retake Solution
author: "Instructor Xing Shi Cai"
date: 2023-08-31
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
#using Plots
#using Random
#using Distributions
#using DataFrames

myseed = 1234;

function short_answer()
print(L"""
\bigskip{}
Answer: $\underline{\hspace{5cm}}$
""")
end;

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}
\large{}
\emoji{bomb} To write a proper proof, it is necessary to provide both equations and explanatory statements.
\end{tcolorbox}

\pagebreak{}

# Statements (0.5pt)

Which of the following statements are logically equivalent to
"If a person eats :carrot:, they can sing like a :bird:."

1. For a person to be able to sing like a :bird:, eating :carrot: is sufficient.
2. If a person cannot sing like a :bird:, they do not eat :carrot:.
3. If a person can sing like a :bird:, they eat :carrot:.
4. Having the ability to sing like a :bird: implies that a person eats :carrot:.
5. To eat :carrot:, a necessary condition is to be able to sing like a :bird:.

Answer: 
<!--
$\underline{\hspace{5cm}}$
-->
$\underline{1, 2, 5}$

# Functions (0.5pt)

The following functions all have domain $\{1, 2, 3, 4, 5\}$ 
and codomain $\{1, 2, 3, 4, 5, 6\}$. 
Which of the following functions are neither injective nor surjective?

$f: \left(\begin{array}{cccccc}
1 & 2 & 3 & 4 & 5 \\
1 & 3 & 4 & 5 & 3 
\end{array}\right)$

$g: \left(\begin{array}{cccccc}
1 & 2 & 3 & 4 & 5 \\
6 & 2 & 3 & 1 & 4
\end{array}\right)$

$h(x) =
\begin{cases} 
x & \text{if } x \leq 5 \\
x - 1 & \text{if } x > 5 
\end{cases}$

$\ell(x) =
\begin{cases} 
\frac{x+1}{2} & \text{if $x$ is odd} \\
x - 1 & \text{otherwise}
\end{cases}$

Answer:
<!--
$\underline{\hspace{5cm}}$
-->
$\underline{f, \ell}$

# Logic :gift:

Assume that we are on a magical island on which 

1. a troll is either a knight (who always tells the truth) or a knave (who always lies),
2. a :gift: contains either a :bomb: or a :carrot:.

A troll gave you two :gift: labelled x and y, and told you

* $S_1$: At least one of :gift: x and :gift: y contains a :carrot:;
* $S_2$: :gift: x contains a :bomb:.

Let $X$ be ":gift: x contains a :carrot:" and $Y$ be ":gift: y contains a :carrot:".

## (0.1pt)

Write $S_1$ with $X$ and $Y$.

<!--
Answer: $S_1 = \underline{\hspace{5cm}}$
-->
Answer: $S_1 = X \vee Y$

## (0.1pt)

Write $S_2$ with $X$ and $Y$.

<!--
Answer: $S_2 = \underline{\hspace{5cm}}$
-->
Answer: $S_2 = \neg X$

## (0.1pt)

Let $S = S_1 \iff S_2$, i.e., $S$ denotes "$S_1$ if and only if $S_2$".
Express $S$ in another way
using $S_1$, $S_2$, $\neg S_1$, $\neg S_2$, $\vee$ and $\wedge$.

<!--
Answer: $S = \underline{\hspace{5cm}}$
-->
Answer: $S = (S_1 \wedge S_2) \vee (\neg S_1 \wedge \neg S_2)$

## (0.5pt)

Create a truth table with columns $X$, $Y$ and $S$.

:bulb: You can add some auxiliary columns.

Answer:

<!--
\vfill{}
-->

| $X$   | $Y$   |  $\neg X$ | $S_1$   | $S_2$   | $S$   |
|-------|-------|-----------|---------|---------|-------|
|  T    |  T    |    F      |    T    |    F    |  F    |
|  T    |  F    |    F      |    T    |    F    |  F    |
|  F    |  T    |    T      |    T    |    T    |  T    |
|  F    |  F    |    T      |    F    |    T    |  F    |

## (0.2pt)

Is the troll a knight?

Answer:

<!--
- [ ] Yes
- [ ] No
-->

- [x] Yes
- [ ] No

# Proof (1pt)

Let $p$ be a prime number.
We know that an integer $a$ is divisible by $p$ if and only if $a^2$ is divisible by $p$.
Use this fact and proof by contradiction to show that $\sqrt{p}$ is
irrational.

Answer:

<!--
(Use space on the last page.)

\pagebreak{}
-->

Assume, for the sake of contradiction, that $\sqrt{p}$ is rational. Then it can be expressed as a fraction $\sqrt{p} = \frac{a}{b}$, where $a$ and $b$ are coprime integers, and $b \neq 0$.

Squaring both sides, we have:
\begin{equation*} p = \frac{a^2}{b^2} \implies a^2 = pb^2. \end{equation*}

Since $p$ is prime and it divides $a^2$, from the given result, it must divide $a$. So, we can write $a = kp$ for some integer $k$.

Substituting back into our equation:
\begin{equation*} (kp)^2 = pb^2 \implies k^2p^2 = pb^2 \implies k^2p = b^2. \end{equation*}

Again, since $p$ divides $b^2$, it must divide $b$. 
But this contradicts the assumption that $a$ and $b$ are coprime, as both are divisible by $p$.

Therefore, our assumption that $\sqrt{p}$ is rational must be false, and we conclude that $\sqrt{p}$ is irrational.

\begin{center}
--- This is the end of the quiz. ---
\end{center}

![Created by `https://bing.com/create`](./bunny.jpg){width=400px}
