---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 01
author: "Instructor Xing Shi Cai"
date: 2023-08-24
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
#using Plots
#using Random
#using Distributions
#using DataFrames

myseed = 1234;

function short_answer()
print(L"""
\bigskip{}
Answer: $\underline{\hspace{5cm}}$
""")
end;

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}
\large{}
\emoji{bomb} To write a proper proof, it is necessary to provide both equations and explanatory statements.
\end{tcolorbox}

\pagebreak{}

# Statements (0.5pt)

Which of the following statements are logically equivalent to
"If a person avoids heavy meals, they can run fast like a :rabbit_face:."

1. A person can run fast like a :rabbit_face: if and only if they avoid heavy meals.
2. A person can run fast like a :rabbit_face: only if they avoid heavy meals.
3. A person who can run fast like a :rabbit_face: must have avoided heavy meals.
4. Avoiding heavy meals is a necessary condition for a person to be able to run fast like a :rabbit_face:.
5. For a person to avoid heavy meals, it is sufficient to be able to run fast like a :rabbit_face:.
6. For a person to be able to run as fast like a :rabbit_face:, avoiding heavy meals is sufficient.
7. If a person has heavy meals, they cannot run fast like a :rabbit_face:.
8. If a person can run fast like a :rabbit_face:, they avoid heavy meals.
9. The ability to run fast like a :rabbit_face: implies that a person avoids heavy meals.
10. To avoid heavy meals, it is necessary to be able to run fast like a :rabbit_face:.

\bigskip{}
Answer: 
$\underline{\hspace{5cm}}$
<!--
$\underline{6, 10}$
-->

# Functions (0.5pt)

The following functions all have domain $\{1, 2, 3, 4, 5\}$ 
and codomain $\{1, 2, 3, 4\}$. 
Which of the following functions are neither injective nor surjective?

$f: \left(\begin{array}{cccccc}
1 & 2 & 3 & 4 & 5 \\
1 & 2 & 4 & 2 & 1 
\end{array}\right)$

$g: \left(\begin{array}{cccccc}
1 & 2 & 3 & 4 & 5 \\
4 & 1 & 2 & 3 & 4 
\end{array}\right)$

$h(x) =
\begin{cases} 
x & \text{if } x \leq 3 \\
x - 1 & \text{if } x > 3 
\end{cases}$

$\ell(x) =
\begin{cases} 
\frac{x+1}{2} & \text{if $x$ is odd} \\
x - 1 & \text{otherwise}
\end{cases}$

\bigskip{}
Answer:
$\underline{\hspace{5cm}}$
<!--
$\underline{f, \ell}$
-->

# Logic (1pt)

Assume that we are on a magical island on which 

1. a troll is either a knight (who always tells the truth) or a knave (who always lies),
2. a :gift: contains either a :bomb: or a :carrot:.

A troll gave you two :gift: labelled x and y, and told you

* $S_1$: Either :gift: x contains a :bomb: or :gift: y contains a :carrot:;
* $S_2$: :gift: x contains a :carrot:.

Let $X$ be ":gift: x contains a :carrot:" and $Y$ be ":gift: y contains a :carrot:".

## (0.1pt)

Write $S_1$ with $X$ and $Y$.

\bigskip{}
Answer: $S_1 = \underline{\hspace{5cm}}$
<!--
Answer: $S_1 = \neg X \vee Y$
-->

## (0.1pt)

Write $S_2$ with $X$ and $Y$.

\bigskip{}
Answer: $S_2 = \underline{\hspace{5cm}}$
<!--
Answer: $S_2 = X$
-->

## (0.1pt)

Since the troll is either a knight or a knave,
$S_1$ and $S_2$ must both true, or both false.
Use $S$ to express this.

\bigskip{}
Answer: $S = \underline{\hspace{5cm}}$
<!--
Answer: $S = S_1 \leftrightarrow S_2$
-->

## (0.5pt)

Create a truth table with columns $X$, $Y$ and $S$.

:bulb: You can add some auxiliary columns.

Answer:

\vspace{5cm}

<!--
| $X$ | $Y$ | $\neg X$ | $S_1$ | $S_2$ | $S$ |
|-------|-------|-----------|---------|---------|-------|
|  T    |  T    |    F      |    T    |    T    |  T    |
|  T    |  F    |    F      |    F    |    T    |  F    |
|  F    |  T    |    T      |    T    |    F    |  F    |
|  F    |  F    |    T      |    T    |    F    |  F    |

Here's how to read the table:

- $X$ and $Y$ are the possibilities for whether x and y contain a carrot, respectively.
- $\neg X$ is the negation of $X$.
- $S_1$ is $\neg X \vee Y$, meaning either x doesn't contain a carrot or y contains a carrot.
- $S_2$ is the same as $X$, meaning x contains a carrot.
- $S$ is the equivalence of $S_1$ and $S_2$, meaning both are either true or false.

From the table, we can see that there is only one scenario where both statements
are true, which is when x contains a carrot and y contains a carrot. In all
other scenarios, the statements are not consistent with each other, reflecting
the paradoxical nature of trolls!
-->

## (0.2pt)

Select all the :gift: which contains a :carrot:.

Answer:

- [ ] :gift: x
- [ ] :gift: y
<!--
- [ ] :gift: x
- [ ] :gift: y
-->


# Proof (1pt)

Use proof by contrapositive to show that if $a^2$ is divisible by $5$, then $a$ is divisible by $5$.

Answer:

(Use the space on next page to write.)

<!--
The contrapositive of the statement "If $a^2$ is divisible by 5, then $a$ is divisible by 5" is "If $a$ is not divisible by 5, then $a^2$ is not divisible by 5."

So we'll prove this contrapositive statement:

1. **Assume that $a$ is not divisible by 5.** Then $a$ can be written as $a = 5k + r$, where $k$ is an integer and $r$ is one of the remainders 1, 2, 3, or 4 when dividing by 5.
2. **Square both sides:** $a^2 = (5k + r)^2 = 25k^2 + 10kr + r^2$.
3. **Now let's examine the remainder when dividing by 5:** Since $r$ is one of 1, 2, 3, or 4, $r^2$ will also leave a remainder when divided by 5. The entire expression for $a^2$ will also leave a remainder when divided by 5, as the other terms are clearly divisible by 5.
4. **Conclude that $a^2$ is not divisible by 5:** Since the remainder is not zero, $a^2$ is not divisible by 5.

Thus, we've proved the contrapositive, so the original statement is also true: If $a^2$ is divisible by 5, then $a$ is divisible by 5. 
-->

\pagebreak{}


\vspace*{12cm}


\begin{center}
--- This is the end of the quiz. ---
\end{center}

![Created by `https://bing.com/create`](./bunny.jpg){width=400px}
