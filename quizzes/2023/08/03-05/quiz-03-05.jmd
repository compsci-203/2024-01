---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 03 (05)
author: "Instructor Xing Shi Cai"
date: 2023-09
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using SymPy
using MathLink
using WeaveHelper

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} $\text{\emoji{rabbit-face}}$ Honour Pledge}]
\large{}
During the quiz/exam I will \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{eye}] consult any material other than one A4-sized and double-sided cheat sheet;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}

\bigskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} \Large{} $\text{\emoji{bomb}}$ Exam Guidelines}]
\large{}
\begin{itemize}
    \item[\emoji{ab}] To write a proper proof, it is necessary to provide both equations and explanatory statements.
    \item[\emoji{robot}] When writing your name and short mathematical answers,
    use clear and distinct letters to facilitate AI recognition.
    \item[\emoji{pencil}] Avoid using pencil as its visibility diminishes upon scanning.
\end{itemize}
\end{tcolorbox}

\pagebreak{}

# Inclusion Exclusion Formula (0.5pt)

How many integer solutions are there to the equation 
$x_1 + x_2 + x_3 = 22$ with
$2 \le x_i \le 6$ for $i \in [3]$?

:bulb: Answer with a formula containing binomial coefficients.

```julia;

answer_short = L"""
    \binom{19-1}{3-1}
    -
    \binom{3}{1}
    \binom{19-5-1}{3-1}
    +
    \binom{3}{2}
    \binom{19-10-1}{3-1}
    -
    \binom{3}{3}
    \binom{19-15-1}{3-1}
"""
short_answer("", answer_short, connector="")
```

# Generating Functions (0.5pt)

```julia; results="hidden"

x = Sym("x")
expr = 2 / (1 - x^3) + 1 / (1 + x)
expr_simplified = sympy.simplify(expr)
taylor_expansion = sympy.series(expr, x, 0, 17).removeO()
polynomial = sympy.Poly(taylor_expansion, x)
coefficients = polynomial.all_coeffs()[1:end-1]

```

For the following infinite sequence
\begin{equation*}
    `j print(join(coefficients, ", "))`, \ldots
\end{equation*}
find its generating function in closed form, i.e., not as an infinite sum.

`j short_answer(L"\text{The generating function}", [expr, expr_simplified])`

# Generating Functions for Zoo Collections (1pt)

Let $a_n$ be the number of different types of
zoo collections containing $n$ animals chosen from :tiger_face:, :rabbit_face:,
and :zebra: that can be made subject to the following restrictions:

- There are at least 3 :tiger_face:,
- The number of :rabbit_face: is a multiple of 4,
- There are at most 3 :zebra:.

## (0.5pt)

Write down the generating function of $a_n$ in closed form.

<!--
$\underline{\hspace{10cm}}$
-->

```julia; results="hidden"

x = Sym("x")
gf = x^3/(1-x) * 1/(1-x^4) * (1+x+x^2+x^3)
gf_simplified = sympy.simplify(gf).factor()
gf_str = sympy.mathematica_code(gf_simplified)

coefficient_expr = "ToString[TeXForm[FullSimplify[SeriesCoefficient[$gf_str, {x, 0, n}], n \\[Element] Integers && n >= 0 ]]]"
answer = weval(W`$coefficient_expr`)


```

`j short_answer(L"\sum_{n=0}^n a_n x^n", [gf, gf_simplified])`

## (0.5pt)

Find a closed formula of $a_n$ for $n > 0$.

`j short_answer(L"a_n", answer)`

# Exponential Generating Functions and Strings (1pt)

```julia; results="hidden"

x = Sym("x")
gf = ((exp(x)+exp(-x))/2)+((exp(x)+exp(-x))/2)*((exp(x)-exp(-x))/2)
gf_simplified = sympy.expand(gf)
gf_str = sympy.mathematica_code(gf_simplified)

coefficient_expr = "ToString[TeXForm[FullSimplify[n!*SeriesCoefficient[$gf_str, {x, 0, n}], n \\[Element] Integers && n >= 0 ]]]"
answer = weval(W`$coefficient_expr`)

```

Let $a_n$ represent the number of strings of length $n$ composed from the set
$\{\text{\emoji{apple}, \emoji{banana}, \emoji{cherries}}\}$ that must
satisfy \emph{exactly one} of the following conditions:

\begin{itemize}
    \item The string is comprised solely of even numbers \emoji{apple}s,
    \item or the string includes an even number of \emoji{banana}s and an odd number of \emoji{cherries}s.
\end{itemize}

## (0.5pt)

Find the exponential generating function of $(a_n)_{n \in \mathbb{N}}$ in closed
from.

`j short_answer(L"\sum_{n=0}^\infty \frac{a_n}{n!} x^n", [gf, gf_simplified])`

## (0.5pt)

Find a formula of $a_n$ for $n \ge 0$ in closed form.

`j short_answer(L"a_n", answer)`

\begin{center}
--- This is the end of the quiz. ---
\end{center}
