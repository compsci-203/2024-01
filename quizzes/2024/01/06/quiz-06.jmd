---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 06
author: "Instructor: Xing Shi Cai"
date: 2024-02-29
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper
using Random
using Printf
using MathLink
using PyCall
using Graphs
using GraphPlot, Compose, Cairo, Colors

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your name and \textbf{Duke ID} (not your Net ID) in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Name: $`j print_empty_box(5.7, 1.5)`$
    \hfil
    Duke ID: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}


# Degrees (1pt)

```julia; results="hidden"

deg = [4, 4, 3, 2, 1, 1]
deg_str = join(deg, ", ")
deg_sum = sum(deg)

answer = L"""
Such a graph does not exist because the sum of the degrees should be twice of the number
of edges, i.e., an even number.
However, the given degrees sum to $%$deg_sum$.
"""
```

Draw a graph with $6$ vertices having degrees $(`j print(deg_str)`)$ or explain why such
a graph does not exist.

`j long_answer(answer, vfill=true)`

# Prüfer Code (1pt)

```julia; results="hidden"

Random.seed!(1237)
vertex_num = 9
sequence = rand(1:vertex_num, vertex_num-2)
nx_sequence = sequence .- 1
nx = pyimport("networkx")
nx_tree = nx.from_prufer_sequence(nx_sequence)
nx_edges = collect(nx_tree.edges())
tree_edges = map(e->e .+ (1, 1), nx_edges)
tree = Graph(vertex_num)
for e in tree_edges
    add_edge!(tree, e[1], e[2])
end
plt = gplot(tree,
        nodelabel=1:vertex_num,
        NODELABELSIZE=40,
        NODESIZE=0.12,
        EDGELINEWIDTH=4,
        nodefillc=colorant"orange",
        nodestrokec=colorant"black",
        nodestrokelw=0.5,
        )
file_path = tempname() * ".pdf"
draw(PDF(file_path, 64cm, 64cm), plt)

```

Draw the tree of $`j vertex_num`$ vertices labelled with $1, \cdots, 9$ whose Prüfer code is
$(`j print(join(sequence, ", "))`)$

```julia; results="tex"

answer = L"""
\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.4\textwidth]{%$file_path}
    \caption{Answer to Problem}
\end{figure}
"""

long_answer(answer, vfill=true)

quiz_pagebreak()
```

# Hamiltonian Graph (1pt)

Determine if the following graph is hamiltonian.

- If it is hamiltonian, highlight the edges that form a hamiltonian cycle by thickening them.
- If it is not hamiltonian, provide an explanation as to why it cannot have a hamiltonian cycle.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.5\textwidth]{./graph.png}
    \caption{The graph for Problem 3}
\end{figure}

```julia; results="tex"

answer = L"""
No, this is not a hamiltonian graph.

This is the bipartite graph $K_{3,4}$.
A bipartite graph in which each part has different number of vertices cannot be hamiltonian.
"""

long_answer(answer, vfill=true)

quiz_pagebreak()
```

# Trees (1pt)

Prove that if $T$ is a tree with $v$ vertices and $e$ edges,
then $e = v - 1$.

```julia; results="tex"

answer = L"""
This is Proposition 4.2.4 in Discrete Mathematics (DM).
The proof in the text book removes an edge connected to a leaf.
This is slightly different from what is given in class, which removes an abitrary edge
from the tree.
Both solutions will be considerred correct.
"""

long_answer(answer, vfill=true)
```

