(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      7986,        252]
NotebookOptionsPosition[      6201,        211]
NotebookOutlinePosition[      6590,        227]
CellTagsIndexPosition[      6547,        224]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["3", "Section",
 CellChangeTimes->{
  3.913762422846012*^9},ExpressionUUID->"800dc151-1086-4341-92f4-\
05ddebd8367e"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"sr", "[", 
    RowBox[{"R_", ",", "B_"}], "]"}], ":=", 
   RowBox[{"Xor", "[", 
    RowBox[{
     RowBox[{"Not", "[", "R", "]"}], ",", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"R", "&&", "B"}], ")"}], "||", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Not", "[", "R", "]"}], "&&", 
        RowBox[{"Not", "[", "B", "]"}]}], ")"}]}]}], "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"sb", "[", 
    RowBox[{"R_", ",", "B_"}], "]"}], ":=", 
   RowBox[{"Xor", "[", 
    RowBox[{
     RowBox[{"Not", "[", "B", "]"}], ",", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"R", "&&", 
        RowBox[{"Not", "[", "B", "]"}]}], ")"}], "||", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Not", "[", "R", "]"}], "&&", "B"}], ")"}]}]}], "]"}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.913762426880801*^9, 3.9137624332470703`*^9}, {
  3.914808905405744*^9, 3.914808908631299*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"2a28d6f9-35d5-44f8-a615-6f831b9355fe"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"sr", "[", 
   RowBox[{"R", ",", "B"}], "]"}], "//", "LogicalExpand"}]], "Input",
 CellChangeTimes->{{3.914808911002448*^9, 3.914808921420231*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"6ebcbca3-adac-4614-91d9-5445071cee67"],

Cell[BoxData["B"], "Output",
 CellChangeTimes->{{3.914808914239407*^9, 3.914808921651022*^9}},
 CellLabel->"Out[8]=",ExpressionUUID->"3f94b3cc-4a89-4ea1-9bc1-cd290e85cb92"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"sb", "[", 
   RowBox[{"R", ",", "B"}], "]"}], "//", "LogicalExpand"}]], "Input",
 CellChangeTimes->{{3.914808930600079*^9, 3.91480893085609*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"135de0f2-8a0c-47d1-8ce2-6c636efe2605"],

Cell[BoxData[
 RowBox[{"!", "R"}]], "Output",
 CellChangeTimes->{3.914808931274562*^9},
 CellLabel->"Out[9]=",ExpressionUUID->"306bf257-5860-418a-8a66-5354c566c34d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"truthTable", "=", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"R", ",", "B", ",", 
       RowBox[{"sr", "[", 
        RowBox[{"R", ",", "B"}], "]"}], ",", 
       RowBox[{"sb", "[", 
        RowBox[{"R", ",", "B"}], "]"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"R", ",", 
       RowBox[{"{", 
        RowBox[{"True", ",", "False"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"B", ",", 
       RowBox[{"{", 
        RowBox[{"True", ",", "False"}], "}"}]}], "}"}]}], "]"}]}], 
  ";"}], "\n", 
 RowBox[{
  RowBox[{"Flatten", "[", 
   RowBox[{"truthTable", ",", "1"}], "]"}], "//", "Grid"}]}], "Input",
 CellChangeTimes->{{3.913762426880801*^9, 3.9137624332470703`*^9}, 
   3.914808905405744*^9},ExpressionUUID->"853a779a-64d6-418d-8028-\
22aed448a58e"],

Cell[BoxData[
 TagBox[GridBox[{
    {"True", "True", "True", "False"},
    {"True", "False", "False", "False"},
    {"False", "True", "True", "True"},
    {"False", "False", "False", "True"}
   },
   AutoDelete->False,
   GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
  "Grid"]], "Output",
 CellChangeTimes->{3.913762433969949*^9, 3.9148089035695667`*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"59e8d997-01c3-437b-9e80-f321891e5eb4"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"!", 
   RowBox[{"(", 
    RowBox[{"Xor", "[", 
     RowBox[{"R", ",", "B"}], "]"}], ")"}]}], "//", 
  "LogicalExpand"}]], "Input",
 CellChangeTimes->{{3.9148093969093657`*^9, 3.914809401775353*^9}, {
  3.9148094613534737`*^9, 3.914809469928288*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"0a4cbe44-61ab-4d30-84c8-ee2dba35a78c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"B", "&&", "R"}], ")"}], "||", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"!", "B"}], "&&", 
    RowBox[{"!", "R"}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.914809450854663*^9, 3.914809470298732*^9}},
 CellLabel->"Out[4]=",ExpressionUUID->"d8527415-007a-4005-aa8b-803ceb0bb2bf"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"R", "&&", "B"}], ")"}], "||", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"Not", "[", "R", "]"}], "&&", 
     RowBox[{"Not", "[", "B", "]"}]}], ")"}]}], "//", 
  "LogicalExpand"}]], "Input",
 CellChangeTimes->{{3.914809423413999*^9, 3.914809427597406*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"6caa4f84-482a-4cd0-aadc-4b3aa325a00b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"B", "&&", "R"}], ")"}], "||", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"!", "B"}], "&&", 
    RowBox[{"!", "R"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.914809452624682*^9},
 CellLabel->"Out[2]=",ExpressionUUID->"2419f8c4-2223-45ff-a91b-4978d2833b8f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"R", "\[Implies]", "B"}], ")"}], "&&", 
   RowBox[{"(", 
    RowBox[{"B", "\[Implies]", "R"}], ")"}]}], "//", 
  "LogicalExpand"}]], "Input",
 CellChangeTimes->{{3.914809856394361*^9, 3.914809874966555*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"2cb5bef9-ebeb-4e29-b91f-0bd0a3f238cf"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"B", "&&", "R"}], ")"}], "||", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"!", "B"}], "&&", 
    RowBox[{"!", "R"}]}], ")"}]}]], "Output",
 CellChangeTimes->{3.914809875707621*^9},
 CellLabel->"Out[6]=",ExpressionUUID->"5ce8ffee-9a2e-48e1-85b9-8f2de4b8e9fd"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1152, 648},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"f8e50dad-7479-4c8e-8753-48b6f53ddcd0"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 122, 3, 67, "Section",ExpressionUUID->"800dc151-1086-4341-92f4-05ddebd8367e"],
Cell[705, 27, 1034, 32, 50, "Input",ExpressionUUID->"2a28d6f9-35d5-44f8-a615-6f831b9355fe"],
Cell[CellGroupData[{
Cell[1764, 63, 259, 5, 29, "Input",ExpressionUUID->"6ebcbca3-adac-4614-91d9-5445071cee67"],
Cell[2026, 70, 172, 2, 33, "Output",ExpressionUUID->"3f94b3cc-4a89-4ea1-9bc1-cd290e85cb92"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2235, 77, 258, 5, 29, "Input",ExpressionUUID->"135de0f2-8a0c-47d1-8ce2-6c636efe2605"],
Cell[2496, 84, 165, 3, 33, "Output",ExpressionUUID->"306bf257-5860-418a-8a66-5354c566c34d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2698, 92, 837, 25, 50, "Input",ExpressionUUID->"853a779a-64d6-418d-8028-22aed448a58e"],
Cell[3538, 119, 460, 11, 82, "Output",ExpressionUUID->"59e8d997-01c3-437b-9e80-f321891e5eb4"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4035, 135, 362, 9, 29, "Input",ExpressionUUID->"0a4cbe44-61ab-4d30-84c8-ee2dba35a78c"],
Cell[4400, 146, 331, 9, 33, "Output",ExpressionUUID->"d8527415-007a-4005-aa8b-803ceb0bb2bf"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4768, 160, 399, 11, 29, "Input",ExpressionUUID->"6caa4f84-482a-4cd0-aadc-4b3aa325a00b"],
Cell[5170, 173, 307, 9, 33, "Output",ExpressionUUID->"2419f8c4-2223-45ff-a91b-4978d2833b8f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5514, 187, 349, 9, 29, "Input",ExpressionUUID->"2cb5bef9-ebeb-4e29-b91f-0bd0a3f238cf"],
Cell[5866, 198, 307, 9, 33, "Output",ExpressionUUID->"5ce8ffee-9a2e-48e1-85b9-8f2de4b8e9fd"]
}, Open  ]]
}, Open  ]]
}
]
*)

