---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 05
author: "Instructor: Xing Shi Cai"
date: 2024-02-22
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper
using Random
using Printf
using MathLink

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your name and \textbf{Duke ID} (not your Net ID) in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Name: $`j print_empty_box(5.7, 1.5)`$
    \hfil
    Duke ID: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

# Integer Partition (1pt)

Let $d_n$ be the number of ways to partition a natural number $n$ into distinct parts.
Let $o_n$ be the number of ways to partition a natural number $n$ into odd parts.
Prove that $d_n = o_n$ for all $n \in \mathbb{N}$.

:bulb: This is Theorem 8.16 in Applied Combinatorics.
You should prove the theorem by showing the generating function of $d_n$ and $o_n$ are
the same.

```julia

answer = L"""
See the textbook.
"""

long_answer(answer)
```

`j quiz_pagebreak()`

# Exponential Generating Function (1pt)

```julia; results="hidden"

@syms x
egf = "Exp[2x]*x"
expr = MathLink.parseexpr("""
    ToString[
        TeXForm[
            FullSimplify[
                n!*SeriesCoefficient[
                    $egf, {x,0,n}
                ], 
                Assumptions->n>=0
                ]
            ]
        ]""")
coeff_str = weval(expr)
coeff_str = replace(coeff_str, "True" => "Otherwise")
egf_str = weval(MathLink.parseexpr("ToString[TeXForm[$egf]]"))


```

Give a formula in closed form for the sequence $\{a_n\}_{n \ge 0}$ whose
*exponential generating function* is $`j print(egf_str)`$.


`j short_answer(L"a_n", coeff_str, height=4)`

# Linear Recurrence (1pt)

Find the general solution to the nonhomogeneous linear recurrence
\begin{equation*}
(A-3)^3 f=3n+1
\end{equation*}

:bulb: This is Exercise 9.9.9 (c) in Applied Combinatorics.

`j short_answer(L"f(n)", L"c_1 3^nn^2+c_2 3^n n+c_3 3^n -\frac{3 n}{8}-\frac{11}{16}", width=11)`

```julia
answer = L"""
"""

print_in_solution(answer)
```

# Generating Function (1pt)


Consider the sequence $\{r_n\}_{n \geq 0}$, which is defined by the recurrence relation
$r_n = r_{n-1} + 6r_{n-2}$ for $n \geq 2$, with the initial conditions $r_0 = 1$ 
and $r_1 = 3$.
Your task is to find the generating function for the sequence $\{r_n\}_{n \geq 0}$
*without* directly solving the recurrence relation. 

Please describe the steps you took to find the generating function.

:bulb: This is Exercise 9.9.15 in Applied Combinatorics.

:bulb: If you require more space for your solution, feel free to use the space provided on the next page.

```julia

answer = L"""
The generating function is
\begin{equation*}
    \frac{1}{1-3x}
\end{equation*}
See the solution manuls for the steps to find the generating function.
"""

long_answer(answer)
```

`j quiz_pagebreak()`

`j print_in_quiz(L"\null{}\vfill{}")`

