---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 02
author: "Instructor Xing Shi Cai"
date: 2024-01-25
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper
using Random
using Printf

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your \textbf{Duke ID} (not your Net ID) in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Duke ID: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

# Series (1pt)

Write the given repeating decimal as a rational.

## (a)

```julia

function to_rational(x, y, d)
    yr = rationalize(y)
    i = Sym("i")
    return rationalize(x)+sympy.Sum(yr*10^(-i*d), (i, 0, sympy.oo)).doit()
end

function to_rational(y, d)
    return to_rational(0.0, y, d)
end

r = to_rational(0.113, 3)

short_answer(L"0.\overline{113}", L"%$r")

```

## (b)

```julia

r = to_rational(0.01, 0.007, 1)

short_answer(L"0.01\overline{7}", L"%$r")

```

# Convergent Test (1pt)

Select *all* the series below which converges.

* [ ]  $\sum_{n=0}^{\infty} \cos\left(2 n \pi\right)$.
* `j print_checked()` $\sum_{n=11}^{\infty} \cfrac{1}{n(n-1)}$.
* `j print_checked()` $\sum_{n=2}^{\infty} \cfrac{n+1}{3^n(n-1)}$.
* [ ]  $\sum_{n=1}^{\infty} \cfrac{1}{n+\sqrt{n}}$.
* `j print_checked()` $\sum_{n=16}^{\infty} \cfrac{\log_2(n)}{n^{1.1}}$.

:bomb: Any incorrect choice will result in a zero point for this question.

:bulb: The tests we have seen in this course are:

1. Monotone Bounded Test
2. Comparison Test for Series 
3. $n$-th Term Test for Divergence 
4. Ratio Test 
5. Integral Test 
6. p-series Test 
7. Limit Comparison Test
8. Telescoping Series Test 
9. Alternating Series Test 
10. Absolute Convergence Test 

```julia

answer = L"""
Explanation:

The first one is the same as $\sum 1$.

The second one is a telescoping series.

The third one converges by the ratio test.

The fourth one diverges by comparison with $\sum \frac{1}{2 n}$.

The last one converges by noticing that $\log_2(n) \le \sqrt{n}$ for $n \ge 16$.
"""

print_in_solution(answer)

```

# Alternating Series (1pt)

Decide if the given series diverges, converges conditionally, or converges absolutely.

## (a)

\begin{equation*}
    \sum_{n=2}^{\infty} \frac{(-1)^n}{n \log_2(n)}
\end{equation*}

* [ ] Diverges
* `j print_checked()` Converges conditionally
* [ ] Converges absolutely

:bomb: Incorrect answer will result in a zero point for (b).

## (b)

Prove your answer in (a).

:bulb: You can use the following:

1. $(\log(x))' = 1/x$ where $\log(x)$ denote the natural logarithm of $x$,
2. and the chain rule for taking derivatives:
\begin{equation*}
    (u(v(x))'
    =
    u'(v(x)) v'(x)
\end{equation*}

```julia

@syms x
ret = sympy.latex(integrate(1/(x*log(2,x)), x))

answer = L"""
The sequence converges by the alternating series test.

Note that
\begin{equation*}
    \int_{2}^{\infty} \frac{1}{x \log_2(x)} \, \mathrm{d}x
    =
    \log(2)
    \int_{2}^{\infty} \frac{1}{x \log(x)} \, \mathrm{d}x
    =
    \left.
    %$ret
    \right|_{2}^{\infty}
    =
    \infty
\end{equation*}
So by the integral test, we can conclude that
\begin{equation*}
    \sum_{n=2}^{\infty} \frac{1}{n \log_2(n)}
    =
    \infty
\end{equation*}
Thus the sequence $\sum_{n=2}^{\infty} \frac{(-1)^n}{n \log_2(n)}$ converge conditionally.
"""

long_answer(answer, vfill=true)

```

`j quiz_pagebreak()`

## (c)

\begin{equation*}
    \sum_{n=0}^∞ \frac{(-1)^n \pi^{1 + 2 n}}{(1 + 2 n)!}
\end{equation*}

* [ ] Diverges
* [ ] Converges conditionally
* `j print_checked()` Converges absolutely

:bomb: Incorrect answer will result in a zero point for (d).

## (d)

Prove your answer in (c).

```julia

answer = L"""
Let
\begin{equation*}
    a_n = 
    \frac{(-1)^n \pi^{1 + 2 n}}{(1 + 2 n)!}
\end{equation*}
Then
\begin{equation*}
    \lim_{n \to ∞} 
    \frac{|a_{n+1}|}{|a_n|}
    =
    \lim_{n \to ∞} 
    \frac{\pi^{1 + 2 (n+1)}}{(1 + 2 (n+1))!}
    \frac{(1 + 2 n)!}{\pi^{1 + 2 n}}
    \lim_{n \to ∞} 
    \frac{\pi^2}{(2n+3)(2n+2)}
    =
    0
\end{equation*}
So by the ratio test, we can conclude that the sereis converge absolutely.
"""

long_answer(answer, vfill=true)
```

# Taylor Series (1pt)

Let $f(x) = \frac{x}{\sqrt{1 + x^3}}$.
Write out the first three nonzero terms in the Taylor's series for 
$f(x)$ about $0$.

:bulb: You do not need to include the big-$O$ term.

```julia

x = Sym("x")
taylor_series = series(x/sqrt(x^3+1), x, 0, 10)

short_answer(L"f(x)", taylor_series)

y = Sym("y")
taylor_series_y = series(1/sqrt(y+1), y, 0, 3)


solution = L"""
Explanation:

To find the Taylor series of $\frac{x}{\sqrt{1+x^3}}$, we can start with the Taylor
series expansion of $\frac{1}{\sqrt{1+y}}$ centered at $y=0$:
\begin{equation*}
    \frac{1}{\sqrt{1+y}}
    =
    %$(sympy.latex(taylor_series_y))
\end{equation*}

Next, we substitute $y = x^3$
\begin{equation*}
    \frac{1}{\sqrt{1+x^3}}
    =
    %$(sympy.latex(taylor_series_y.subs(y, x^3)))
\end{equation*}
Multiply both sides by $x$, we have
\begin{equation*}
    \frac{x}{\sqrt{1+x^2}}
    =
    %$(sympy.latex(x*taylor_series_y.subs(y, x^3)))
\end{equation*}
"""

print_in_solution(solution)
```
