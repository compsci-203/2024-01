---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 00 (Mock Quiz)
author: "Instructor: Xing Shi Cai"
date: 2024-01-08
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper
using Random

```

\begin{tcolorbox}[title={\centering{} $\text{\emoji{eye}}$ Don't Cheat}]
During the quiz/exam do \emph{not}
\begin{itemize}
\item[\emoji{adhesive-bandage}] give or receive aid;
\item[\emoji{stopwatch}] read the questions before the quiz begins;
\item[\emoji{stop-sign}] keep writing when the quiz ends;
\item[\emoji{iphone}] use a phone, calculator, computer or the internet;
\item[\emoji{toilet}] hide textbooks or other aids in the toilet.
\end{itemize}
\end{tcolorbox}

\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black, title={\centering{} $\text{\emoji{zap}}$ Follow Rules}]
\begin{itemize}
    \item[\emoji{student}] Enter your \textbf{Duke ID} (not your Net ID) in the designated space below.
    \item[\emoji{ab}] Support your proofs with both equations and descriptive reasoning.
    \item[\emoji{pencil}] Use only pen for writing as pencil marks are not clearly visible.
    \item[\emoji{question}] Restrict your inquiries to clarifications on typos or English vocabulary only.
\end{itemize}
\end{tcolorbox}

\begin{center}
    Exam ID: $`j print_empty_box(5.7, 1.5)`$
\end{center}

![Good Luck](./bunny.jpg){width=240px}

# Statements (1pt)

Consider the statement 

* $S$ -- If a person avoids heavy meals, they can run fast like a :rabbit_face:.

Select the following statements which are equivalent to $S$.

* [ ] 1. A person can run fast like a :rabbit_face: if and only if they avoid heavy meals.
* [ ] 2. A person can run fast like a :rabbit_face: only if they avoid heavy meals.
* [ ] 3. A person who can run fast like a :rabbit_face: must have avoided heavy meals.
* [ ] 4. Avoiding heavy meals is a necessary condition for a person to be able to run fast like a :rabbit_face:.
* [ ] 5. For a person to avoid heavy meals, it is sufficient to be able to run fast like a :rabbit_face:.

```julia
rubric = L"""
**Answer:**
None of these are equivalent to $S$.

**Rubrics:**
For each wrong choice, -0.2pt.
"""

print_in_solution(rubric)
```

# Functions (1pt)

The following functions all have domain $\{1, 2, 3, 4, 5\}$ 
and codomain $\{1, 2, 3, 4\}$. 
Which of the following functions are neither injective nor surjective?
\begin{itemize}
    \item $f: \left(\begin{array}{cccccc}
    1 & 2 & 3 & 4 & 5 \\
    1 & 2 & 4 & 2 & 1 
    \end{array}\right)$
    \item $g: \left(\begin{array}{cccccc}
    1 & 2 & 3 & 4 & 5 \\
    4 & 1 & 2 & 3 & 4 
    \end{array}\right)$
    \item $h(x) =
    \begin{cases} 
    x & \text{if } x \leq 3 \\
    x - 1 & \text{if } x > 3 
    \end{cases}$
    \item $\ell(x) =
    \begin{cases} 
    \frac{x+1}{2} & \text{if $x$ is odd} \\
    x - 1 & \text{otherwise}
    \end{cases}$
\end{itemize}

`j short_answer(L"f, \ell")`

```julia
rubric = L"""
**Rubrics:**
For each wrong choice, -0.25pt.
"""

print_in_solution(rubric)
```
# Logic (1pt)

Assume that we are on a magical island on which 

1. a troll is either a knight (who always tells the truth) or a knave (who always lies),
2. a :gift: contains either a :bomb: or a :carrot:.

A troll gave you two :gift: labelled x and y, and told you

* $S_1$: Either :gift: x contains a :bomb: or :gift: y contains a :carrot:;
* $S_2$: :gift: x contains a :carrot:.

Let $X$ be ":gift: x contains a :carrot:" and $Y$ be ":gift: y contains a :carrot:".

## (0.1pt)

Write $S_1$ with $X$ and $Y$.

`j short_answer(L"S_1", L"\neg X \vee Y")`

## (0.1pt)

Write $S_2$ with $X$ and $Y$.

`j short_answer(L"S_2", L"X")`

## (0.1pt)

Since the troll is either a knight or a knave,
$S_1$ and $S_2$ must both true, or both false.
Let $S$ denote this and write it in terms of $S_1$ and $S_2$.

`j short_answer(L"S", L"S_1 \leftrightarrow S_2")`

## (0.5pt)

Create a truth table with columns $X$, $Y$ and $S$.

:bulb: You can add some auxiliary columns.

Answer:

```julia

template = L"""
\begin{center}
\huge
\begin{tabular}{|c|c|c|c|c|c|}
\hline
$X$ & $Y$ & $\neg X$ & $S_1$ & $S_2$ & $S$ \\
\hline
T & T & & & & \\
T & F & & & & \\
F & T & & & & \\
F & F & & & & \\
\hline
\end{tabular}
\end{center}
"""

answer = L"""
| $X$ | $Y$ | $\neg X$ | $S_1$ | $S_2$ | $S$ |
|-------|-------|-----------|---------|---------|-------|
|  T    |  T    |    F      |    T    |    T    |  T    |
|  T    |  F    |    F      |    F    |    T    |  F    |
|  F    |  T    |    T      |    T    |    F    |  F    |
|  F    |  F    |    T      |    T    |    F    |  F    |
"""

print_in_quiz(template)
print_in_solution(answer)

```

## (0.2pt)

Select all the :gift: which contains a :carrot:.

Answer:

```julia

print_in_quiz(L"""
    - [ ] :gift: x
    - [ ] :gift: y
    """)
print_in_solution(L"""
    - [x] :gift: x
    - [x] :gift: y
    """)

rubric = L"""
**Rubrics:**
If you answer one subproblem wrong, each subsequent subproblem will get no points.
"""

print_in_solution(rubric)
```


# Proof (1pt)

Use proof by contrapositive to show that if $a^2$ is divisible by $5$, then $a$ is divisible by $5$.

```julia
answer = L"""
The contrapositive of the statement "If $a^2$ is divisible by 5, then $a$ is divisible by 5" is "If $a$ is not divisible by 5, then $a^2$ is not divisible by 5."

So we'll prove this contrapositive statement:

1. **Assume that $a$ is not divisible by 5.** Then $a$ can be written as $a = 5k + r$, where $k$ is an integer and $r$ is one of the remainders 1, 2, 3, or 4 when dividing by 5.
2. **Square both sides:** $a^2 = (5k + r)^2 = 25k^2 + 10kr + r^2$.
3. **Now let's examine the remainder when dividing by 5:** Since $r$ is one of 1, 2, 3, or 4, $r^2$ will also leave a remainder when divided by 5. The entire expression for $a^2$ will also leave a remainder when divided by 5, as the other terms are clearly divisible by 5.

Thus, we've proved the contrapositive, so the original statement is also true: If $a^2$ is divisible by 5, then $a$ is divisible by 5. 

**Rubrics**:

1. Correctly write down the contrapositive statement gets 0.2pt.
2. Point 1 and 2 each gets 0.2pt.
3. Point 3 gets 0.4 pt.
4. Incomprehensible proofs will get 0.
5. Proof contains only equtions without any explaination will get 0.
"""

long_answer(answer)
```
