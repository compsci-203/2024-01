(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     22881,        626]
NotebookOptionsPosition[     18439,        540]
NotebookOutlinePosition[     18870,        557]
CellTagsIndexPosition[     18827,        554]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Knight and Knave", "Title",
 CellChangeTimes->{{3.856987316209855*^9, 
  3.856987319035616*^9}},ExpressionUUID->"192ef465-0c7d-42f2-8f5f-\
30e4bab746da"],

Cell[CellGroupData[{

Cell["Lecture 3", "Section",
 CellChangeTimes->{
  3.856987351774232*^9, {3.870212963687004*^9, 
   3.870212965270207*^9}},ExpressionUUID->"ef4b3c73-d710-41ad-b06f-\
45eb758a0a98"],

Cell[CellGroupData[{

Cell["First", "Subsection",
 CellChangeTimes->{{3.870213087727872*^9, 
  3.870213090759076*^9}},ExpressionUUID->"846a6f24-7937-4861-9270-\
74107594435f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sa", "=", 
  RowBox[{"Xor", "[", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"\[Not]", "aa"}], "\[And]", 
      RowBox[{"\[Not]", "bb"}]}], ")"}], ",", 
    RowBox[{"\[Not]", "aa"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.856987364970934*^9, 3.8569874135605392`*^9}, {
  3.870213172488599*^9, 3.870213178919491*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"60d4cce0-494b-416c-9e3b-9508adbab53f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"!", "aa"}], "&&", 
    RowBox[{"!", "bb"}]}], ")"}], "\[Xor]", 
  RowBox[{"!", "aa"}]}]], "Output",
 CellChangeTimes->{{3.856987406359213*^9, 3.856987414507152*^9}, 
   3.870212995987962*^9, 3.870213193436843*^9, 3.913876721025454*^9},
 CellLabel->"Out[1]=",ExpressionUUID->"59fabddb-3d10-4e03-9035-c5c2f1df4df3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TableForm", "[", 
  RowBox[{
   RowBox[{"BooleanTable", "[", 
    RowBox[{"{", 
     RowBox[{"aa", ",", " ", "bb", ",", "sa"}], "}"}], "]"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"TableHeadings", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"None", ",", 
      RowBox[{"{", 
       RowBox[{"\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<SA\>\""}], "}"}]}], 
     "}"}]}]}], "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.856987420895252*^9, 3.8569874934719543`*^9}, {
  3.8702131831443872`*^9, 3.8702131880397*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"42c43bc2-ccc8-468f-9bda-87acf12c78fb"],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      TagBox["\<\"A\"\>",
       HoldForm], 
      TagBox["\<\"B\"\>",
       HoldForm], 
      TagBox["\<\"SA\"\>",
       HoldForm]},
     {"True", "True", "False"},
     {"True", "False", "False"},
     {"False", "True", "True"},
     {"False", "False", "False"}
    },
    GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
    GridBoxDividers->{
     "Columns" -> {{False}}, "Rows" -> {False, True, {False}, False}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[2.0999999999999996`]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}],
   {None, OutputFormsDump`HeadedColumns}],
  Function[BoxForm`e$, 
   TableForm[
   BoxForm`e$, TableHeadings -> {None, {"A", "B", "SA"}}]]]], "Output",
 CellChangeTimes->{
  3.856987439748652*^9, {3.8569874800348053`*^9, 3.85698749528994*^9}, 
   3.870212997792479*^9, {3.8702131892591543`*^9, 3.870213194272749*^9}, 
   3.9138767220104837`*^9},
 CellLabel->
  "Out[2]//TableForm=",ExpressionUUID->"ac5b4611-6d42-4fca-bc73-c595d34d7280"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Second", "Subsection",
 CellChangeTimes->{{3.870213096743392*^9, 
  3.870213097446761*^9}},ExpressionUUID->"f678760d-30eb-41d6-b9f8-\
04f78dc88ed9"],

Cell[BoxData[
 RowBox[{
  RowBox[{"sa", "=", 
   RowBox[{"Xor", "[", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"bb", " ", "\[Or]", "cc"}], ")"}], ",", " ", 
     RowBox[{"\[Not]", "aa"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.8702131062165194`*^9, 3.8702131660073*^9}, {
  3.8702131988246193`*^9, 3.8702132003273993`*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"05a58d23-81e2-4c82-a247-cfa12b3ba144"],

Cell[BoxData[
 RowBox[{
  RowBox[{"sb", "=", 
   RowBox[{"Xor", "[", 
    RowBox[{
     RowBox[{"\[Not]", "aa"}], ",", " ", 
     RowBox[{"\[Not]", "bb"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.870213201400077*^9, 3.870213231721263*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"74b7b9c8-a3a9-4ecb-b202-4f92150328c6"],

Cell[BoxData[
 RowBox[{
  RowBox[{"sc", "=", 
   RowBox[{"Xor", "[", 
    RowBox[{
     RowBox[{"Xor", "[", 
      RowBox[{
       RowBox[{"\[Not]", "bb"}], ",", " ", 
       RowBox[{"\[Not]", "aa"}]}], "]"}], ",", 
     RowBox[{"\[Not]", "c"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.870213241416649*^9, 3.870213307177249*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"33bd8518-6cde-40f7-9f5b-9fc8e9581b09"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TableForm", "[", 
  RowBox[{
   RowBox[{"BooleanTable", "[", 
    RowBox[{"{", 
     RowBox[{
     "aa", ",", " ", "bb", ",", "cc", ",", "sa", ",", "sb", ",", "sc"}], 
     "}"}], "]"}], ",", "\[IndentingNewLine]", 
   RowBox[{"TableHeadings", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"None", ",", 
      RowBox[{"{", 
       RowBox[{
       "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<SA\>\"", 
        ",", "\"\<SB\>\"", ",", "\"\<SC\>\""}], "}"}]}], "}"}]}]}], 
  "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.8702133209677896`*^9, 3.8702133391436768`*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"5309361f-ce1d-496f-aac5-951522f45e2d"],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      TagBox["\<\"A\"\>",
       HoldForm], 
      TagBox["\<\"B\"\>",
       HoldForm], 
      TagBox["\<\"C\"\>",
       HoldForm], 
      TagBox["\<\"SA\"\>",
       HoldForm], 
      TagBox["\<\"SB\"\>",
       HoldForm], 
      TagBox["\<\"SC\"\>",
       HoldForm]},
     {"True", "True", "True", "True", "False", "False"},
     {"True", "True", "False", "True", "False", "False"},
     {"True", "True", "True", "True", "False", "True"},
     {"True", "True", "False", "True", "False", "True"},
     {"True", "False", "True", "True", "True", "True"},
     {"True", "False", "False", "False", "True", "True"},
     {"True", "False", "True", "True", "True", "False"},
     {"True", "False", "False", "False", "True", "False"},
     {"False", "True", "True", "False", "True", "True"},
     {"False", "True", "False", "False", "True", "True"},
     {"False", "True", "True", "False", "True", "False"},
     {"False", "True", "False", "False", "True", "False"},
     {"False", "False", "True", "False", "False", "False"},
     {"False", "False", "False", "True", "False", "False"},
     {"False", "False", "True", "False", "False", "True"},
     {"False", "False", "False", "True", "False", "True"}
    },
    GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
    GridBoxDividers->{
     "Columns" -> {{False}}, "Rows" -> {False, True, {False}, False}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[2.0999999999999996`]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}],
   {None, OutputFormsDump`HeadedColumns}],
  Function[BoxForm`e$, 
   TableForm[
   BoxForm`e$, 
    TableHeadings -> {None, {"A", "B", "C", "SA", "SB", "SC"}}]]]], "Output",
 CellChangeTimes->{3.870213340115964*^9, 3.913876722194708*^9},
 CellLabel->
  "Out[6]//TableForm=",ExpressionUUID->"7570adfe-9aaf-4d4e-b84a-2be1f1f26022"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Problem 5", "Subsection",
 CellChangeTimes->{{3.913876726434865*^9, 
  3.913876728177651*^9}},ExpressionUUID->"6f69f1b7-42ff-439f-a750-\
8932931251ad"],

Cell["\<\
This can be found at https://puzzlewocky.com/brain-teasers/knights-and-knaves/\
\>", "Text",
 CellChangeTimes->{{3.913876730335115*^9, 
  3.913876742257469*^9}},ExpressionUUID->"b95aa029-ac69-4974-8112-\
32d13287aef3"],

Cell["\<\
To complicate matters, a new island is discovered, with a third type of \
inhabitant: spies, who can either tell the truth or lie. You encounter three \
people, one of each, but you don\[CloseCurlyQuote]t know which is which. Red \
says, \[OpenCurlyDoubleQuote]I am a knight.\[CloseCurlyDoubleQuote] Blue \
says, \[OpenCurlyDoubleQuote]Red is telling the \
truth.\[CloseCurlyDoubleQuote] Green says, \[OpenCurlyDoubleQuote]I am the \
spy.\[CloseCurlyDoubleQuote] Who is what?\
\>", "Text",
 CellChangeTimes->{
  3.9138767526103277`*^9},ExpressionUUID->"d24f2538-031d-4bba-9801-\
3e523a91758c"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"r", "=", 
   RowBox[{"!", 
    RowBox[{"And", "[", 
     RowBox[{"rk", ",", "rs"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b", "=", 
   RowBox[{"!", 
    RowBox[{"And", "[", 
     RowBox[{"bk", ",", "bs"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"g", "=", 
   RowBox[{"!", 
    RowBox[{"And", "[", 
     RowBox[{"gk", ",", "gs"}], "]"}]}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.913876777255719*^9, 3.913876841368412*^9}, {
  3.913877425598974*^9, 3.913877432286319*^9}, {3.913877469407876*^9, 
  3.913877485663115*^9}},
 CellLabel->"In[42]:=",ExpressionUUID->"197386ed-fce9-409c-ac69-be074b89bd73"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sr", "=", 
  RowBox[{
   RowBox[{"Or", "[", 
    RowBox[{"rs", ",", 
     RowBox[{"Xor", "[", 
      RowBox[{
       RowBox[{"!", "rk"}], ",", "rk"}], "]"}]}], "]"}], "//", 
   "FullSimplify"}]}]], "Input",
 CellChangeTimes->{{3.91387686428321*^9, 3.913876923438953*^9}, {
  3.913876969187449*^9, 3.9138769849446573`*^9}, {3.913877025001272*^9, 
  3.913877026601733*^9}, {3.913877078336302*^9, 3.913877184494854*^9}, {
  3.913878083814658*^9, 3.91387808595604*^9}, {3.9138783286490183`*^9, 
  3.913878332407138*^9}},
 CellLabel->"In[78]:=",ExpressionUUID->"26625d63-201f-44e7-a1bf-d2fb06ba9b3c"],

Cell[BoxData["True"], "Output",
 CellChangeTimes->{
  3.9138771351160307`*^9, {3.913877166379332*^9, 3.913877185486514*^9}, 
   3.9138774501118183`*^9, 3.913877489107833*^9, 3.9138780406513453`*^9, 
   3.913878088412875*^9, 3.9138783382985907`*^9},
 CellLabel->"Out[78]=",ExpressionUUID->"5e49d05c-4b63-4739-8244-48b6772dc5b2"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sb", "=", 
  RowBox[{
   RowBox[{"Or", "[", 
    RowBox[{"bs", ",", 
     RowBox[{"Xor", "[", 
      RowBox[{
       RowBox[{"!", "bk"}], ",", 
       RowBox[{"Xor", "[", 
        RowBox[{
         RowBox[{"!", "rk"}], ",", "rk"}], "]"}]}], "]"}]}], "]"}], "//", 
   "FullSimplify"}]}]], "Input",
 CellChangeTimes->{{3.913877190462797*^9, 3.913877192660034*^9}, 
   3.913878100207012*^9, {3.9138783413881702`*^9, 3.9138783426341143`*^9}, {
   3.913879106814857*^9, 3.913879136188529*^9}},
 CellLabel->
  "In[115]:=",ExpressionUUID->"e1048169-1f99-4cef-b95d-4d4778a288cf"],

Cell[BoxData[
 RowBox[{"bk", "||", "bs"}]], "Output",
 CellChangeTimes->{3.9138771930754347`*^9, 3.9138774501364937`*^9, 
  3.9138774891390257`*^9, 3.91387812108552*^9, 3.913878343076807*^9, 
  3.913879108184855*^9, 3.9138791382134943`*^9},
 CellLabel->
  "Out[115]=",ExpressionUUID->"27020892-d473-46b5-bb90-76c10491051f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sg", "=", 
  RowBox[{
   RowBox[{"Or", "[", 
    RowBox[{"gs", ",", 
     RowBox[{"Xor", "[", 
      RowBox[{
       RowBox[{"!", "gk"}], ",", "gs"}], "]"}]}], "]"}], "//", 
   "FullSimplify"}]}]], "Input",
 CellChangeTimes->{{3.9138771958369627`*^9, 3.913877199376775*^9}, 
   3.913878123698637*^9, {3.913878346524603*^9, 3.9138783467462263`*^9}, {
   3.913878927362145*^9, 3.913878927599478*^9}, 3.913878998112411*^9},
 CellLabel->
  "In[109]:=",ExpressionUUID->"688e57ad-a5be-4c7e-b9df-293951691d53"],

Cell[BoxData[
 RowBox[{"gk", "\[Implies]", "gs"}]], "Output",
 CellChangeTimes->{3.9138771996700583`*^9, 3.9138774501631603`*^9, 
  3.9138774891651993`*^9, 3.913878125010084*^9, 3.913878347100154*^9, 
  3.913878928106454*^9, 3.913878998516124*^9},
 CellLabel->
  "Out[109]=",ExpressionUUID->"3bb4b325-d330-4a8d-9fb1-1ae4b1dc2020"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"us", "=", 
  RowBox[{"bs", "\[Xor]", "gs", "\[Xor]", "rs", "\[Xor]", 
   RowBox[{"(", 
    RowBox[{"rs", "&&", "bs", "&&", "gs"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.913877252486251*^9, 3.913877258499649*^9}, {
   3.91387729216047*^9, 3.913877304240986*^9}, {3.9138776148023033`*^9, 
   3.913877620397146*^9}, {3.913877660751212*^9, 3.913877671050022*^9}, {
   3.913877747410266*^9, 3.913877780752303*^9}, 3.9138778843428383`*^9},
 CellLabel->"In[81]:=",ExpressionUUID->"57047b26-4802-44dc-bda9-009fa5c8d7f5"],

Cell[BoxData[
 RowBox[{"bs", "\[Xor]", "gs", "\[Xor]", "rs", "\[Xor]", 
  RowBox[{"(", 
   RowBox[{"rs", "&&", "bs", "&&", "gs"}], ")"}]}]], "Output",
 CellChangeTimes->{
  3.913877304810182*^9, 3.913877450188397*^9, 3.913877489191766*^9, {
   3.913877753144003*^9, 3.91387778170397*^9}, 3.913877889900365*^9, 
   3.9138781260952597`*^9, 3.913878348453925*^9},
 CellLabel->"Out[81]=",ExpressionUUID->"b029e2ec-6f13-4159-bcde-fb22656a370d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"uk", "=", 
  RowBox[{"bk", "\[Xor]", "gk", "\[Xor]", "rk", "\[Xor]", 
   RowBox[{"(", 
    RowBox[{"rk", "&&", "bk", "&&", "gk"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.913877252486251*^9, 3.913877258499649*^9}, {
  3.91387729216047*^9, 3.91387734083119*^9}, {3.9138778961005583`*^9, 
  3.9138779062977133`*^9}},
 CellLabel->"In[82]:=",ExpressionUUID->"feb8aed5-2c48-4b7c-9d3f-771038d07a2d"],

Cell[BoxData[
 RowBox[{"bk", "\[Xor]", "gk", "\[Xor]", "rk", "\[Xor]", 
  RowBox[{"(", 
   RowBox[{"rk", "&&", "bk", "&&", "gk"}], ")"}]}]], "Output",
 CellChangeTimes->{3.913877341529718*^9, 3.9138774502142897`*^9, 
  3.913877489219018*^9, 3.913877907333209*^9, 3.91387812684865*^9, 
  3.913878349191674*^9},
 CellLabel->"Out[82]=",ExpressionUUID->"3ba43fbc-d293-49b8-a1bc-7d802601730e"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"cons", "=", 
   RowBox[{"And", "[", 
    RowBox[{
    "r", ",", "b", ",", "g", ",", "sr", ",", "sb", ",", "sg", ",", "us", ",", 
     "uk"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.913877354866769*^9, 3.91387738881817*^9}, {
  3.913878225702442*^9, 3.91387822646988*^9}, {3.913878387306417*^9, 
  3.913878395674225*^9}, {3.91387853088181*^9, 3.913878534335895*^9}, {
  3.913878932175418*^9, 3.913878932366943*^9}},
 CellLabel->
  "In[110]:=",ExpressionUUID->"858f0f83-1df0-4849-8c81-a5310295efbe"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SatisfiableQ", "[", "cons", "]"}]], "Input",
 CellChangeTimes->{{3.9138784668960733`*^9, 3.9138784717417383`*^9}, {
  3.913878523523452*^9, 3.913878527630335*^9}},
 CellLabel->
  "In[111]:=",ExpressionUUID->"17658983-fff4-4d8a-849c-c22561b3cac8"],

Cell[BoxData["True"], "Output",
 CellChangeTimes->{
  3.91387847257304*^9, {3.913878527964315*^9, 3.9138785360928392`*^9}, 
   3.91387893443174*^9, 3.9138790041234207`*^9},
 CellLabel->
  "Out[111]=",ExpressionUUID->"c0f39f0a-200c-4655-be99-269852296e31"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"vars", "=", 
   RowBox[{"{", 
    RowBox[{"rk", ",", "rs", ",", "bk", ",", "bs", ",", "gk", ",", "gs"}], 
    "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.913878640406645*^9, 3.9138786585374403`*^9}},
 CellLabel->
  "In[107]:=",ExpressionUUID->"39480899-7be1-4a00-b3bb-c573b4094bc2"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SatisfiabilityCount", "[", 
  RowBox[{"cons", ",", "vars"}], "]"}]], "Input",
 CellChangeTimes->{{3.913878556583194*^9, 3.91387855885653*^9}, {
  3.9138786608739567`*^9, 3.913878661755557*^9}},
 CellLabel->
  "In[112]:=",ExpressionUUID->"13de267f-fa2c-4878-bb83-15c72d6aa787"],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{3.913878559714511*^9, 3.9138786622711143`*^9, 
  3.913879006399824*^9},
 CellLabel->
  "Out[112]=",ExpressionUUID->"d88cb088-0d48-4fcc-b610-20852589aefe"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sols", "=", 
  RowBox[{"SatisfiabilityInstances", "[", 
   RowBox[{"cons", ",", "vars", ",", "3"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.9138785751668253`*^9, 3.913878578775628*^9}, {
  3.9138786647773542`*^9, 3.913878666075185*^9}, {3.913878724792779*^9, 
  3.9138787440689774`*^9}},
 CellLabel->
  "In[113]:=",ExpressionUUID->"11bebf27-2742-41a6-95a4-cc296d928c9b"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
   "True", ",", "False", ",", "False", ",", "True", ",", "False", ",", 
    "False"}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.913878579660447*^9, 3.9138785950399637`*^9}, 
   3.9138786665208263`*^9, {3.913878725429858*^9, 3.913878744748687*^9}, 
   3.913878937330635*^9, 3.913879007344244*^9},
 CellLabel->
  "Out[113]=",ExpressionUUID->"ae27f100-995b-4a51-b9ef-40e0919e8d96"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"sol", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"rk", "->", "True"}], ",", 
     RowBox[{"rs", "->", "False"}], ",", 
     RowBox[{"bk", "->", "False"}], ",", 
     RowBox[{"bs", "->", "True"}], ",", 
     RowBox[{"gk", "->", "False"}], ",", 
     RowBox[{"gs", "->", "False"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.913878163680523*^9, 3.9138782161357727`*^9}, 
   3.913878369585537*^9},
 CellLabel->"In[86]:=",ExpressionUUID->"e4cffc5d-d6ca-4533-a151-1ec1e5863172"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{
   "r", ",", "b", ",", "g", ",", "sr", ",", "sb", ",", "sc", ",", "us", ",", 
    "uk"}], "}"}], "/.", "sol"}]], "Input",
 CellChangeTimes->{{3.913878242608303*^9, 3.9138782495822773`*^9}},
 CellLabel->"In[87]:=",ExpressionUUID->"aaf765ec-b02a-4f9f-a429-aaf1808d4a9c"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "True", ",", "True", ",", "True", ",", "True", ",", "True", ",", "True", 
   ",", "True", ",", "True"}], "}"}]], "Output",
 CellChangeTimes->{
  3.9138782501394787`*^9, {3.913878353197094*^9, 3.9138783717855453`*^9}},
 CellLabel->"Out[87]=",ExpressionUUID->"e58d0edf-b045-43d5-9c34-3909abdcc8c3"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{572.4, 644.4},
WindowMargins->{{1.7999999999999998`, Automatic}, {
  Automatic, 1.7999999999999998`}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"3d3c9ffa-b94e-4afa-8a11-d83a953d7c6b"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 159, 3, 98, "Title",ExpressionUUID->"192ef465-0c7d-42f2-8f5f-30e4bab746da"],
Cell[CellGroupData[{
Cell[764, 29, 180, 4, 67, "Section",ExpressionUUID->"ef4b3c73-d710-41ad-b06f-45eb758a0a98"],
Cell[CellGroupData[{
Cell[969, 37, 153, 3, 54, "Subsection",ExpressionUUID->"846a6f24-7937-4861-9270-74107594435f"],
Cell[CellGroupData[{
Cell[1147, 44, 437, 11, 29, "Input",ExpressionUUID->"60d4cce0-494b-416c-9e3b-9508adbab53f"],
Cell[1587, 57, 378, 9, 33, "Output",ExpressionUUID->"59fabddb-3d10-4e03-9035-c5c2f1df4df3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2002, 71, 631, 15, 75, "Input",ExpressionUUID->"42c43bc2-ccc8-468f-9bda-87acf12c78fb"],
Cell[2636, 88, 1164, 34, 114, "Output",ExpressionUUID->"ac5b4611-6d42-4fca-bc73-c595d34d7280"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3849, 128, 154, 3, 54, "Subsection",ExpressionUUID->"f678760d-30eb-41d6-b9f8-04f78dc88ed9"],
Cell[4006, 133, 417, 10, 33, "Input",ExpressionUUID->"05a58d23-81e2-4c82-a247-cfa12b3ba144"],
Cell[4426, 145, 327, 8, 33, "Input",ExpressionUUID->"74b7b9c8-a3a9-4ecb-b202-4f92150328c6"],
Cell[4756, 155, 417, 11, 33, "Input",ExpressionUUID->"33bd8518-6cde-40f7-9f5b-9fc8e9581b09"],
Cell[CellGroupData[{
Cell[5198, 170, 695, 17, 75, "Input",ExpressionUUID->"5309361f-ce1d-496f-aac5-951522f45e2d"],
Cell[5896, 189, 1990, 50, 320, "Output",ExpressionUUID->"7570adfe-9aaf-4d4e-b84a-2be1f1f26022"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7935, 245, 157, 3, 54, "Subsection",ExpressionUUID->"6f69f1b7-42ff-439f-a750-8932931251ad"],
Cell[8095, 250, 228, 5, 58, "Text",ExpressionUUID->"b95aa029-ac69-4974-8112-32d13287aef3"],
Cell[8326, 257, 603, 11, 127, "Text",ExpressionUUID->"d24f2538-031d-4bba-9801-3e523a91758c"],
Cell[8932, 270, 685, 19, 70, "Input",ExpressionUUID->"197386ed-fce9-409c-ac69-be074b89bd73"],
Cell[CellGroupData[{
Cell[9642, 293, 619, 14, 29, "Input",ExpressionUUID->"26625d63-201f-44e7-a1bf-d2fb06ba9b3c"],
Cell[10264, 309, 327, 5, 33, "Output",ExpressionUUID->"5e49d05c-4b63-4739-8244-48b6772dc5b2"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10628, 319, 595, 16, 29, "Input",ExpressionUUID->"e1048169-1f99-4cef-b95d-4d4778a288cf"],
Cell[11226, 337, 323, 6, 33, "Output",ExpressionUUID->"27020892-d473-46b5-bb90-76c10491051f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11586, 348, 527, 13, 29, "Input",ExpressionUUID->"688e57ad-a5be-4c7e-b9df-293951691d53"],
Cell[12116, 363, 330, 6, 33, "Output",ExpressionUUID->"3bb4b325-d330-4a8d-9fb1-1ae4b1dc2020"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12483, 374, 541, 9, 29, "Input",ExpressionUUID->"57047b26-4802-44dc-bda9-009fa5c8d7f5"],
Cell[13027, 385, 439, 8, 33, "Output",ExpressionUUID->"b029e2ec-6f13-4159-bcde-fb22656a370d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13503, 398, 420, 8, 29, "Input",ExpressionUUID->"feb8aed5-2c48-4b7c-9d3f-771038d07a2d"],
Cell[13926, 408, 388, 7, 33, "Output",ExpressionUUID->"3ba43fbc-d293-49b8-a1bc-7d802601730e"]
}, Open  ]],
Cell[14329, 418, 541, 12, 29, "Input",ExpressionUUID->"858f0f83-1df0-4849-8c81-a5310295efbe"],
Cell[CellGroupData[{
Cell[14895, 434, 270, 5, 29, "Input",ExpressionUUID->"17658983-fff4-4d8a-849c-c22561b3cac8"],
Cell[15168, 441, 255, 5, 33, "Output",ExpressionUUID->"c0f39f0a-200c-4655-be99-269852296e31"]
}, Open  ]],
Cell[15438, 449, 321, 8, 29, "Input",ExpressionUUID->"39480899-7be1-4a00-b3bb-c573b4094bc2"],
Cell[CellGroupData[{
Cell[15784, 461, 300, 6, 29, "Input",ExpressionUUID->"13de267f-fa2c-4878-bb83-15c72d6aa787"],
Cell[16087, 469, 202, 4, 33, "Output",ExpressionUUID->"d88cb088-0d48-4fcc-b610-20852589aefe"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16326, 478, 395, 8, 29, "Input",ExpressionUUID->"11bebf27-2742-41a6-95a4-cc296d928c9b"],
Cell[16724, 488, 441, 10, 33, "Output",ExpressionUUID->"ae27f100-995b-4a51-b9ef-40e0919e8d96"]
}, Open  ]],
Cell[17180, 501, 521, 13, 50, "Input",ExpressionUUID->"e4cffc5d-d6ca-4533-a151-1ec1e5863172"],
Cell[CellGroupData[{
Cell[17726, 518, 320, 7, 29, "Input",ExpressionUUID->"aaf765ec-b02a-4f9f-a429-aaf1808d4a9c"],
Cell[18049, 527, 338, 7, 33, "Output",ExpressionUUID->"e58d0edf-b045-43d5-9c34-3909abdcc8c3"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

