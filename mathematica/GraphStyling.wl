(* ::Package:: *)

BeginPackage["GraphStyling`"]

(* Exported symbols added here with SymbolName::usage *)
myGraphStyle::usage = "myGraphStyle[g, edgeThickness, vertexThickness, labelSize, vertexSize] styles a graph g with specified parameters for edge thickness, vertex thickness, label size, and vertex size."
colorData::usage = "colorData is an association of custom color names to RGBColor values."

Begin["`Private`"]

(* Default label style *)
myLabelStyle = {FontFamily -> "Latin Modern Roman", FontSize -> 20}

(* Custom color palette association *)
colorData = Association[
    "JuliaOrange" -> RGBColor["#FFA500"],
    "TrolleyGrey" -> RGBColor["#808080"],
    "BrightRed" -> RGBColor["#DD2E44"],
    "MintGreen" -> RGBColor["#78B159"],
    "DaylightBlue" -> RGBColor["#55ACEE"],
    "PastelYellow" -> RGBColor["#EAE559"],
    "SeaGreen" -> RGBColor["#2CA687"],
    "MintWhisper" -> RGBColor["#CCFFCC"],
    "GoldenWhisper" -> RGBColor["#FDCB58"],
    "LavenderDream" -> RGBColor["#AA8ED6"]
]

(* Load MaTeX package for LaTeX-styled text in graphs *)
Needs["MaTeX`"]

(* Graph styling function with customisable parameters *)
myGraphStyle[g_, edgeThickness_, vertexThickness_, labelSize_, vertexSize_] := 
Module[{vlabels, gnew, vl, g1, g2},
    (* Create vertex labels using MaTeX *)
    vl = Alphabet[][[1;;VertexCount[g]]];
    g1 = VertexReplace[g, Thread[VertexList[g] -> vl]];
    vlabels = Table[vl[[i]] -> Placed[MaTeX[vl[[i]], Magnification -> labelSize], Center], {i, VertexList[g]}];
    
    (* Style the graph with custom settings *)
    g2 = Annotate[g1, {
        VertexSize -> vertexSize,
        VertexStyle -> Directive[colorData[[1]], EdgeForm[{Thickness[vertexThickness], Black, Opacity[1]}]],
        EdgeStyle -> Directive[colorData[[2]], Thickness[edgeThickness], Arrowheads[Large]],
        VertexLabels -> vlabels,
        ImageSize -> Full,
        BaseStyle -> myLabelStyle
    }];
    g2
]

(* Overloaded function versions for different levels of customisation *)
myGraphStyle[g_, edgeThickness_, vertexThickness_, labelSize_] := myGraphStyle[g, edgeThickness, vertexThickness, labelSize, Large]
myGraphStyle[g_] := myGraphStyle[g, 0.005, 0.005, 2, 0.3]

End[] (* End Private Context *)

EndPackage[] (* End Package Context *)
