# Regarding the Textbooks

All three textbooks are freely available online:

* [Applied Combinatorics](https://www.appliedcombinatorics.org/appcomb/)
* [Discrete Mathematics: An Open Introduction](https://discrete.openmathbooks.org/dmoi3.html)
* [Elementary Calculus](http://www.mecmath.net/calculus/)

## Applied Combinatorics

The book has a errata page [here](https://www.appliedcombinatorics.org/appcomb/errata/).

Over the years teaching with the book,
students also found some more typos:

1. Lemma 8.12: In the last expression $m+2$ should be $m+1$.
2. Theorem 11.2: The second $|S_1|$ should be $|S_2|$.

### Supplement Solutions

The original solution manuals of the book, i.e., these names `chapterx-solutions.pdf` 
often contains mistakes.

Corrections and additional solutions are thus provided in the files named
`applied-combinatorics-ch-xx.pdf`.

## Discrete Mathematics: An Open Introduction

The book also has a errata on its [website](https://discrete.openmathbooks.org/dmoi3.html).
