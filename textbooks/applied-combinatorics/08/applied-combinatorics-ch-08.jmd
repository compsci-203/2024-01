---
title: Solutions of Exercises in Applied Combinatorics 8.8
author: "Author: Xing Shi Cai"
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia; results="hidden"

using Latexify
using LaTeXStrings
using StatsBase
using SymPy
using WeaveHelper
using Random
using Printf
using MathLink

```

# 2 {-}

## (a) {-}

$$\sum_{n=1}x^n=-\frac{x}{x-1}$$

## (b) {-}

$$\sum_{n=0}x^{3 n}=\frac{1}{1-x^3}$$

## (c) {-}

$$\sum_{n=0}2^n x^n=\frac{1}{1-2 x}$$

## (d) {-}

$$\sum_{n=4}x^n=-\frac{x^4}{x-1}$$

## (e) {-}

$$\sum_{n=0}(-1)^n x^n=\frac{1}{x+1}$$

## (f) {-}

$$\sum_{n=0}2^{8-n} \binom{8}{n} x^n=(x+2)^8$$

## (g) {-}

$$\sum_{n=0}x^n-x^4-x^3=-x^4-x^3+\frac{1}{1-x}$$

## (h) {-}

$$\sum_{n=3}(n-2) x^n=\frac{x^3}{(x-1)^2}$$

## (i) {-}

$$\sum_{n=3}x^n+4 x^2+2 x+3=-\frac{x^3}{x-1}+4 x^2+2 x+3$$

## (j) {-}

$$\sum_{n=0}2 x^{3 n+1}=\frac{2 x}{1-x^3}$$

## (k) {-}

$$\sum_{n=0}6 x^{4 n}+\sum_{n=0}-6 x^{4 n+2}=\frac{6}{1-x^4}-\frac{6 x^2}{1-x^4}$$

# 3 {-}

## (b) {-}

We have

$$\sum_{m=0}x^{4 m}=\frac{1}{1-x^4}$$

This means the coefficient of $x^n$ is $1$ if $n$ is a multiple of $4$ and $0$ otherwise.

## (d) {-}

$$\frac{1-x^4}{1-x}=\frac{1}{1-x}-\frac{x^4}{1-x}=\sum_{n=0}x^n-x^4 \sum_{n=0}x^n=1+x+x^2+x^3$$

## (h) {-}

:bulb: Hint:

$$\frac{x^5}{(1-x)^4}=\frac{x^5 }{3!} \frac{\mathrm{d}^3}{\mathrm{d}x^3} \left(\frac{1}{1-x}\right)$$

The solution is

$$
\begin{cases}
 \frac{1}{6} (n-4) (n-3) (n-2) & n>1 \\
 0 & \text{Otherwise} \\
\end{cases}
 \\
$$

## (j) {-}

The argument is similar to that of b

# 4 {-}

## (a) {-}

As an example of how to solve this with computer, copy and paste this line of code like to WolframAlpha


```
SeriesCoefficient[
    (x^3 + x ^5 + x ^6) (x^ 4 + x ^5 + x^ 7) Sum[x^(5 n), {n, 0, Infinity}], 
    {x, 0, 10} ];
```

Every single one this problem can be solved in this way.

Alternatively, you can drop any $x^n$ where $n \ge 10$ and simply expand 

\begin{equation*}
\begin{aligned}
& 
\left(1+x^5\right) \left(x^3+x^5+x^6\right) \left(x^4+x^5+x^7\right)
\\
&
=
x^7+x^8+x^9+3 x^{10}+x^{11}+2 x^{12}+2 x^{13}+x^{14}+3 x^{15}+x^{16}+x^{17}+x^{18}
\end{aligned}
\end{equation*}

# 11 {-}

The solution manual is wrong. It answers a different question --- How many ways are there
to pay \$100 with \$1 coin, \$1 bill and \$2
bill?

The GF is

$$\frac{1}{(1-x)^2 \left(1-x^2\right)}$$

Convert this to partial fractions, we have

$$-\frac{1}{2 (-1+x)^3}+\frac{1}{4 (-1+x)^2}-\frac{1}{8 (-1+x)}+\frac{1}{8 (1+x)}$$

So the coefficient of $x^n$ for each term is

$$
\frac{1}{4} (1+n) (2+n)
,
\qquad
\frac{1+n}{4}
,
\qquad
\frac{1}{8}
,
\qquad
\frac{(-1)^n}{8}
$$

So the coefficient of $x^n$ in the GF is

$$
\frac{1}{8} \left(7+(-1)^n+2 n (4+n)\right)
$$

# 21 {-}

## (b) {-}

There is a typo in the solution manual. The correct one is

$$
\begin{cases}
 3^{n-2} (n-1) n & n\geq 2 \\
 0 & \text{Otherwise} \\
\end{cases}
 \\
$$

## (c) {-}

There is a typo in the solution manual. The correct one is

$$(-1)^n n!$$

## (d) {-}

The answer should be,

$$a_n = \begin{cases} \frac{n!}{(n/4)!} & \text{if } n = 4k, k\in\mathbb{N} \\ 0 & \text{otherwise} \end{cases}$$

Notice that,

$$e^{x^4} = \sum_{n\geq 0} \frac{x^{4n}}{n!} = \sum_{n\geq 0} \frac{(4n)!}{n!}\frac{x^{4n}}{(4n)!}$$

So the coefficient of $\frac{x^n}{n!}$ is $\frac{n!}{(n/4)!}$ if $n$ is a multiple of $4$, and $0$ otherwise.


# 23 {-}

There is a typo in the solution manual. For the letter $c$, the EGF should be

$$\frac{1}{2} \left(e^{-x}+e^x\right)$$

So the entire EGF should

$$\left(-1+e^x\right)\times e^x\times \left(\frac{1}{2} \left(e^{-x}+e^x\right)\right)\times e^x$$

We have the coefficient of $\frac{x^n}{n!}$ given by

$$\frac{1}{2} e^{2 x} \left(-1+e^x\right) \left(e^{-x}+e^x\right)=-\frac{e^x}{2}+\frac{e^{2 x}}{2}-\frac{e^{3 x}}{2}+\frac{e^{4 x}}{2}=\sum_{n=0}\frac{\left(-1+2^n-3^n+4^n\right) x^n}{2 n!}$$

# 27 {-}

```julia; results="hidden"

@syms x
gen_fun = ((1/(1-x))^2)*(x^2/(1-x))*(1/(1-x^4))*(x+x^2+x^3)
gf1 = simplify(gen_fun)
gf2 = apart(gen_fun)

coeff_str_list = String[]
for i in 0:3
    expr = MathLink.parseexpr("""
        ToString[
            TeXForm[
                Assuming[n>=0 && Mod[n, 4] == $i && n ∈ Integers,
                    FullSimplify[
                        SeriesCoefficient[
                            $gf2, {x,0,n} 
                            ]
                        ]
                    ]
                ]
            ]""")
    coeff_str = weval(expr)
    coeff_str = replace(coeff_str, "True" => "Otherwise")
    push!(coeff_str_list, coeff_str)
end
coeff_str_list

```

First the exercise needs a bit fix.
The variables cannot be negative, otherwise their is no answer to the question.

The original problem is the same as asking for the number integers solutions to
\begin{equation*}
    x_1 + x_2 + x_3 + x_4 + x_5 = n
\end{equation*}
such that 
$x_1 \ge 0$,
$x_2 \ge 2$,
$x_3 \in \{0, 4, 8, \dots \}$,
$1 \le x_4 \le 3$,
$x_5 \ge 0$.
So the generating function is
\begin{equation*}
`j print(sympy.latex(gen_fun))` = `j print(sympy.latex(gf1))`
\end{equation*}


Extracting the coefficient is to complicated without using a computer.
It will not be required in the quiz.

However, if you really want to know the answer, you can first perform a
partial fraction decomposition:
\begin{equation*}
`j print(sympy.latex(gf2))`
\end{equation*}
Thus you can get coefficient from each term and add them up to get
\begin{equation*}
    c_n = 
    \begin{cases}
```julia
for i in 0:3
    print(coeff_str_list[i+1])
    print(L" & \text{if $n \equiv %$i \pmod{4}$ } \\ ")
end
```
    \end{cases}
\end{equation*}


# Exercise in the Slide Decks {-}

## The generating function of $\{n^3\}$ {-}

You can also use example 8.7. It will give you

$$\frac{1}{(1-x)^4} = \sum_{n=0} \binom{n+3}{3} x^n$$

$$\frac{1}{(1-x)^3} = \sum_{n=0} \binom{n+2}{2} x^n$$

$$\frac{1}{(1-x)^2} = \sum_{n=0} \binom{n+1}{1} x^n$$

Then you can use

$$n^3=-1+7 \binom{n+1}{1}-12 \binom{n+2}{2}+6 \binom{n+3}{3}$$
