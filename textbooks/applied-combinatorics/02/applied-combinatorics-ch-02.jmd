---
title: Solutions of Exercises in Applied Combinatorics 2.9
author: "Author: Xing Shi Cai"
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

# 33 {-}

## (a) {-}

We can show this using a bijection to Dyck paths.
An opening bracket can be seen an Up move and a multiplication sign can be seen an Down move.

For example, 
$\left(\left(\left(a_1 \times a_2\right)\times a_3\right)\times a_4\right)$ 
is mapped to the Dyck Path 
$\nearrow \nearrow \nearrow \searrow \searrow \searrow$ 
and $\left(\left(a_1 \times a_2\right)\times \left(a_3\times a_4\right)\right)$ 
is mapped to $\nearrow \nearrow \searrow \searrow \nearrow \searrow$.

## (b) {-}

Again, we can use a bijection to Dyck paths. A $1$ corresponds to an Up move and a
$-1$ corresponds a Down move.

## (c) {-}

A good lattice path from $(0,0)$ to $(n,n)$ has $n$ moves to the right.
The $i$-th right-move has $y$-cooridnate at most $i - 1$.
So we treat $a_1 - 1, a_2 -1, \dots a_n - 1$ as the $y$-coordinates
of each these moves,
we get a unique lattice path from $(0,0)$ to $(n,n)$.

To see how this works, consider the following five good pathes
and their corresponding sequences:

\begin{figure}[htpb]
    \centering
    \newcommand{\drawLatticePath}[1]{
        \begin{tikzpicture}[
            scale=0.6,
            every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
            ]
            \draw[thin, gray] (0, 0) grid (3, 3);
            \draw[dashed, gray] (0, 0) -- (3, 3);
            % Initialize the previous value (a_0)
            \pgfmathsetmacro{\prev}{0}
            \foreach \j [count=\i] in {#1}{
                % Draw horizontal line
                \draw[red, ultra thick] (\i-1, \j-1) -- (\i, \j-1);
                % Draw vertical line if not the first index
                \ifnum\i>1
                    \draw[red, ultra thick] (\i-1, \prev) -- (\i-1, \j-1);
                \fi
                % Update previous value
                \pgfmathsetmacro{\prev}{\j-1}
                \global\let\prev\prev
            }
            % Draw the final segments
            \draw[red, ultra thick] (3, \prev) -- (3,3);
            \node[draw=none, fill=none] at (1.5, -1) {$#1$};
        \end{tikzpicture}
    }
    \drawLatticePath{1, 1, 1}%
    \drawLatticePath{1, 1, 2}%
    \drawLatticePath{1, 1, 3}%
    \drawLatticePath{1, 2, 2}%
    \drawLatticePath{1, 2, 3}%
    \caption{Good lattice paths and their corresponding sequences}
\end{figure}

# Catalan Numbers and Stair-Step Tiling

Let $C(n)$ denote the Catalan numbers. The task is to prove that $C(n)$ corresponds to the
number of ways to tile a stair-step shape of height $n$ using $n$ rectangles. To
illustrate, consider the examples for $n = 3$ and $n = 4$ shown in the accompanying
figures.

\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.45\textwidth]{c3-stair-steps.jpg}
    \hfil
    \includegraphics[width=0.45\textwidth]{c4-stair-steps.jpg}
    \caption{Illustration of tiling stair-step shapes with heights $3$ and $4$.}
    \label{fig:stair-steps}
\end{figure}

:bulb: One way to do this by establishing
a bijection between the tiling configurations and binary trees.
If you adopt this method, you should

1. clearly describe your bijection
2. and provide a demonstration of how this bijection maps
each tiling configuration of a stair-step shape of height $3$ to corresponding binary trees.

You do not need to argue why the mapping is a bijection.


Answer:

We can map the tiling configurations of $n = 3$ to binary trees as follows:

1. Put a node at the top left corner of each rectangle.
2. Connect two adjacent nodes if they share the same $x$-coordinate or the same $y$-coordinate.
3. Rotate the picture by 45 degrees clockwise.

The result will be a binary tree with $n$.

The following pictures demonstrate this bijection for $n=3$.

\begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (3, -1);
            \draw (0,-1) rectangle (2, -2);
            \draw (0,-2) rectangle (1, -3);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -1.0) {};
            \node (n3) at (0.0, -2.0) {};
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (3, -1);
            \draw (0,-1) rectangle (1, -3);
            \draw (1,-1) rectangle (2, -2);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -1.0) {};
            \node (n3) at (1.0, -1.0) {};
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (2, -2);
            \draw (0,-2) rectangle (1, -3);
            \draw (2,0) rectangle (3, -1);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -2.0) {};
            \node (n3) at (2.0, -0.0) {};
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (1, -3);
            \draw (1,0) rectangle (3, -1);
            \draw (1,-1) rectangle (2, -2);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (1.0, -0.0) {};
            \node (n3) at (1.0, -1.0) {};
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (1, -3);
            \draw (1,0) rectangle (2, -2);
            \draw (2,0) rectangle (3, -1);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (1.0, -0.0) {};
            \node (n3) at (2.0, -0.0) {};
    \end{tikzpicture}
    \caption{Adding nodes}
\end{figure}

\begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (3, -1);
            \draw (0,-1) rectangle (2, -2);
            \draw (0,-2) rectangle (1, -3);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -1.0) {};
            \node (n3) at (0.0, -2.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (3, -1);
            \draw (0,-1) rectangle (1, -3);
            \draw (1,-1) rectangle (2, -2);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -1.0) {};
            \node (n3) at (1.0, -1.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (2, -2);
            \draw (0,-2) rectangle (1, -3);
            \draw (2,0) rectangle (3, -1);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -2.0) {};
            \node (n3) at (2.0, -0.0) {};
            \draw (n1) -- (n2);
            \draw[red, ultra thick] (n1) -- (n3);
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (1, -3);
            \draw (1,0) rectangle (3, -1);
            \draw (1,-1) rectangle (2, -2);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (1.0, -0.0) {};
            \node (n3) at (1.0, -1.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
            \draw (0,0) rectangle (1, -3);
            \draw (1,0) rectangle (2, -2);
            \draw (2,0) rectangle (3, -1);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (1.0, -0.0) {};
            \node (n3) at (2.0, -0.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
    \end{tikzpicture}
    \caption{Connecting nodes}
\end{figure}

\begin{figure}[htpb]
    \centering
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
        \begin{scope}[rotate=-45]
            \draw (0,0) rectangle (3, -1);
            \draw (0,-1) rectangle (2, -2);
            \draw (0,-2) rectangle (1, -3);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -1.0) {};
            \node (n3) at (0.0, -2.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
        \end{scope}
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
        \begin{scope}[rotate=-45]
            \draw (0,0) rectangle (3, -1);
            \draw (0,-1) rectangle (1, -3);
            \draw (1,-1) rectangle (2, -2);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -1.0) {};
            \node (n3) at (1.0, -1.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
        \end{scope}
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
        \begin{scope}[rotate=-45]
            \draw (0,0) rectangle (2, -2);
            \draw (0,-2) rectangle (1, -3);
            \draw (2,0) rectangle (3, -1);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (0.0, -2.0) {};
            \node (n3) at (2.0, -0.0) {};
            \draw (n1) -- (n2);
            \draw[red, ultra thick] (n1) -- (n3);
        \end{scope}
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
        \begin{scope}[rotate=-45]
            \draw (0,0) rectangle (1, -3);
            \draw (1,0) rectangle (3, -1);
            \draw (1,-1) rectangle (2, -2);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (1.0, -0.0) {};
            \node (n3) at (1.0, -1.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
        \end{scope}
    \end{tikzpicture}
    \hfil
    \begin{tikzpicture}[
        scale=0.6,
        every node/.style={draw=red, inner sep=1.5pt, circle, fill=red},
        ]
        \begin{scope}[rotate=-45]
            \draw (0,0) rectangle (1, -3);
            \draw (1,0) rectangle (2, -2);
            \draw (2,0) rectangle (3, -1);
            \node (n1) at (0.0, -0.0) {};
            \node (n2) at (1.0, -0.0) {};
            \node (n3) at (2.0, -0.0) {};
            \draw[red, ultra thick] (n1) -- (n2) -- (n3);
        \end{scope}
    \end{tikzpicture}
    \caption{Rotation}
\end{figure}
