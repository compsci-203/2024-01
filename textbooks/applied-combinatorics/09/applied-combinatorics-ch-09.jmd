---
title: Solutions of Exercises in Applied Combinatorics 9.9
author: "Author: Xing Shi Cai"
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

# 1 {-}

## (e) {-}

There is a typo in the solution manual. The correct answer should be

$$\left(A^5-4A^4-A^2+3\right)f=(-1)^{n+1}$$

# 5 {-}

The solution given mistakenly declared that they will start with $f_0=1$ and $f_1=1$. In fact, the solution works with $f_0=0$ and $f_1=1$.
The solution given there

$$f(n)=\frac{\left(\frac{1}{2} \left(1+\sqrt{5}\right)\right)^n-\left(\frac{1}{2} \left(1-\sqrt{5}\right)\right)^n}{\sqrt{5}}$$

will result in the first few terms

$$\{0,1,1,2,3,5\}$$

# 9 {-}

## (b) {-}

The equation is the same as

$$-g(n)+3 g(1+n)+g(2+n)=(-1)^n+2^n$$

One good guess is is

$$g(n)=(-1)^n d_{1}+2^n d_{2}$$

Put this back to the original equation and simplify

$$(-1)^n (-1-3 d_{1})+2^n (-1+9 d_{2})=0$$

For this equations to hold for all $n \ge 0$, we must have

$$\begin{array}{l}
 -1-3 d_{1}=0 \\
 -1+9 d_{2}=0 \\
\end{array}$$

Solving this equations, we get

$$d_{1}= -\frac{1}{3}, \qquad d_{2}= \frac{1}{9}$$

This gives us a particular solution

$$g(n)=-\frac{1}{3} (-1)^n+\frac{2^n}{9}$$

So the general solution is

$$g(n)= -\frac{1}{3} (-1)^n+\frac{2^n}{9}+\left(-\frac{2}{-3+\sqrt{13}}\right)^n c_1+\left(\frac{1}{2} \left(-3+\sqrt{13}\right)\right)^n
c_2$$

## (e) {-}

The equation is the same as

$$(A-4) (A-2) g=9^n+3 n^2$$

i.e.,

$$8 g(n)-6 g(1+n)+g(2+n)=9^n+3 n^2$$

One good guess is is

$$g(n)=9^n d_{1}+n^2 d_{2}+n d_{3}+d_{4}$$

Put this back to the original equation and simplify

$$9^n (-1+35 d_{1})-2 d_{2}+n^2 (-3+3 d_{2})-4 d_{3}+n (-8 d_{2}+3 d_{3})+3 d_{4}=0$$

For this equations to hold for all $n\text{$>$=}0$, we must have

$$\begin{array}{l}
 -1+35 d_{1}=0 \\
 -3+3 d_{2}=0 \\
 -8 d_{2}+3 d_{3}=0 \\
 -2 d_{2}-4 d_{3}+3 d_{4}=0 \\
\end{array}$$

Solving this equations, we get

$$d_{1}= \frac{1}{35}, \qquad d_{2}= 1, \qquad d_{3}= \frac{8}{3}, \qquad d_{4}= \frac{38}{9}$$

This gives us a particular solution

$$g(n)=\frac{38}{9}+\frac{9^n}{35}+\frac{8 n}{3}+n^2$$

So the general solution is

$$g(n)= \frac{38}{9}+\frac{9^n}{35}+\frac{8 n}{3}+n^2+2^n c_1+4^n c_2$$

# 15 {-}

The linear recurrence

$$\begin{array}{l}
 r(n)=6 r(n-2)+r(n-1) \\
 r(0)=1 \\
 r(1)=3 \\
\end{array}$$

can be solved with the Advancement operator method to get

$$r(n)= 3^n$$

Now we show how to do this with generating functions.

Let

$$R(x)=\sum_{n=0}x^n r(n)$$

Then we have

$$x R(x)=\sum_{n=1}x^n r(n-1)$$

$$6 x^2 R(x)=\sum_{n=2}6 x^n r(n-2)$$

Adding the equations together, we get

$$R(x)-x R(x)-6 x^2 R(x)=-\sum_{n=2}6 x^n r(n-2)-\sum_{n=1}x^n r(n-1)+\underset{n=0}{\overset{\infty
}{\sum }}x^n r(n)$$

$$R(x)-x R(x)-6 x^2 R(x)=r(0)-x r(0)+x r(1)-\sum _{n=2}^{\infty } 6 x^n r(n-2)-\sum _{n=2}^{\infty } x^n r(n-1)+\sum _{n=2}^{\infty }
x^n r(n)$$

$$R(x)-x R(x)-6 x^2 R(x)=1+2 x+\sum _{n=2}^{\infty } \left(-6 x^n r(n-2)-x^n r(n-1)+x^n r(n)\right)$$

$$-\left(\left(-1+x+6 x^2\right) R(x)\right)=1+2 x+\sum _{n=2}^{\infty } x^n (-6 r(n-2)-r(n-1)+r(n))$$

$$-\left(\left(-1+x+6 x^2\right) R(x)\right)=1+2 x$$

Solving this, we get

$$R(x)= \frac{1}{1-3 x}$$

Thus we have a closed formula for $r(n)$:

$$r(n)=3^n$$

