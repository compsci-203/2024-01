---
title: Solutions of Exercises in Applied Combinatorics 11.8
author: "Author: Xing Shi Cai"
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

# 1 {-}

## (a) {-}

\begin{equation*}
\frac{3}{35}
\end{equation*}

## (b) {-}

\begin{equation*}
\frac{\binom{3}{2}}{\binom{3}{3}}
\end{equation*}

## (c) {-}

\begin{equation*}
\frac{\binom{7}{2}\binom{2}{1}}{\binom{3}{3}}
\end{equation*}

## (d) {-}

\begin{equation*}
\frac{\binom{2}{3}}{\binom{3}{3}}
\end{equation*}

# 2 {-}

## Seven {-}

When you roll a pair of dice, the cases when you get a $7$ are

\begin{equation*}
\{\{1,6\},\{2,5\},\{3,4\},\{4,3\},\{5,2\},\{6,1\}\}
\end{equation*}

The probability of getting a $7$ is

\begin{equation*}
6\times \left(1\times \frac{1}{6}\right)\times \left(1\times \frac{1}{6}\right)==\frac{1}{6}
\end{equation*}

Thus getting at lest one $7$ in three rolls is

\begin{equation*}
1+(-1)\times \left(1+(-1)\times \frac{1}{6}\right)^3==\frac{91}{216}
\end{equation*}

This is approximately

\begin{equation*}
0.421296
\end{equation*}

## Five {-}

Using the same argument, the probability of getting at lest one $5$ in six rolls is

\begin{equation*}
\frac{269297}{531441}\approx 0.50673
\end{equation*}

# 3 {-}

\begin{figure}[htpb]
    \centering
    \includegraphics{chapter-10_gr1.eps}
\end{figure}

\subsection*{(a) }

\begin{equation*}1-\left(1-\frac{3}{8}\right)^3=\frac{387}{512}\end{equation*}

## (b) {-}

\begin{equation*}1-\left(1-\frac{1}{8}\right)^3=\frac{169}{512}\end{equation*}

## (c) {-}

\begin{equation*}\frac{1/4}{3/8+1/4}=\frac{2}{5}\end{equation*}

## (d) {-}

\begin{equation*}1 \frac{1}{8}+2 \frac{1}{4}+3 \frac{1}{8}+4 \frac{1}{8}+5 \frac{3}{8}=\frac{27}{8}\end{equation*}

# 4 {-}

By theorem 7.12, the probability of getting a derangement is about $1/e$.

So the expected return is

\begin{equation*}\frac{2.5}{e}-1=-0.0803014\end{equation*}

and this is not a fair game.

# 5 {-}

## (a) {-}

For $E_S$ to happen, three edges must be added. This has probability $\left(\frac{1}{2}\right)^3$

## (b) {-}

If $| S\cap T| \text{$<$=}1$, then there edges which needs to be added for $E_S$ and $E_T$ to happen are disjoints. Since edges are added independent
at random, the two events $E_S$ and $E_T$ are independent.

## (c) {-}

Conditioning $E_T$ (the red part in the graph), $E_T$ only needs two extra edges.

Thus

\begin{equation*}\mathbb{P}\left[E_S | E_T\right]=\left(\frac{1}{2}\right)^2=\frac{1}{4}\end{equation*}

\begin{figure}[htpb]
    \centering
    \includegraphics{chapter-10_gr2.eps}
\end{figure}

Conditioning $E_T E_U$ (the red part in the graph), $E_T$ still needs two extra edges.

Thus

\begin{equation*}\mathbb{P}\left[E_S | E_T E_U\right]=\left(\frac{1}{2}\right)^2=\frac{1}{4}\end{equation*}

\begin{figure}[htpb]
    \centering
    \includegraphics{chapter-10_gr3.eps}
\end{figure}

# 6 {-}

First, when the first four pairs have been drawn, the remaining balls in the jar are known. Either's it's pair which sums to $11$ or it's
not. So everyone should bet in the first cases and not to bet in the latter.

An observant can track the probability of draw a winning pair from the remaining balls and bet whenever this probability is strictly great than $\frac{1}{9}$.

A simpler version of this is to bet on the 2nd round if the first pair drawn sums to $11$, which has a probability of $\frac{4}{\binom{8}{2}}=\frac{1}{7}>\frac{1}{9}$

If there are $2n$ balls left, the probability of winning is at most $\frac{n}{\binom{2}{2}}=\frac{1}{2n-1}$

Since $n\ge 2$, $\frac{1}{2n-1}\ge \frac{1}{3}$. So if the payout ratio is greater than $\frac{1}{3}$, there is not winning strategy.
