# Week 4 Study Guide

The following is what we have learned in week 4 and what you are expected to be able to do
in Quiz 04.

## **AC Chapter 7**: Inclusion-Exclusion

* **Inclusion-Exclusion Principle**: 
    * You should be able to apply the inclusion-exclusion formula to solve
      counting problems, as illustrated in the given examples and exercises.
* **Surjection Numbers**: 
    * You need to recall the formula for computing the number of surjections from one set to another.
    * Employ the notation $S(m, n)$ effectively to solve problems akin to counting surjections.
* **Derangements**:
    * You need to recall the formula for calculating the number of derangements.
    * Utilize the notation $d_n$ to tackle problems related to counting derangements.
    * Be prepared to prove identities involving $d_n$, as presented in the exercises.

## **AC Chapter 8**: Generating Functions

* **Generating Functions**:
    * You should be able to write the generating function in closed form for a given sequence.
    * For integer composition problems (such as distributing $n$ carrots among $m$ bunnies with certain restrictions),
      you're expected to write the generating function in closed form.
    * When presented with a generating function,
      you must be able to determine the coefficient of $x^n$,
      using techniques like partial fraction decomposition
      or the identity in Example 8.7. 

* **Newton's Binomial Theorem**:
    * You should be able to compute expressions like $P(p,k)$ and $\binom{p}{k}$ when $p$ is a non-integer.
    * You are expected to apply Newton's Binomial Theorem to expressions such as $1/(1-4x)^{1/2}$.
