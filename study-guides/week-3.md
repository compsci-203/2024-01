# Week 3 Study Guide

The following is what we have learned in week 3 and what you are expected to be able to do
in Quiz 03.

## **AC Chapter 2**: Strings, Sets, and Binomial Coefficients

* **Strings**: Understanding and counting strings, especially under various restrictions.
* **Permutations**: Knowledge of permutations, calculating $P(m,n)$, and related concepts.
* **Combinations**: Understanding combinations, computing $\binom{m}{n}$, and application.
* **Combinatorial Proofs**: Ability to prove identities using combinatorial arguments.
* **Integer Composition**: Methods of distributing $n$ carrots among $k$ bunnies with restrictions.
* **Lattice Paths**: Counting lattice paths (including those with obstacles) using binomial coefficients.
* **Catalan Numbers**: Proving certain quantities as Catalan numbers through bijections with
  Dyck paths or Binary Trees, or via proving it satisfies the recursion for Catalan numbers.
* **Binomial Theorem**: Understanding and applying the theorem to prove simple identities
  involving binomial coefficients.
* **Multinomial Theorem**: Remembering the theorem and understanding multinomial coefficients.

## **AC Chapter 3**: Induction

* **Recursions**: Developing recursions for counting problems involving strings with
  restrictions and grid tiling.
* **Induction**: Utilizing induction and strong induction to prove the results provided in the exercises.
