# Week 5 Study Guide

## **AC Chapter 8**: Generating Functions

* **Integer Partition**:
    * Given a simple integer partition problem, what is the generating function? How do you extract the coefficient from the generating function?
    * Can you demonstrate that the ways to partition $n$ into odd parts and distinct parts are the same?
* **Exponential Generating Function**:
    * Given a sequence, can you express its exponential generating function in closed form?
    * Given a string counting problem, can you solve it using exponential generating functions?

## **AC Chapter 9**: Recurrence Equations

* **Recurrence Equations**:
    * Given a simple counting problem, can you formulate a recurrence as the solution?
* **Homogeneous Recurrence Equations**:
    * Given a simple homogeneous linear recurrence problem, can you solve it by factorising the polynomial of the advancement operator?
* **Nonhomogeneous Recurrence Equations**:
    * Given a simple nonhomogeneous linear recurrence problem, can you find the general solution to the homogeneous part and a particular solution to the nonhomogeneous part?
* **Solving Recurrence with Generating Functions**:
    * Given a linear recurrence, can you solve it by deriving the generating function?
* **Nonlinear Recurrence Equations**:
    * Given a function similar to $C(x) = x + C(x)^2$, can you determine the coefficients of $C(x)$?
