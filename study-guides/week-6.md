# Week 6 Study Guide

## **AC Chapter 5 & DM Chapter 4**: Graph Theory

* **Basic Concepts**:
    * Familiarize yourself with essential graph theory terminology, including vertices, edges, subgraphs, induced subgraphs, walks, paths, cycles, etc.
    * Learn the definitions and notations of common graph families, such as complete graphs and induced subgraphs.
    * Develop the skill to verify or refute the isomorphism between two graphs.
* **Trees**:
    * Grasp the concepts of trees and forests.
    * Determine whether a graph qualifies as a tree or a forest.
    * Prove characteristics of trees, such as in Proposition 4.21 (AC), highlighting that every edge in a tree is a cut edge.
* **Rooted Trees**:
    * Understand what rooted trees are.
    * Learn the terms associated with rooted trees, including parent, child, and siblings.
* **Bipartite Graphs**:
    * Comprehend the definition of bipartite graphs.
    * Identify bipartite graphs from a given set.
* **Spanning Trees**:
    * Understand spanning trees and their significance.
    * Learn how to identify and enumerate spanning trees within a graph.
* **Cayley's Formula**:
    * Master the conversion between a labelled tree and its Prüfer code.
    * Understand how to count the number of strings with duplicate letters, leading to the enumeration of Prüfer codes comprising given letters.
    * Apply Cayley's formula to compute the number of labelled trees for a given degree sequence.
* **Eulerian and Hamiltonian Graphs**:
    * Define eulerian and hamiltonian graphs.
    * Assess whether a graph is eulerian or hamiltonian, supporting your conclusion with proofs.
    * Execute the algorithm discussed in class to find an eulerian circuit within an eulerian graph.
    * Prove Theorem 5.18 (AC) as required.
