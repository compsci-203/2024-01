# Week 7 Study Guide

## Graph Colouring: *Applied Combinatorics* Chapter 5.4 & *Discrete Mathematics* Chapter 4.4

- **Basics**:
    - Grasp the concept of a proper graph colouring and the chromatic number.
    - Learn to identify and justify the chromatic number for well-known graph types such
      as complete graphs, independent graphs, star graphs, and cycles.
    - Comprehend what it means for a graph to be $k$-colourable.
    - Demonstrate that a graph is $2$-colourable if and only if it lacks cycles of odd length.

- **Clique Number**:
    - Understand the concept of the clique number and its role in providing a lower bound
      for the chromatic number.
    - Master the generalized Pigeonhole Principle.
    - Familiarize yourself with the key ideas to reproduce the proofs of Proposition 5.25
      from *Applied Combinatorics*, showcasing that triangle-free graphs can have a high
      chromatic number.

- **Interval Graphs**:
    - Prove that $\chi(G) \le \Delta(G) + 1$ for any graph $G$.
    - Recall Brook's Theorem.
    - Understand what interval graphs are.
    - Prove that for interval graphs, the chromatic and clique numbers coincide.

- **Perfect Graphs**:
    - Grasp the definition of perfect graphs.
    - Show that interval graphs fall under the category of perfect graphs.
    - Establish that not all perfect graphs are interval graphs.
    - Employ The Strong Perfect Graph Theorem to assess whether certain graphs are perfect.

## Graph Colouring: *Applied Combinatorics* Chapter 5.5 & *Discrete Mathematics* Chapter 4.3

- Understand what planar graphs are.
- Master the proof of Euler's formula.
- Justify why $K_5$ and $K_{3,3}$ cannot be planar graphs.
- Apply Kuratowski's Theorem to evaluate graph planarity.
- Familiarize yourself with the Four Colour Theorem's statement.
- Be able to show that outerplanar graph is 3-colourable.
