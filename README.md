# COMPSCI203 -- Discrete Mathematics for Computer Science (2024-01)

This repository contains materials which [I (Xing Shi Cai)](https://newptcai.gitlab.io)
prepared for the course 
COMPSCI203 (Discrete Mathematics for Computer Science)
of [Duke Kunshan University](https://dukekunshan.edu.cn/)
which started in January 2024.

See the `pdf` folder for lecture slides and quizzes.

You are free to use any material here.
