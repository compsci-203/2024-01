import networkx as nx
import matplotlib.pyplot as plt

# Construct the trees from the degree sequence
degree_sequence = [3, 2, 2, 1, 1, 1]
trees = []

s = 4445
while len(trees) < 2:
    s += 1
    tree = nx.configuration_model(degree_sequence, seed=s)
    if nx.is_tree(tree):
        # Check if the tree is isomorphic to any previously generated tree
        if all(not nx.is_isomorphic(tree, t) for t in trees):
            code = nx.to_prufer_sequence(tree)
            code = [str(i+1) for i in code]
            print("".join(code))
            trees.append(tree)

# Define drawing options
options = {
    "font_size": 20,
    "node_size": 1500,
    "node_color": "orange",
    "edgecolors": "black",  # Add black borders
    "linewidths": 3,
    "width": 3,
}

# Generate layout and draw the trees
pos = [nx.spring_layout(tree, seed=42) for tree in trees]  # Added seed for reproducibility

# Create a label mapping to add 1 to each label
label_mappings = [{node: node + 1 for node in tree.nodes()} for tree in trees]

# Draw each tree with updated labels and save them
for i, tree in enumerate(trees):
    nx.draw_networkx(tree, pos[i], labels=label_mappings[i], **options)
    plt.savefig(f"../slides/images/graphs/prufer-code-degree-example-{i+1}.pdf")
    plt.clf()  # Clear the current figure
