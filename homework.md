---
title: Homework Requirements for COMPSCI 203
author: Xing Shi Cai
date: 2021-09-03
geometry:
- margin=1.5in
...

# What is allowed?

1. Consult our textbooks, slides, and your own notes
2. Discuss the homework with other students in the **class**
3. Use the following websites
    1. [OEIS](https://oeis.org/)
    2. [WolframAlpha](https://wolframalpha.com/)
    3. [MathWorld](https://mathworld.wolfram.com/)
    4. [Wikipedia](https://en.wikipedia.org/)
4. Use a computer for calculations

# What is not allowed?

1. Copy other people's solutions (you **must** write your own answers)
2. Search the answer with search engines
3. Ask homework questions on Q&A websites
4. Ask someone else to do the work for you

# LaTeX

All answers should be typed into a computer and submit through GradeScope (link on Sakai).
You are *recommended* to use the website's [LaTeX features](https://help.gradescope.com/article/3vm6obxcyf-latex-guide) when you write mathematical
equations.
But that is *not* mandatory.
