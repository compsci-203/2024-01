---
title: Presentation Requirements for COMPSCI 203
author: Xing Shi Cai
date: 2023-01-30
geometry:
- margin=1.5in
...

# The Presentation

In this course, you'll deliver a 15-minute presentation on a discrete
mathematics topic to hone your presentation skills in mathematics.

The presentations will be held on Fridays of week 5-7.
See details in the
[schedule](https://prodduke-my.sharepoint.com/:x:/r/personal/xc171_duke_edu/Documents/2023/08-COMPSCI203/COMPSCI203-2023-08%20Presentation.xlsx?d=w22438cbac0434a61879349e813fd43b3&csf=1&web=1&e=Qc6bhC).

You're not required to attend presentation sessions, 
except for your own. You may choose between a slide-show or a whiteboard presentation. 
All presentations will be recorded for grading purposes.

You can also choose to do a slide-show or give a whiteboard presentation.

The presentations will be recorded for grading purpose.

# Topic

You can choose a problem of your like, as long as 

1. it belongs to logic, combinatorics, graph theory, probability theory, or
   computer algorithms, or sequences;
2. we have **not** covered the topic at the time of your presentation;
3. it is suitable for an audience of other students of this course;
4. it is has nothing to do with Dijkstra's algorithm. (I have listened to this
   topic at least five times now.)

Some books from which you may find a suitable problem ---

1. [Discrete Mathematics --- An Open Introduction, 3rd edition](https://discrete.openmathbooks.org/) (our textbook)
2. [Applied Combinatorics](https://www.rellek.net/book/app-comb.html) (our textbook)
3. [Concrete Mathematics: A Foundation for Computer Science](https://www-cs-faculty.stanford.edu/~knuth/gkp.html)
4. [Combinatorics Through Guided Discovery](https://bogart.openmathbooks.org/ctgd/ctgd.html)
5. [Graph Theory with Applications by Bondy and Murty](http://www.maths.lse.ac.uk/Personal/jozef/LTCC/Graph_Theory_Bondy_Murty.pdf)
6. [Quanta Magazine](https://www.quantamagazine.org/)


:bomb: Please submit your topic in the
[schedule](https://prodduke-my.sharepoint.com/:x:/r/personal/xc171_duke_edu/Documents/2023/08-COMPSCI203/COMPSCI203-2023-08%20Presentation.xlsx?d=w22438cbac0434a61879349e813fd43b3&csf=1&web=1&e=Qc6bhC)
before week 4. Otherwise you will be given a topic.

# Exemplary talks

Here are [links](https://duke.box.com/s/lousc9o14adugoin2yu1jgjc2kye8wk6) to 
some excellent student presentations from previous years.

:bulb: Some tips for preparing the talk ---

* Choose a topic that keeps your audience awake.
* Think about what the audience can get from your talk if they only listen for
  the first three minutes.
* Rehearse the talk once with a friend.
* Expect to be interrupted during the talk.
* Prepare a bit more than you plan to speak about.
* Prepare to cut your talk short in case you go slower than expected.

\pagebreak{}

# Rubrics

Your presentation will graded based on which of the following category describe
it best.

### Complete (2-5pt)

| Criteria             | Excellent (5pt)                                                   | Good (4pt)                                 | Acceptable (3pt)                        | Needs Improvement (2pt)                  |
|----------------------|-------------------------------------------------------------------|--------------------------------------------|-----------------------------------------|------------------------------------------|
| Draft (10%)          | Submitted before week 5, covers all key points                    | Submitted in week 5, mostly complete      | Submitted after week 5, mostly complete | Late submission                          |
| Difficulty (10%)     | Advanced yet comprehensible topics                                | Slightly too simple or advanced            | Moderately too simple or advanced       | Too simple or advanced                   |
| Slides (15%)         | Well-organized, visually appealing, error-free, clear formulas    | Well-organized, clear formulas, minor errors| Generally organized, some errors       | Disorganized, numerous errors            |
| Explanation (20%)    | Precise, easy to understand, strongly motivated                   | Mostly clear, strong motivation            | Clear, may lack depth or motivation     | Difficult to follow, weak motivation     |
| Creativity (20%)     | Engaging, creative, questions for the audience, appropriate humor | Some creativity and humor                  | Lacks creativity or humor               | Unengaging, devoid of creativity or humor|
| Response (10%)       | Confidently handles feedback, strong understanding                | Responds effectively to most questions     | Addresses some questions, may struggle  | Difficulty responding to questions       |
| Time Control (5%)    | Within the 15-minute time frame                                   | Slightly exceeds allotted time             | Shorter than allotted time              | Significantly shorter than allotted time |
| English Fluency (5%) | Excellent articulation and fluency                                | Effective articulation and fluency         | Basic proficiency in English            | Struggles with fluency and articulation  |
| Feedback (5%)        | Provides detailed feedback form after the presentation            | Provides feedback form                     | Omits some sections in feedback form    | Fails to provide feedback form           |

###  Incomplete (1pt) 

Presentation not given, but prior communication with the instructor occurred. 

###  Did Not Meet Expectations (0pt) 

Presentation not given without prior communication with the instructor. 
