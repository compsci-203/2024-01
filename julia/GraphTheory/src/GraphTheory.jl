module GraphTheory

using Graphs,
      Multigraphs,
      GraphPlot,
      Cairo,
      Compose,
      Colors,
      ColorSchemes,
      Random,
      GraphRecipes,
      Plots,
      PyCall,
      LayeredLayouts

export get_eulerian,
    get_bipartite,
    is_euerlian,
    save_graph,
    save_bipartite,
    save_graph_recipe,
    get_knoigsberg,
    get_graph,
    is_even_degree,
    is_odd_degree,
    clique_num,
    has_degree

const default_style = (
                        nodefillc = colorant"orange", 
                        nodestrokelw=1,
                        nodestrokec = colorant"black",
                        edgestrokec = colorant"gray",
                        NODESIZE=0.2, 
                        NODELABELSIZE=12,
                      )

function get_eulerian(n, m, seed=1234)
    return get_graph(n, m, is_euerlian, seed)
end

function get_bipartite(n1, n2, m)
    g = SimpleGraph(n1+n2)

    e = ne(g)

    while e < m && e <  n1*n2
        v1 = rand(1:n1)
        v2 = rand(n1+1:n1+n2)
        add_edge!(g, v1, v2)
        e = ne(g)
    end

    return g
end

function get_graph(n, m, cond, seed=1234)
    Random.seed!(seed)

    g = erdos_renyi(n, m)
    while ! cond(g)
        g = erdos_renyi(n, m)
    end

    return g
end

# This is not very useful since we cannot draw multi-graphs.
function get_knoigsberg()
    g = Multigraph(4)
    for i in 2:4
        add_edge!(g, 1, i)
    end

    add_edge!(g, 2, 3, 2)
    add_edge!(g, 3, 4, 2)

    return g
end

function is_euerlian(g)
    if !is_connected(g) || !is_even_degree(g)
        return false
    end
    return true
end

function is_even_degree(g)
    return all(map(iseven, degree(g)))
end

function is_odd_degree(g)
    return all(map(isodd, degree(g)))
end

function has_degree(g, d)
    return any(map(x->x==d, degree(g)))
end

function save_graph(g, alphabet=true, C=20, w=16, h=16; kwargs...)
    g_path = tempname() * ".pdf"
    if alphabet
        nodelabel = ('a':'z')[1:nv(g)]
    else
        nodelabel = 1:nv(g)
    end
    layout=(args...)->spring_layout(args...; C=C)
    draw(PDF(g_path, (w)cm, (h)cm), 
        gplot(g; 
            layout=layout,
            nodelabel=nodelabel,
            #nodesize=1,
            #nodelabeldist=2, 
            #nodelabelangleoffset=π/4,
            #nodestrokec=colorant"black",
            #nodestrokelw=2,
            #nodefillc=colorant"orange",
            default_style...,
            kwargs...))
    return g_path
end

function save_graph(g, x_loc::Vector{T}, y_loc::Vector{T}, w=16, h=16; kwargs...) where T <: Number
    g_path = tempname() * ".pdf"
    draw(PDF(g_path, (w)cm, (h)cm), 
        gplot(g, x_loc, y_loc; 
            #nodesize=1,
            #nodelabeldist=2, 
            #nodelabelangleoffset=π/4,
            #nodestrokec=colorant"black",
            #nodestrokelw=2,
            #nodefillc=colorant"orange";
            default_style...,
            kwargs...))
    return g_path
end

function get_petersen()
    g = SimpleGraph(10)

    for i in 1:5
        add_edge!(g, i, mod(i+1, 5))
        add_edge!(g, 5 + i,  5 + mod(i+1, 5))
        add_edge!(g, i, i+5)
    end

    return g
end


function save_graph_recipe(g;kw...)
    pyplot()
    g_path = tempname() * ".pdf"
    plt = graphplot(g; 
                    method = :stress,
                    fontsize = 10,
                    names=1:nv(g),
                    curves=false,
                    nodeshape=:circle, 
                    nodesize=0.2,
                    linecolor=:black,
                    markercolor=:orange,
                    self_edge_size=0.25,
                    kw...)
    savefig(plt, g_path)
    return g_path
end

function clique_num(g)
    cliques = maximal_cliques(g)
    
    return length(cliques[1])
end

# Make a graph with a clique of 5 vertices
function make_clique_5()
    g = get_graph(8, 17, g->clique_num(g) == 5, 1236)
    path = save_graph(g, false, 8;
                      NODESIZE=0.1,
                      NODELABELSIZE=8,
                     )
    return [path]
end

function graph_g3()
    g = cycle_graph(5)
    nodelabel = ["x($i)" for i in 1:nv(g)]
    nodelabeldist= fill(2, nv(g))
    nodelabeldist[1]=-2
    nodelabeldist[5]=-2
    return save_graph(g, false, 100;
                      layout=circular_layout, 
                      nodelabel=nodelabel,
                      nodelabeldist=nodelabeldist
                     )
end

function make_g4()
    n = 5
    g = cycle_graph(n)

    # duplicate vertices

    add_vertices!(g, n)

    for e in edges(g)
        if e.src <= n && e.dst <= n
            add_edge!(g, e.src, n + e.dst)
            add_edge!(g, n + e.src, e.dst)
        end
    end

    add_vertex!(g)

    z = 2*n + 1
    for i in n + 1:2*n
        add_edge!(g, z, i)
    end

    return g, n, z
end

function make_g4(gsmall)
    n = nv(gsmall)
    g = copy(gsmall)

    # duplicate vertices

    add_vertices!(g, n+1)

    for e in edges(g)
        if e.src <= n && e.dst <= n
            add_edge!(g, e.src, n + e.dst)
            add_edge!(g, n + e.src, e.dst)
        end
    end

    z = 2*n + 1
    for i in n + 1:2*n
        add_edge!(g, z, i)
    end

    return g, n, z
end

function graph_g4_1()
    g, n, z = make_g4()

    loc1 = hcat([[cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)
    loc2 = 2 * loc1
    loc = hcat(loc1, loc2, [0.6; -0.2])

    return save_g4(g, loc, n, z)
end

function graph_g4_2()
    g, n, z = make_g4()

    loc1 = hcat([[cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)[:, [1, 4, 2, 5, 3]]
    loc2 = 2*hcat([[cos(2*π/n*i + π/n), sin(2*π/n*i + π/n)] for i in 1:n]...)[:, [3, 1, 4, 2, 5]]
    loc = hcat(loc1, loc2, [0; 0])

    return save_g4(g, loc, n, z)
end

function graph_g4_3()
    g, n, z = make_g4()

    loc1 = hcat([[3*i, 0] for i in 1:n]...)
    loc2 = hcat([[3*i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [3 * (1+5)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z; linetype="curve", nodelabeldist=nodelabeldist,outangle = -π / 20 )
end

function graph_g4_4()
    g, n, z = make_g4()

    cl = [colorant"purple", colorant"blue", colorant"red", colorant"green"]
    nodefillc=vcat(cl[[1,2,1,2,3]], fill(cl[4], n), cl[1])

    loc1 = hcat([[3*i, 0] for i in 1:n]...)
    loc2 = hcat([[3*i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [3 * (1+5)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z;
                   linetype="curve",
                   nodelabeldist=nodelabeldist,
                   outangle = -π / 20,
                   nodefillc=nodefillc,)
end

function graph_g4_5()
    g, n, z = make_g4()

    cl = [colorant"purple", colorant"blue", colorant"red"]
    nodefillc=vcat(cl[[1,2,1,2,3]], fill(colorant"white", n+1))

    loc1 = hcat([[3*i, 0] for i in 1:n]...)
    loc2 = hcat([[3*i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [3 * (1+n)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z;
                   linetype="curve",
                   nodelabeldist=nodelabeldist,
                   outangle = -π / 20,
                   nodefillc=nodefillc,)
end

function graph_g5()
    g, n, z = make_g4()
    g, n, z = make_g4(g)

    loc1 = hcat([[i, 0] for i in 1:n]...)
    loc2 = hcat([[i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [(1+n)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z;
                   NODESIZE=0.03,
                   linetype="curve",
                   nodelabeldist=nodelabeldist,
                   outangle = -π / 8,
                   nodelabel=nothing,
                  )
end

function save_g4(g, loc, n, z; kwargs...)
    el = edges(g) |> collect
    edgestrokec = fill(colorant"lightblue", ne(g))
    for i in 1:ne(g)
        e = el[i]
        if e.src <= n && e.dst <= n
            edgestrokec[i] = colorant"lightgray"
        end
        if e.src == z || e.dst == z
            edgestrokec[i] = colorant"pink"
        end
    end

    nodelabel = vcat(["x($i)" for i in 1:n], ["y($i)" for i in 1:n], "z")
    nodelabeldist= fill(3, nv(g))
    nodelabeldist[n + 1]=-3
    nodelabeldist[n + n]=-3
    return save_graph(g, loc[1, :], loc[2, :]; 
                      NODESIZE=0.05,
                      nodestrokelw=0.2,
                      nodelabel=nodelabel,
                      nodelabeldist=nodelabeldist,
                      edgestrokec=edgestrokec,
                      kwargs...
                     )
end

function graph_g4_kelly()
    n = 5
    g = cycle_graph(5)
    ni = (n-1)* 3 + 1
    add_vertices!(g, 2*n+ni)

    for e in edges(g)
        if e.src <= n && e.dst <=n
            add_edge!(g, e.src + n, e.dst +n)
            add_edge!(g, e.src + 2*n, e.dst + 2*n)
        end
    end

    for i in 1:5
        add_edge!(g, i, i + 3 *n)
    end

    loc1 = hcat([[1 + cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)
    loc2 = hcat([[6 + cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)    
    loc3 = hcat([[12 + cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)    
    loc4 = hcat([[i, -2] for i in 1:ni]...)
    loc = hcat(loc1, loc2, loc3, loc4)

    cl = [colorant"green", colorant"blue", colorant"green", colorant"blue", colorant"salmon"]
    nodefillc = vcat(repeat(cl, outer=3), fill(colorant"white", ni))
    nodestrokec=vcat(fill(colorant"black", 3*n), fill(colorant"red", ni)) 

    g_path = tempname() * ".pdf"
    draw(PDF(g_path, 24cm, 6cm), 
         gplot(g, 
               loc[1, :],
               loc[2, :]; 
               NODESIZE=0.03,
               nodestrokelw=0.2,
               nodefillc=nodefillc,
               nodestrokec=nodestrokec,
              ))
    return g_path
end

# Make a random bipartite graph
function save_bipartite(n1=4,n2=5,m=13,seed=1111;kwargs...)
    Random.seed!(seed)
    g = get_bipartite(n1, n2, m)
    return save_graph(g, false, 5;
                      NODESIZE=0.1,
                      nodelabel=nothing,
                      kwargs...
                     )
end

# Make a random graph
function save_random()
    Random.seed!(1234)
    g = erdos_renyi(7, 12)
    nodefillc = fill(colorant"orange", nv(g))
    nodefillc[2] = colorant"green"
    nodefillc[4] = colorant"blue"
    nodefillc[5] = colorant"red"
    nodefillc[7] = colorant"blue"
    return save_graph(g;
                      nodefillc = nodefillc
                     )
end

# Make a cycle graph
function make_cycle(n::Integer)
    g = cycle_graph(n)
    loc = circular_loc(n)
    return save_graph(g, loc[1, :], loc[2, :]; 
                      nodelabel=1:n)
end

function make_cycle(ns)
    Random.seed!(1234)
    return [make_cycle(n) for n in ns]
end

# Make a graph for Brook's theorem
function make_brook(n)
    g = complete_graph(n-1)
    add_vertex!(g)
    add_edge!(g, 2, n)
    return g
end

# Save a graph for Brook's theorem
function save_brook_line()
    n = 5
    g = make_brook(n)
    x_loc = collect(1:n)
    y_loc = fill(0, n)
    nodelabel=["v($i)" for i in 1:n]

    return save_graph(g, x_loc, y_loc, 24, 12;
                      nodelabeldist=4, 
                      outangle=-π/2,
                      NODESIZE=0.05,
                      NODELABELSIZE=8,
                      nodelabel=nodelabel, 
                      linetype="curve")
end

# Make a graph for Brook's theorem
function save_brook_natural()
    n = 5
    g = make_brook(n)

    nodelabel=["v($i)" for i in 1:n]
    nodelabeldist=fill(2.5, n)
    nodelabeldist[[1, 3]] .*= -1

    Random.seed!(1234)
    return save_graph(g;
                      nodelabelangleoffset=π/2,
                      nodelabeldist=nodelabeldist, 
                      NODESIZE=0.1,
                      NODELABELSIZE=6,
                      nodelabel=nodelabel)
end

# Just to make all graphs similar
function save_bowtie()
    n = 5
    g = SimpleGraph(5)
    eset = [1 2;
             2 3;
             3 1;
             1 4;
             4 5;
             5 1;]
    for e in eachrow(eset)
        add_edge!(g, e[1], e[2])
    end

    x_loc = [0; -1; -1; 1; 1]
    y_loc = [0; 1; -1; 1; -1]

    nodelabel=["v($i)" for i in 1:n]
    nodelabeldist=fill(4, n)
    nodelabeldist[[3, 5]] .*= -1

    return save_graph(g, x_loc, y_loc, 16, 8;
                      nodelabeldist=nodelabeldist,
                      nodelabelangleoffset=π/2,
                      NODESIZE=0.1,
                      NODELABELSIZE=6,
                      nodelabel=nodelabel)
end

function make_complete(n)
    g = complete_graph(n)

    return save_graph(g, false; layout=circular_layout)
end

function make_complete(ns::Vector{Int})
    return [make_complete(n) for n in ns]
end

function save_hole(n)
    g = cycle_graph(n)

    return save_graph(g;
                      nodelabel=nothing,
                      layout=circular_layout)
end

function make_anti_hole(n::Integer)
    g = Graphs.complement(cycle_graph(n))
    loc = circular_loc(n)
    return save_graph(g, loc[1, :], loc[2, :]; 
                      nodelabel=1:n)
end

function make_anti_hole(ns)
    return [make_anti_hole(n) for n in ns]
end

# Make a graph for the utility example
function save_uitlity()
    g = complete_bipartite_graph(3, 3)
    x_loc = 4 * vcat(fill(0, 3), fill(1, 3))
    y_loc = repeat(1:3, 2)
    labels_1 = ["Water", "Electricity", "Natural Gas"]
    labels_2 = "Home " .* string.(1:3)
    nodelabel = vcat(labels_1, labels_2)
    nodelabeldist = fill(-5, 6)
    nodelabeldist[4:6] .*= -1
    return save_graph(g, x_loc, y_loc, 16, 8;
                      nodelabelangleoffset=π,
                      nodelabeldist=nodelabeldist, 
                      nodelabel=nodelabel,
                      NODELABELSIZE=8,
                     )
end

function save_bipartite_1()
    g = complete_bipartite_graph(3, 3)
    x_loc = 4 * vcat(fill(0, 3), fill(1, 3))
    y_loc = repeat(1:3, 2)
    nodelabeldist = fill(-5, 6)
    nodelabeldist[4:6] .*= -1
    return save_graph(g, x_loc, y_loc, 8, 8;
                      nodelabelangleoffset=π,
                      nodelabeldist=nodelabeldist, 
                      nodelabel=nothing,
                     )
end

function save_k4_planar()
    g = complete_graph(4)
    loc = hcat([[cos(π/2+i*2*π/3), -sin(π/2+i*2*π/3)] for i in 0:2]...)
    loc = hcat(loc, [0, 0])
    x_loc = 30 .* loc[1, :]
    y_loc = 30 .* loc[2, :]
    return save_graph(g, x_loc, y_loc;
                      nodelabel='a':'d'
                     )
end

# An example of number of faces
function save_planar()
    n = 6
    g = SimpleGraph(n)
    eset = [1 4;
            1 2;
            1 5;
            1 6;
            2 3;
            2 5;
            3 4;
            3 5;
            4 6;
           ]

    for e in eachrow(eset)
        add_edge!(g, e...)
    end

    Random.seed!(1233)
    x_loc, y_loc = spring_layout(g)

    x_loc[2] += 1
    y_loc[2] += 0.5

    return save_graph(g, x_loc, y_loc;
                      nodelabel=nothing,
                     )
end

# An example of face edge pairs
function save_face_edge()
    n = 5
    g = SimpleGraph(n)
    eset = [1 4;
            1 2;
            1 5;
            2 3;
            2 5;
            3 5;
           ]

    for e in eachrow(eset)
        add_edge!(g, e...)
    end

    Random.seed!(1234)
    x_loc, y_loc = spring_layout(g)
    loc = hcat(x_loc, y_loc)
    loc[2, :] .+= 0.3

    return save_graph(g, loc[:, 1], loc[:, 2];
                      nodelabel=nothing,
                     )
end

function save_i6()
    g = SimpleGraph(6)
    return save_graph(g;
                      layout=circular_layout,
                      nodelabel=nothing,
                     )
end


function save_wedding_graph(seed=1234;kwargs...)
    g = SimpleGraph(9)

    Random.seed!(seed)

    for i in 1:5
        for j in 6:9
            if true == rand(0:1)
                add_edge!(g, i, j)
            end
        end
    end

    names = pyimport("names")
    fnames = [names.get_first_name(gender="female") for i in 1:5]
    mnames = [names.get_first_name(gender="male") for i in 6:9]

    nodelabel = vcat(fnames, mnames)
    nodefillc = vcat(
                     fill(colorant"orange", 5),
                     fill(colorant"turquoise", 4))
    x_loc = 2 * vcat(1:5, 1.5:1:4.5)
    y_loc = vcat(fill(1.0, 5), fill(2.0, 4))

    g_path = tempname() * ".pdf"
    draw(PDF(g_path, 16cm, 8cm), 
        gplot(g, 
            x_loc,
            y_loc,
            nodesize=1,
            nodelabeldist=2.2, 
            nodelabelangleoffset=π/4,
            nodelabel=nodelabel,
            nodestrokec=colorant"black",
            nodestrokelw=2,
            nodefillc=nodefillc;
            kwargs...))
    return g_path
end


## Save the graph
function save_clique_number()
    Random.seed!(1235)
    for i in 1:3
        n = 10
        p = 0.4
        g = Graphs.SimpleGraphs.erdos_renyi(n, p)
        cliques = maximal_cliques(g)
        cliquenum = maximum(length.(cliques))
        println("Clique number is $cliquenum")
        nodelabel = 1:n
        plt = gplot(g, nodelabel=nodelabel, NODESIZE=0.1, NODELABELSIZE=9)
        draw(PNG("/tmp/clique-number-01-$i.png", 16cm, 16cm, dpi=300), plt)
    end
end

# Create a disconnected graph
function make_disconnected_eulerian()
    g = get_graph(6, 6, x->(is_even_degree(x) 
                                      && !has_degree(x, 0)
                                      && !is_connected(x)), 1234)
    gp = save_graph(g, false;
                    NODESIZE=0.12, 
                    NODELABELSIZE=10,
                   )
    return [gp]
end

# Graph with isolated vertex
function make_isolated_eulerian()
    g = get_graph(6, 6, x->(is_even_degree(x) 
                            && has_degree(x, 0)
                            && !is_connected(x)), 1234)
    gp = save_graph(g, false;
                    NODESIZE=0.12, 
                    NODELABELSIZE=10,
                   )
    return [gp]
end

function make_eulerian()
    ns=[6, 7, 16]
    ms=[10, 11, 28]
    Cs=[5, 4, 3]
    seeds = [1234, 2319, 212]
    ps = String[]
    NODESIZE = [0.12, 0.12, 0.05]
    NODELABELSIZE = [10, 10, 4]
    for i in 1:length(ns)
        g = get_eulerian(ns[i], ms[i], seeds[i])
        path = save_graph(g, false, Cs[i];
                          NODESIZE=NODESIZE[i], 
                          NODELABELSIZE=NODELABELSIZE[i],
                        )
        push!(ps, path)
    end
    return ps
end

function make_hamiltonian(n=8, seed=2019)
    Random.seed!(seed)

    low = convert(Int, ceil(n/2))
    m = convert(Int, ceil(n*low/2))
    if isodd(m)
        m = m+1
    end
    cond = g -> minimum(degree(g)) >= low
    g = get_graph(n, m, cond)

    return save_graph(g, false, 4;
                      NODESIZE=0.12, 
                      NODELABELSIZE=10,
                     )
end

## Create a K5
function make_k5()
    g = CompleteGraph(5)

    ## Set node labels
    nodelabel = 'a':'e'

    ## Layout list
    layoutlist = [spring_layout, random_layout, random_layout, circular_layout, spectral_layout, random_layout]

    ## Save the graph
    for i in 1:6
        plt = gplot(g, nodelabel=nodelabel, layout=layoutlist[i], NODESIZE=0.2, NODELABELSIZE=12)
        draw(PNG("/tmp/graph-k5-$i.png", 16cm, 16cm, dpi=300), plt)
    end
end

## Save a complete graph of size n
function make_complete_graph(n)
    g = CompleteGraph(n)

    ps = String[]

    ## Save the graph
    Random.seed!(1111)
    path = save_graph(g, false;
               )
    push!(ps, path)

    ## Colour the graph
    Random.seed!(1111)
    nodefillc = ColorSchemes.tol_light.colors[1:n]
    path = save_graph(g, false;
                      nodefillc=nodefillc,
               )
    push!(ps, path)
    
    return ps
end

## Create a simple graph
function make_graph_example()
    g = SimpleGraph(5);
    edgelist = [(1,2), (3,4), (1,4), (2, 4)]
    for e in edgelist
        add_edge!(g, e...)
    end

    ## Set node labels
    nodelabel = 'a':'e'

    ## Save the graph
    Random.seed!(1234)
    for i in 1:6
        layout=(args...)->spring_layout(args...; C=15+2*i)
        plt = gplot(g; 
                    nodelabel=nodelabel, 
                    layout=layout, 
                    default_style...
                   )
        draw(PDF("/tmp/graph-01-$i.pdf", 16cm, 16cm, dpi=300), plt)
    end
end

## Make two isomorphic graphs
function make_isomorphism(seed=1111, n=6, ne=6)
    Random.seed!(seed)
    g = erdos_renyi(n, ne)

    ## Save the 1st graph
    path1 = save_graph(g;
                       nodelabel=('a':'z')[1:n])
    path2 = save_graph(g;
                       nodelabel=1:n,
                       nodefillc=colorant"lightgray"
                      )

    return (path1, path2)
end


## Save a complete graph of size n
function make_independent_graph(n)
    g = Graph(n)

    ps = String[]
    path = save_graph(g, false)
    push!(ps, path)

    return ps
end

## Save a complete graph of size n
function make_star_graph(ns)
    ps = String[]
    for n in ns
        g = star_graph(n+1)

        loc = circular_loc(n)
        loc = hcat([0, 0], loc)

        path = save_graph(g, loc[1,:], loc[2,:];
                          nodelabel=1:n+1
                         )
        push!(ps, path)
    end

    return ps
end

## Make a subgraph example
function make_subgraph(;
        induced=false,
        spanning=false,
        spanning_tree=false,
        seed=1111,
        n=12, 
        subne=15, 
        subvl=[2, 3, 5, 7, 8, 9], 
        subel=[(2,3), (2,8), (7,9), (3,8), (3,5)],
    )
    Random.seed!(seed)
    g = erdos_renyi(n, subne)

    if spanning || spanning_tree
        subvl = 1:n
    end

    if induced
        subel = Tuple{Int64, Int64}[]
        subnv = length(subvl)
        for i in 1:subnv-1
            for j in 2:subnv
                push!(subel, (subvl[i], subvl[j]))
            end
        end
    end

    # Colour the vertices
    nodefillc = fill(default_style[:nodefillc], n)
    nodestrokec = fill(default_style[:nodestrokec], n)
    nodestrokelw = fill(0.3, n)
    for v in subvl
        nodefillc[v] = colorant"palegreen"
        nodestrokelw[v] = 2.0
        nodestrokec[v] = colorant"green"
    end

    # Colour the edges
    edgestrokec = fill(default_style[:edgestrokec], subne)
    edgelinewidth = fill(1, subne)

    subgraph_edgs = Edge[]
    if spanning_tree
        subgraph_edgs  = boruvka_mst(g)[1]
    else
        for e in subel
            push!(subgraph_edgs, Edge(e[1], e[2]))
            push!(subgraph_edgs, Edge(e[2], e[1]))
        end
    end

    for (i, e) in enumerate(edges(g))
        if e in subgraph_edgs
            edgestrokec[i] = colorant"green"
            edgelinewidth[i] = 2.0
        end
    end

    EDGELINEWIDTH = 5/sqrt(n)

    return save_graph(g, false, 20; 
                      layout = spring_layout,
                      NODESIZE=0.1, 
                      NODELABELSIZE=8,
                      nodefillc=nodefillc,
                      nodestrokelw=nodestrokelw,
                      nodestrokec=nodestrokec,
                      edgestrokec=edgestrokec,
                      edgelinewidth=edgelinewidth,
                      EDGELINEWIDTH=EDGELINEWIDTH
                     )
end

const graph2_el = [
          (9, 8),
          (3, 8),
          (3, 6),
          (2, 6),
          (2, 12),
          (3, 12),
          (5, 6),
          (6, 1),
          (11, 1),
          (11, 6),
          (11, 7),
          (12, 7),
          (4, 7),
          (4, 10),
          (11, 10),
         ]
    
# Make the graph-2.pdf in the slides
function make_graph2(seed=111)
    g = Graph(12)
    for e in graph2_el
        add_edge!(g, e[1], e[2])
    end

    Random.seed!(seed)
    return save_graph(g, false, 20, 32, 16;
                      layout = spring_layout,
                      NODESIZE=0.08, 
                      NODELABELSIZE=8,
                     )
end

# Make the graph-2.pdf in the slides
function make_graph2_colour(seed=111)
    n = 12
    g = Graph(n)
    for e in graph2_el
        add_edge!(g, e[1], e[2])
    end
    nodefillc = fill(colorant"palegreen", n)
    nodefillc[[6, 7, 9, 10]] .= colorant"pink"
    nodefillc[[2, 3, 5, 11]] .= colorant"yellow"

    Random.seed!(seed)
    path = save_graph(g, false, 20, 32, 16;
                      layout = spring_layout,
                      NODESIZE=0.08, 
                      NODELABELSIZE=8,
                      nodefillc=nodefillc
                     )
    return [path]
end


# Make the graph-2.pdf in the slides
function make_graph6(seed=115)
    n = 12
    
    Random.seed!(seed)
    g = erdos_renyi(n, n-3)

    while is_cyclic(g)
        g = erdos_renyi(n, n-3)
    end

    return save_graph(g, false, 7, 32, 16;
                      NODESIZE=0.08, 
                      NODELABELSIZE=9,
                     )
end

# Make the tree-large.pdf in the slides
function make_tree_large(seed=129, n=16)
    
    Random.seed!(seed)
    g = erdos_renyi(n, n-1)

    while is_cyclic(g)
        g = erdos_renyi(n, n-1)
    end

    dg = tree_to_digraph(g, 5)
    nc = fill(colorant"orange", n)
    nc[5] = colorant"palegreen"
    nc[4] = colorant"pink"

    path = save_graph(g, false, 4, 24, 24;
                      NODESIZE=0.08, 
                      NODELABELSIZE=7,
                     )
    ps = [path]

    xs, ys, _ = solve_positions(Zarate(), dg);
    path =  save_graph(Graph(g), ys, xs, 24, 24;
                        nodelabel=1:n,
                        NODESIZE=0.08, 
                        NODELABELSIZE=8,
                        nodefillc=nc,
                        )
    push!(ps, path)
    return ps
end

# Make the bipartite-1.pdf in the slides
function make_bipartite1()
    g = complete_bipartite_graph(3,3)
    x_loc = [0, 0, 0, 1, 1, 1]
    y_loc = [1, 2, 3, 1, 2, 3]

    return save_graph(g, x_loc, y_loc;
                      nodelabel=1:nv(g))
end

# Make the bipartite-2.pdf in the slides
function make_bipartite2()
    g = complete_bipartite_graph(3,3)
    add_edge!(g, 2, 3)

    x_loc = [0, 0, 0, 1, 1, 1]
    y_loc = [1, 2, 3, 1, 2, 3]

    return save_graph(g, x_loc, y_loc;
                      nodelabel=1:nv(g))
end

# Make the bipartite-3.pdf in the slides
function make_bipartite3()
    g = complete_bipartite_graph(3,3)

    Random.seed!(113)
    return save_graph(g;
                      nodelabel=1:nv(g))
end

# Make the bipartite-4.pdf in the slides
function make_bipartite4()
    g = complete_bipartite_graph(3,3)
    add_edge!(g, 4, 6)

    Random.seed!(118)
    return save_graph(g;
                      nodelabel=1:nv(g))
end

# Make the spanning-tree-1.pdf in the slides
function make_spanning1()
    g = Graph(7)
    for i in 2:5
        add_edge!(g, 1, i)
    end

    for i in 2:5
        j = i + 1
        if j > 5
            j = 2
        end
        add_edge!(g, i, j)
    end

    add_edge!(g, 6, 2)
    add_edge!(g, 6, 3)

    add_edge!(g, 7, 4)
    add_edge!(g, 7, 5)

    x_loc = [0, -1, -1, 1, 1, -2, 2]
    y_loc = [0, 1, -1, -1, 1, 0, 0]

    Random.seed!(118)
    return save_graph(g, x_loc, y_loc, 16, 8;
                      NODESIZE=0.1,
                      NODELABELSIZE=7,
                      nodelabel=1:nv(g))
end

# Make the tree.pdf in the slides
function make_tree(seed=2019)
    g = Graph(9)
    el = [
          (1, 8),
          (1, 4),
          (4, 6),
          (4, 3),
          (6, 5),
          (6, 2),
          (3, 7),
          (3, 9),
         ]
    
    for e in el
        add_edge!(g, e[1], e[2])
    end

    Random.seed!(seed)
    return save_graph(g, false, 3;
                      NODESIZE=0.14, 
                      NODELABELSIZE=10,
                     )
end

# Make the circuit-1.pdf in the slides
function make_circuit1(seed=2019)
    g = Graph(11)
    el = [
          (1,2),
          (2,4),
          (3,4),
          (3,1),
          (2,5),
          (8,5),
          (8,2),
          (4,10),
          (6,10),
          (6,4),
          (4,7),
          (6,7),
          (4,9),
          (6,9),
          (7,9),
          (11,9),
          (11,7),
         ]
    
    for e in el
        add_edge!(g, e[1], e[2])
    end

    Random.seed!(seed)
    return save_graph(g, false, 12;
                      NODESIZE=0.1, 
                      NODELABELSIZE=8,
                     )
end

function circular_loc(n, shift=0)
    loc = hcat([[cos(π/2+i*2*π/n+shift), -sin(π/2+i*2*π/n+shift)] for i in 0:(n-1)]...)
    return loc
end

function make_petersen()
    g = smallgraph(:petersen)

    loc = circular_loc(5)

    loc = hcat(loc[:, :], 2 .* loc[:, :])
    
    # Permeate the locations
    p = [1, 4, 2, 5, 3, 6, 9, 7, 10, 8]

    return save_graph(g, loc[1, p], loc[2, p];
                      NODESIZE=0.15, 
                      NODELABELSIZE=11,
                      nodelabel=1:nv(g),
                     )
end

# Make the tree-random-x.pdf in the slides
function make_tree_random(seed=129, n=7)
    Random.seed!(seed)
    ps = String[]
    
    for _ in 1:6
        g = erdos_renyi(n, n-1)
        path = save_graph(g, false, 16; 
                        NODESIZE=0.1, 
                        NODELABELSIZE=8,
                        )

        push!(ps, path)
    end
    return ps
end

# Make the tree-x.pdf in the slides
function make_tree_x(seed=129, n = 9)
    Random.seed!(seed)
    tree = erdos_renyi(n, n-1)

    while is_cyclic(tree)
        tree = erdos_renyi(n, n-1)
    end
    dg1 = tree_to_digraph(tree, 1)
    nc1 = fill(colorant"orange", n)
    nc1[1] = colorant"palegreen"
    dg2 = tree_to_digraph(tree, 5)
    nc2 = fill(colorant"orange", n)
    nc2[5] = colorant"palegreen"
    ncs = [nc1, nc2]

    ps = String[]

    path =  save_graph(tree, true, 4;
                    NODESIZE=0.12, 
                    NODELABELSIZE=10,
                    )
    push!(ps, path)

    for (i, g) in enumerate([dg1, dg2])
        xs, ys, _ = solve_positions(Zarate(), g);
        path =  save_graph(Graph(g), ys, xs;
                           nodelabel=('a':'z')[1:n],
                           NODESIZE=0.12, 
                           NODELABELSIZE=10,
                           nodefillc=ncs[i],
                          )
        push!(ps, path)
    end
    return ps
end

# Turn a tree into a digraph
function tree_to_digraph(tree, root)
    n = nv(tree)
    dg = SimpleDiGraph(n)
    processed = []
    to_process = [root]
    while length(to_process) != 0
        current = popfirst!(to_process)
        ns = neighbors(tree, current)
        for v in ns
            if !((v in processed) || (v in to_process))
                push!(to_process, v)
                add_edge!(dg, Edge(current, v))
            end
        end
        push!(processed, current)
    end
    return dg
end

function make_radio_map()
    gr()
    ps = String[]
    seed = 2238

    # The first plot
    Random.seed!(seed)
    len = 300
    n = 12

    loc = len .* rand(Float64, (2, n))
    xloc = loc[1, :]
    yloc = loc[2, :]
    plt = scatter(xloc,
                  yloc;
                  xlims=(-10, len),
                  ylims=(-10, len),
                  markersize=40,
                  aspect_ratio=1,
                  c=colorant"orange",
                  legend=nothing,
                  texts=1:n,
                  tickfontsize=20,
                  size=(600, 600),
                 )
    path = tempname() * ".pdf"
    savefig(plt, path)
    push!(ps, path)

    # The graph
    g = Graph(n)
    for i in 1:n-1
        for j in (i+1):n
            d = sqrt((xloc[i]-xloc[j])^2 + (yloc[i]-yloc[j])^2)
            if d <= 100
                add_edge!(g, i, j)
                plot!(xloc[[i,j]], 
                      yloc[[i,j]];
                      linewidth=6,
                      c=colorant"turquoise")
            end
        end
    end
    path = tempname() * ".pdf"
    savefig(plt, path)
    push!(ps, path)

    nodefillc = fill(colorant"palegreen", n)
    nodefillc[[2, 8, 4]] .= colorant"pink"
    nodefillc[[7, 10]] .= colorant"yellow"
    nodefillc[[3, 11]] .= colorant"turquoise"
    nodefillc[[12]] .= colorant"orange"
    path =  save_graph(g, false, 5, 32, 32;
                        NODESIZE=0.12, 
                        NODELABELSIZE=15,
                        nodefillc=nodefillc,
                        )
    push!(ps, path)

    return ps
end

# Make clique-0x.pdf
function make_clique(n=11, m=17, cn=4)
    cond(g) = is_connected(g) && (clique_num(g) == cn)
    ps = String[]
    for s in 1237:1237
        g = get_graph(n, m, cond, s)
        Random.seed!(s)
        path =  save_graph(g, false, 2;
                            NODESIZE=0.1, 
                            NODELABELSIZE=10,
                            )
        push!(ps, path)
    end
    return ps
end

# Make first-fit-01.pdf
function make_first_fit0(n=7, m=10)
    cond(g) = is_connected(g)
    ps = String[]
    for s in 1239:1239
        g = get_graph(n, m, cond, s)
        Random.seed!(s)
        path =  save_graph(g, false, 2;
                            NODESIZE=0.1, 
                            NODELABELSIZE=10,
                            )
        push!(ps, path)
    end
    return ps
end

# Make lollipop-x.pdf
function make_lollipop(n=5)
    ps = String[]
    g = complete_graph(n)
    add_vertex!(g)
    add_edge!(g, n+1, 1)
    for s in 1230:1230
        Random.seed!(s)
        path =  save_graph(g, false, 8;
                            NODESIZE=0.12, 
                            NODELABELSIZE=10,
                            )
        push!(ps, path)
    end
    return ps
end

# Expand the circuit for a larger one
function find_eulerian(g, circuit)
    # Build the initial list of edges
    circuit_graph = Graph(nv(g))
    for i in 1:length(circuit)-1
        add_edge!(circuit_graph, circuit[i], circuit[i+1])
    end

    left_graph = difference(g, circuit_graph)

    vstart = -1
    for v in circuit
        if degree(left_graph, v) >0
            vstart = v
            break
        end
    end

    if vstart == -1
        println("No vertex expand")
        return circuit
    end
    println("Expand at $vstart")

    # Expand
    vcurrent = vstart
    expand = [vstart]
    while true
        vnext_list = neighbors(left_graph, vcurrent)
        if length(vnext_list) == 0
            println("Get stuck at $expand")
            return nothing
        end
        vnext = minimum(vnext_list)
        rem_edge!(left_graph, vcurrent, vnext)
        push!(expand, vnext)

        if vnext == vstart
            break
        end

        vcurrent = vnext
    end

    pos = findfirst(x->x==vstart, circuit)
    vcat(circuit[1:pos-1], expand, circuit[pos+1:end])
end

function find_eulerian(g)
    enum = ne(g)
    c = [1]
    cl = [c]
    println(c)
    while length(c)-1 < enum
        cnext = find_eulerian(g, c)
        if cnext === nothing
            println("Odd vertices")
            return
        elseif length(cnext) == length(c)
            println("Cannot expand anymore")
            return
        end
        println(cnext)
        c = cnext
        push!(cl, c)
    end
    println("Find an eulerian circuit")
    return cl
end

# The problem in AC 5.9.9
function ac_5_9_9()
    vnum = l2n('n')
    g = Graph(vnum)

    ed = Dict(
              'a' => "blke",
              'b' => "al",
              'c' => "fjim",
              'd' => "ijlh",
              'e' => "af",
              'f' => "ekic",
              'g' => "mn",
              'h' => "dm",
              'i' => "fcdm",
              'j' => "ldcm",
              'k' => "af",
              'l' => "abjd",
              'm' => "cjihgn",
              'n' => "mg",
             )
    for s in keys(ed)
        sint = l2n(s)
        for d in ed[s]
            dint = l2n(d)
            add_edge!(g, sint, dint)
        end
    end

    cl =  find_eulerian(g)
    clstr = [join([n2l(n) for n in c], ", ") for c in cl]
    Random.seed!(1239)
    gpath = save_graph(g, true, 10, 32; 
                       NODESIZE=0.08,
                       NODELABELSIZE=8)
    return (gpath, clstr)
end

# Convert a letter label to number label
function l2n(l)
    return Int(l) - Int('a') + 1
end

function n2l(n::Int)
    return ('a':'z')[n]
end


function to_prufer_code(t)
    nx = pyimport("networkx")
    nx_t = nx.Graph()
    for e in edges(t)
        # nx starts with index 0
        println(e)
        nx_t.add_edge(e.src-1, e.dst-1)
    end
    code = nx.to_prufer_sequence(nx_t)
    return 1 .+ code
end

end
