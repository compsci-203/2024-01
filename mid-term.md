---
title: Mid-term Exam for COMPSCI 203
author: Xing Shi Cai
date: 2021-08 session
geometry:
- margin=1.5in
...

# Date, Time and Format

The exam will be carried out through [GradeScope](https://www.gradescope.com/) during 15:00-18:00, 
on Saturday September 18th, 2021 ([CST](https://en.wikipedia.org/wiki/Time_in_China)).

There will be six problems with equal weights.

Please keep an eye on our [Microsoft Teams channel](https://teams.microsoft.com/l/channel/19%3arzW1CJtgHBKamvQb_0uXG9SZZDqmphSTFOtx1-5Qads1%40thread.tacv2/General?groupId=85f158e0-ce88-41a9-a711-493bdbf9f9a2&tenantId=cb72c54e-4a31-4d9e-b14a-1ea36dfac94c).
Corrections or clarifications of the problems will be posted there.
You can also send me a message or call me on MS teams to ask questions.

# What is allowed in an exam?

1. Consulting our textbooks, slides, and your own notes
2. Using the following websites
    1. [OEIS](https://oeis.org/)
    2. [WolframAlpha](https://wolframalpha.com/)
    3. [MathWorld](https://mathworld.wolfram.com/)
    4. [Wikipedia](https://en.wikipedia.org/)
3. Using a computer for calculations
4. Using pencils and papers (of course!)

# What is not allowed?

1. Discussing the exam with **anyone** (human or non-human, online or offline)
2. Search the answer on the Internet or in any books other than the two textbooks

# Scope

The exam will cover the content of the first two modules --- *Logic* and
*Combinatorics*. More specifically ---

Module 1: Logic

1. Statements [DM (Discrete Mathematics) 0.1-0.2]
2. Statements, Sets and Functions [DM (Discrete Mathematics) 0.2-0.4]
3. Propositional Logic [DM 3.1]
4. Basic Proof Techniques [DM 3.2]

Module 2: Combinatorics

5. Strings, Combinations, Permutations, Combinatorial Proofs [AC (Applied Combinatorics) 2.1-2.4]
6. Binomial Coefficients, Catalan Numbers [AC 2.4-2.7]
7. Sequences and Inductions [DM 2.1, 2.5, AC 3] 
8. Recurrence equations [AC 3.5, 9.1-9.4]
9. Inclusion-exclusion (1) [AC 7.1-7.3]
10. Inclusion-exclusion (2) [AC 7.4-7.6]
11. Generating functions (1) [AC 8.1-8.2]
12. Generating functions (2) [AC 8.2-8.4]
13. Generating functions (3) [AC 8.5, 9.6]

# LaTeX

You are *recommended* to use the GradeScope's [LaTeX features](https://help.gradescope.com/article/3vm6obxcyf-latex-guide) when you write mathematical
equations.
But that is *not* mandatory.
