\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum --- Strings, Sets, and Binomial Coefficients (2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Section 2.4-2.5
                        \item Section 2.5 until Example 2.25.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 2.9: 15, 17, 19, 21.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 2.4 Combinatorial Proofs}

\subsection{Summation formulas}

\begin{frame}
    \frametitle{Sum of the First \(n\) Positive Integers}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \think{} How to prove that for $n \ge 1$
            \[
                1+2+\dots+n = \frac{n (n+1)}{2}
            \]
            using a combinatorial argument?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{sum-1.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Sum of the First \(n\) Positive Odd Integers}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \think{} How to prove that for $n \ge 1$
            \[
                1+3+\cdots+2 n -1 = n^2
            \]
            using a combinatorial argument?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{sum-2.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Combinatorial Identities}

\begin{frame}[t]
    \frametitle{Example 2.17}

    Prove that
    \[
        \binom{n}{0}+\binom{n}{1}+\dots+\binom{n}{n}=2^n.
    \]
\end{frame}

\begin{frame}[t]
    \frametitle{Example 2.18}
    Let $n$ and $k$ be integers with $0 \le k < n$.
    Find a combinatorial proof for
    \[
        \binom{n}{k+1}
        =
        \binom{k}{k}
        +
        \binom{k+1}{k}
        +
        \binom{k+2}{k}
        +
        \dots
        +
        \binom{n-1}{k}
        .
    \]
    Proof: \cake{} Why is the \ac{lhs} the number of strings consisting of $k+1$ \emoji{apple}
    and $n-(k+1)$ \emoji{banana}?

    \pause{}

    \think{} Why is the \ac{rhs} also the number of strings consisting of $k+1$ \emoji{apple}
    and $n-(k+1)$ \emoji{banana}.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 2.19}
    Prove that for any $n \in \dsN$,
    \[
        \binom{n}{0}2^{0}+\binom{n}{1}2^{1}+\dots+\binom{n}{n}2^{n}=3^n
    \]
\end{frame}

\begin{frame}[t]
    \frametitle{Example 2.20}
    Prove that for any $n \in \dsN$,
    \[
        \binom{2n}{n} = \binom{n}{0}^{2} + \binom{n}{1}^{2} + \dots \binom{n}{n}^{2}
    \]
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Let $k$ and $n$ be integers with $0 < k \le n$.
    Find a combinatorial proof for
    \[
        \binom{k}{1} \binom{n}{k}
        =
        \binom{n}{1}
        \binom{n-1}{k-1}
    \]
    \hint{} The \ac{lhs} counts the ways of choosing one \emoji{crown} and $k-1$ \emoji{top-hat} from $n$ people.
    Argue that the \ac{rhs} counts the same number.

    %\practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Exercise: Combinatorial Identities}
    \puzzle{} 
    Let $n \in \dsN$.
    Find a combinatorial proof for
    \[
        \sum^{n}_{i=1}\binom{n}{i}i=n 2^{n-1}
    \]

    \pause{}

    \hint{}  Both sides are the sum of the sizes of all subsets of \([n]\).

    \emoji{eye} See a proof \href{https://math.stackexchange.com/q/610110/1618}{here}.
\end{frame}

\begin{frame}
    \frametitle{Exercise: Combinatorial Identities}

    \puzzle{} 
    Let $n \in \dsN$.
    Find a combinatorial proof for
    \[
        \sum_{k=1}^n k^2 
        =\binom{n+1}{2} + 2 \binom{n+1}{3}
    \]

    \pause{}

    \hint{} Both sides count the number of ordered triples $(i,j,k)$ with condition (what to
    put here?).  

    \emoji{eye} See a proof \href{https://math.stackexchange.com/q/95047/1618}{here}.
\end{frame}

\begin{frame}
    \frametitle{\zany{} Challenge: Combinatorial Identities}

    \puzzle{} 
    Let $n \ge 1$ be an integer.
    Find a combinatorial proof for
    \[
        \sum_{k=1}^n k^3 = \binom{n+1}{2}^2
    \]

    \pause{}

    Proof: \ac{lhs} counts the number of ordered 4-tuple $(h, i,j,k)$ with condition $0 \leq h,i,j < k
    \leq n$.  

    \ac{rhs} counts $(x_1,x_2), (x_3,x_4)$ with $0 \leq x_1 < x_2 \leq n$ and $0 \leq x_3 < x_4 \leq n$.  

    \hint{}
    You need a bijection between both sides.

    \emoji{eye} See a proof \href{https://math.stackexchange.com/q/95047/1618}{here}
    and many other proofs \href{https://math.stackexchange.com/q/61482/1618}{here}.
\end{frame}

\begin{frame}[c]
    \frametitle{Computer assisted proof}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Many combinatorial identities can be \emph{proved} by
            \href{https://www.wolframalpha.com/input/?i=sum\_(k\%3D0)\%5En+binomial(n,+k)\%5E2+binomial(3+n+\%2B+k,+2+n)}{computers}
            using algorithms.

            \medskip{}

            \href{http://sites.math.rutgers.edu/~zeilberg/}{Doron Zeilberger} 
            came up with some of these algorithms.

            \medskip{}

            \laughing{} Check out his article:
            \href{https://sites.math.rutgers.edu/~zeilberg/Opinion92.html}{Twenty Pieces of Advice for a Young (and also not so young) Mathematician}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Zeilberger.jpg}
                \caption*{Doron Zeilberger}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 2.5 The Ubiquitous Nature of Binomial Coefficients}

\subsection{Integer Composition}

\begin{frame}[c]
    \frametitle{Integer composition}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            How many distinct ways can we distribute 10 \carrot{}s among 3 \bunny{}s, 
            ensuring that each \bunny{} receives at least one \carrot{}?

            \onslide<2>{%
                This is an example of \alert{integer composition} ---
                What is the number of \emph{integer} solutions of
                \[
                    x_{1}+x_{2}+\dots + x_{n} = m
                \]
                with $x_{i} > 0$ for all $i \in [n]$.
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-carrot.jpg}
                \caption{Ten \carrot{}s and three \bunny{}s}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The Argument}

    Put \(10\) \alert{identical} \carrot{} in a line.

    Insert \(2\) \pepper{} into the gaps in the line, e.g.,
    \begin{itemize}
        \item \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \pepper{}  \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{}  \carrot{} \carrot{} \carrot{}
    \end{itemize}

    \pause{}

    \cake{} In how many ways can we use this method to separate $10$ \carrot{}
    into \(3\) \emph{non-empty} piles? Answer with a binomial coefficient.

    \hint{} We need to avoid the following cases
    \begin{itemize}
        \item \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \pepper{} \carrot{} \carrot{} \carrot{}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The General Formula for Integer Compositions}
    we want to give \(m\) \alert{identical} \carrot{} and \(n\) \alert{distinct}
    \bunny{}.

    To do this, we put \(m\) \alert{identical} \carrot{} in a line.

    By inserting \blankshort{} \pepper{} into the \blankshort{} gaps in the line,
    we get \(n\) non-empty piles of \carrot{}.

    \cake{} Thus, the number of ways to do this is
    \[
        \binom{\blankshort{}}{\blankshort{}}
    \]
\end{frame}

\begin{frame}
    \frametitle{When \bunny{}s Are Allowed to Be Empty-handed}
    
    In how many ways can we distribute our 10 \carrot{} to 3 \bunny{},
    if we are \emph{allowed} to give no \carrot{} to any \bunny{}.

    One way to do this is as follows:
    \begin{equation*}
        \text{\hspace{1em} \pepper{} \hspace{1em} \carrot{} \carrot{} \carrot{} \carrot{}
        \carrot{} \carrot{} \carrot{} \pepper{} \hspace{1em} \carrot{} \carrot{} \carrot{}}
    \end{equation*}
    \pause{}%
    \hint{} If we then give each \bunny{} an extra \carrot{}:
    \begin{equation*}
        \text{\carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{}}
    \end{equation*}
    then this is a solution of distributing 13 \carrot{} to 3 \bunny{}, \emph{not} allowing empty-handed \bunny{}.

    \cake{} What is the number of ways to do this?
\end{frame}

\begin{frame}
    \frametitle{When There Are a Bit More \carrot{}}
    
    Now we wants to give our $3$ \bunny{} $10 + 3$ \carrot{}, 
    so that each \bunny{} receives at least one \carrot{}.

    For example, assume that the distribution is
    \begin{equation*}
        \text{\carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{}}
    \end{equation*}
    \pause{}%
    If we takes one \carrot{} back from each \bunny{}, then the distribution is
    \begin{equation*}
        \text{\hspace{1em} \pepper{} \hspace{1em} \carrot{} \carrot{} \carrot{} \carrot{}
        \carrot{} \carrot{} \carrot{} \pepper{} \hspace{1em} \carrot{} \carrot{} \carrot{}}
    \end{equation*}

    This is a solution of distributing 10 \carrot{} to 3 \bunny{}, allowing empty-handed \bunny{}.
\end{frame}

\begin{frame}
    \frametitle{A Bijection}

    Let $X$ be the set of integer solutions for
    \[
        x_{1}+x_{2}+\dots + x_{n} = m
    \]
    with $x_{i} \ge 0$ for all $i \in [n]$,
    and $Y$ to be the integer solutions for
    \[
        x_{1}'+x_{2}'+\dots + x_{n}' = m+n
    \]
    with $x_{i}' > 0$ for all $i \in [n]$

    \pause{}
    Then the function $f:X \rightarrow Y$ defined by
    \begin{equation*}
        f(x_1, x_2, \dots, x_n) = (x_1 + 1, x_2 + 1, \dots, x_n + 1)
    \end{equation*}
    is a bijection.

    Thus, $\abs{X} = \abs{Y}$, i.e., the number of solutions for both problems is the
    same.

    \cake{} What is the number?
\end{frame}

\begin{frame}[t]
    \frametitle{Another Solution}

    In how many ways can we distribute our 10 \carrot{} to 3 \bunny{},
    if we are \emph{allowed} to give no \carrot{} to any \bunny{}.

    \hint{} Consider two cases ---
    \begin{itemize}
        \item \pepper{}s in different gaps: \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{}
        \item \pepper{}s in the same gap: \carrot{} \pepper{} \pepper{}  \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{}  \carrot{} \carrot{} \carrot{}
    \end{itemize}

    \cake{} What should the answer be?
\end{frame}

\begin{frame}
    \frametitle{Example 2.25}

    What is the number of integer solutions to the inequality
    \begin{equation*}
        x_{1}+x_{2}+x_{3}+x_{4} + x_{5} + x_{6} \le 538
    \end{equation*}
    subject the restriction that
    \only<1>{%
        $x_{i} > 0$ for $i \in [6]$ and equality holds.
    }%
    \only<2>{%
        $x_{i} \ge 0$ for $i \in [6]$ and equality holds.
    }%
    \only<3>{%
        $x_1 , x_2 , x_4 , x_6 > 0, x_3 = 52, x_5 = 194$, and equality holds
    }%
    \only<4>{%
        $x_{i} > 0$ for $i \in [6]$ and the inequality is strict.
    }%
    \only<5>{%
        $x_{i} \ge 0$ for $i \in [6]$ and the inequality is strict.
    }%
    \only<6>{%
        $x_{i} \ge 0$ for $i \in [6]$.
    }%
\end{frame}

\end{document}
