\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Generating Function (3)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 8.5
                        \item
                            Section 8.6 excluding Example 8.20
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 8.8: 13, 17, 19, 21, 23, 25, 27.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}
\section{\acs{ac} 8.5 Partition of an Integer}

\subsection{Integer Partition}

\begin{frame}
    \frametitle{Integer Partition: An Example}

    We want to partition 10 \carrot{} into several non-empty piles.

    Two partitions are considered identical 
    if they have the same number of \carrot{} in each pile,
    when the piles are ordered from the largest to the smallest.

    \pause{}

    For example, the following partitions are counted as identical:

    \begin{itemize}
        \item \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{}
        \item \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{}
        \item \carrot{} \carrot{} \carrot{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{} \pepper{} \carrot{} \carrot{}
    \end{itemize}

    \think{} In how many ways can we do this?
\end{frame}

\begin{frame}
    \frametitle{Integer Partition: A Clear Definition}

    Consider an integer \(m\). An integer partition of \(m\) is an expression of \(m\) as a sum of integers:
    \[
        m = a_{1} + a_{2} + \ldots + a_{n}
    \]
    where \(a_{1} \ge a_{2} \ge \ldots \ge a_{n} \ge 1\). Each sequence of \(a_{i}\)'s that satisfies these conditions represents a \alert{partition} of \(m\).

    \hint{} The question involving \carrot{} seeks the total number of \alert{partitions} for
    $10$.

    \pause{}

    \bomb{} Unfortunately, there's no straightforward formula to calculate the number of partitions for any given \(m\). 
    
    \cake{} How about we try to list all partitions of $4$?
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Two Giants who Studied Integer Partitions}
    \begin{columns}[c]
        \column{0.5\textwidth}

        \begin{figure}
            \centering
            \includegraphics[width=0.8\textwidth]{hardy.jpg}
            \caption*{G. H. Hardy (1877-1947)}
        \end{figure}

        \column{0.5\textwidth}

        \begin{figure}
            \centering
            \includegraphics[width=0.8\textwidth]{Ramanujan.jpg}

            \caption*{Srinivasa Ramanujan (1887-1920)}
        \end{figure}

    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} A film about Ramanujan}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The story of Ramanujan and Hardy is 
            told in the film
            \href{https://youtube.com/watch?v=QqRnkFXuMmo}{The Man Who Knew Infinity}.

            \medskip{}

            In one scene from the film, 
            Hardy explained $4$ can be partitioned in five ways.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\linewidth]{the-man-who-knew-infity.jpg}
            \end{figure}
        \end{column}
    \end{columns}
    
\end{frame}

\subsection{Integer Partition with Restrictions}

\begin{frame}
    \frametitle{Paying for Lunch with Cash}

    You forgot your \emoji{iphone} and need to pay \$100 for your \emoji{sandwich} using
    cash.

    You can use as many bills of each denomination as needed, but the total must be
    exactly \$100.

    Let's explore the number of ways to pay \$100 using unlimited \$1, \$2, and \$5 bills.

    \cake{} Can you give some examples of the ways to do this?
\end{frame}

\begin{frame}[c]
    \frametitle{Paying with \$\only<1>{1}\only<2>{2}\only<3>{5} bills}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            Let $a_{\only<1>{1}\only<2>{2}\only<3>{5}, n}$ be the number of ways to pay \$$n$ with \$\only<1>{1}\only<2>{2}\only<3>{5} bills.

            Then
            \begin{equation*}
                a_{\only<1>{1}\only<2>{2}\only<3>{5}, n} = \blankshort{}
            \end{equation*}
            and
            \begin{equation*}
                A_{\only<1>{1}\only<2>{2}\only<3>{5}}(x) 
                = 
                \sum_{n \ge 0} a_{\only<1>{1}\only<2>{2}\only<3>{5}, n} x^n
                =
                \blankveryshort{}
            \end{equation*}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=\textwidth]{bunny-1-dollar.jpg}%
                \includegraphics<2>[width=\textwidth]{bunny-2-dollar.jpg}%
                \includegraphics<3>[width=\textwidth]{bunny-5-dollar.jpg}%
                \caption{\$\only<1>{1}\only<2>{2}\only<3>{5} bill of the forest kingdom}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Paying with \$1, \$2, \$5 bills}

    Let $a_{n}$ be the number of ways to pay \$$n$ with \$1, \$2, \$5 bills.

    The \ac{gf} of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            A(x) 
            &
            =
            \sum_{n=0}^{\infty} a_{n} x^n
            =
            A_{1}(x)
            A_{2}(x)
            A_{5}(x)
            \\
            &
            =
            (1+x+x^{2} + \cdots)
            (1+x^{2}+(x^{2})^{2} + \cdots)
            (1+x^{5}+(x^{5})^{2} + \cdots)
            \\
            &
            =
            \frac{1}{1-x}
            \frac{1}{1-x^{2}}
            \frac{1}{1-x^{5}}
        \end{aligned}
    \end{equation*}

    \bomb{} There is no closed form for $a_{n}$ in general.

    But it is easy to get $a_{100} = 541$ by \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5B1\%2F\%28\%281-x\%29\%281-x\%5E2\%29\%281-x\%5E5\%29\%29\%2C+\%7Bx\%2C+0\%2C+100\%7D\%5D}{WolframAlpha}.
\end{frame}

\begin{frame}
    \frametitle{Why Does This Work?}
    
    Let's compute the coefficient of, say $x^{4}$, in the \ac{gf}
    \begin{equation*}
        \begin{aligned}
            &
            A_{1}(x)
            A_{2}(x)
            A_{5}(x)
            \\
            &
            =
            {\color{red} ((1+x^{1}+(x^{1})^{2} + \cdots)}
            {\color{blue} (1+x^{2}+(x^{2})^{2} + \cdots)}
            {\color{orange} (1+x^{5}+(x^{5})^{2} + \cdots)}
            \\
            &
            =
            \dots 
            + 
            \left(
            {\color{red} (x^{1})^{4}}
            {\color{blue} (x^{2})^{0}}
            {\color{orange} (x^{5})^{0}}
            +
            {\color{red} (x^{1})^{2}}
            {\color{blue} (x^{2})^{1}}
            {\color{orange} (x^{5})^{0}}
            +
            {\color{red} (x^{1})^{0}}
            {\color{blue} (x^{2})^{2}}
            {\color{orange} (x^{5})^{0}}
            \right)
            + 
            \cdots 
        \end{aligned}
    \end{equation*}
    \pause{}%
    Thus the coefficient of $x^{4}$ is $3$, 
    corresponding to the only three ways to 
    pay \$4 with \$1, \$2, \$5 bills:
    \begin{equation*}
        \begin{aligned}
            4 & = {\color{red} 1 + 1 + 1 + 1} \\
              & = {\color{blue} 2} + {\color{red} 1 + 1} \\
              & = {\color{blue} 2 + 2} \\
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Paying with \$$m$ bills}
    
    Let $a_{m, n}$ be the number of ways to pay \$$n$ with \$$m$ bills for some
    integer $m > 0$.

    The \ac{gf} $A_{m}(x)$ of $(a_{m, n})_{n \ge 0}$ is
    \begin{equation*}
        1 + x^{m} + x^{2 m} + x^{3 m} + \cdots = \frac{1}{1-x^{m}}
    \end{equation*}

    \pause{}

    Let \( P_{n} \) be the number of ways to pay \$\(n\) using bills with
    positive integer denominations, i.e., \$1, \$2, \$3, \dots.

    The \ac{gf} of $(P_{n})_{n \ge 0}$ is
    \begin{equation*}
        P(x) 
        =
        \sum_{n=1}^{\infty} P_{n} x^{n}
        =
        \prod_{m=1}^{\infty}
        A_{m}(x)
        =
        \prod_{m=1}^{\infty}
        \frac{1}{1-x^{m}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}

    Let $b_{n}$ be the number of ways to pay \$n with \$1 and \$2 bills.
    Then $b_{1} = 1$ and $b_{2} = 2$.

    Show that
    \begin{equation*}
        b_{n} = \ceil*{\frac{n+1}{2}}, \qquad \text{for all } n \in \dsN
    \end{equation*}
    with \ac{gf}.

    \pause{}

    Can you find a combinatorial proof for your formula?

    \hint{} Find a recursion for $b_{n}$.
\end{frame}

\subsection{Partitions with Odd Parts and Distinct Parts}

\begin{frame}
    \frametitle{Two Ways to Pay \$n}

    Let \( o_{n} \) be the number of ways to pay \$\(n\) using bills with odd denominations, i.e., \$1, \$3, \$5, \ldots.

    According to the \href{https://oeis.org/A000009}{OEIS}, the initial values for \( o_{n} \) are
    \[
        1, 1, 1, 2, 2, 3, 4, 5, 6, 8, 10, 12, 15
    \]

    \pause{}

    Let \( d_{n} \) denote the number of ways to pay \$\(n\) using at most one of each type of bill: \$1, \$2, \$3, \ldots.

    According to the \href{https://oeis.org/A000009}{OEIS}, the initial values for \( d_{n} \) are
    \[
        1, 1, 1, 2, 2, 3, 4, 5, 6, 8, 10, 12, 15
    \]

    \cake{} Do you see something strange?
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.16}

    For each \( n \ge 1 \), $d_n = o_n$, i.e., the number of partitions of \( n \) into distinct
    parts is the same as the number of partitions of \( n \) into odd parts.

    \hint{} The proof is uses generating functions.

    \zany{} See
    \href{https://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/syl84.html}{here}
    for a combinatorial proof.
\end{frame}

\section{\acs{ac} 8.6 Exponential Generating Function}

\subsection{What is \ac{egf}?}

\begin{frame}
    \frametitle{Understanding \acf{egf}}

    Given an infinite sequence \(\{a_{n}\}_{n \ge 0}\), 
    we call
    \[
        F(x) = \sum_{n \ge 0} \frac{a_{n}}{n!} x^{n}.
    \]
    \alert{\acf{egf}} of \((a_{n})_{n \ge 0}\).

    In other words, the \ac{egf} of \( \{a_{n}\}_{n \ge 0} \) is just the \ac{gf} of
    \( \{\frac{a_{n}}{n!}\}_{n \ge 0} \).

    \weary{} Why do we need \acf{egf}? 

    \hint{} Some combinatorial problems are much easy to solve using \acf{egf}.
\end{frame}

\subsection{Example 8.17: \acp{egf} and Strings}

\begin{frame}
    \frametitle{Strings consisting of one letter}
    
    Let $a_{n}$ be the number of strings of length $n$ consisting of only the letter
    \emoji{apple}. 
    Then $a_n = \blankshort$ for all $n \ge 0$.

    The \acf{egf} of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        A(x) = \sum_{n = 0}^{\infty} = \blanklong{}.
    \end{equation*}

    \pause{}

    Let $b_{n}$ be the number of strings of length $n$ consisting of only the letter
    \emoji{banana}.
    Then $b_n = \blankshort$ for all $n \ge 0$.

    The \acf{egf} of $(b_{n})_{n \ge 0}$ is
    \begin{equation*}
        B(x) = \blanklong{}.
    \end{equation*}

    \hint{} Note that $A(x) = B(x)$.
\end{frame}

\begin{frame}
    \frametitle{Strings Consisting of Two Letters}
    
    Let $c_{n}$ be the number of strings of length $n$ consisting of either the
    letter \emoji{apple} or the letter \emoji{banana}.
    Then $c_n = \blankshort$ for all $n \ge 0$.

    The \acf{egf} of $(c_{n})_{n \ge 0}$ is
    \begin{equation*}
        C(x) = \blanklong{}
    \end{equation*}
    Note that
    \begin{equation*}
        \begin{aligned}
            A(x) B(x) = C(x).
        \end{aligned}
    \end{equation*}

    \astonished{} How is this possible?
\end{frame}

\begin{frame}
    \frametitle{Why Does This Work?}

    \think{} Let's compute the coefficient of, say \( x^{3} \), 
    in the \ac{egf} \( A(x) \times B(x) \).
    \begin{equation*}
        \begin{aligned}
            &
            A(x) \times B(x)
            \\
            &=
            {\color{red} \left(\sum_{n \ge 0} \frac{a_n}{n!} x^{n}\right)}
            {\color{blue} \left(\sum_{m \ge 0} \frac{b_m}{m!} x^{m}\right)}
            \\
            &=
            \cdots
            +
            \left(
                {\color{red} \frac{a_{0}}{0!} x^{0}}
                {\color{blue} \frac{b_{3}}{3!} x^{3}}
                +
                {\color{red} \frac{a_{1}}{1!} x^{1}}
                {\color{blue} \frac{b_{2}}{2!} x^{2}}
                +
                {\color{red} \frac{a_{2}}{2!} x^{2}}
                {\color{blue} \frac{b_{1}}{1!} x^{1}}
                +
                {\color{red} \frac{a_{3}}{3!} x^{3}}
                {\color{blue} \frac{b_{0}}{0!} x^{0}}
            \right)
            +
            \cdots
            \\
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{An Application of \ac{egf}: Mixing Two Types of Strings}

    Let $a_n$ and $b_n$ be the numbers of strings of length $n$ with in
    $\scA$ and $\scB$ respectively.

    Let $c_n$ be the number of strings of length $n$ in $\scC$
    which contains strings that
    can be uniquely decomposed into two substring in $\scA$ and $\scB$.

    \only<1>{%
        For example, we can have 
        \begin{itemize}
            \item $\scA = \{\text{Strings consists of \emoji{apple}}\}$
            \item $\scB = \{\text{Strings consists of \emoji{banana}}\}$
            \item $\scC = \{\text{Strings consists of \emoji{apple} or \emoji{banana}}\}$
        \end{itemize}
    }%
    \only<2>{%
        For example, we can have 
        \begin{itemize}
            \item $\scA = \{\text{Chinese words}\}$
            \item $\scB = \{\text{English words}\}$
            \item $\scC = \{\text{Words consist of part Chinese part English}\}$
        \end{itemize}
    }%
    \only<3>{%
        Let $A(x), B(x), C(x)$ be the corresponding \acp{egf}.

        Then we have
        \begin{equation*}
            C(x) = A(x) B(x).
        \end{equation*}
    }%
\end{frame}

\subsection{Ternary Strings}

\begin{frame}
    \frametitle{Strings with even number of \emoji{cherries}}
    
    Let $d_{n}$ be  the number of strings consisting only of even number of
    \emoji{cherries}.

    The \ac{egf} of $\{d_{n}\}_{n \ge 0}$ is
    \begin{equation*}
        \sum_{n = 0}^{\infty} 
        \frac{d_{n}}{n!} x^{n} 
        = 1 + \frac{x^{2}}{2!} + \cdots = \frac{e^{x} + e^{-x}}{2}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 8.19: Ternary strings with even number of \emoji{cherries}}
    
    Let $e_{n}$ be  the number of \emph{ternary} strings consisting of \emoji{apple},
    \emoji{banana} and \emph{even number} of \emoji{cherries}.

    The \ac{egf} of $(e_{n})_{n \ge 0}$ is
    \begin{equation*}
        \sum_{n = 0}^{\infty} \frac{e_{n}}{n!} x^{n} 
        =  e^{x} e^{x} \frac{e^{x} + e^{-x}}{2}
    \end{equation*}

    \pause{}

    \cake{} From this, how to get
    \begin{equation*}
        e_{n} = \frac{3^{n} + 1}{2}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}
    \puzzle{} Can you prove that the number of ternary strings of length $n$ 
    with even number of $0$ is
    \begin{equation*}
        e_{n} = \frac{3^{n} + 1}{2}.
    \end{equation*}
    using strong induction instead?

    \hint{}
    You can use the recursion
    \begin{equation*}
        e_{n} 
        = 
        2 e_{n-1} + (3^{n-1} - e_{n-1})
    \end{equation*}
    and $e_{0} = 1, e_{1} = 2$.
\end{frame}

\begin{frame}
    \frametitle{Example 8.19}

    How many ternary strings of length $n$ have at least one $0$ and at least one $1$?
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}
    
    Given that the \ac{egf} of \( \{a_{n}\}_{n \ge 0} \) is
    \begin{equation*}
        \frac{1}{1-x} \log \frac{1}{1-x},
    \end{equation*}
    find a formula for computing $a_{n}$.

    \pause{}

    Answer:
    \begin{equation*}
        a_{n} =
        \begin{cases}
            n! H_n & n \ge 1 \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}
    where $H_{n} = \sum_{k=1}^{n} \frac{1}{k!}$ is the harmonic number.

    \zany{} The number $a_{n}$ has a combinatorial meaning.
    See Chapter 2 of Analytic Combinatorics.
\end{frame}

\begin{frame}[label=current]
    \frametitle{\puzzle{} Exercise}
    Let $a_n$ be the number of strings of length $n$
    formed from the set $\{x, y, z, w \}$
    such that the following conditions are met:

    \begin{itemize}
        \item The number of $y$'s must be odd.
        \item There can be no more than one $z$'s.
    \end{itemize}
    Find a formula of $a_n$ in closed form.

    \pause{}

    Answer:
    The \ac{egf} of \( \{a_{n}\}_{n \ge 0} \) is
    \begin{equation*}
        \frac{1}{2} e^{2x} (e^x - e^{-x}) (x + 1)
    \end{equation*}
    Thus,
    \begin{equation*}
        a_n 
        = 
        \frac{3^{n-1}} n{2} -\frac{n}{2}+\frac{3^n}{2}-\frac{1}{2}
        .
    \end{equation*}
\end{frame}

\end{document}
