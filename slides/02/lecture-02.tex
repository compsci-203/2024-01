\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\input{../tikz.tex}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    \acs{dm} Section 0.3-0.4.
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    \acs{dm} Section 0.3: 1-9, 11-15.
                \item
                    \acs{dm} Section 0.4: 1, 2, 5, 7, 9, 20.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{dm} 0.3 Sets}

\begin{frame}
    \frametitle{Sets}
    
    A \alert{set} is simply an unordered collection of objects. 

    \zany{} What are objects? We have to rely on our common intuition.

    \pause{}

    \begin{example}[]
        \begin{itemize}
            \item All actors in Marvel films.
            \item Your pets -- \emoji{unicorn} \emoji{crocodile} \emoji{dragon}.
            \item All positive integer numbers which are less than $10$.
        \end{itemize}
    \end{example}

    \cake{} Can you think of a set? (Preferably a funny one. \smiling{})
\end{frame}

\subsection{Notations}

\begin{frame}
    \frametitle{Numbers}
    
    A \alert{natural number} is an integer greater than or equal to $0$.

    We use $\dsN$ to denote natural numbers, i.e.,
    \begin{equation*}
        \dsN = \{0, 1, 2, 3, \ldots \}.
    \end{equation*}
    \bomb{} The number $0$ is a natural number in this course.

    \pause{}
    Some other sets of numbers include
    \begin{itemize}
        \item Integers: $\dsZ = \{\ldots, -1, 0, 1, 2, 3, \ldots \}$,
        \item Real numbers: $\dsR$,
        \item Rational numbers: $\dsQ$,
        \item Complex numbers: $\dsC$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Notations}

    Set notations:

    \begin{itemize}
        \item Defining a set --- $A = \{1, 2, 3\}$.
        \item In a set --- $a \in \{a, b, c\}$.
        \item Not in a set --- $d \notin \{a, b, c\}$.
        \item The \alert{empty set}, denoted by $\emptyset$, contains no element.
        \item The \alert{universal set}, denoted by $\scU$, contains all elements we are
    considering.
    \end{itemize}

    \pause{}

    \cake{} 
    Let
    \begin{equation*}
        A = \{1, b, \{x, y, z\}, \emptyset\}.
    \end{equation*}
    Is $x \in A$?
\end{frame}

\begin{frame}
    \frametitle{Set Builder Notation}

    When we define a set by $\dots$, such as
    \begin{equation*}
        A = \{0, 2, 4, \dots \}
    \end{equation*}
    it is not entirely clear what is in $A$.

    We can make it precise by using the \emph{set builder notations} ---
    \begin{equation*}
        A = \{x \in \dsN: \exists n \in \dsN(x = 2 n) \}.
    \end{equation*}
    What is this set in plain English?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    What are the following sets in plain English?
    \begin{enumerate}
        \item $\{x : x + 3 \in \dsN \}$.
        \item $\{x \in \dsN : x + 3 \in \dsN \}$.
        \item $\{x : x \in \dsN \vee −x \in \dsN \}$.
        \item $\{x : x \in \dsN \wedge −x \in \dsN \}$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{A Variation of Notation}
    
    While the condition is generally given after the ``such that'', 
    sometimes it is hidden in the first part.

    \begin{exampleblock}{Example 0.3.2}
        What are these two sets?
        \begin{enumerate}
            \item $A = \{x \in \dsZ: x^{2} \in \dsN\}$.
            \item $B = \{x^{2}: x \in \dsN\}$.
        \end{enumerate}
        How can we write $B$ in the form 
        \begin{equation*}
            B = \{x \in \dsN : \blanklong{}\}
        \end{equation*}
    \end{exampleblock}
\end{frame}

\subsection{Relationships Between Sets}

\begin{frame}
    \frametitle{Equality}
    
    If two sets have exactly the same elements, then the sets are equal, e.g.,
    \begin{equation*}
        \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\} = \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\}
    \end{equation*}

    \bomb{} The order in which the elements of a set are listed does not matter.

    \pause{}

    \cake{} Which of the following equations true?
    \begin{equation*}
        \begin{aligned}
            \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\} & = 
            \{\temoji{panda}, \temoji{dragon}, \temoji{unicorn}\} \\
            \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\} & =
            \{\temoji{panda}, \temoji{unicorn}, \temoji{dragon}\} \\
            \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\} & =
            \{\temoji{snake}, \temoji{unicorn}, \temoji{panda}\} \\
            \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\} & = 
            \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}, \temoji{bug}\}
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Subsets}

    A set $A$ is \alert{a subset} of a set $B$, 
    denoted by $A \subseteq B$, if every element of $A$ is also an element of $B$.

    A set $A$ is \alert{a proper subset} of a set $B$, 
    denoted by $A \subset B$, if $A \neq B$ and $A \subseteq B$.

    \pause{}
    
    Let $A = \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\}$ and $B = \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}, \temoji{bug}\}$?

    \cake{} Which of the followings are true?
    \begin{equation*}
        \begin{aligned}
            & A \subseteq B \\
            & A \subset B \\
            & B \subseteq A \\
            & B \subset A \\
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Power sets}
    
    The \alert{power set}  of a set $A$, denote by $\scP(A)$, is the set of all subsets of $A$.
    
    \begin{exampleblock}{Example 0.3.4}
    Let $A = \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\}$. Find the
    power set $\scP(A)$.

    \vspace{5cm}
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Cardinality (size)}
    
    The \alert{cardinality (size)} of a set $A$, denote by $\abs{A}$, is the number of elements in $A$.

    \begin{exampleblock}{Example 0.3.5}
        \begin{enumerate}
            \item Find the cardinality of $A = \{23, 24, \dots, 37, 38\}$.
            \item Find the cardinality of $B = \{1, \{2, 3, 4\}, \emptyset\}$.
            \item If $C = \{1, 2, 3\}$, what is the cardinality of $\scP(C)$?
        \end{enumerate}
        \vspace{3cm}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Let 
    \begin{itemize}
        \item $A = \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}, \temoji{bug}, \temoji{dog}, \temoji{crocodile}\}$, 
        \item $B = \{\temoji{unicorn}, \temoji{bug}, \temoji{crocodile}\}$, 
        \item $C = \{\temoji{dragon}, \temoji{unicorn}, \temoji{panda}\}$ ,
        \item $D = \{\temoji{lady-beetle}, \temoji{elephant}, \temoji{bird}\}$.
    \end{itemize}

    \cake{} Which of the following are true, false, or meaningless?
    \vspace{-1em}
    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.33\textwidth}
            \begin{enumerate}
                \item $A \subset B$.
                \item $B \subset A$.
                \item $B \in C$.
            \end{enumerate}
        \end{column}
        \begin{column}{0.34\textwidth}
            \begin{enumerate}
                \setcounter{enumi}{3}
                \item $\emptyset \in A$.
                \item $\emptyset \subset A$.
                \item $A < D$.
            \end{enumerate}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{enumerate}
                \setcounter{enumi}{6}
                \item $\temoji{panda} \in C$.
                \item $\temoji{panda} \subset C$.
                \item $\{\temoji{panda}\} \subset C$.
            \end{enumerate}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Operations On Sets}

\begin{frame}[c]
    \frametitle{Union}

    We define the \alert{union} of two sets $A$ and $B$ as
    \begin{equation*}
        A \cup B = \{x \in \scU: x \in A \vee x \in B\}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (-0.5,0) rectangle (5.5,4); % Universal set U
            \fill[gray!30] (1.5,2) circle (1.5); % Shading A
            \fill[gray!30] (3.5,2) circle (1.5); % Shading B
            \draw (1.5,2) circle (1.5); % Set A
            \draw (3.5,2) circle (1.5); % Set B
            \node at (1.5,2) {\(A\)};
            \node at (3.5,2) {\(B\)};
            \node at (0.5,3.5) {\(\scU\)};
        \end{tikzpicture}
        \caption*{$A \cup B$}
    \end{figure}

    \cake{} What is the union of all even and odd numbers?
\end{frame}

\begin{frame}[c]
    \frametitle{Intersection}

    We define the \alert{intersection} of two sets $A$ and $B$ as
    \begin{equation*}
        A \cap B = \{x \in \scU: x \in A \wedge x \in B\}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (-0.5,0) rectangle (5.5,4); % Universal set U
            \begin{scope} % Scope for intersection
                \clip (1.5,2) circle (1.5);
                \fill[gray!30] (3.5,2) circle (1.5);
            \end{scope}
            \draw (1.5,2) circle (1.5); % Set A
            \draw (3.5,2) circle (1.5); % Set B
            \node at (1.5,2) {\(A\)};
            \node at (3.5,2) {\(B\)};
            \node at (0.5,3.5) {\(\scU\)};
        \end{tikzpicture}
        \caption*{$A \cap B$}
    \end{figure}

    \cake{} What is the intersection of all even and odd numbers?
\end{frame}

\begin{frame}[c]
    \frametitle{Difference}

    We define the \alert{set deference} between two sets $A$ and $B$ as
    \begin{equation*}
        A \setminus B = \{x \in \scU: x \in A \wedge x \notin B\}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (-0.5,0) rectangle (5.5,4); % Universal set U
            \begin{scope}
                \clip (1.5,2) circle (1.5);
                \fill[gray!30] (1.5,2) circle (1.5);
                \fill[white] (3.5,2) circle (1.5);
            \end{scope}
            \draw (1.5,2) circle (1.5); % Set A
            \draw (3.5,2) circle (1.5); % Set B
            \node at (1.5,2) {\(A\)};
            \node at (3.5,2) {\(B\)};
            \node at (0.5,3.5) {\(\scU\)};
        \end{tikzpicture}
        \caption*{$A \setminus B$}
    \end{figure}

    \cake{} What is the set difference between $\dsZ$ and $\dsN$?
\end{frame}

\begin{frame}[c]
    \frametitle{Venn Diagram --- Complement}

    We define the \alert{complement} of a set $A$ as
    \begin{equation*}
        \overline{A} = \{x \in \scU: x \notin A\}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (-0.5,0) rectangle (5.5,4); % Universal set U
            \fill[gray!30] (-0.5,0) rectangle (5.5,4); % Shading U
            \fill[white] (1.5,2) circle (1.5); % Removing A
            \draw (1.5,2) circle (1.5); % Set A
            \node at (1.5,2) {\(A\)};
            \node at (0.5,3.5) {\(\scU\)};
        \end{tikzpicture}
        \caption*{$\overline{A}$}
    \end{figure}

    \dizzy{} What is the complement of even numbers?
\end{frame}

\begin{frame}
    \frametitle{Cartesian Product}
    
    The \alert{Cartesian product}  of $A$ and $B$ is
    \begin{equation*}
        A \times B = \{(a,b) : a \in A \wedge b \in B\}\text{.}
    \end{equation*}

    \begin{exampleblock}{Example 0.3.8.}
        Let $A = \{\temoji{bread}, \temoji{rice}\}$ and $B = \{\temoji{pineapple}, \temoji{banana}, \temoji{apple}\}$.
        Then
        \begin{align*}
            A \times B
            &
            =
            \left\{
            \begin{matrix}
                (\temoji{bread}, \temoji{pineapple}) & (\temoji{bread}, \temoji{banana}) & (\temoji{bread}, \temoji{apple})\\
                (\temoji{rice}, \temoji{pineapple}) & (\temoji{rice}, \temoji{banana}) & (\temoji{rice}, \temoji{apple})
            \end{matrix}
            \right\}
        \end{align*}
        \cake{} What is $\abs{A \times A}$?
    \end{exampleblock}
\end{frame}

\begin{frame}[c]
    \frametitle{Set theory notations}

    \begin{center}
        \begin{tabular}{ c  l }
            \toprule
            Notations & Meaning \\
            \midrule
            $A \subseteq B$ & A is a \alert{subset} of B.\\
            $A \subset B$ & A is a \alert{proper subset} of B.\\
            $A \cap B$ & The \alert{intersection} of A and B.\\
            $A \cup B$ & The \alert{union} of A and B.\\
            $A \times B$ & The \alert{Cartesian product} of A and B.\\
            $A \setminus B$ & The \alert{set difference} between A and B.\\
            $\overline{A}$ & The \alert{complement} of A.\\
            $\abs{A}$ & The \alert{cardinality} (or size) of A.\\
            \bottomrule
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let 
    \begin{itemize}
        \item $A = \{\temoji{snake}, \temoji{unicorn}, \temoji{panda}\}$, 
        \item $B = \{\temoji{unicorn}, \temoji{panda}, \temoji{bug}\}$, 
        \item and $\scU = \{\temoji{snake}, \temoji{unicorn}, \temoji{panda},
            \temoji{bug}, \temoji{dog}, \temoji{dragon}, \temoji{whale}, \temoji{lady-beetle}\}$. 
    \end{itemize}
    \cake{} What are the following sets?
    \begin{itemize}
        \item $A \cup B = \{\blankshort{}\}$.
        \item $A \cap B = \{\blankshort{}\}$.
        \item $\overline{A} = \{\underline{\hspace{7cm}}\}$.
        \item $A \setminus B = A \cap \overline{B} = \blankshort{}$.
    \end{itemize}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Russell's Paradox}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Bertrand Russell was a \emoji{uk} logician, mathematical and philosopher.

            \medskip{}

            He give the following example to show that naive set theory is
            \href{https://en.wikipedia.org/wiki/Russell\%27s\_paradox}{inconsistent}.

            \medskip{}

            Let $R$ be the set of all sets that do not contain themselves.
            Then a paradox arises since
            \begin{equation*}
                R \in R \iff R \notin R
            \end{equation*}

        \end{column}
        \begin{column}{0.5\textwidth}

            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{Russell.jpg}
                \caption*{Bertrand Russell (1872-1970)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Exercise: Operations on sets}
%    
%    Let 
%    \begin{itemize}
%        \item $A = \{\emoji{dragon}, \emoji{unicorn}, \emoji{panda}, \emoji{bug}, \emoji{dog}, \emoji{crocodile}\}$, 
%        \item $B = \{\emoji{unicorn}, \emoji{bug}, \emoji{crocodile}\}$,  $C = \{\emoji{dragon}, \emoji{unicorn}, \emoji{panda}\}$, $D = \{\emoji{lady-beetle}, \emoji{elephant}, \emoji{bird}\}$,
%        \item $\scU = \{\emoji{dragon}, \emoji{unicorn}, \emoji{panda}, \emoji{bug}, \emoji{dog}, \emoji{crocodile}, \emoji{whale}, \emoji{lady-beetle}, \emoji{elephant}, \emoji{bird}\}$.
%    \end{itemize}
%    Find the sizes of:
%    \begin{columns}[totalwidth=\textwidth]
%        \begin{column}{0.33\textwidth}
%            \begin{enumerate}
%                \item $A \cup B$.
%                \item $B \cap A$.
%                \item $B \cap C$.
%            \end{enumerate}
%        \end{column}
%        \begin{column}{0.34\textwidth}
%            \begin{enumerate}
%                \setcounter{enumi}{3}
%                \item $A \cap D$.
%                \item $\overline{B \cup C}$.
%                \item $A \setminus B$.
%            \end{enumerate}
%        \end{column}
%        \begin{column}{0.33\textwidth}
%            \begin{enumerate}
%                \setcounter{enumi}{6}
%                \item $(D \cap \overline{C}) \cup \overline{A \cap B}$.
%                \item $C \cup \emptyset$.
%                \item $C \cap \emptyset$.
%            \end{enumerate}
%        \end{column}
%    \end{columns}
%
%    \poll{}
%\end{frame}

\section{\acs{dm} 0.4 Functions}

\begin{frame}
    \frametitle{Functions}

    A \alert{function} is a rule that assigns each input \alert{exactly} one output
    \emph{deterministically}.

    We call the output the \alert{image} of the input.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{function.png}
        \caption*{By \href{https://commons.wikimedia.org/w/index.php?curid=10562739}{Wvbailey} at English Wikipedia}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Domain and Codomain}

    The set of all inputs for a function is called the \alert{domain}. 

    The set of all allowable outputs is called the \alert{codomain}.

    The set of all images of all inputs is the \alert{range}.

    We use the notation
    \begin{equation*}
        f:A \mapsto B
    \end{equation*}
    to indicate that $f$ is a function with domain $A$ and codomain $B$.

    \pause{}

    \cake{} What is the domain, codomain and range of
    the function $f:\dsN \mapsto \dsN$ defined by 
    \begin{equation*}
        f(x)=x+1. 
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Not Functions!}

    \cake{} The following are not functions. Why?
    \begin{enumerate}
        \item $f:\dsN \mapsto \dsN$ defined by $f(n)=\frac{n}{2}$.
        \item The rule that matches each person to their cellphone numbers. 
        \item ChatGPT
    \end{enumerate}
\end{frame}

%\begin{frame}
%    \frametitle{Functions as rules}
%
%    \cake{} Can you guess how is this function defined?
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.5\linewidth]{function-1.png}
%        \caption*{By \href{https://commons.wikimedia.org/w/index.php?curid=20802095}{Wvbailey} at English Wikipedia}
%    \end{figure}
%\end{frame}

\subsection{Describing Functions}

\begin{frame}
    \frametitle{Describing functions (1)}

    Consider  the same function $f : \{1, 2, 3\} \to \{1, 2, 3\}$ defined by two pictures

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \colorbox{white}{%
                    \begin{tikzpicture}[dot/.style={circle,fill,inner sep=2pt}]
                        % Draw grid
                        \draw[thin,gray!40] (0,0) grid (3,3);
                        % Draw axes
                        \draw[thick,->] (0,0) -- (3.1,0) node[anchor=north west] {$x$};
                        \draw[thick,->] (0,0) -- (0,3.1) node[anchor=south east] {$y$};

                        % Plot points
                        \node[dot] at (1,2) {};
                        \node[dot] at (2,1) {};
                        \node[dot] at (3,3) {};
                    \end{tikzpicture}
                }%
            \end{figure}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \colorbox{white}{%
                    \begin{tikzpicture}[every node/.style={inner sep=1pt}]
                        % Nodes
                        \node (a1) at (0,0) {1};
                        \node (a2) at (1,0) {2};
                        \node (a3) at (2,0) {3};
                        \node (b1) at (0,-1) {1};
                        \node (b2) at (1,-1) {2};
                        \node (b3) at (2,-1) {3};

                        % Arrows
                        \draw[->] (a1) -- (b2);
                        \draw[->] (a2) -- (b1);
                        \draw[->] (a3) -- (b3);
                    \end{tikzpicture}
                }%
            \end{figure}

        \end{column}
    \end{columns}

    \cake{} What is $f(1) + f(3)$?
\end{frame}

\begin{frame}
    \frametitle{Describing functions (2)}

    The function $f$ can also be described by formula
    \begin{equation*}
        f(x) =
        \begin{cases}
            x+1 & \text{if } x = 1 \\
            x-1 & \text{if } x = 2 \\
            x & \text{if } x = 3
        \end{cases}
    \end{equation*}
    or by a table
    \begin{equation*}
        \begin{array}{c|ccc}
            x & 1 & 2 & 3 \\
            \hline
            f(x) & 2 & 1 & 3 \\
        \end{array}
    \end{equation*}
    or simply as matrix
    \begin{equation*}
        f
        =
        \begin{pmatrix}
            1 & 2 & 3 \\
            2 & 1 & 3 \\
        \end{pmatrix}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 0.4.3}

    Let $X = \{1, 2, 3, 4\}$ and $Y = \{a, b, c, d\}$.
    Which of the following are functions? 
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{example-0.4.3.png}
    \end{figure}

    How to write these functions as matrices?
\end{frame}

\begin{frame}[t]
    \frametitle{Recursively Defined Functions}
    
    For a function $f : \{n_0, n_0 + 1, \dots \} \mapsto \dsN$, a \alert{recursive definition} consists of 
    \begin{itemize}
        \item initial condition --- the value of $f(n_0)$. 
        \item recurrence relation --- a formula for $f (n + 1)$ in terms for $f (n)$
            and $n$.
    \end{itemize}
    
    \begin{exampleblock}{Example 0.4.5}
        Consider the function $f : \dsN \mapsto \dsN$ given by $f (0) =0$ and 
        $f (n + 1) = f (n) + 2n + 1$. Find $f (4)$.
        \vspace{2.5cm}
    \end{exampleblock}
\end{frame}

\subsection{Surjections, Injections and Bijections}

\begin{frame}
    \frametitle{Surjective (onto) function}

    When a function's range contains the codomain, we
    say that is a \alert{surjective/onto function} or a \alert{surjection}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{surjective.png}
    \end{figure}

    \cake{} Which functions are surjective (i.e., onto)?
    \begin{itemize}
        \only<1>{%
            \item $f : \dsZ \mapsto \dsZ$ defined by $f (n) = 3n$.
        }%
        \only<2>{%
            \item $g : \{1, 2, 3\} \mapsto \{a, b, c\}$ defined by 
            $g = \begin{pmatrix} 1 & 2 & 3 \\ c & a & a \end{pmatrix}$. 
        }%
        \only<3>{%
            \item $h : \{1, 2, 3\} \mapsto \{1, 2, 3\}$ defined as follows:
                \begin{figure}[htpb]
                    \centering
                    \includegraphics[width=0.25\linewidth]{function-5.png}
                \end{figure}
        }%
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Injective (one-to-one) function}

    When each element of the codomain is the image at most one
    element of the domain, 
    we say that a function is a \alert{injective/one-to-one function} or a
    \alert{injection}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{injective.png}
    \end{figure}

    \cake{} Which functions are injective (i.e., one-to-one)?

    \begin{itemize}
        \only<+>{\item $f : \dsZ \mapsto \dsZ$ defined by $f (n) = 3n$.}
        \only<+>{
            \item $g : \{1, 2, 3\} \mapsto \{a, b, c\}$ defined by 
                $g = \begin{pmatrix}
                    1 & 2 & 3 \\ 
                    a & a & b
                \end{pmatrix}$.
            }
        \only<+>{
        \item $h : \{1, 2, 3\} \mapsto \{1, 2, 3\}$ defined as follows:
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.25\linewidth]{function-5.png}
            \end{figure}
        }
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Bijection}

    A function is \alert{bijective}, if it is both injective and surjective.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{bijective.png}
        \caption*{A bijective function. By \href{https://commons.wikimedia.org/w/index.php?curid=1059694}{Schapel} from English Wikipedia}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Composition of functions}
%    
%    The \alert{composition}  of $f$ and $g$, denoted by $g \circ f$ is the function which maps
%    $x$ to $g(f(x))$.
%
%    \cake{} Let $f(x) = 2 x$ and $g(x) = x + 1$.
%    What are $(g \circ f)(x)$ and $(f \circ g)(x)$?
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Find examples of function,
    \begin{equation*}
        f:\{1,2,3\} \mapsto \{a, b,c,d\}
        ,
        \qquad
        g:\{a,b,c,d\}\mapsto\{X, Y, Z\}
    \end{equation*}
    such that
    \begin{itemize}
        \item $g$ is not injective but $x \mapsto g(f(x))$ is injective.
        \item $f$ is not surjective but $x \mapsto g(f(x))$ is surjective.
    \end{itemize}
    Answer here:
    \begin{center}
        \colorbox{white}{%
            \begin{tikzpicture}[every node/.style={inner sep=1pt}]
                % Nodes
                \node (f) at (-1, -0.5) {$f$};
                \node (g) at (-1, -1.5) {$g$};
                \node (a1) at (0,0) {$1$};
                \node (a2) at (1,0) {$2$};
                \node (a3) at (2,0) {$3$};
                \node (b1) at (0,-1) {$a$};
                \node (b2) at (1,-1) {$b$};
                \node (b3) at (2,-1) {$c$};
                \node (b4) at (3,-1) {$d$};
                \node (c1) at (0,-2) {$X$};
                \node (c2) at (1,-2) {$Y$};
                \node (c3) at (2,-2) {$Z$};
            \end{tikzpicture}
        }%
    \end{center}
\end{frame}


\subsection{Image and Inverse Image}

\begin{frame}
    \frametitle{Image of a subset of the domain}
    For a function $f : X \mapsto Y$, the \alert{image} of $\chi \subseteq X$
    under $f$ is defined by
    \begin{equation*}
        f(\chi) = \{f(a):a \in \chi\}.
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{image.png}
        \caption*{By \href{https://commons.wikimedia.org/w/index.php?curid=11409441}{Damien Karras} from English Wikipedia }
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Inverse image of a subset of the codomain}

    For a function $f : X \mapsto Y$, the \alert{inverse image} of $B \subseteq Y$
    under $f$ is
    \begin{equation*}
        f^{-1}(B) = \{x \in X: f(x) \in B\}.
    \end{equation*}
    For brevity, we also write
    \begin{equation*}
        f^{-1}(\{b\}) = f^{-1}(b).
    \end{equation*}

    \pause{}

    \begin{exampleblock}{Example 0.4.8}
        Consider
        \begin{equation*}
            f
            =
            \begin{pmatrix}
                1 & 2 & 3 & 4 & 5 & 6 \\
                a & a & b & b & b & c
            \end{pmatrix}
        \end{equation*}
        \cake{} What is $f(\{1,2,3\})$, $f^{-1}(\{a,b\})$, and $f^{-1}(d)$?
        \vspace{2cm}
    \end{exampleblock}
\end{frame}

%\begin{frame}[t]
%    \frametitle{Exercise}
%    
%    For of the following, give a domain of discourses which make it true, and another
%    which makes it false.
%
%    \begin{enumerate}
%        \item $\forall x\exists y(y^2 =x)$.
%
%        \item $\forall x\forall y(x < y \imp  \exists z(x < z < y))$.
%
%        \item $\exists x\forall y\forall z(y < z \imp  y \le  x \le  z)$.
%    \end{enumerate}
%
%    \hint{} The domain does not need to be infinite.
%
%    \practice{}
%\end{frame}

%\begin{frame}
%    \frametitle{Implicit Quantifiers}
%    
%    The following is not a statement --
%
%    \begin{quote}
%        If a shape is a square, then it is a rectangle.
%    \end{quote}
%
%    What we really want to say is
%
%    \begin{quote}
%        \emph{For all} shape $x$, if $x$ is a square, then $x$ is a rectangle.
%    \end{quote}
%\end{frame}

\end{document}
