\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Graph Colouring (1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 5.4, 5.4.1
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 5.9: 15--17.
                    \end{itemize}
            \end{itemize}
            \href{http://discrete.openmathbooks.org/dmoi3}{\acf{dm}}
            \begin{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 4.4: 1, 3, 5, 7, 12.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 5.4 Graph Colouring}

\subsection{Motivational Example}

\begin{frame}
    \frametitle{Example 1: Distinct Cars for Ex-partners}

    A group of \bunny{} plans a car trip.

    Ex-partners, as indicated in the graph, must travel in separate cars.

    \think{} What's the fewest number of cars needed?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.75\linewidth]{graph-2.pdf}
        \caption{Ex-partner \bunny{}}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Colours to Help}

    We can colour the vertices to denote which car each \bunny{} is in.

    The task is to find the minimum number of colours ensuring no adjacent
    vertices share one.

    The picture below shows that with three colours suffice. 

    \cake{} Can we use fewer colours?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{graph-2-colour.pdf}
        \caption{Distinct colours, distinct cars}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 2: Frequency Allocation for Radio Towers}

    Radio \emoji{tokyo-tower} within \(100\) km must use distinct frequency bands.

    Locations are depicted below.

    \think{} What's the fewest number of frequencies to avoid interference?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{radio-1.pdf}
        \caption{Radio \emoji{tokyo-tower}}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Colouring to Help Again}

    Let \emoji{orange-circle} symbolize radio \emoji{tokyo-tower}, and edges denote proximity
    within \(100\) km.

    Let the colour denote the frequency band a \emoji{tokyo-tower} uses.

    \think{} How many colours suffice to ensure adjacent vertices differ?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{radio-2.pdf}
        \caption{Radio \emoji{tokyo-tower} as Graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Can We Do Better?}

    As shown below, $5$ colours are enough.

    \cake{} Can we use less?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{radio-3.pdf}
        \caption{A colouring of the \emoji{tokyo-tower} graph}
    \end{figure}
\end{frame}

\subsection{Chromatic Number}

\begin{frame}
    \frametitle{Chromatic Number}

    Given \( G = (V, E) \) and a colour set \( C \),

    A \alert{proper colouring} is a function \( \phi: V \to C \) such that \( \phi(x) \ne \phi(y) \) for \( xy \in E \).

    The smallest \( t \) allowing a \emph{proper colouring} with \( t \) colours is the \alert{chromatic number},
    which we denote by \( \chi(G) \).

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{graph-2-colour.pdf}
        \caption{A graph with chromatic number $3$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Nature of Colours}

    Elements in \( C \) are arbitrary and serve to categorise vertices. For example, the \( K_5 \) graph can be coloured using different sets for \( C \) as follows:
    \only<1>{\( C = \{\temoji{red-circle}, \temoji{blue-circle}, \temoji{green-circle}, \temoji{yellow-circle}, \temoji{purple-circle}\} \)}
    \only<2>{\( C = \{\temoji{cat-face}, \temoji{dog-face}, \temoji{cow-face}, \temoji{rabbit-face}, \temoji{tiger-face}\} \)}
    \only<3>{\( C = \{1, 2, 3, 4, 5\} \)}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graph node},
            style={graph edge},
            scale=0.8]
            % Vertices
            \foreach \i/\colour/\animal in {1/BrightRed/cat-face, 2/DaylightBlue/dog-face, 3/MintGreen/cow-face, 4/GoldenWhisper/rabbit-face, 5/LavenderDream/tiger-face}
            {
                \only<1>{\node[label={\i*72-90: \( v_{\i} \)}, fill=\colour] (\i) at (\i*72-90:2cm) {};}
                \only<2>{\node[fill=none, label={\i*72-90: \( v_{\i} \)}] (\i) at (\i*72-90:2cm) 
                    {\Large \emoji{\animal}};}
                \only<3>{\node[label={\i*72-90: \( v_{\i} \)}] (\i) at (\i*72-90:2cm) {\i};}
            }
            % Edges
            \foreach \i in {1,...,4} {
                \pgfmathtruncatemacro{\jstart}{\i+1}
                \foreach \j in {\jstart,...,5} {
                    \draw (\i) -- (\j);
                }
            }
        \end{tikzpicture}
        \caption{Example: \( K_{5} \) colouring}
    \end{figure}
\end{frame}

\subsection{Chromatic Number of Specific Graphs}

\begin{frame}
    \frametitle{Trivial Bounds}
    
    Let $G$ be a graph with $n \ge 1$ vertices.

    \cake{}
    Do you see why
    \begin{equation*}
        1 \le \chi(G) \le n
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Independent Graphs}
    Recall that an independent graph \( I_n \) is a graph with \( n \) vertices and no edges.

    \cake{} Why do we have \(\chi(I_n) \le 1\) and $\chi(I_n) \ge 1$?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graph node, minimum size=1em},
            style={graph edge},
            scale=0.7]
            % Vertices
            \foreach \i in {1,...,8}
            {
                \node[label={\i*45-90: \( v_{\i} \)}, fill=MintGreen] (\i) at
                    (\i*45-90:2cm) {};
            }
        \end{tikzpicture}
        \caption{Example: \( I_{8} \) colouring}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complete Graphs}

    \cake{} What is $\chi(K_n)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{k5-2.pdf}
        \caption{Example: $K_5$ colouring}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Chromatic Number and Subgraphs}
    
    \cake{}
    Let $H$ be a subgraph of $G$.
    Do you see why $\chi(G) \ge \chi(H)$?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{radio-3.pdf}
        \caption{What is the chromatic number of this graph $G$?}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{A Recipe for Finding the Chromatic Number of a Graph}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            To show that $\chi(G) = k$, we need to:
            \begin{itemize}
                \item Show \( \chi(G) \le k \) by finding a proper colouring of \( G \) with \( k \) colours.
                \item Show \( \chi(G) \ge k \), often using proof by contradiction.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-colour-graph.jpg}
                \caption{How to find the chromatic number?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Star Graphs}

    Let $S_n$ denote the \tree{} with one internal node and $n$ \emoji{leaves}.

    \cake{} What is $\chi(S_n)$?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graph node},
            style={graph edge},
            scale=1]
            % Vertices
            \node[fill=GoldenWhisper] (0) at (0, 0) {};
            \foreach \i in {1,...,5}
            {
                \node[fill=MintGreen] (\i) at
                    (\i*72-90:2cm) {};
            }
            % Edges
            \foreach \i in {1,...,5} {
                \draw (\i) -- (0);
            }
        \end{tikzpicture}
        \caption{Example: \( S_{5} \) colouring}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Not so simple!}
%
%    \think{} 
%    \only<1>{What is $\chi(G)$?}
%    \only<2>{Clearly $\chi(G) \le 4$? But does $3$ colours suffices?}
%
%    \begin{figure}[htpb]
%        \centering
%        \begin{tikzpicture}[
%            style={graph edge},
%            every node/.style={graph node},
%            scale=2,
%            ]
%            \foreach \i in {1,...,5}{
%                \pgfmathsetmacro\j{int(\i+5)};
%                \node (\i) at (\i,0) {\i};
%                \node (\j) at (\i,1) {\j};
%            }
%            \node (11) at (3, 2) {11};
%            \foreach \i in {2,...,5}{
%                \pgfmathsetmacro\j{int(\i+5)};
%                \pgfmathsetmacro\ilast{int(\i-1)};
%                \draw (\i) -- (\ilast);
%            }
%            \foreach \i in {2,...,4}{
%                \pgfmathsetmacro\jlast{int(\i+4)};
%                \pgfmathsetmacro\jnext{int(\i+6)};
%                \draw (\i) -- (\jnext);
%                \draw (\i) -- (\jlast);
%                \pgfmathsetmacro\j{int(\i+5)};
%                \pgfmathsetmacro\ilast{int(\j-4)};
%                \pgfmathsetmacro\inext{int(\j-6)};
%                \draw (\j) -- (\inext);
%                \draw (\j) -- (\ilast);
%            }
%            \draw (6) -- (5);
%            \draw (1) -- (10);
%            \foreach \j in {6,...,10}{
%                \draw (\j) -- (11);
%            }
%            \draw (1) to [out=315,in=225] (5);
%            \only<2>{
%                % Draw some new nods to colour things
%                \foreach \i in {2,5,11}{
%                    \node[fill=LightGreen] at (\i) {\i};
%                }
%                \foreach \i in {6,7,8,9,10}{
%                    \node[fill=pink] at (\i) {\i};
%                }
%                \foreach \i in {1, 4}{
%                    \node[fill=yellow] at (\i) {\i};
%                }
%                \foreach \i in {3}{
%                    \node[fill=LightBlue] at (\i) {\i};
%                }
%            }
%        \end{tikzpicture}
%    \end{figure}
%\end{frame}

\subsection{Chromatic Number of Cycles}

\begin{frame}
    \frametitle{Cycles}
    
    Let $C_{n}$ denote a cycle of $n$ vertices.

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c3.pdf}
            \end{center}
            \caption{$C_3$}
        \end{subfigure}
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c4.pdf}
            \end{center}
            \caption{$C_4$}
        \end{subfigure}
        
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c5.pdf}
            \end{center}
            \caption{$C_5$}
        \end{subfigure}
        \begin{subfigure}{0.25\textwidth}
            \begin{center}
                \includegraphics[width=0.8\textwidth]{c6.pdf}
            \end{center}
            \caption{$C_6$}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Even Cycle}
    
    \cake{} What is \( \chi(C_{2n}) \)?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graph node},
            style={graph edge},
            scale=0.8]
            % Vertices
            \foreach \i [evaluate=\i as \colour using {int(mod(\i,2))}] in {1,...,6}
            {
                \ifnum\colour=0
                    \node[label={\i*60-90:\( v_{\i} \)}, fill=MintGreen] (\i) at (\i*60-90:2cm) {};
                \else
                    \node[label={\i*60-90:\( v_{\i} \)}, fill=Salmon] (\i) at (\i*60-90:2cm) {};
                \fi
            }
            % Edges
            \foreach \i in {1,...,5} {
                \pgfmathtruncatemacro{\j}{\i+1}
                \draw (\i) -- (\j);
            }
            \draw (6) -- (1);
        \end{tikzpicture}
        \caption{Example: \( C_{6} \) Colouring}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Odd Cycle}
    
    \think{} What is $\chi(C_{2n+1})$?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            font=\footnotesize,
            every node/.style={graph node},
            style={graph edge},
            scale=1]
            % Vertices
            \foreach \i [evaluate=\i as \colour using {int(mod(\i,2))}] in {1,...,9}
            {
                \ifnum\colour=0
                    \node[label={\i*40-90:\( v_{\i} \)}, fill=MintGreen] (\i) at (\i*40-90:2cm) {};
                \else
                    \ifnum\i=9
                        \node[label={\i*40-90:\( v_{\i} \)}, fill=none] (\i) at (\i*40-90:2cm) {};
                    \else
                        \node[label={\i*40-90:\( v_{\i} \)}, fill=Salmon] (\i) at (\i*40-90:2cm) {};
                    \fi
                \fi
            }
            % Edges
            \foreach \i in {1,...,8} {
                \pgfmathtruncatemacro{\j}{\i+1}
                \draw (\i) -- (\j);
            }
            \draw (9) -- (1);
            \node[graphannotation] (text1) at (0,0) {
                What colour can \\ we put here?
            };
            \draw[grapharrow] (text1) -- (9);
        \end{tikzpicture}
        \caption{Example: \( C_{9} \) Colouring}
    \end{figure}
\end{frame}

\subsection{Chromatic Number of Bipartite Graphs}

%\begin{frame}
%    \frametitle{A wedding puzzle}
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\textwidth]{bunny-wedding.jpg}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{A Wedding Puzzle}

    You need to arrange seats for guests of a \emoji{wedding}.

    People who are ex-couples, as shown in the graph, cannot sit at the same table.

    Can we do this with \emph{at most} two tables?

    \begin{figure}[htpb]
        \huge
        \centering
        \includegraphics[width=0.7\textwidth]{wedding.pdf}
    \end{figure}

    This is to ask, if the graph is $2$-colourable.
\end{frame}

\begin{frame}
    \frametitle{$k$-colourable graphs}
    
    A graph $G$ is \alert{$k$-colourable} if $\chi(G) \le k$.

    \bomb{} Being $k$-colourable means we need \emph{at most} $k$ colours for a proper colouring.
    It does not mean we need \emph{at least} $k$ colours.

    For example,
    \begin{itemize}
        \item $K_n$ is $k$-colourable for any $k \ge \blankshort{}$.
        \item $I_n$ is $k$-colourable for any $k \ge \blankshort{}$.
        \item $S_n$ is $k$-colourable for any $k \ge \blankshort{}$.
        \item $C_n$ is $k$-colourable for any $k \ge \blankshort{}$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{$2$-Colourable Graphs}

    A graph is $2$-colourable if and only if it is bipartite.

    \begin{figure}[htpb]
        \centering
        \drawKmn[MintGreen,Salmon]{3}{4}
        \caption{Example: \( K_{3,4} \) colouring}
        \label{fig:}
    \end{figure}

    \hint{} Note that $K_1$ is also $2$-colourable and thus bipartite.
\end{frame}

%\begin{frame}
%    \frametitle{The distance in a graph}
%    
%    The \alert{distance} between two vertices $u$ and $v$,
%    denoted by $d(u,v)$,
%    is the length of the shortest path from $u$ to $v$.
%
%    \cake{} What are $d(3,3), d(3, 8)$, $d(6, 9)$ and $d(10, 9)$?
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{graph-2}
%        \caption{A graph $G$}
%    \end{figure}
%\end{frame}

\begin{frame}[label=current]
    \frametitle{Theorem 5.21 (\acs{ac}) --- Another characterization}

    A graph is $2$-colourable if and only 
    if it does not contain an \emph{odd} cycle.

    \only<1>{%
        For the ``only if'' part, we use proof by contrapositive.
    }%
    \only<2>{%
        For the ``if'' part, we use proof by contradiction.

        Take an arbitrary vertex $v$. Colour all vertices at even
        distance from $v$ with \emoji{blue-circle} and all vertices at odd distance with \emoji{red-circle}.

        We show that this is a proper colouring.
    }%
\end{frame}

\end{document}
