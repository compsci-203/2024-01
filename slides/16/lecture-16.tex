\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Generating Function (2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Section 8.2: Example 8.5 and 8.6 only.
                        \item Section 8.3-8.4.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 8.7: 7, 9.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 8.2 Another look at distributing carrots}

\subsection{Example 8.5: Integer Composition with Restrictions}

\begin{frame}
    \frametitle{Example 8.5}

    A grocery store is preparing holiday fruit baskets for sale. 

    Each fruit basket will contain $n$ pieces of fruit, chosen from \emoji{apple}, \emoji{banana}, \emoji{cherries}, and \emoji{orange}.

    How many different ways can such a basket be prepared if
    \begin{itemize}
        \item the number of \emoji{apple} must be at least one,
        \item the number of \emoji{banana} must be at most three,
        \item and the number of \emoji{cherries} must be a multiple of four.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{\ac{gf} for \emoji{apple}}

    Let $a_{n}$ be the number of ways to put in $n$ \emoji{apple} in the basket so that we have at least one \emoji{apple}.  

    Then
    \begin{equation*}
        a_n
        =
        \begin{cases}
            \blankshort{} & n = 0 \\
            \blankshort{} & n \ge 1.
        \end{cases}
    \end{equation*}

    \cake{} What is the \ac{gf} for \emoji{apple}, i.e., $\sum_{n = 0}^{\infty} a_{n} x^{n}$?
\end{frame}


\begin{frame}
    \frametitle{\ac{gf} for \emoji{banana}}

    Let $b_{n}$ be the number of ways to put in $n$ \emoji{banana} in the basket so that
    we have at most three \emoji{banana}. 

    Then
    \begin{equation*}
        b_n
        =
        \begin{cases}
            \blankshort{} & 0 \le n \le 3 \\
            \blankshort{} & n > 3.
        \end{cases}
    \end{equation*}

    \cake{} What is the \ac{gf} for \emoji{banana}, i.e., $\sum_{n = 0}^{\infty} b_{n} x^{n}$?
\end{frame}

\begin{frame}
    \frametitle{\ac{gf} for \emoji{cherries}}

    Let $c_{n}$ be the number of ways to put in $n$ \emoji{cherries} in the basket so that
    we have a multiple of four \emoji{cherries}. 

    Then
    \begin{equation*}
        c_n
        =
        \begin{cases}
            \blankshort{} & \text{if $n = 4k$ for some $k \in \dsN$} \\
            \blankshort{} & \text{otherwise}
        \end{cases}
    \end{equation*}

    \cake{} What is the \ac{gf} for \emoji{cherries}, i.e., $\sum_{n = 0}^{\infty}
    c_{n} x^{n}$?
\end{frame}

\begin{frame}
    \frametitle{\ac{gf} for \emoji{orange}}

    Let $d_{n}$ be the number ways to put in $n$ \emoji{orange} in the basket.

    Then
    \begin{equation*}
        d_n
        =
        \blankshort{},
        \qquad
        \text{for all $n \ge 0$}
    \end{equation*}

    \cake{} What is the \ac{gf} for \emoji{orange}, i.e., $\sum_{n = 0}^{\infty}
    d_{n} x^{n}$?
\end{frame}

\begin{frame}
    \frametitle{The \ac{gf} for fruit baskets}

    Let $f_{n}$ be the number of ways to put $n$ fruits in the \emoji{wastebasket} 
    so \emph{all} conditions are satisfied.

    Show that \ac{gf} for $\{f_{n}\}$ is
    \begin{equation*}
        \sum_{n=0}^{\infty} f_{n} x^{n}
        =
        \sum_{n=0}^{\infty} \binom{n+1}{2} x^{n}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Why Does This Work?}
    To see why
    \begin{equation*}
        \sum_{n=0}^{\infty} f_{n} x^{n}
        =
        \left(\sum_{n=0}^{\infty} \textcolor{red}{a_{n}} x^{n}\right)
        \left(\sum_{n=0}^{\infty} \textcolor{blue}{b_{n}} x^{n}\right)
        \left(\sum_{n=0}^{\infty} \textcolor{orange}{c_{n}} x^{n}\right)
        \left(\sum_{n=0}^{\infty} \textcolor{teal}{d_{n}} x^{n}\right)
    \end{equation*}
    note that the \alert{rhs} equals
    \only<1>{%
        \begin{equation*}
            (\textcolor{red}{a_{0}} \textcolor{blue}{b_{0}} \textcolor{orange}{c_{0}} \textcolor{teal}{d_{0}}) x^{0}
            +
            \cdots
        \end{equation*}
        where \(\textcolor{red}{a_{0}} \textcolor{blue}{b_{0}} \textcolor{orange}{c_{0}} \textcolor{teal}{d_{0}}\) is the number of ways to put \(0\) of each type of fruits in the basket,
        i.e., $f_{0}$.
    }%
    \only<2>{%
        \begin{equation*}
            \cdots
            +
            (\textcolor{red}{a_{1}} \textcolor{blue}{b_{0}} \textcolor{orange}{c_{0}} \textcolor{teal}{d_{0}} + \textcolor{red}{a_{0}} \textcolor{blue}{b_{1}} \textcolor{orange}{c_{0}} \textcolor{teal}{d_{0}} + \cdots) x^{1}
            +
            \cdots
        \end{equation*}
        where 
        \begin{itemize}
            \item \(\textcolor{red}{a_{1}} \textcolor{blue}{b_{0}} \textcolor{orange}{c_{0}} \textcolor{teal}{d_{0}}\) is the number of ways to put only \(1\) \emoji{apple} and nothing else in the basket,
            \item \(\textcolor{red}{a_{0}} \textcolor{blue}{b_{1}} \textcolor{orange}{c_{0}} \textcolor{teal}{d_{0}}\) is the number of ways to put only \(1\) \emoji{banana} and nothing else in the basket,
            \item \dots
        \end{itemize}
        which add up to $f_{1}$.
    }%
    \only<3>{%
        \begin{equation*}
            \cdots
            +
            (\cdots +
            \textcolor{red}{a_{k_{1}}} \textcolor{blue}{b_{k_{2}}}
            \textcolor{orange}{c_{k_{3}}} \textcolor{teal}{d_{k_{4}}} 
            + \cdots) x^{n}
            +
            \cdots
        \end{equation*}
        where 
        \(
        \textcolor{red}{a_{k_{1}}} \textcolor{blue}{b_{k_{2}}}
        \textcolor{orange}{c_{k_{3}}} \textcolor{teal}{d_{k_{4}}} 
        \)
        is the number of ways to put \(k_{1}\) \emoji{apple}, \(k_{2}\) \emoji{banana}, \(k_{3}\) \emoji{cherries}, and \(k_{4}\) \emoji{orange} in the basket,
        so that we have in total $n = k_{1} + k_{2} + \cdots + k_{4}$ piece of fruit in the basket.
    }%
\end{frame}

\subsection{Partial Fractions}

\begin{frame}
    \frametitle{Example 8.6}

    \think{} How to find the number of integer solutions to the equation
    \begin{equation*}
        x_1 + x_2 + x_3 = n
    \end{equation*}
    with $x_1 \ge 0$ being even, $x_2 \ge 0$, and $2 \ge x_3 \ge 0$.

    \pause{}

    Let $f_n$ be the number of such solutions. 

    \think{}
    How to show that 
    \begin{equation*}
        \sum_{n=0}^{\infty} f_n x^{n}
        = \frac{1+x+x^2}{(1+x)(1-x)^2}
    \end{equation*}
    \only<2>{%
        \hint{} Think of $x_{1}, x_{2}, x_{3}$ as the number of \emoji{apple},
        \emoji{banana}, and \emoji{cherries} in a fruit basket.
    }%

    \pause{}

    With the help of
    \href{https://www.wolframalpha.com/input?i=SeriesCoefficient\%5B\%281\%2Bx\%2Bx\%5E2\%29\%2F\%28\%281-x\%29\%281-x\%5E2\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}, we get
    \begin{equation*}
        f_n = 
        \frac{1}{4} (6n + (-1)^n + 3), \qquad n \in \dsN.
    \end{equation*}
    \think{} How can we do this with only \emoji{pencil}?
\end{frame}

%\begin{frame}
%    \frametitle{\zany{} Partial fraction decomposition}
%    
%    A \alert{rational function} is any function of the form $\frac{f(x)}{g(x)}$
%    where $f(x)$ and $g(x)$ are polynomials.
%
%    We can \emph{always} express a rational function in the form
%    \begin{equation*}
%        \frac{f(x)}{g(x)} = p(x) + \sum_{j} \frac{f_j(x)}{g_j(x)}
%    \end{equation*}
%    where 
%    \begin{itemize}
%        \item $p(x)$ is a polynomial that may be zero, 
%        \item and $f_j(x)$ and $g_j(x)$ are polynomials with the degree of $f_j(x)$ less than the degree of $g_j(x)$, 
%        \item and each $g_j(x)$ is an \alert{irreducible polynomial} .
%    \end{itemize}
%
%    This is called \alert{partial fraction decomposition}.
%\end{frame}

\begin{frame}
    \frametitle{Partial fraction decomposition --- an example}

    A \href{https://en.wikipedia.org/wiki/Partial\_fraction\_decomposition\#Statement}{result in algebra} 
    tells us we have
    \begin{equation*}
        \frac{1+x+x^2}{(1+x)(1-x)^2}
        =
        \frac{A}{1+x}
        +
        \frac{B}{1-x}
        +
        \frac{C}{(1-x)^2}
        ,
    \end{equation*}
    where $A$, $B$, and $C$ are constants.

    This is called a \alert{partial fraction decomposition} of the \ac{lhs}.

    \only<1>{%
        \think{} How can we find $A, B, C$?
    }%

    \pause{}

    Solving the above equation, we get
    \begin{equation*}
        \sum_{n=0}^{\infty} f_n x^{n}
        = 
        \frac{1+x+x^2}{(1+x)(1-x)^2}
        =
        \frac{1}{4(1+x)}
        -
        \frac{3}{4(1-x)}
        +
        \frac{3}{2 (1-x)^2}
    \end{equation*}

    Partial fraction decomposition can also be done with
    \href{https://www.wolframalpha.com/input?i=Apart\%5B\%281\%2Bx\%2Bx\%5E2\%29\%2F\%28\%281\%2Bx\%29\%281-x\%29\%5E2\%29\%5D}{WolframAlpha}.
\end{frame}

\begin{frame}
    \frametitle{Example 8.6: The Solution}
    
    \think{}
    Given the partial fraction decomposition
    \begin{equation*}
        \sum_{n=0}^{\infty} f_n x^{n}
        =
        \frac{1}{4(1+x)}
        -
        \frac{3}{4(1-x)}
        +
        \frac{3}{2 (1-x)^2}
    \end{equation*}
    how to show that
    \begin{equation*}
        f_n = \frac{1}{4} (-1)^n - \frac{3}{4}  + \frac{3(n+1)}{2}
    \end{equation*}
    \hint{}
    Use Example 8.7:
    \begin{equation*}
        \frac{1}{(1-x)^{k+1}}
        =
        \sum_{n=0}^{\infty} \binom{n+k}{k} x^{n},
        \qquad
        \text{for all $k \in \dsN$}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}
    
    Consider the equality
    \begin{equation*}
        x_1 + x_2 + x_{3} = n
    \end{equation*}
    where $x_1, x_{2}, x_3, n \ge 0$ are all integers, 
    $x_1 \ge 2$,
    $x_2$ is a multiple of $5$, 
    and $0 \le x_3 \le 4$. 

    Let $d_n$ be the number of solutions.
    What is the \ac{gf} for $\{d_n\}$?

    Can you find a closed formula for $d_n$?

    Answer: See \href{https://bit.ly/494DGfE}{WolframAlpha}.
\end{frame}

\section{\acs{ac} 8.3 Newton's Binomial Theorem}

\begin{frame}[c]
    \frametitle{A Combinatorial Identity}
    
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            Consider the following identity
            \begin{equation*}
                2^{2n} = \sum_{k = 0}^{n} \binom{2k}{k} \binom{2n-2k}{n-k}
            \end{equation*}
            which holds for all $n \in \dsN$

            \weary{} A combinatorial proof is
            \href{https://math.stackexchange.com/q/37971/1618}{a bit hard}.

            \smiling{} But it's easy with \ac{gf}!
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-cut-cake.jpg}
                \caption{A piece of \cake{}!}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Definition 8.8}

    Recall that 
    for integers $m \ge n \ge 0$,
    we have defined
    \begin{equation*}
        P(m, n) 
        = 
        \frac{m!}{n!}
        =
        m (m-1) \cdots (m-n+1)
    \end{equation*}

    \only<1>{%
        By Proposition 2.6 (\ac{ac}),
        $P(m,n)$ is the number of strings of $n$ distinct characters chosen from
        an alphabet with $m$ characters.
    }%

    \pause{}

    We now extent this definition:
    for all $p \in \dsR$ and $k \in \dsN$, 
    let
    \begin{equation*}
        P(p, k) = 
        \begin{cases}
            1 & \text{if $k = 0$,} \\
            p P(p − 1, k − 1) & \text{if $k \ge 1$},
        \end{cases}
    \end{equation*}

    \only<2>{\cake{} What is $P(p, k)$ if $p \in \dsN$ and $p \ge k$?}
    \only<3>{\cake{} What is $P(p, k)$ if $p \in \dsN$ and $p < k$?}
    \only<4>{\cake{} What is $P(p, k)$ if $p \notin \dsZ$?}
\end{frame}

\begin{frame}
    \frametitle{Definition 8.9 -- Binomial Coefficients}
    Now let's extend the definition of binomial coefficients.

    For all $p \in \dsR$ and $k \in \dsN$,  let
    \[
        \binom{p}{k}
        =
        \frac{P(p,k)}{k!}
    \]

    \only<1>{%
        \cake{} What is $\binom{p}{k}$ if $p \in \dsN$ and $p \ge k$?
    }%
    \only<2>{%
        \cake{} What is $\binom{p}{k}$ if $p \in \dsN$ and $p < k$?
    }%
    \only<3>{%
        \cake{} What is $\binom{p}{k}$ if $p \notin \dsZ$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Newton's Binomial Theorem}
    \begin{block}{Theorem 2.30 (\acs{ac}) --- Binomial Theorem}
        For all \(p \in \dsN\) with \(p \ge 0\),
        \begin{equation*}
            (1+x)^{p} = \sum_{n = 0}^{p} \binom{p}{n} x^{n}
        \end{equation*}
        \vspace{-0.5em}
    \end{block}

    \pause{}

    \begin{block}{Theorem 8.10 (\acs{ac}) --- Newton's Binomial Theorem}
        For all \(p \in \mathbb R\) with \(p \ne 0\),
        \begin{equation*}
            (1+x)^{p} = \sum_{n = 0}^{\infty} \binom{p}{n} x^{n}
        \end{equation*}
        \vspace{-0.5em}
    \end{block}

    \emoji{eye} If you are interested about the proof,
    see \href{https://en.wikipedia.org/wiki/Binomial\_series}{Wikipedia}.
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Sir Isaac Newton}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \only<1>{%
                Sir Isaac Newton was an \emoji{uk} \emoji{teacher}, \emoji{zap},
                \emoji{telescope}, \emoji{alembic}, \emoji{latin-cross}.

                \bigskip{}

                He is credited with many things such as
                \begin{itemize}
                    \item laws of \emoji{train}
                    \item law of universal \emoji{earth-asia}
                    \item theory of \emoji{rainbow}
                \end{itemize}
            }%

            \only<2>{%
                Do you believe that Newton got hit by a falling \emoji{apple}
                on head \emoji{face-with-head-bandage} 
                so he begun to think about the theory of gravity?
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=0.8\textwidth]{Newton.jpg}
                \includegraphics<2>[width=\textwidth]{Newton-under-tree.jpg}
                \caption*{%
                    \only<1>{%
                        Sir Isaac Newton (1642--1726)
                    }%
                    \only<2>{%
                        Newton under an \emoji{apple} tree
                    }%
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\section{\acs{ac} 8.4 An Application of the Binomial Theorem}

\begin{frame}
    \frametitle{Lemma 8.12}

    For all integers \(k \ge 0\), 
    \vspace{-1em}
    \[
        \binom{-1/2}{k}=(-1)^{k} \frac{\binom{2 k}{k}}{2^{2 k}}. 
    \]
    \vspace{-1em}
\end{frame}

\begin{frame}[label=current]
    \frametitle{Theorem 8.13 --- A generating function}
    We have
    \[
        \frac{1}{\sqrt{1-4x}}
        =
        \sum_{n = 0}^{\infty} \binom{2n}{n} x^n
    \]

    \hint{} The proof applies Newton's binomial theorem to \ac{lhs}
\end{frame}

\begin{frame}
    \frametitle{Corollary 8.14 --- The identity which we are looking for}
    
    For all integers \(n \ge 0\)
    \begin{equation*}
        2^{2n} = \sum_{k \ge 0} \binom{2k}{k} \binom{2n-2k}{n-k}.
    \end{equation*}
\end{frame}

\end{document}
