\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Test of Convergence}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \ac{ec}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 9.2 until ratio test.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item
                            Exercise 9.2: 6-21.%
                            \footnote{%
                                May require knowledge from the next lecture.
                            }
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ec} 9.2 Test of Convergence}

\subsection{Sequences Differ in Finitely Many Places}

\begin{frame}
    \frametitle{Make a Guess}

    \cake{} 
    What is \begin{equation*}
        \lim_{{n \to \infty}} \frac{1}{n^2}
    \end{equation*}

    \only<2->{%
        Let
        \begin{equation*}
            a_n =
            \begin{cases}
                n^2 & n \leq 10^{\only<2>{10}\only<3>{10^{10^{10}}}}, \\
                \frac{1}{n^{2}} & \text{otherwise}.
            \end{cases}
        \end{equation*}
        \cake{}
        What is \begin{equation*}
            \lim_{{n \to \infty}} a_n
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{Impact of Finite Changes on Limits}
    
    Let \(\{a_n\}\) be a sequence with \( \lim_{{n \to \infty}} a_n = M \).
    
    If \( \{a_n'\} \) differs from \( \{a_n\} \) 
    by finitely many terms, then \( \lim_{{n \to \infty}} a_n' = M \).

    \pause{}
    
    In other words, if there exists a number \( N\ge 0 \) such that 
    \[
        a_n' = a_n, \quad \text{for all } n \geq N,
    \]
    then
    \begin{equation*}
        \lim_{{n \to \infty}} a_n' = \lim_{{n \to \infty}} a_n = M.
    \end{equation*}

    \puzzle{} Try to prove this.

    \hint{} When looking for the limit, we can ignore finitely many terms.
\end{frame}

%\begin{frame}
%    \frametitle{Example}
%    
%    Consider the sequence
%    \begin{equation*}
%        a_n =
%        \begin{cases}
%            e^n, & n \le 10^{9}, \\
%            2^{-n}, & n > 10^{9}.
%        \end{cases}
%    \end{equation*}
%
%    \cake{} What is $\lim_{n \to \infty} a_n$?
%\end{frame}

\subsection{Monotone Bounded Test}

\begin{frame}
    \frametitle{Example: Bounded Sequence}
    
    The sequence
    \begin{equation*}
        a_n = \frac{1}{(-1)^{n+1} n}, \quad \text{for all } n \ge 1.
    \end{equation*}
    is \alert{bounded above} by $1$ and \alert{bounded below}  by $-\frac{1}{2}$

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines = middle,
                xlabel = \( n \),
                ymax = 1.05,
                ymin = -0.55,
                xmin = 0,
                xmax = 15.05,
                width = 10cm,  % Adjust the width as needed
                height = 5cm,  % Adjust the height as needed
                ]
                % Original plot
                \addplot[ycomb, blue, thick, mark=*, domain=1:15, samples=15] {(-1)^(x+1)*(x^(-1))};

                \addplot[red, dashed, domain=0:15] {1};

                \addplot[green, dashed, domain=0:15] {-0.5};
            \end{axis}
        \end{tikzpicture}
        \caption{The sequence $\frac{1}{(-1)^{n+1} n}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Bounded Sequence}

    \only<1->{%
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{bounded from
        above} if there exists $M \in \mathbb{R}$ such that
        \begin{equation*}
            a_n \leq M, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }
    \only<2->{%
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{bounded from
        below} if there exists $N \in \mathbb{R}$ such that
        \begin{equation*}
            a_n \geq N, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }
    \only<3>{%
        A sequence $(a_n)_{n \in \mathbb{N}}$ is said to be \alert{bounded}
        if it is both bounded from above and from below.
    }
\end{frame}

\begin{frame}
    \frametitle{Exercise: Bounded Sequence}

    \cake{} 
    Are the following sequences bounded from above, from below, both, or neither?

    \begin{itemize}
        \item $\{2^{n} - n\}_{n \ge 1}$
        \item $\{-\log(n)\}_{n \ge 1}$
        \item $\{n 2^{-n}\}_{n \ge 1}$
        \item $\{(-2)^{n}\}_{n \ge 1}$
    \end{itemize}

    \puzzle{} Try to prove the answers.
\end{frame}

\begin{frame}
    \frametitle{Example: Monotone Decreasing Sequence}
    
    The sequence
    \begin{equation*}
        a_n = \frac{1}{n}, \quad \text{for all } n \ge 1,
    \end{equation*}
    is \alert{monotone decreasing}. 

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines = middle,
                xlabel = \( n \),
                ymax = 1.05,
                ymin = -0.05,
                xmin = 0,
                xmax = 15.05,
                width = 10cm,  % Adjust the width as needed
                height = 5cm,  % Adjust the height as needed
                ]
                % Original plot
                \addplot[ycomb, blue, thick, mark=*, domain=1:15, samples=15] {(x^(-1))};
            \end{axis}
        \end{tikzpicture}
        \caption{The sequence $\frac{1}{n}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Monotone Increasing Sequence}
    
    The sequence
    \begin{equation*}
        a_n = -\frac{1}{n}, \quad \text{for all } n \ge 1,
    \end{equation*}
    is \alert{monotone increasing}. 

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines = middle,
                xlabel = \( n \),
                ymax = 0.05,
                ymin = -1.05,
                xmin = 0,
                xmax = 15.05,
                width = 10cm,  % Adjust the width as needed
                height = 5cm,  % Adjust the height as needed
                ]
                % Original plot
                \addplot[ycomb, blue, thick, mark=*, domain=1:15, samples=15] {-(x^(-1))};
            \end{axis}
        \end{tikzpicture}
        \caption{The sequence $-\frac{1}{n}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Monotone Sequence}

    \only<1->{%
        A sequence $\{a_n\}_{n \in \mathbb{N}}$ is said to be \alert{monotone
        increasing} if
        \begin{equation*}
            a_n \leq a_{n+1}, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }
    \only<2->{%
        A sequence $\{a_n\}_{n \in \mathbb{N}}$ is said to be \alert{monotone
        decreasing} if
        \begin{equation*}
            a_n \geq a_{n+1}, \quad \forall n \in \mathbb{N}.
        \end{equation*}
    }
    \only<3>{%
        A sequence $\{a_n\}_{n \in \mathbb{N}}$ is said to be \alert{monotone}
        if it is either monotone increasing or monotone decreasing.
    }
\end{frame}

\begin{frame}
    \frametitle{Exercise: Monotone Sequence}
    
    \cake{} 
    Are the following sequences monotone increasing, decreasing, both, or neither?

    \begin{itemize}
        \item $\{2^{n} - n\}_{n \ge 100}$
        \item $\{-\log(n)\}_{n \ge 100}$
        \item $\{n 2^{-n}\}_{n \ge 100}$
        \item $\{(-2)^{n}\}_{n \ge 100}$
    \end{itemize}

    \puzzle{} Try to prove the answers.
\end{frame}

\begin{frame}
    \frametitle{Monotone Bounded Test}

    If a sequence $(a_n)_{n \in \mathbb{N}}$ is 
    \alt<2>{\emoji{up-arrow}}{\emoji{down-arrow}}
    (\emph{monotone \alt<2>{increasing}{decreasing}})
    and \emph{bounded from \alt<2>{above}{below}}, 
    then it is convergent.

    \begin{figure}[htpb]
        \begin{tikzpicture}
            \begin{axis}[
                axis lines=middle,
                xmin=0, 
                xmax=22,
                xlabel={$n$},
                width = 10cm,  % Adjust the width as needed
                height = 5cm,  % Adjust the height as needed
                ]
                \only<1>{\addplot[ycomb, blue, mark=*, samples=20, domain=1:20] {1/x}};
                \only<2>{\addplot[ycomb, blue, mark=*, samples=20, domain=1:20] {-1/x}};
                \addplot[thin, domain=1:20] {0};
            \end{axis}
        \end{tikzpicture}
        \caption*{Example: \(\frac{\alt<2>{-1}{1}}{n} \to 0\)}
    \end{figure}

    \cake{} Why such a sequence is also \emph{bounded from \alt<2>{below}{above}}?
\end{frame}

\begin{frame}
    \frametitle{Monotone Bounded Test (Compact Form)}

    Since 
    \begin{itemize}
        \item a monotone \emoji{up-arrow} sequence is bounded from below,
        \item and a monotone \emoji{down-arrow} sequence is bounded from above,
    \end{itemize}
    we can have the following more compact form of the test:

    If a sequence $(a_n)_{n \in \mathbb{N}}$ is \emph{monotone} and \emph{bounded}, then
    it is convergent.

    \zany{} \emoji{eye} See Theorem 2.1.10 \href{https://www.jirka.org/ra/realanal.pdf}{\acl{bai}} by Jiří Lebl.
\end{frame}

\begin{frame}
    \frametitle{Example 7.9}

    Show the sequence
    \begin{equation*}
        a_n =
        \frac{1 \cdot 3 \cdot 5 \dots (2n-1)}{2 \cdot 4 \cdot 6 \dots 2n},
        \qquad
        n \ge 1,
    \end{equation*}
    is convergent.

    \pause{}

    \begin{block}{\zany{} What is the limit?}
        Can you find (and prove) the limit of $\{a_n\}$?

        \hint{} Show that
        \begin{equation*}
            a_n = \frac{(2n)!}{n! n! 2^{2n}}
        \end{equation*}
        and use the following inequality
        \begin{equation*}
            \sqrt{2\pi n} \left( \frac{n}{e} \right)^n e^{\frac{1}{12n+1}} < n! < \sqrt{2\pi n} \left( \frac{n}{e} \right)^n e^{\frac{1}{12n}},
            \quad
            \text{for all }
            n \in \mathbb{N}.
        \end{equation*}
    \end{block}
\end{frame}

\subsection{Comparison Test}

\begin{frame}
    \frametitle{Comparison Test}
    
    \only<1>{%
        If \( a_n \to \infty \) and there exists $N$ such that
        \begin{equation*}
            a_n \leq b_n, \quad \forall n > N
        \end{equation*}
        then
        \( b_n \to \infty \).

        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines = middle,
                    xlabel = \( n \),
                    ymax = 5.5,
                    ymin = -0.55,
                    xmin = 0,
                    xmax = 16,
                    width = 10cm,  % Adjust the width as needed
                    height = 6cm,  % Adjust the height as needed
                    legend entries={$\sqrt{n}+1$, $\sqrt{n}$},
                    legend pos = north west,
                    ]
                    \addplot[ycomb, red, thick, mark=*, domain=0:15, samples=16] {sqrt(x)+1};
                    \addplot[ycomb, blue, thick, mark=*, domain=0:15, samples=16] {sqrt(x)};
                \end{axis}
            \end{tikzpicture}
            \caption{Example: $\sqrt{n}\to \infty$ implies $\sqrt{n}+1\to \infty$}
        \end{figure}
    }
    \only<2>{
        If \( b_n \to -\infty \) and there exists $N$ such that
        \begin{equation*}
            a_n \leq b_n, \quad \forall n > N
        \end{equation*}
        then
        \( a_n \to -\infty \).

        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines = middle,
                    xlabel = \( n \),
                    ymin = -5.5,
                    ymax = 0.55,
                    xmin = 0,
                    xmax = 16,
                    width = 10cm,  % Adjust the width as needed
                    height = 6cm,  % Adjust the height as needed
                    legend entries={$-\sqrt{n}-1$, $-\sqrt{n}$},
                    legend pos = south west,
                    ]
                    \addplot[ycomb, red, thick, mark=*, domain=0:15, samples=16] {-sqrt(x)-1};
                    \addplot[ycomb, blue, thick, mark=*, domain=0:15, samples=16] {-sqrt(x)};
                \end{axis}
            \end{tikzpicture}
            \caption{Example: $-\sqrt{n}\to -\infty$ implies $-\sqrt{n}-1\to \infty$}
        \end{figure}
    }
\end{frame}

\subsection{$n$-th Term Test}

\begin{frame}
    \frametitle{$n$-th Term Test}
    
    If $\sum a_n$ converges, then $\lim_{n \to \infty} a_n = 0$.

    \only<1>{\think{} How to prove this?}

    \pause{}

    The contrapositive of the above is that
    \begin{itemize}
        \item $\lim_{n \to \infty} a_n$ does not exist,
        \item or $\lim_{n \to \infty} a_n = L$ where $L \in \dsR$ and $L \neq 0$,
    \end{itemize}
    then $\sum a_n$ diverges.

    \pause{}

    \cake{} What does this test tell us about
    \begin{equation*}
        \sum_{n=1}^{\infty} \only<3>{10^{-10^{10000000000}}}\only<4->{\frac{1}{n}}
    \end{equation*}
    \only<5>{%
        \bomb{} $n$-th term test can only prove a series diverges.
    }
\end{frame}

\begin{frame}
    \frametitle{Example 9.8}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{n}{2n+1}
        =
        \frac{1}{3}
        +
        \frac{1}{5}
        +
        \frac{1}{7}
        +
        \cdots
    \end{equation*}
\end{frame}

\subsection{Ratio Test}

\begin{frame}
    \frametitle{Ratio Test}
    
    Let $\sum a_n$ be a series with positive terms. Let
    \begin{equation*}
        R = \lim_{n \to \infty} \frac{a_{n+1}}{a_n}
    \end{equation*}
    \pause{}
    Then
    \begin{itemize}[<+->]
        \item[\smiling{}] if $R > 1$, then $\sum_{n=0}^{\infty} a_n$ diverges;
        \item[\weary{}] if $R < 1$, then $\sum_{n=0}^{\infty} a_n$ converges;
        \item[\expressionless{}] if $R = 1$ or $R$ does not exist, then $\sum_{n=0}^{\infty} a_n$
            may or may not converge.
    \end{itemize}
    \pauseafteritem{}
    \cake{} What does this test tell us about
    \begin{equation*}
    \sum_{n=1}^{\infty} \only<+>{2^n}\only<+>{2^{-n}}\only<+>{\frac{1}{n}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.9}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{n}{2^{n}}
    \end{equation*}
\end{frame}

\end{document}
