\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Induction}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Section 3.4-3.5, excluding 3.5.1, 3.5.2.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 3.11: 5, 9, 11, 13, 15, 17, 19.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
            \href{https://discrete.openmathbooks.org/dmoi3/dmoi.html}{\acf{dm}}
            \begin{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 2.5: 11, 29.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 3.4 Binomial Coefficients Revisited}

\begin{frame}
    \frametitle{Example of A Recursive Formula}

    How to show that for 
    all integers $n, k$ with \(0 < k < n\)
    \begin{equation*}
        \binom{n}{k}=\binom{n-1}{k-1} + \binom{n-1}{k}
    \end{equation*}
    using a combinatorial argument?

    \hint{} Show that both sides counts choosing $k$ out of $n$ \bunny{}.
\end{frame}

\begin{frame}
    \frametitle{Pascal's Triangle}

    The recursion
    \begin{equation*}
        \binom{n}{k}=\binom{n-1}{k-1} + \binom{n-1}{k}
    \end{equation*}
    is often demonstrated through \href{https://en.wikipedia.org/wiki/Pascal\%27s\_triangle}{Pascal's
    triangle}:

    \begin{center}
        \begin{tabular}{ccccccccc}
            & & & & $\binom{0}{0}$ & & & & \\
            & & & $\binom{1}{0}$ & & $\binom{1}{1}$ & & & \\
            & & $\binom{2}{0}$ & & $\binom{2}{1}$ & & $\binom{2}{2}$ & & \\
            & $\binom{3}{0}$ & & $\binom{3}{1}$ & & $\binom{3}{2}$ & & $\binom{3}{3}$ & \\
            $\binom{4}{0}$ & & $\binom{4}{1}$ & & $\binom{4}{2}$ & & $\binom{4}{3}$ & & $\binom{4}{4}$ \\
        \end{tabular}
    \end{center}
    where $\binom{n}{0} = \binom{n}{n} = 1$ for all $n \in \dsN$.

    This is an example where we can use a recursive formula to compute what we want.
\end{frame}

\section{\acs{ac} 3.5 Solving Combinatorial Problems Recursively}

\subsection{A Game of \emoji{crown}}

\begin{frame}
    \frametitle{Example 3.3: A Game of \emoji{crown}}

    We draw some infinitely long lines as boundaries to divide an infinitely
    large continent into a number kingdoms such that
    \begin{itemize}
        \item \emph{No} point on the continent belongs to more than two lines,
        \item and \emph{no} two lines are parallel.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \hfill
        \begin{tikzpicture}[scale=0.4,
            draw=brown,
            every node/.style={scale=1.5}]
            \draw[fill=MintWhisper, draw=none] plot[smooth cycle, tension=.7] coordinates {(0,0) (7,0) (7,5) (0,5)};
            \draw[thick] (0, 0) -- (7, 5);
            \draw[thick] (7, 0) -- (0, 5);
            \draw[thick] (-0.9, 2.5) -- (7.9, 2.5);
        \end{tikzpicture}
        \hfill
        \begin{tikzpicture}[scale=0.4,
            draw=brown,
            every node/.style={scale=1.5}]
            \draw[fill=MintWhisper, draw=none] plot[smooth cycle, tension=.7] coordinates {(0,0) (7,0) (7,5) (0,5)};
            \draw[thick] (-0.9, 3.5) -- (7.9, 3.5);
            \draw[thick] (-0.9, 1.5) -- (7.9, 1.5);
        \end{tikzpicture}
        \hfill\null
        \caption{Prohibited lines positions}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Three Lines}
    
    \think{} With the above conditions, how many kingdoms do we get with $n$ straight lines?

    \begin{figure}[htpb]
        \centering
        \resizebox{7cm}{!}{\input{../tikz-snippet/kindom.tex}}
        \caption{Three lines divide the continent into seven kingdoms.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{An Experiment}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            Let $r_n$ be the number of kingdoms with $n$ lines.

            Then
            \begin{equation*}
                r_0 = 1
                \onslide<2->{%
                    , r_1 = 2
                }%
                \onslide<3->{%
                    , r_2 = 4
                }%
                \onslide<4->{%
                    , r_3 = 7
                }%
            \end{equation*}
            \onslide<4>{%
                \cake{} Can you guess a recursive formula for $r_n$?
                \begin{equation*}
                    r_n - r_{n-1} = \blankshort{}, \qquad (n \ge 1)
                \end{equation*}
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}[scale=0.6,
                    draw=brown,
                    every node/.style={scale=1}]
                    \draw[fill=MintWhisper, draw=none] plot[smooth cycle, tension=.7] coordinates {(0,0) (7,0) (7,5) (0,5)};
                    \draw<2->[red, thick] (0, 0) -- (7, 5);
                    \draw<3->[thick] (0, 0) -- (7, 5);
                    \draw<3->[red, thick] (7, 0) -- (0, 5);
                    \draw<4->[thick] (7, 0) -- (0, 5);
                    \draw<4->[red, thick] (-0.9, 3.5) -- (7.9, 3.5);
                    \node<1-> at (3.5, 3.1) {\emoji{mouse-face}};
                    \node<2-> at (3.5, 1) {\emoji{cow-face}};
                    \node<3-> at (0.4, 4) {\emoji{tiger-face}};
                    \node<4-> at (0.5, 2) {\emoji{dragon-face}};
                    \node<3-> at (6.5, 2) {\bunny{}};
                    \node<4-> at (6.5, 4) {\emoji{snake}};
                    \node<4-> at (3.5, 4.5) {\emoji{horse-face}};
                \end{tikzpicture}
                \caption{\only<1>{No line}\only<2>{1 line}\only<3>{2 lines}\only<4>{3 lines}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 3.3: The Proof}

    \think{} How can we show that our recursive formula is correct?
    \begin{equation*}
        r_n - r_{n-1} = \blankshort{}, \qquad \text{for all } n \ge 1
    \end{equation*}

    \only<2>{%
    \hint{} When we draw the $n$th line, how many intersections does it have with other
    lines?
    }%

    \only<1>{%
        \begin{figure}
            \centering
            \begin{tikzpicture}[scale=0.7,
                draw=brown,
                every node/.style={scale=1}]
                \draw[fill=MintWhisper, draw=none] plot[smooth cycle, tension=.7] coordinates {(0,0) (7,0) (7,5) (0,5)};
                \draw[thick] (0, 0) -- (7, 5);
                \draw[thick] (7, 0) -- (0, 5);
                \draw[thick] (-0.9, 3.5) -- (7.9, 3.5);
                \node[label=left:{Line 1}] at (0,0) {};
                \node[label=left:{Line 2}] at (0,5) {};
                \node[label=left:{Line 3}] at (-0.9,3.5) {};
            \end{tikzpicture}
            \caption{What happens when we draw the $4$th line?}
        \end{figure}
    }%
\end{frame}

\subsection{Tiling a Grid}

\begin{frame}[t]
    \frametitle{Example 3.5: Tiling a Grid}

    Let $t_n$ be the number of ways to tile a $2 \times n$ grid by 
    two types of tiles 
    \begin{itemize}
        \item $1 \times 1$ squares \tikz[scale=0.3]{\drawtile{1}{1}}
        \item L-shaped trominoes \tikz[scale=0.3]{\drawLtile{}} (rotation allowed)
    \end{itemize}

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=1, transform shape]
                \drawchessboard{2}{5};
            \end{tikzpicture}
        \end{center}
        \caption{%
            Try to tile the above $2 \times 5$ grid with
            \tikz[scale=0.3]{\drawtile{1}{1}} and \tikz[scale=0.3]{\drawLtile{}}
        }%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{A Recursive Formula}
    Can we find a \emph{recursive} formula for $t_n$?
    \begin{equation*}
        t_{n} = 
        \blankveryshort{} t_{n-1}
        +
        \blankveryshort{} t_{n-2}
        +
        \blankveryshort{} t_{n-3}
        \quad (n \ge 3)
    \end{equation*}

    \hint{} Consider the right end of the board is tiled.
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=0.6, transform shape]
                \drawchessboard{2}{5};
                \begin{scope}[shift={(4,0)}]
                    \drawtile{1}{1};
                \end{scope}
                \begin{scope}[shift={(4,1)}]
                    \drawtilemono{1}{1}{Salmon};
                \end{scope}
            \end{tikzpicture}
            \hfill
            \begin{tikzpicture}[scale=0.6, transform shape]
                \drawchessboard{2}{5};
                \begin{scope}[shift={(3,0)}]
                    \drawLtile{};
                \end{scope}
                \begin{scope}[shift={(4,1)}]
                    \drawtile{1}{1};
                \end{scope}
            \end{tikzpicture}
            \hfill
            \begin{tikzpicture}[scale=0.6, transform shape]
                \drawchessboard{2}{5};
                \begin{scope}[shift={(2,0)}]
                    \drawLtile{};
                \end{scope}
                \begin{scope}[shift={(3,0)}, rotate around={-180:(2,2)}]
                    \drawLtile{};
                \end{scope}
            \end{tikzpicture}
        \end{center}
        \caption{%
            In how many ways can we finish in each case?
        }%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{A Second Example of Tiling a Grid}

    Let $t_n$ be the number of ways to tile a $2 \times n$ grid
    using only dominoes \tikz[scale=0.3]{\drawtilemono{1}{2}{MintWhisper}} (rotation allowed).

    Can we find a \emph{recursive} formula?
    \begin{equation*}
        t_{n} = 
        \blankveryshort{} t_{n-1}
        +
        \blankveryshort{} t_{n-2}
        \quad (n \ge 2)
    \end{equation*}

    \hint{} Consider the right end of the board is tiled.
    \begin{figure}[htpb]
        \begin{center}
            \hfill
            \begin{tikzpicture}[scale=0.6, transform shape]
                \drawchessboard{2}{5};
                \begin{scope}[shift={(4,0)}]
                    \drawtilemono{1}{2}{SeaGreen};
                \end{scope}
                \begin{scope}[shift={(4,1)}]
                    \drawtilemono{1}{2}{MintWhisper};
                \end{scope}
            \end{tikzpicture}
            \hfill
            \begin{tikzpicture}[scale=0.6, transform shape]
                \drawchessboard{2}{5};
                \begin{scope}[shift={(6,0)}, rotate around={90:(1,1)}]
                    \drawtilemono{1}{2}{MintWhisper};
                \end{scope}
            \end{tikzpicture}
            \hfill
        \end{center}
        \caption{%
            In how many ways can we finish in each case?
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $f_n$ be the number of ways to tile a $1 \times n$ grid
    using only dominoes 
    \tikz[scale=0.3]{\drawtilemono{1}{2}{MintWhisper}} 
    and squares 
    \tikz[scale=0.3]{\drawtile{1}{1}}.

    \cake{} Can you find a recursive formula for $f_n$?
    \begin{equation*}
        f_{n+2} = \blankveryshort{} f_{n+1} + \blankveryshort{} f_{n} \qquad \text{for all } n \ge 1
    \end{equation*}

    \hint{} Consider the right end of the board is tiled.
    \begin{figure}[htpb]
        \centering
        \hfill
        \begin{tikzpicture}[scale=0.6]
            \drawchessboard{1}{5}; % Draws a 1x8 chessboard
            \begin{scope}[shift={(4,0)}]
                \drawtile{1}{1};
            \end{scope}
        \end{tikzpicture}
        \hfill
        \begin{tikzpicture}[scale=0.6]
            \drawchessboard{1}{5}; % Draws a 1x8 chessboard
            \begin{scope}[shift={(3,0)}]
                \drawtilemono{1}{2}{MintWhisper};
            \end{scope}
        \end{tikzpicture}
        \hfill\null
        \caption{%
            In how many ways can we finish in each case?
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{What is $f_0$?}
    
    Given a recursive equation like
    \begin{equation*}
        f_{n+2} = f_{n+1} + f_{n} \qquad \text{for all } n \ge 1
    \end{equation*}
    and initial values $f_1 = 1, f_2 = 2$,
    we often say that $f_0 = 1$ so that the recursive equation becomes
    \begin{equation*}
        f_{n+2} = f_{n+1} + f_{n} \qquad \text{for all } n \ge \alert{0}
    \end{equation*}
    So we have
    \begin{equation*}
        \{f_{n}\}_{n \ge 0}
        =
        1, 1, 2, 3, 5, 8, \ldots
    \end{equation*}

    \pause{}

    \hint{}
    From a combinatorial perspective, this also make sense ---
    There is only one way to tile a $1 \times 0$ grid: doing nothing at all.
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise: Another Recursive Formula}
    
    Can you find a combinatorial proof that
    \begin{equation*}
        f_{2n} = f_{n}^2 + f_{n-1}^{2} \qquad \text{for all } n \ge 0
    \end{equation*}

    \hint{} Consider the following two cases.
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}
                \drawchessboard{1}{8} % Draws a 1x8 chessboard
                \begin{scope}[xshift=3cm]
                    \drawtilemono{1}{2}{MintWhisper};
                \end{scope}
            \end{tikzpicture}
        \end{center}
        \begin{center}
            \begin{tikzpicture}
                \drawchessboard{1}{8} % Draws a 1x8 chessboard
                \begin{scope}[xshift=3cm]
                    \drawtile{1}{1};
                \end{scope}
                \begin{scope}[xshift=4cm]
                    \drawtilemono{1}{1}{Salmon};
                \end{scope}
            \end{tikzpicture}
        \end{center}
        \caption{%
            In how many ways can we finish in each case?
        }%
    \end{figure}
\end{frame}

\subsection{Ternary Strings}

\begin{frame}[t]
    \frametitle{Example 3.6: Ternary String}

    \emoji{fearful} In some cultures,
    eating a \emoji{cheese} after a \emoji{apple} brings bad luck.

    Let use use a ternary string of alphabet
    \(\{\temoji{apple},\temoji{banana},\temoji{cheese}\}\) 
    encode what you eat in a meal.

    \pause{}

    Such a string is \alert{good} if there's no \emoji{cheese} followed by
    \emoji{apple}.  For example,
    \begin{itemize}
        \item \emoji{apple} \emoji{apple} \emoji{banana} \emoji{cheese}
            \emoji{banana} \emoji{banana} \emoji{apple} is \emph{good};
        \item \alert{\emoji{cheese} \emoji{apple}} \emoji{banana} \emoji{banana}
            \emoji{apple} \emoji{apple} \emoji{cheese} \emoji{banana} \emoji{cheese}
            \emoji{cheese} is \emph{bad}.
    \end{itemize}
\end{frame}

\begin{frame}[t]{Example 3.6: The Recursive Formula}
    Let $g(n)$ be the number of good strings of length \(n\).

    Can we find a recursive formula for $g(n)$?
    \begin{equation*}
        g(n) = \blankveryshort{} g(n-1) + \blankveryshort{} g(n-2)
    \end{equation*}

    \hint{}
    How many good string of length \(n\) are there in each case?
    \begin{itemize}[<+->]
        \item[\smiling{}] The string ends with \emoji{apple} 
        \item[\smiling{}] The string ends \emoji{banana} 
        \item[\emoji{fearful}] The string ends with \emoji{cheese}
    \end{itemize}
\end{frame}

\section{\acs{ac} 3.8 Proof by Induction}

\subsection{What Is Induction}

\begin{frame}{How Dominoes Fall}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{dominoes-fall.jpg}
        \caption{%
            \emoji{eye} Watch 40 seconds of
            \href{https://youtu.be/NglwqMEbl0A?feature=shared\&t=12}{this YouTube video}
        }%
    \end{figure}
\end{frame}

\begin{frame}{Proof by Induction}
    Let $S_{n}$ be a statement.

    To prove $S_{n}$ is true for all $n \ge n_0$, 
    we can use
    \alert{induction} ---

    \begin{itemize}[<+->]
        \item Base case (\emoji{student}\emoji{point-right}\emoji{mahjong}): Show \(S_{n_0}\) is true.
        \item Induction step (\emoji{mahjong}\emoji{point-right}\emoji{mahjong}): 
            \begin{enumerate}
                \item Assume that the statement $S_k$ is true for some integer
                    \(k \ge n_0\).
                \item Show that $S_{k+1}$ is true.
            \end{enumerate}
        \item Conclude that \(S_{n}\) is true for
            all \(n \ge n_0\). \emoji{tada}
    \end{itemize}

    \pauseafteritem{}

    \astonished{} Induction is in fact an axiom about natural numbers.
    In other words, we simply agree that it works because it feels so natural.
\end{frame}

\subsection{Example Proofs by Induction}

\begin{frame}[t]
    \frametitle{Proposition 3.1.3}
    Prove by induction that
    \begin{equation*}
        \sum_{i=1}^{n} (2 i-1) = n^{2}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Proposition 3.14}
    
    Let $n$ and $k$ be integers with $0 \le k \le n$.
    Prove by induction that
    \begin{equation*}
        \binom{n+1}{k+1} = \sum_{i=k}^{n} \binom{i}{k}
    \end{equation*}
    \hint{} This is \ac{ac} Example~2.18.
\end{frame}

\subsection{More Interesting Example Proofs by Induction}

\begin{frame}[t]
    \frametitle{\ac{dm} Section~2.5: Stamps}

    \emoji{cry} Imagine that we are in a world where the internet has collapsed,
    and we need to send \emoji{envelope} again.
    
    Show that we can make $n$ cents of postage using just $8$-cent
    and 5-cent stamps for $n \ge 28$.

    \only<1>{%
        \cake{} Let's try it out.
        \begin{equation*}
            \begin{aligned}
                28 
                & = \blankveryshort{} \temoji{five} + \blankveryshort{} \temoji{eight}
                \\
                29 
                & = 
                \blankveryshort{} \temoji{five} + \blankveryshort{} \temoji{eight}
                \\
                30
                & = 
                \blankveryshort{} \temoji{five} + \blankveryshort{} \temoji{eight}
                \\
                31
                & = 
                \blankveryshort{} \temoji{five} + \blankveryshort{} \temoji{eight}
            \end{aligned}
        \end{equation*}
    }%
    \only<2>{%
        \cake{} Is the statement still true if we change $28$ to $27$?
        \begin{equation*}
            27 = \blankveryshort{} \temoji{five} + \blankveryshort{} \temoji{eight}
            \txtq{}
            \txtq{}
            \txtq{}
            \txtq{}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{The Precise Statement}

    The stamp is equivalent to the following statement:

    For all integers $n \ge 28$, there exist $a, b \in \dsN$ such that
    \begin{equation*}
        n = 5 a + 8 b.
    \end{equation*}
    \think{} How to prove this by induction?
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise: A Grid With a Missing Cell}

    We prove by induction that a grid of size $2^n \times 2^n$ with one tile missing can
    be tiled with L-shaped trominoes \tikz[scale=0.3]{\drawLtile{}}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{chess-L-tiling.png}
        \caption{An example of tiling a chessboard with a missing tile}
    \end{figure}

    \emoji{eye} See \href{https://matheducators.stackexchange.com/a/1255/19945}{here} for a proof.
\end{frame}

\subsection{Strong Induction}

\begin{frame}{Strong Induction}
    Let $S_{n}$ be a statement.  

    To prove $\forall n \ge n_0 \, S_{n}$, 
    we can use \alert{strong induction}:

    \begin{itemize}[<+->]
        \item Base case (\emoji{student}\emoji{point-right}\emoji{mahjong}): Show \(S_{n_0}\) is true.
        \item Induction step (\emoji{mahjong}\emoji{mahjong}\emoji{mahjong}\dots\emoji{mahjong}\emoji{point-right}\emoji{mahjong}): 
            \begin{enumerate}
                \item Assume that the statement $S_n$ is true \emph{for all $n$
                        with \(n_0 \le n \le k\)} for some integer $k$.
                \item Show that $S_{k+1}$ is true.
            \end{enumerate}
        \item Conclude that \(S_{n}\) is true for all \(n \ge n_0\).
    \end{itemize}

    \pauseafteritem{}

    \hint{} Some times it is more convenient to try to prove something stronger!
\end{frame}

\begin{frame}[t]
    \frametitle{Example of strong induction}
    
    Let $f(n) = 2 f(n-1) - f(n-2)$ with $f(1) = 3$ and $f(2) = 5$.
    Prove by strong induction that
    \begin{equation*}
        f(n+1) = 2n+1, \quad \forall n \in \dsN.
    \end{equation*}
\end{frame}

\end{document}
