#!/bin/bash

for m in {1..4}
do
  for n in {1..4}
  do
    lualatex -jobname="k${m}-${n}" "\def\mym{$m} \def\myn{$n} \input{bipartite-graph.tex}"
  done
done
