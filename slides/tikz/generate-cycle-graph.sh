#!/bin/bash

for n in {1..12}
do
  lualatex -jobname="c$n" "\def\myn{$n} \input{cycle-graph.tex}"
done
