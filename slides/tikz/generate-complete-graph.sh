#!/bin/bash

for n in {1..12}
do
  lualatex -jobname="k$n" "\def\myn{$n} \input{complete-graph.tex}"
done
