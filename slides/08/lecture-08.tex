\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Power Series and Taylor Series}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \ac{ec}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 9.4 except the part of Bessel Functions.
                        \item
                            Section 9.5.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 9.4: 1, 3, 5, 7, 10.
                        \item Exercise 9.5: 1, 3, 5, 7, 9, 11, 13, 15, 18, 19, 21.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ec} 9.4 Power Series}

\begin{frame}
    \frametitle{Power Series}

    In section \ac{ec} 9.1, we have seen that the following geometric progression converges
    \begin{equation*}
        \sum_{n=0}^{\infty} (x-0)^{n}
        =
        \sum_{n=0}^{\infty} x^{n}
        =
        1 + x + x^{2} + x^{3} + \cdots
        =
        \frac{1}{1-x}
    \end{equation*}
    when $-1 < x < 1$.

    \pause{}

    This is an example of a \alert{power series} --- an infinite series of the form
    \begin{equation*}
        \sum a_n (x-c)^n
    \end{equation*}
    where  $x$ is a variable and $c$ is a constant.
\end{frame}

\subsection{Interval of Convergence}

\begin{frame}
    \frametitle{Interval of Convergence}

    In general, the set of $x$ for which a $\sum a_n(x-c)^{n}$
    converges is an interval, which we call the \alert{interval of convergence}.

    The \alert{radius of convergence} is half of the length
    of this interval.

    \pause{}

    In other words, the set of $x$ for which the power series
    converges must be one of the following:
    \begin{itemize}
        \item $\dsR$
        \item $(c-R, c+R)$, $[c-R, c+R]$, $(c-R, c+R]$ or $[c-R, c+R)$
        \item $\{c\}$, i.e., when $R = 0$
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{How to Find Interval of Convergence}

    One property of power series is that if
    \begin{equation*}
        \sum_{n=0}^{\infty} a_n (x-c)^{n}
    \end{equation*}
    converges at $x_0$,
    then
    \begin{equation*}
        \sum_{n=0}^{\infty} \abs{a_n (x-x_0)^{n}}
    \end{equation*}
    converges for all $x$ with $\abs{x-c} < \abs{x_0-c}$.

    \emoji{eye} See Theorem 6.5.1 in 
    \href{https://csunibo.github.io/analisi-matematica/libri/real-analysis.pdf}{Understanding
    Analysis}.

    In other words, $\sum a_n(x-c)^{n}$ and $\sum \abs{a_n(x-c)^{n}}$
    have \emph{the same radius of convergence}.

    Thus we can apply ratio test on the latter to find their radius of convergence.
\end{frame}

%\begin{frame}
%    \frametitle{Power Series and Generating Functions}
%
%    When we consider a power series
%    \[
%        \sum_{n=0}^{\infty} a_{n} x^{n},
%    \]
%    we view it as a function of \(x\), 
%    which has an associated interval of convergence.
%
%    \pause{}
%
%    However, when we view the same series as a generating function
%    \[
%        \sum_{n=0}^{\infty} a_{n} x^{n},
%    \]
%    it is treated as a formal sum to encode the sequence \( \{ a_{n} \} \), with no concern for convergence.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Determining the Interval of Convergence}
%
%    Consider a power series $\sum_{n=1}^{\infty} f_n(x)$. Define:
%    $$r(x) = \lim_{n \to \infty} \left| \frac{f_{n+1}(x)}{f_n(x)} \right|.$$
%
%    Apply the Ratio Test:
%    \begin{itemize}
%        \item $r(x) < 1$: Series converges.
%        \item $r(x) > 1$: Series diverges.
%        \item $r(x) = 1$: Test is inconclusive.
%    \end{itemize}
%
%    Identify the values of $x$ for which the series converges to determine its interval of convergence.
%\end{frame}

\begin{frame}
    \frametitle{Example 9.16}

    Find the interval of convergence of $\sum_{n=1}^{\infty} \frac{x^n}{n!}$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.17}

    Find the interval of convergence of $\sum_{n=1}^{\infty} \frac{x^n}{n}$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.18}

    Find the interval of convergence of $\sum_{n=1}^{\infty} n! x^n$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Find the interval of convergence of
    \[
        \sum_{n=0}^{\infty} \frac{(-1)^n}{(2n+1)!} x^{2n+1} = x - \frac{x^3}{3!} + \frac{x^5}{5!} - \frac{x^7}{7!} + \cdots
    \]
\end{frame}

\subsection{Differentiation and Integration}

\begin{frame}
    \frametitle{Differentiation and Integration}

    Let
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} a_n (x-c)^{n}.
    \end{equation*}
    If the power series converges for $x$ with $\abs{x-c} < R$,
    then
    \only<1>{%
        \begin{equation*}
            f'(x)
            =
            \sum_{n=0}^{\infty} n a_n (x-c)^{n-1}
        \end{equation*}
    }%
    \only<2>{%
        \begin{equation*}
            \int f(x) \dd{x}
            =
            C
            +
            \sum_{n=0}^{\infty} \frac{a_n}{n+1} (x-c)^{n+1}
        \end{equation*}
    }%
    which converge for $x$ with $\abs{x-c} < R$.

    \hint{} A power series can be \only<1>{differentiated}\only<2>{integrated} term by term.

    \bomb{} The convergence of \alt<2>{$\int f(x) \dd{x}$}{$f'(x)$} at $c-R$ and $c+R$
    needs to be checked.
\end{frame}

\begin{frame}
    \frametitle{Example 9.19}

    Let $f(x) = \sum_{n=0}^{\infty} x^{n}$.

    Find the interval of convergence of $f'(x)$.

    \pause{}

    Can $f(x)'$ be written in a non-series form?
\end{frame}

%\begin{frame}
%    \frametitle{Bessel Functions}
%
%    Many applications in engineering and physics requires solving differential equations
%    of the form
%    \[
%        x^2 y'' + x y' + (x^2 - m^2) y = 0.
%    \]
%    
%    The solution called Bessel Functions of order \(m\), denoted by \(J_m(x)\),
%    can also be expressed as a power series:
%    \[
%        J_m(x) = \sum_{n=0}^{\infty} \frac{(-1)^n}{n! (n + m)!} \left( \frac{x}{2} \right)^{2n + m}.
%    \]
%
%    \cake{} What is the interval of convergence for \(J_{m}(x)\)?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Bessel Functions in Picture}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{bessel-functions.pdf}
%        \caption*{Bessel Functions}
%    \end{figure}
%\end{frame}

\section{\acs{ec} 9.5 Taylor Series}

\subsection{Taylor's Formula}

\begin{frame}
    \frametitle{An Experiment}
    
    Consider the function
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} a_n (x-c)^{n}
    \end{equation*}
    \cake{} What are $f(c), f'(c), f''(c), f'''(c)$?
\end{frame}

\begin{frame}
    \frametitle{Taylor's Formula}
    
    Given a function
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} a_n (x-c)^{n}
    \end{equation*}
    we have
    \begin{equation*}
        a_{n} = \frac{f^{(n)}(c)}{n!},
        \quad \text{for} \quad n \ge 0.
    \end{equation*}

    \pause{}

    In other words,
    \begin{equation*}
        f(x) = \sum_{n=0}^{\infty} \frac{f^{(n)}(c)}{n!} (x-c)^{n}.
    \end{equation*}

    This is known as \emph{Taylor series} for $f(x)$ \emph{about} $x=c$.

    \bomb{} The Taylor series for the same function is different
    for different $c$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.20}
    
    Find the Taylor series for $f(x) = e^x$ about $x=0$.
\end{frame}

\begin{frame}
    \frametitle{How Do You Define It?}
    
    The exponential function $e^{x} = \exp(x)$ can be defined by
    \begin{equation*}
        e^{x} = \sum_{n=0}^{\infty} \frac{1}{n!} x^n
    \end{equation*}
    \pause{}%
    or
    \begin{equation*}
        e^{x} = \lim_{n \to \infty} \left(1 + \frac{x}{n}\right)^{n}
    \end{equation*}
    \pause{}%
    or the solution of the differential equation
    \begin{equation*}
        y(x)' = y(x)
    \end{equation*}
    \cake{} Which one are we using in Example 9.20?
\end{frame}

\begin{frame}
    \frametitle{Example 9.22}
    
    Find the Taylor series for $f(x) = \sin(x)$ about $x=0$.

    \only<1>{%
        \cake{} If we use the power series definition of sine functions:
        \begin{equation*}
            \sin(x) = \sum_{n=0}^{\infty} \frac{(-1)^n}{(2n+1)!} x^{2n+1}
        \end{equation*}
        what is the answer?
    }%
    \only<2>{%
        \hint{} Normally we use the definition
        from high school:
        \begin{equation*}
            \sin(\beta) = \frac{\text{opposite}}{\text{hypotenuse}}
        \end{equation*}
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[scale=0.75, thick]
                % Draw the right triangle
                \coordinate (A) at (0,0);
                \coordinate (B) at (3,0);
                \coordinate (C) at (0,4);
                \draw[fill=MintWhisper] (A) -- (B) -- (C) -- cycle;
                \draw[JuliaOrange] (A) -- (B) node[midway, below] {$b$ (adjacent)};
                \draw[SeaGreen] (A) -- (C) node[midway, left] {$a$ (opposite)};
                \draw[BrightRed] (B) -- (C) node[midway, above right] {$h$ (hypotenuse)};

                % Right angle symbol
                \draw[fill=SkyBlue] (A) rectangle +(0.4,0.4);

                % Points
                \fill (A) circle (3pt);
                \fill (B) circle (3pt);
                \fill (C) circle (3pt);

                % Labels
                \node at (A) [below left] {A};
                \node at (B) [below right] {B};
                \node at (C) [above] {C};

                % Draw the angle at B
                \pic [draw, fill=SkyBlue, -, angle eccentricity=1.5, angle radius=0.6cm] 
                {angle = C--B--A};

            % Label for angle
            \node at ($(B)-(1.0,-0.5)$) {$\beta$};
        \end{tikzpicture}
        \caption{The definition of the sine function.}
    \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 9.25}
    
    Find the Taylor series for $f(x) = \exp(x^{2})$ about $x=0$.

    \weary{} It is a little bit complicated to compute directly
    \begin{equation*}
        f'(x), f''(x), f'''(x), \ldots
    \end{equation*}
    \pause{}%
    \think{} Is there an easier way?
\end{frame}

\subsection{Approximate a Function}

%\begin{frame}
%    \frametitle{Use Taylor series to Approximate a Function}
%
%    Given a function $f(x)$, we can approximate $f(x)$ by its Taylor series:
%    \begin{equation*}
%        f(x) \approx \sum_{n=0}^{M} \frac{f^{(n)}(c)}{n!} (x-c)^{n}
%    \end{equation*}
%    where $M$ is a fixed integer.
%
%    Increasing $M$ increases the accuracy of the approximation.
%
%    \pause{}
%
%    For example:
%    \begin{equation*}
%        e^{x} 
%        \approx 1 + x 
%        \approx 1 + x + \frac{x^2}{2!}
%        \approx 1 + x + \frac{x^2}{2!} + \frac{x^{3}}{3!}
%        \approx \cdots
%    \end{equation*}
%
%    \emoji{wink} If all \emoji{computer} vanishes overnight, we can still compute $e^x$.
%\end{frame}

\begin{frame}
    \frametitle{Review: Big-$O$ Notation}

    The formula
    \begin{equation*}
        f(x) = O(g(x)) \qquad \text{for all } x \in A
    \end{equation*}
    means there exists a constant $C$ such that
    \begin{equation*}
        \abs{f(x)} \le C \abs{g(x)} \qquad \text{for all } x \in A
    \end{equation*}

    \only<1>{%
        For example, we have
        \begin{equation*}
            x^{2} - x = O(x^2) \qquad \text{for all } x \ge 1.
        \end{equation*}

        \hint{} The big-$O$ notation captures the \emph{rate of growth} of the function
        $f(x)$.
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines = middle,
                    xlabel = \( n \),
                    xmin = 0,
                    xmax = 3.5,
                    width = 10cm,  % Adjust the width as needed
                    height = 5.5cm,  % Adjust the height as needed
                    legend entries={$x^2$, $x^2-x$, $x^{-2}$},
                    legend pos = north west,
                    domain=1:3,
                    samples=30,
                    ]
                    \addplot[thick, draw=blue] {x^2};
                    \addplot[thick, draw=red] {x^2 - x};
                    \addplot[thick, draw=orange] {-x^2};
                \end{axis}
            \end{tikzpicture}
            \caption{Example: $x^{2} - x = O(x^2)$}
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{$O(x^n)$ approximation}
    
    We define \alert{the $n$-th degree Taylor polynomial} $P_n(x)$ of a function $f(x)$
    about $x = c$ by
    \begin{equation*}
        P_n(x) = \sum_{k=0}^{n} \frac{f^{(k)}(c)}{k!} (x-c)^{k}.
    \end{equation*}
    \pause{}

    As $P_n(x) = O(x^n)$, it is called the \alert{$O(x^n)$ approximation to $f(x)$}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{sin-approximation.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Remainder Theorem}
    
    Let $P_n(x)$ be the $n$-th degree Taylor polynomial of $f(x)$.

    Let
    \begin{equation*}
        R_{n}(x) = f(x) - P_n(x).
    \end{equation*}
    Then
    \begin{equation*}
        R_{n}(x) = \frac{f^{(n+1)}(c + \theta (c-x))}{(n+1)!} (x-c)^{n+1}.
    \end{equation*}
    for some $\theta \in [0, 1]$.

    \hint{} The exact value of $\theta$ is not important. 
    We usually just want an upper bound of $\abs{R_{n}(x)}$.
\end{frame}

\begin{frame}
    \frametitle{Example: Remainder Theorem}
    How well does
    \begin{equation*}
        P_5(x) = x - \frac{x^{3}}{3!} + \frac{x^{5}}{5!}
    \end{equation*}
    approximate $\sin(x)$ on $[-2, 2]$?

    \pause{}

    By the Remainder Theorem
    \begin{equation*}
        \abs{\sin(x) - P_5(x)} \le \abs{\frac{\cos(\theta x)}{6!}x^{4}}
    \end{equation*}
    where $\theta \in [0, 1]$.

    How large can this be for $x \in [-2, 2]$?
\end{frame}

\end{document}
