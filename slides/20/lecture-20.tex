\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%------------------------------------------------------------------------------
% Loading Optional Packages
%------------------------------------------------------------------------------
\input{../table.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- A Game!}

\begin{document}

\maketitle

\begin{frame}[standout]
    
    Who is going to be the winner?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{bunny-game-winner.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Choose Your Challenge}
    \tableofcontents{}
\end{frame}

\section{Strings, Sets and Binomial Coefficients}

\begin{frame}{\coin{} A spider with shoes and socks}
    A spider needs a sock and a shoe for each of its eight legs. In how many ways can it put on the shoes and socks, if socks must be put on before the shoe?

    \hint{} You can answer this with binomial coefficients.

    \begin{center}
        \resizebox{0.2\linewidth}{!}{\emoji{spider}}
    \end{center}

    \pause{}

    Answer: Label the legs $\{a,b,\dots,g,h\}$. 
    A string of exactly $2$ of each letter gives an order. 
    So the answer is
    \begin{equation*}
        \binom{16}{2,2,2,2,2,2,2,2}
        =
        \binom{16}{2}
        \binom{14}{2}
        \binom{12}{2}
        \binom{10}{2}
        \binom{8}{2}
        \binom{6}{2}
        \binom{4}{2}
        \binom{2}{2}
    \end{equation*}
\end{frame}

\begin{frame}{\coin{} Peaceful rooks}
    How many possibilities are there to put 8 identical rooks on a chessboard so they do not
    attack each other (i.e., no two rooks are in the same row or same column)? 
    
    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{rook-1.jpg}
    \end{figure}

    \pause{}

    Answer: $8!$
\end{frame}

\begin{frame}{\coin{}\coin{} Peaceful rooks}
    What if we have 8 distinct rook?
    
    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{rook-2.jpg}
    \end{figure}

    \pause{}

    Answer: $8! 8!$
\end{frame}

\begin{frame}{\coin{}\coin{} Peaceful rooks}
    What if we have one red, 3 blue, and 4 pink rooks?
    
    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{rook-3.jpg}
    \end{figure}

    \pause{}

    Answer: $8! \binom{8}{3,4,1}$
\end{frame}

\begin{frame}{\coin{}\coin{}\coin{} Apples and bananas}
    Prove that the number of strings of $m$ \apple{} and \alert{at most} $n$ \banana{} is 
    \[
        \binom{m+n+1}{m+1}.
    \]

    \pause{}

    \hint{} This is the number of strings of length $m+n+1$ with $m+1$ \apple{} and $n$ \banana{}.
\end{frame}

\begin{frame}{\puzzle{} Apples and bananas}
    Prove that the number of strings of \alert{at most} $m$ \apple{} and \alert{at most} $n$ \banana{} is 
    \[
        \binom{m+n+2}{m+1}-1.
    \]

    \hint{} Modify the proof from the previous one.
\end{frame}

\section{Induction}

\begin{frame}
    \frametitle{\coin{} A Flawed Proof!}

    Theorem: All \emoji{horse-face} are the same colour.

    \pause{}

    Base case: A single \emoji{horse-face} is of course the same colour as itself.
    
    \pause{}

    Inductive step: Consider $n+1$ \emoji{horse-face}. Label them $x$ and $y$.

    Assume a group of $n$ \emoji{horse-face} including $x$ are all one colour. Repeat for $y$. 

    By induction, $y$ matches $x$'s colour, implying all $n+1$ \emoji{horse-face} are identical in colour.

    \dizzy{} Spot the fallacy in this argument.
\end{frame}

\begin{frame}
    \frametitle{\coin{}\coin{} Dividing a Square}

    Can you show that a square can be divided into $n$ squares for any integer $n \ge 6$?

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[scale=1.3]
                \draw[fill=blue!20] (0,0) rectangle (2,2);
                \draw[fill=brown!20] (0,2) rectangle (1,3);
                \draw[fill=gray!20] (1,2) rectangle (2,3);
                \draw[fill=red!20] (2,2) rectangle (3,3);
                \draw[fill=green!20] (2,0) rectangle (3,1);
                \draw[fill=orange!20] (2,1) rectangle (3,2);
            \end{tikzpicture}
            \hfil
            \begin{tikzpicture}[scale=1]
                \draw[fill=blue!20] (0,0) rectangle (2,2);
                \draw[fill=brown!20] (2,0) rectangle (4,2);
                \draw[fill=gray!20] (0,2) rectangle (2,4);
                \draw[fill=red!20] (2,2) rectangle (3,3);
                \draw[fill=green!20] (3,2) rectangle (4,3);
                \draw[fill=orange!20] (2,3) rectangle (3,4);
                \draw[fill=yellow!20] (3,3) rectangle (4,4);
            \end{tikzpicture}
        \end{center}
    \caption{Dividing a square into $6$ and $7$ squares}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\coin{}\coin{}\coin{} Breaking a Chocolate}

    If you want to break the following chocolate bar below into its $28$ individual pieces, 
    then what is the minimum number of breaks to do this with? 
    You can only break one chunk of chocolate into two pieces at a time.

    Can you see why the answer is $28$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{chocolate.jpg}
    \end{figure}
\end{frame}

\section{Problems of Integer Composition}

\begin{frame}
    \frametitle{\coin{} What is the Number?}
    
    What is the number of integer solutions to
    \begin{equation*}
        x_{1} + x_{2} + \dots + x_{6} < 777
    \end{equation*}
    when all $x_1, \dots, x_6 > 0$?

    \pause{}

    Answer:
    \begin{equation*}
        \binom{777}{6}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\coin{}\coin{} What is the Number?}
    
    What is the number of integer solutions to
    \begin{equation*}
        x_{1} + x_{2} + \dots + x_{6} < 777
    \end{equation*}
    when all $x_1, \dots, x_6 \ge 0$?

    \pause{}

    Answer:
    \begin{equation*}
        \binom{777+6}{6}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\coin{}\coin{}\coin{} What is the Number?}
    
    What is the number of integer solutions to
    \begin{equation*}
        x_{1} + x_{2} + \dots + x_{6} < 777
    \end{equation*}
    when all $x_1, \dots, x_6 \ge 0$, 
    $x_2 \le 10$ and $x_4 \le 10$.

    \pause{}

    Answer: 
    \begin{equation*}
        \binom{777 + 6}{6}
        -
        2 \binom{777 + 5 - 10}{6}
        +
        \binom{777 + 4 - 20}{6}
    \end{equation*}
\end{frame}

\section{\Acf{gf}}

\begin{frame}
    \frametitle{\coin{} What is the \ac{gf}?}

    What is the \ac{gf} of the sequence
    \begin{equation*}
        2, 5, 2, 5, 2, 5, \dots
    \end{equation*}

    \pause{}

    Answer:
    \begin{equation*}
        \frac{3 x}{1 - x^{2}} + \frac{2}{1 - x}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\coin{}\coin{} What is the \ac{gf}?}
    
    What is the \ac{gf} of the sequence
    \begin{equation*}
        0, 2, 6, 12, 20, 30, 42, 56, 72, 90, \dots
    \end{equation*}

    \pause{}

    \hint{} Recall that
    \begin{equation*}
        \frac{1}{(1-x)^{k+1}} = \sum_{n=0}^{\infty} \binom{n+k}{k} x^{n}
    \end{equation*}

    \pause{}

    Answer:
    \begin{equation*}
        \frac{2x}{(1-x)^{3}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\coin{}\coin{} What is the \ac{egf}?}

    Let $a_n$ be the number of strings of length $n$ formed from the set 
    $\{\temoji{apple}, \temoji{banana}, \temoji{cherries}, \temoji{orange}\}$ 
    such that the following conditions are met:

    \begin{itemize}
        \item The number of \banana{} must be odd.
        \item There can be no more than one \emoji{cherries}.
    \end{itemize}

    Find the exponential generating function of $(a_n)_{n \in \mathbb{N}}$ in closed
    from.

    \pause{}

    Answer: 
    \begin{equation*}
        e^{2x} \frac{e^x - e^{-x}}{2} (x + 1)
    \end{equation*}
\end{frame}


\begin{frame}
    \frametitle{\coin{}\coin{} What is the \ac{egf}?}

    Let $a_n$ be the number of different types of
    zoo collections containing $n$ animals chosen from \lion{}, 
    \tiger{},
    \bunny{},
    and \emoji{zebra} that can be made subject to the following restrictions:

    \begin{itemize}
        \item There are either 0 or 3 \lion{}
        \item There are at least 2 \tiger{}
        \item The number of \bunny{} is a multiple of 4
        \item There are at most 3 \emoji{zebra}
    \end{itemize}

    \pause{}

    Answer: 
    \begin{equation*}
        G(x) = \frac{x^5 + x^2}{1 - x}
    \end{equation*}
\end{frame}


\begin{frame}
    \frametitle{\coin{}\coin{}\coin{} What is the \ac{egf}?}

    Let $a_n$ be the number of strings of length $n$ formed from the set 
    $\{\temoji{apple}, \temoji{banana}, \temoji{cherries}, \temoji{orange}\}$ 
    such that the following conditions are met:

    \begin{itemize}
        \item There must be exactly two \apple{}
        \item There are odd number of \banana{}
    \end{itemize}

    Find the exponential generating function of $(a_n)_{n \in \mathbb{N}}$ in closed
    from.

    \pause{}

    Answer: 
    \begin{equation*}
    \frac{x^2}{2} \frac{e^x-e^{-x}}{2} e^{x}
    \end{equation*}
\end{frame}

\end{document}
