\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\input{../language.tex}
\input{../table.tex}
\input{../graphs/kn.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Probability and Graph Theory (2)}

\begin{document}

\maketitle

\lectureoutline{}

\section{\acs{ac} 11.3 Estimate Ramsey Number}

\begin{frame}[c]
    \frametitle{Small numbers versus large numbers}

    We do not even know what is $R(5,5)$.

    But can we say something about $R(n, n)$ when $n$ is large?
    
    \begin{quote}
        Some times small numbers are very hard and some times large numbers are
        very easy.

        \flushright{Italo Simonelli}
    \end{quote}
\end{frame}

\begin{frame}
    \frametitle{Big \(O\) and small \(o\) Notation}
    
    \begin{block}{Big \(O\) Notation}
        \(f(n) = O(g(n))\) if there exist constants \(C > 0\) and \(n_0 \geq 0\) such that \(f(n) \leq C \cdot g(n)\) for all \(n \geq n_0\).
    \end{block}
    \pause{}
    
    \begin{block}{small \(o\) Notation}
        \(f(n) = o(g(n))\) if \(\lim_{{n \to \infty}} \frac{f(n)}{g(n)} = 0\).
    \end{block}
    \pause{}
    
    \begin{block}{Example}
        \begin{itemize}
            \item \(3n^2 + 2n = O(n^2)\)
            \item \(n = o(n^2)\)
        \end{itemize}
    \end{block}
    
\end{frame}

\begin{frame}[c]
    \frametitle{Stirling's approximation}
    
    To approximate $n!$, we can use \href{https://mathworld.wolfram.com/StirlingsApproximation.html}{Stirling's approximate}
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+\frac{1}{12
        n}+\frac{1}{288 n^2}-\frac{139}{51840
    n^3}+O\left(\left(\frac{1}{n}\right)^4\right)\right),
    \end{equation*}
    or the simpler version
    \begin{equation*}
        n! = \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
        \left(1+O\left(\frac{1}{n}\right)\right).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Estimate Ramsey Number}

    Given that
    \begin{equation*}
        n! \approx \sqrt{2 \pi n } \left(\frac{n}{e}\right)^{n} 
    \end{equation*}
    we have
    \begin{equation*}
        R(n,n) \le \binom{2 n - 2}{n-1} \approx \frac{2^{2n}}{4 \sqrt{\pi n}}.
    \end{equation*}

    \pause{}

    \astonished{} The best known lower bound is by Joel Spencer (1974):
    \begin{equation*}
        R(n,n) \ge \frac{\sqrt{2}}{e}n 2^{n/2}.
    \end{equation*}

    \pause{}

    More recently, Morris, Campos, Griffiths and Sahasrabudhe (2023) showed that
    \begin{equation*}
        R(n,n) \le (4-\epsilon)^{n}
    \end{equation*}
    for \(\epsilon = 2^{-7}\).
\end{frame}

\section{\acs{ac} 11.4 Applying Probability to Ramsey Theory}

\begin{frame}
    \frametitle{Theorem 11.4: A Lower Bound for the Ramsey Number}
    
    For \( n \ge 1 \),
    \[
        R(n,n) \ge \frac{1}{e \sqrt{2}}n 2^{n/2}.
    \]

    \pause{}

    The proof is by showing that there exists at least one graph with 
    $\frac{n 2^{n/2}}{e \sqrt{2}}$ vertices
    which does not contain an induced \( K_{n} \) or \( I_{n} \).

    \astonished{} There is no known method to find such graphs.
\end{frame}

\begin{frame}
    \frametitle{Proof Sketch}

    Let \( t > n \) and define \( \scF, \scF_{1}, \scF_{2} \) as sets
    of graphs with \( t \) vertices, excluding induced \( K_{n} \) and
    \( I_{n} \) respectively.

    We can show that if \( t \) is small enough, then,
    \begin{equation*}
        \abs{\scF} 
        >
        \abs{\scF_{1}} + \abs{\scF_{2}}
        \geq
        \abs{\scF_{1} \cup \scF_{2}}.
    \end{equation*}

    Hence, a \( t \)-vertex graph exists without induced \( K_{n} \)
    or \( I_{n} \).
\end{frame}

\begin{frame}
    \frametitle{\zany{} Where is the Probability?}
    
    The proof can also be stated like this:
    
    \begin{itemize}
        \item Start with \( t \) vertices and add each edge with a 50\% chance.
        \item This creates a random graph \( G_{t,1/2} \).
        \item For small \( t \), the chance of this graph lacking both \( K_{n} \) and \( I_{n} \) is greater than zero.
        \item So, a \( t \)-vertex graph without \( K_{n} \) and \( I_{n} \) must exist.
    \end{itemize}
\end{frame}

\section{The Probabilistic Method}

\begin{frame}
    \frametitle{Tournament}

    A tournament of size $n$ is a $K_{n}$ with each given an orientation.

    \hint{} Think $\temoji{rabbit-face} \imp \temoji{cow-face}$ as the \cow{}
    beats the \bunny{} in a game.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{tournament.pdf}
        \caption*{Example: A tournament with five vertices}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{$S_k$ property}
    
    We say that a tournament $T$ has the \alert{$S_k$ property},
    if for any $k$-subset of vertices of $T$,
    there is a vertex that beats them all.

    For example, the graph below satisfies the $S_1$ property.

    \cake{} Does it also satisfy the $S_2$ property?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{tournament.pdf}
        \caption*{Example: A tournament with five vertices}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A theorem on the $S_k$ property}
    
    \begin{block}{Theorem (The tournament theorem)}
        If $\binom{n}{k} (1 − 2^{−k})^{n−k} < 1$, 
        then there is a tournament on n
        vertices that has the property $S_k$.
    \end{block}
\end{frame}

\section{\zany{} Extremal Graph Theory}

\begin{frame}
    \frametitle{Extremal graph theory}
    
    \href{https://en.wikipedia.org/wiki/Extremal_graph_theory}{Extremal Graph
    Theory} is a branch of graph theory that originated from Ramsey numbers.
    
    Its objective is to find \emph{extremal} (min/max) number of edges under some
    restrictions.

    Some famous results include:
    \begin{itemize}
        \item Turán's Theorem 
        \item Erdős-Stone theorem
        \item Mantel's Theorem
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example: Turán's theorem}

    There is a number $T(n, r)$ such that
    every graph $G$ with $n$ vertices that does not contain $K_{r + 1}$ 
    as a subgraph has at most
    \begin{equation*}
        T(n,r)
        =
        \left(1-\frac{1}{r} + o(1) \right) \frac{n^2}{2}
    \end{equation*}
    edges. 

    \cake{} What does this theorem tell us when $r=1$?
\end{frame}

\begin{frame}
    \frametitle{Turán's theorem for $K_{3}$}
    
    It follows that every graph $G$ with $n$ vertices that does not contain
    $K_{3}$ 
    as a subgraph has at most
    \begin{equation*}
        T(n,2)
        =
        \left(1-\frac{1}{2} + o(1) \right) \frac{n^2}{2}
        \approx
        \frac{n^2}{4}
    \end{equation*}
    edges. 

    \cake{} Can you guess what kind of \triangular{}-free graph has this many edges?
\end{frame}

\begin{frame}[c]
    \frametitle{Pál Turán}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Turán was a \emoji{hungary} mathematician. 
            He was sent to labour camps in WWII \emoji{fearful}.

            \bigskip{}

            Yet, even in labour camps, he continued his math research.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Turan.jpg}
                \caption*{18 August 1910 -- 26 September 1976}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            Just go through these two proofs in this lecture.
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{\emoji{clap} The end of graph theory module!}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{seven-bridges-map.png}
%        \caption*{Where Graph Theory Started}
%    \end{figure}
%\end{frame}

\end{document}
