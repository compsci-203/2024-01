\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    \acs{dm} Section 3.1
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    \acs{dm} Section 3.1: 1-6, 8, 10-12.                            
                \item
                Try to solve with truth tables problem 1, 2 at
                \url{https://puzzlewocky.com/brain-teasers/knights-and-knaves/}.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{dm} 3.1 Propositional Logic}

\begin{frame}
    \frametitle{Propositions}
    
    A \alert{proposition} is simply a statement. 

    \alert{Propositional logic} studies the ways statements can interact with each other.

     It does not really care about the content of the statements.

     \pause{}

     For example, the following are all $P \imp Q$ from the perspective of logic.
     \begin{itemize}
         \item If the \emoji{crescent-moon} is made of \emoji{cheese},
             then the \emoji{sun-with-face} is made of \emoji{broccoli}.
         \item If \emoji{spider} have eight \emoji{leg},
             then Sam walks with a \emoji{icecream}.
         \item If \emoji{cat-face} can fly,
             they will rule the \emoji{earth-asia}.
     \end{itemize}
\end{frame}

\subsection{Truth Tables}

\begin{frame}
    \frametitle{The Game of Monopoly}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\linewidth]{monopoly.jpg}
        \caption*{By Anete Lusina on Pexels}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{A Propositional about Monopoly}

    Sam says ---
    \begin{itemize}
        \item In the Game of Monopoly, if you get more doubles than any other player
            then you will lose, 
        \item or if you lose then you must have bought the most
            properties.
    \end{itemize}

    \pause{}

    Asking if Sam is telling the truth is
    to ask if
    \begin{equation*}
        (P \imp Q) \vee (Q \imp R)
    \end{equation*}
    is true.

    \astonished{} How is this possible when we do not know the truth-values of
    $P, Q, R$?
\end{frame}

\begin{frame}
    \frametitle{Truth Tables}
    
    In a \alert{truth table}, each row contains a possible combination of
    \tttrue{}/\ttfalse{} values of atomic statements,
    and then marks down if a proposition is true or false in this case.
    
    \cake{} Can you complete the table?
    \begin{center}
        \only<1>{
            \begin{tabular}{c|c|c}
                \( P \) & \( Q \) & \( P \wedge Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<2>{
            \begin{tabular}{c|c|c}
                \( P \) & \( Q \) & \( P \vee Q \) \\
                \hline
                T & T & T \\
                T & F & T \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<3>{
            \begin{tabular}{c|c|c}
                \( P \) & \( Q \) & \( P \rightarrow Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<4>{
            \begin{tabular}{c|c|c}
                \( P \) & \( Q \) & \( P \iff Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T &   \\
                F & F &   \\
            \end{tabular}
        }
        \only<5>{
            \begin{tabular}{c|c}
                \( P \) & \( \neg P \) \\
                \hline
                T &   \\
                F &   \\
            \end{tabular}
        }
    \end{center}
\end{frame}


\begin{frame}
    \frametitle{The Monopoly Problem}

    \cake{} Can you complete the table?

    \begin{center}
        \begin{tabular}{c|c|c|c|c|c}
            \( P \) & \( Q \) & \( R \) & \( P \rightarrow Q \) & \( Q \rightarrow R \) & \( (P \rightarrow Q) \vee (Q \rightarrow R) \) \\
            \hline
            T & T & T & T & T &   \\
            T & T & F & T & F &   \\
            T & F & T & F & T &   \\
            T & F & F & F & T &   \\
            F & T & T & T & T &   \\
            F & T & F & T & F &   \\
            F & F & T & T & T &   \\
            F & F & F & T & T &   \\
        \end{tabular}
    \end{center}

    \cake{} Is Sam's proposition about Monopoly true?
\end{frame}

\begin{frame}[fragile]
    \frametitle{\zany{} Logic and Modern Compiler}

The following code
{\small
\begin{verbatim}
def is_sam_lying(p, q, r):
    if (!p or q) or (!q or r):
        print("Sam is right!")
    else:
        print("Sam is wrong!")
\end{verbatim}
}
will be optimized by any modern compiler into
{\small
\begin{verbatim}
def is_sam_lying(p, q, r):
    print("Sam is right!")
\end{verbatim}
}
because \texttt{(!p or q) or (!q or r)} will always be \tttrue{}.
\end{frame}

\subsection{\texorpdfstring{$\Xor$ --- Exclusive-or}{Xor --- Exclusive-or}}

\begin{frame}
    \frametitle{$\Xor$}

    We define one more logic connective \alert{exclusive-or}, denote by $\Xor$,
    using the following truth table.\footnote{This subsection is not in the textbook.}

    \begin{center}
        \begin{tabular}{c|c|c}
            \( P \) & \( Q \) & \( \Xor(P, Q) \) \\
            \hline
            T & T & F \\
            T & F & T \\
            F & T & T \\
            F & F & F \\
        \end{tabular}
    \end{center}

    \cake{} What is the difference between $P \vee Q$ and $\Xor(P, Q)$?
\end{frame}

\begin{frame}[c]
    \frametitle{Knights and Knaves Puzzles}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In a \alert{knights and knaves} puzzle,
            a troll is either a \emph{knight} or a \emph{knave}.

            \bigskip{}

            A \emph{knight} always tell the truth,
            and a \emph{knave} always lie.

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{troll-and-knight.jpg}
                \caption{Which troll is telling the truth?}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Solving the Puzzle with a Truth Table}

    Two trolls, \textcolor{blue}{Blue} and \textcolor{red}{Red}, 
    stand before you.  
    \textcolor{blue}{Blue} says, ``We are both knaves.'' 
    What are they really are?

    Let
    \begin{equation*}
        B = \text{\textcolor{blue}{Blue} is knight},
        \,
        R = \text{\textcolor{red}{Red} is knight},
        \,
        SB = \Xor(\neg B, \neg B \wedge \neg R).
    \end{equation*}
    \pause{}%
    Thus we fill a corresponding truth
    \begin{center}
        \small
        \begin{tabular}{c|c|c|c|c|c}
            \( B \) & \( R \) & \( \neg B \) & \( \neg R \) & \( \neg B \wedge \neg R \) & $SB$ \\
            \hline
            T & T & F & F & F & \\
            T & F & F & T & F & \\
            F & T & T & F & F & \\
            F & F & T & T & T & \\
        \end{tabular}
    \end{center}
    \pause{}%
    \cake{} What does the truth table tell us?
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}

    Another two trolls, \textcolor{blue}{Blue} and \textcolor{red}{Red},
    tell you:

    \begin{itemize}
        \item \textcolor{blue}{Blue}: If we are cousins, then we are both knaves.
        \item \textcolor{red}{Red}: We are cousins or we are both knaves.
    \end{itemize}
    What can we say about the identities of the two trolls?

    \begin{columns}[c, totalwidth=\textwidth]
        \begin{column}{0.52\textwidth}
            Let
            \begin{itemize}
                \small
                \item $B$ : \textcolor{blue}{Blue} is knight.
                \item $R$ : \textcolor{red}{Red} is knight.
                \item $C$ : \textcolor{blue}{Blue} and \textcolor{red}{Red} are cousins.
                \item $SB = \Xor(\neg B, 
                    C \imp \underline{\phantom{(\neg B \wedge \neg R)}})$
                \item $SR = \Xor(\neg R,
                    C \vee \underline{\phantom{(\neg B \wedge \neg R)}})$
            \end{itemize}
        \end{column}
        \begin{column}{0.46\textwidth}
            \begin{equation*}
                \small
                \begin{array}{c|c|c|c|c}
                    B & R & C & SB & SR \\
                    \hline
                    T & T & T & F & T \\
                    T & T & F & T & F \\
                    T & F & T & F & F \\
                    T & F & F & T & T \\
                    F & T & T & T & T \\
                    F & T & F & F & F \\
                    F & F & T & F & F \\
                    F & F & F & F & F \\
                \end{array}
            \end{equation*}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Logical Equivalence}

\begin{frame}
    \frametitle{Logical Equivalence}
    
    Two statements $A$ and $B$ are \alert{logically equivalent} if $A$ is
    true precisely when $B$ is true, i.e., $A \iff B$.

    For example, $P \imp Q$ and $\neg P \vee Q$ are logically equivalent.

    \begin{center}
        \begin{tabular}{c|c|c|c}
            \( P \) & \( Q \) & \( P \rightarrow Q \) & \(\neg P \vee Q\) \\
            \hline
            T & T & T & T \\
            T & F & F & F \\
            F & T & T & T \\
            F & F & T & T \\
        \end{tabular}
    \end{center}

    \bomb{} By $\neg P \vee Q$ we mean $(\neg P) \vee Q$, not $\neg (P \vee Q)$.
\end{frame}

\begin{frame}
    \frametitle{Example --- Snow and Rain}
    
    Are the following statements logically equivalent?
    \begin{itemize}
        \item It will not \emoji{droplet} or \emoji{snowflake}.
            (It will neither \emoji{droplet} nor
            \emoji{snowflake}.)
        \item It will not \emoji{droplet} and it will not
            \emoji{snowflake}.
    \end{itemize}
    
    \hint{} Filling in the following truth table gives the answer.
    \begin{center}
        \begin{tabular}{c|c|c|c|c|c}
            \( P \) & \( Q \) & \( \neg (P \vee Q) \) & \( \neg P \) & \( \neg Q\) & \( \neg P \wedge \neg Q \) \\
            \hline
            T & T & F & F & F &  \\
            T & F & F & F & T &  \\
            F & T & F & T & F &  \\
            F & F & T & T & T &  \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Basic Logic Equivalence}
    
    \begin{exampleblock}{Implications are Disjunctions}
        \begin{itemize}
            \item $P \imp Q \iff \neg P \vee Q$.
        \end{itemize}

        \only<1>{%
            Thus the following are logically equivalent:
            \begin{itemize}
                \item If you get A+, then your parents buy you a \emoji{panda}.
                \item You do not get A+, or your parents buy you a \emoji{panda}.
            \end{itemize}
        }%
    \end{exampleblock}

    \pause{}

    \begin{exampleblock}{De Morgan's Laws}
        \begin{itemize}
            \item $\neg (P \wedge Q) \iff \neg P \vee \neg Q$.
            \item $\neg (P \vee Q) \iff \neg P \wedge \neg Q$.
        \end{itemize}

        \only<2>{%
            Thus the following are logically equivalent:
            \begin{itemize}
                \item It will not \emoji{droplet} or \emoji{snowflake}.
                \item It will not \emoji{droplet} and it will not \emoji{snowflake}.
            \end{itemize}
        }%
    \end{exampleblock}

    \pause{}

    \begin{exampleblock}{Double Negation}
        \begin{itemize}
            \item $\neg \neg P \iff P$. 
        \end{itemize}
        Thus the following are logically equivalent:
        \only<3>{%
            \begin{itemize}
                \item Sam is a \emoji{dog}.
                \item It is \emph{not} true that Sam is \emph{not} a \emoji{dog}
            \end{itemize}
        }%
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Negation of an Implication}

    Prove that $\neg (P \imp Q)$ and $P \wedge \neg Q$ are logically
    equivalent.
\end{frame}

\begin{frame}
    \frametitle{Verifying equivalence}

    We \emph{cannot} disapprove 
    \begin{equation*}
        (P \vee Q) \imp R \iff (P \imp R) \vee (Q \imp R)
    \end{equation*}
    using logic equivalence.

    But we can use truth table.
    \begin{center}
        \small
        \begin{tabular}{c|c|c|c|c}
            \( P \) & \( Q \) & \( R \) & \( (P \vee Q) \rightarrow R \) & \( (P \rightarrow R) \vee (Q \rightarrow R) \) \\
            \hline
            T & T & T & T & T \\
            T & T & F & F & F \\
            T & F & T & T & T \\
            T & F & F & F & T \\
            F & T & T & T & T \\
            F & T & F & F & T \\
            F & F & T & T & T \\
            F & F & F & T & T \\
        \end{tabular}
    \end{center}
\end{frame}

\subsection{Deductions --- How to argue with logic}

\begin{frame}[c]
    \frametitle{Valid and invalid arguments}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            An \alert{argument} is a set of statements, one of which is called the
            \alert{conclusion} and the rest of which are called \alert{premises}. 

            \medskip{}

            An argument is \alert{valid} if the conclusion must be true whenever the
            premises are all true. 

            \medskip{}

            Otherwise it is \alert{invalid}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-lawyer.jpg}
                \caption{Keep your arguments valid!}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Is This Valid?}
    Consider the following argument
    \[
        \begin{array}{ r l }
               & \text{If \bunny{} eats its \emoji{apple}, then it can have a \emoji{cookie}.} \\
               & \text{\bunny{} eats its \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{\bunny{} can get a \emoji{cookie}.}
        \end{array}
    \]
    \cake{} Judging from the truth table, is it valid?
    \begin{center}
        \begin{tabular}{c|c|c}
            \emoji{apple} & \emoji{cookie} & \emoji{apple}  \rightarrow \emoji{cookie} \\
            \hline
            T & T & T \\
            T & F & F \\
            F & T & T \\
            F & F & T \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Is This Valid?}
    Consider the following argument
    \[
        \begin{array}{ r l }
               & \text{\puppy{} must eat its \emoji{apple} in order to get a \emoji{cookie}.} \\
               & \text{\puppy{} eats its \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{\puppy{} can get a \emoji{cookie}.}
        \end{array}
    \]
    \cake{} Judging from the truth table, is it valid?
    \begin{center}
        \begin{tabular}{c|c|c}
            \emoji{apple} & \emoji{cookie} & \emoji{cookie}  \rightarrow \emoji{apple} \\
            \hline
            T & T & T \\
            T & F & T \\
            F & T & F \\
            F & F & T \\
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule}
    
    \small
    The argument
    \[
        \begin{array}{ r l }
               & \text{If \bunny{} eats her \emoji{apple}, then she can have a \emoji{cookie}.} \\
               & \text{\bunny{} eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{\bunny{} gets a \emoji{cookie}.}
        \end{array}
    \]
    has the form
    \[
        \small
        \begin{array}{ r l }
               & P \imp Q \\
               & P \\
               \cline{2-2}
            \therefore & Q
        \end{array}
    \]
    This is a \alert{deduction rule}, i.e., an argument form which is
    always \emph{valid}. 
    
    \begin{center}
            \begin{tabular}{c|c|c}
                \( P \) & \( Q \) & \( P \rightarrow Q \) \\
                \hline
                T & T & T \\
                T & F & F \\
                F & T & T \\
                F & F & T \\
            \end{tabular}
    \end{center}
\end{frame}

%\begin{frame}
%    \frametitle{Deduction rule 1}
%    
%    \begin{columns}
%        \begin{column}{0.4\textwidth}
%    
%    If our axioms are
%    \begin{enumerate}
%        \item $P$
%        \item $P \imp Q$
%    \end{enumerate}
%    then $Q$ is true.
%            
%        \end{column}
%        \begin{column}{0.6\textwidth}
%            
%    If we our axioms are
%    \begin{enumerate}
%        \item Sam is a \emoji{dog}.
%        \item If Sam is a \emoji{dog}, then Sam is cute.
%    \end{enumerate}
%    Then \emph{Sam is cute} is true.
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}
    \frametitle{Example 3.1.6}

    \cake{} Is the following a deduction rule?
    \[
        \begin{array}{ r l }
               & P \imp Q \\
               & \neg P \imp Q \\
               \cline{2-2}
            \therefore & Q
        \end{array}
    \]
    Let find out by filling the truth table
    \begin{center}
            \begin{tabular}{c|c|c|c}
                \( P \) & \( Q \) & \( P \imp Q \) & \(\neg P \imp Q\) \\
                \hline
                T & T & & \\
                T & F & & \\
                F & T & & \\
                F & F & & \\
            \end{tabular}
    \end{center}
\end{frame}

\begin{frame}[c]
    \frametitle{Example 3.1.7}

    \begin{columns}[c]
        \begin{column}{0.35\textwidth}
            \cake{} Is this a deduction rule?
            \[
                \begin{array}{ r l }
               & P \imp R \\
               & Q \imp R \\
               & R \\
               \cline{2-2}
                    \therefore & P \vee Q
                \end{array}
            \]
        \end{column}
        \begin{column}{0.65\textwidth}
            Let find out by filling the truth table
            \begin{center}
                \begin{tabular}{c|c|c|c|c|c}
                    \( P \) & \( Q \) & \( R \) & \( P \imp R \) & \( Q \imp R \)  &
                    \(P \vee Q\) \\
                    \hline
                    T & T & T & T & T & \\
                    T & T & F & F & T & \\
                    T & F & T & T & T & \\
                    T & F & F & F & F & \\
                    F & T & T & T & T & \\
                    F & T & F & T & T & \\
                    F & F & T & T & T & \\
                    F & F & F & T & F & \\
                \end{tabular}
            \end{center}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{\zany{} Sound and unsound arguments}
    
    The following argument \emph{is} valid,
    \[
        \begin{array}{ r l }
               & \text{All \emoji{wastebasket} are items made of gold.} \\
               & \text{All items made of gold are \emoji{clock9}-travel devices.} \\
               \cline{2-2}
            \therefore & \text{All \emoji{wastebasket} are \emoji{clock9}-travel devices.}
        \end{array}
    \]
    But it is \alert{unsound}, because the premises are false.

    \bomb{} In mathematics, we only care about \emph{validity}, but in life you should also
    consider \emph{soundness}.

    \emoji{eye} See more at \url{https://iep.utm.edu/val-snd/}
\end{frame}

\subsection{Beyond Propositions}

\begin{frame}
    \frametitle{Universal quantifiers}
    
    In mathematics, we are more interested in statements like

    \begin{quote}
        \emph{All} primes greater than 2 are odd.
    \end{quote}

    In mathematical notations, this can be written as
    \begin{equation*}
        \forall x((P(x) \wedge x > 2) \imp O(x))
    \end{equation*}
    where 
    \begin{itemize}
        \item $\forall$ is the \alert{universal quantifier} ,
        \item  $P(x)$ denotes $x$ is a prime
        \item and $O(x)$ denotes $x$ is odd.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Existential quantifiers}
    
    We may also be interested in statements like

    \begin{quote}
        There \emph{exists} a primes that is odd.
    \end{quote}

    In mathematical notations, this can be written as
    \begin{equation*}
        \exists x((P(x) \wedge O(x))
    \end{equation*}
    where 
    \begin{itemize}
        \item $\exists$ is the \alert{existential quantifier} ,
        \item  $P(x)$ denotes $x$ is a prime
        \item and $O(x)$ denotes $x$ is odd.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Predicate logic}
    
    $P(x)$ and $O(x)$ are not propositions since their truth values depend on
    $x$.

    These are called \alert{predicates}.

    The logic studying statements involving \emph{predicates} is called \alert{predicate logic}.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (0,0) node [
                fill=green!30, 
                draw=green!80,
                minimum height = 2.0cm,
                minimum width = 8cm,
                rounded corners=.5cm,
                label={[label distance=-0.7cm]90:Predicate Logic}
                ] 
                {};
            \draw (0,0) node [
                fill=yellow!30, 
                draw=yellow!80,
                minimum height = 0.8cm,
                minimum width = 6.5cm,
                rounded corners=.5cm,
                label={[label distance=-0.7cm]90:Propositional Logic}
                ] 
                {};
        \end{tikzpicture}
    \end{figure}

    Predicate logic is a fundamental tool used in various domains of scientific research, 
    helping to formalize arguments and hypotheses.
\end{frame}

\begin{frame}
    \frametitle{Logical equivalence for predicate logic}

    There is no analogues to truth-tables for predicate logic.

    All we can do is to use basic logical equivalence which we \emph{agree}
    to be valid.

    \only<1>{
        \begin{example}[Negation of universal quantifiers]
            We agree that
            \begin{equation*}
                \neg \forall x P(x)
                \iff
                \exists x \neg P(x)
            \end{equation*}

            \cake{} Thus
            \begin{quote}
                It is not true that all \emoji{ox} hates \emoji{red-square}.
            \end{quote}
            is equivalent to
            \begin{quote}
                There exists a \emoji{ox} which \blankshort{}.
            \end{quote}
        \end{example}
    }
    \only<2>{
        \begin{example}[Negation of existental quantifiers]
            We agree that
            \begin{equation*}
                \neg \exists x P(x)
                \iff
                \forall x \neg P(x)
            \end{equation*}

            \cake{} Thus
            \begin{quote}
                It is not true that there exists an \emoji{ox} which hates \emoji{red-square}.
            \end{quote}
            is equivalent to
            \begin{quote}
                All \emoji{ox} \blankshort{}.
            \end{quote}
        \end{example}
    }
\end{frame}

%\begin{frame}
%    \frametitle{Example 3.1.8}
%    We have the deduction rule
%    \begin{equation*}
%        \begin{array}{ r l }
%               & \neg \exists x \forall y P(x, y) \\
%               \cline{2-2}
%            \therefore & \forall x \exists y \neg P(x, y) \\
%        \end{array}
%    \end{equation*}
%
%    \pause{}
%
%    \cake{} How to apply this to ``There is no smallest number''?
%    \begin{equation*}
%        \begin{array}{ r l }
%               & \neg \exists x \forall y (x < y)\\
%               \cline{2-2}
%            \therefore & \underline{\phantom{\forall x \exists y \neg P(x, y)}} \\
%        \end{array}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example 3.1.9}
%    
%    Consider another deduction rule
%    \begin{equation*}
%        \begin{array}{ r l }
%               & \exists y \forall x P(x, y) \\
%               \cline{2-2}
%            \therefore & \forall x \exists y P(x, y) \\
%        \end{array}
%    \end{equation*}
%
%    \pause{}
%
%    \cake Applying this to 
%    \begin{itemize}
%        \item There exists a \emoji{woman} who is an ancestor
%            of all living human beings.
%    \end{itemize}
%    what conclusion can we draw?
%\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%    
%    Is the following a valid deduction rule?
%
%    \[
%        \begin{array}{ r l }
%        & A \imp D \\
%        & B \imp D \\
%        & C \imp D \\
%        & D \\
%        \cline{2-2}
%            \therefore & A \vee B \vee C
%        \end{array}
%    \]
%\end{frame}

\end{document}
