\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{}}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \coursereading{}
            \begin{itemize}
                \item
                    \acs{dm} Section 3.2
            \end{itemize}

            \medskip{}

            \courseassignment{}
            \begin{itemize}
                \item
                    \acs{dm} Section 3.2: 1-7, 9, 10, 13, 21-22.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{dm} 3.2 Proofs}

\subsection{What Is a Mathematical Proof?}

\begin{frame}[c]
    \frametitle{\zany{} What Is a Mathematical Proof?}

    A \alert{proof} is a \emph{valid} logical argument consisting of:

    \begin{enumerate}[<+->]
        \item Selecting a \alert{logic system} , such as classical logic,
            constructive logic, etc.
        \item Selecting a set \alert{axioms}.
        \item Selecting a set of \alert{premises}.
        \item Applying \alert{deduction rules} allowed in the logic system
            to these \emph{premises} and \emph{axioms},
            along with previously deduced statements, 
            to derive the \alert{conclusion} being proved.
        \item Concluding that the premises imply the conclusion.
    \end{enumerate}

    \pause[\thebeamerpauses]

    In practice, the \emph{logic system} in use and the \emph{axioms} are usually implicit.
\end{frame}

\begin{frame}
    \frametitle{A classic theorem}

    \begin{block}{Theorem 3.2.1}
        There are infinitely many primes.
    \end{block}

    \only<1>{%
        People use \emoji{computer} to find larger and larger primes.

        The \href{}{largest known prime} now is $2^{82,589,933} - 1$.

        However, only a \emph{proof} can convince us the theorem is \tttrue{}.
    }%
    \pause{}
    \begin{proof}[Euclid's Proof]
        \scriptsize
        \begin{enumerate}[{P}1]
            \item<+-> Suppose there are only finitely many primes. (Suppose this is
                \tttrue{} for now.)
            \item<+-> Let $p$ be the largest prime. (by P1)
            \item<+-> Let $N = p! + 1$. (Just a notation)
            \item<+-> $N > p$. (by P3)
            \item<+-> $N$ is not divisible by any $i \in \{2,\dots, p\}$. (by P3)
            \item<+-> $N$ has a prime factor greater than $p$. (by P4, P5)
            \item<+-> $p$ is not the largest prime. (by P6)
            \item<+-> P7 contradicts P2.
            \item<+-> P1 must be \ttfalse{}.
            \item<+-> There must be infinitely many primes. (by P9)
        \end{enumerate}
    \end{proof}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} Computer Verified Proofs}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            All mathematical proofs can be \emph{computer verified}.

            \bigskip{}

            For example, in 1998, Thomas Hales proved Kepler's Conjecture with the help of
            \emoji{computer}.

            \bigskip{}

            Then in 2014, his team announced they formally verified the proof with \emoji{computer}.

            \bigskip{}

            \emoji{eye} See details
            \href{https://www.quantamagazine.org/in-computers-we-trust-20130222/}{here}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Thomas-Hales.jpg}
                \caption{Thomas Hales, pictured in 1998, used a \emoji{computer} to prove a famous conjecture about the densest way to stack spheres.}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{Direct Proof}

\begin{frame}
    \frametitle{Direct proofs}
    
    A \alert{direct proof} of $P \imp Q$ is an argument of the following form:

    \begin{itemize}
        \item Assume $P$ is \tttrue{}. 
        \item Explain, explain, \dots, explain. 
        \item Therefore $Q$ is \tttrue{}.
    \end{itemize}

    \pause{}

    \begin{exampleblock}{Example 3.2.2}
        For all integers $n$, if $n$ is even, then $n^2$ is even.
    \end{exampleblock}
\end{frame}


\begin{frame}
    \frametitle{\tps{}}
    Prove that for all integers $n$, if $n$ is odd, then $n^2$ is odd.
\end{frame}

\subsection{Proof by Contrapositive}

\begin{frame}
    \frametitle{Proof by contrapositive}

    Recall that $P \imp Q \iff \neg Q \imp \neg P$.
    Thus, to show $P \imp Q$, we can use \alert{proof by contrapositive}:

    \begin{itemize}
        \item Assume $\neg Q$ is \tttrue{}.
        \item Explain, explain, \dots explain.
        \item Therefore $\neg P$ is \tttrue{}.
    \end{itemize}

    \pause{}

    \begin{exampleblock}{Example 3.2.4}
        If $n^2$ is even, then $n$ is even.
    \end{exampleblock}
    \cake{} Do you see why $n$ is even if and only if $n^2$ is even.
\end{frame}


\begin{frame}
    \frametitle{Example 3.2.5}
    
    Prove by contrapositive that for all integers $a$ and $b$, if $a + b$ is odd, then $a$ is odd or $b$ is odd.
\end{frame}

\begin{frame}
    \frametitle{Example 3.2.6}
    
    Prove by contrapositive that for every prime number $p$, if $p \ne 2$, then $p$ is odd.
\end{frame}

\subsection{Proof by Contradiction}

\begin{frame}
    \frametitle{Proof by contradiction}

    A statement like $C \wedge \neg C$ is called a \alert{contradiction}.

    In \emph{classical logic}, a contradiction is always \ttfalse{}.

    \cake{} If $\neg P \imp (C \wedge \neg C)$ is \tttrue{},
    i.e., $\neg P$ implies a contradiction,
    what truth-value (\tttrue{}/\ttfalse{}) must $P$ have?

    \pause{}

    Thus, to show $P$, we can use \alert{proof by contradiction}  ---
    \begin{enumerate}
        \item Assume $\neg P$ is \tttrue{}. Explain, \dots explain \dots, therefore $C$ is \tttrue{}.
        \item Assume $\neg P$ is \tttrue{}. Explain, \dots explain \dots, therefore $\neg C$ is
            \tttrue{}.
        \item Thus there is a \alert{contradiction} and we must have $P$ being \tttrue{}.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example 3.2.7}
    
    A \alert{rational number} is a real number that can be expressed as the quotient \( \frac{a}{b} \) of two integers, \( a \) and \( b \), with \( b \neq 0 \).

    A real number that is not rational is called \alert{irrational}.

    Prove that $\sqrt{2}$ is irrational.
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} The Drowning of Hippasus}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \small

            Hippasus, a Pythagorean philosopher,
            discovered the existence of irrational numbers. 

            \medskip{}

            This revelation shocked the Pythagoreans. 

            \medskip{}

            It's said that Hippasus was drowned at sea as divine punishment.

            \hfill --- From \href{https://en.wikipedia.org/wiki/Hippasus}{Wikipedia}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{Hippasus.jpg}
                \caption*{Hippasus (530-450 BC). By \href{https://books.google.co.uk/books?id=1urLxOK8pzkC}{Girolamo Olgiati}}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 3.2.8}
    \cake{} Prove that there are no integers $x$ and $y$ such that $x^2 = 4y + 2$.
\end{frame}

\begin{frame}
    \frametitle{Proof by Contradiction and by Contrapositive}
    
    In some cases, we can prove $P \imp Q$ by     
    showing that
    \begin{enumerate}[<+->]
        \item Assume $P \wedge \neg Q$ is \tttrue{}, i.e., $\neg (P \imp Q)$ is \tttrue{}.
        \item Obviously $P$ is \tttrue{}.
        \item Given that $\neg Q$ is \tttrue{}. Explain, \dots explain \dots, therefore
            $\neg P$ is \tttrue{}.
        \item Thus there is a \alert{contraction} $P \wedge \neg P$.
    \end{enumerate}

    \pause[\thebeamerpauses]

    Essentially we are just proving that
    \begin{equation*}
        \neg Q \imp \neg P
    \end{equation*}
    In other words, it is the same as proof by contrapositive.
\end{frame}

\begin{frame}
    \frametitle{Pigeonholes}

    Pigeonhole --- a hole or small recess for pigeons to nest.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{TooManyPigeons.jpg}
        \caption*{Source: \href{https://en.wikipedia.org/wiki/Pigeonhole\_principle}{Wikipedia}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3.2.9}

    The Pigeonhole Principle: If more than $n$ \emoji{bird} fly into $n$ pigeon
    holes, then at least one pigeon hole will contain at least two \emoji{bird}.
\end{frame}

\subsection{Proof by (Counter) Example}

\begin{frame}
    \frametitle{A Notation}

    We will use the notation
    {%
        \Huge{}
        \begin{equation*}
            [n] = \{ 1, 2, \dots, n \}
        \end{equation*}%
    }%
    for brevity.

    \emoji{pray} Please do not keep asking what it means.
\end{frame}

\begin{frame}
    \frametitle{An experiment}
    
    \astonished{} For $n \in [7]$, $n^2 - n + 41$ are all primes. 
 
    \begin{equation*}
        \begin{array}{ c c c c c c c c  }
            \toprule
            n & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
            \midrule
            n^2 − n + 41 & 41 & 43 & 47 & 53 & 61 & 71 & 83 \\
            \bottomrule
        \end{array}
    \end{equation*}

    \cake{} Is it \tttrue{} that for all $n \in \dsN$, 
    $n^{2} - n + 41$ is a prime?

    \cake{} If not, can you think of an $n$ such that 
    $n^{2} - n + 41$ is \emph{not} a prime?
\end{frame}

\begin{frame}
    \frametitle{Prove by Example}
    
    \bomb{} It is almost never OK to prove $\forall x P(x)$ by examples.

    For example, to prove that
    \begin{quote}
        \item For all $n \in \dsN$, $n^{2} - n + 41$ is prime,
    \end{quote}

    it is not enough to show it is \tttrue{} for $n \in \{1, \dots, 7\}$.


    \pause{}

    \bigskip{}

    But it is \emoji{ok} to use example to show $\exists x P(x)$.

    For example, to prove that
    \begin{quote}
        \item There exists $n \in \dsN$, $n^{2} - n + 41$ is \emph{not} a prime.
    \end{quote}

    it is enough to show it is \tttrue{} for a specific $n$.
\end{frame}

%\begin{frame}
%    \frametitle{Example 3.2.10}
%    
%    We proved, 
%    \begin{itemize}
%        \item For all integers $a$ and $b$, if $a + b$ is odd, then $a$ is odd or
%    $b$ is odd.
%    \end{itemize}
%    \cake{} Is the converse \tttrue{}? Can you prove your answer?
%\end{frame}

\begin{frame}
    \frametitle{\zany{} Strong Law of Small Numbers}
    
    The so-called \alert{strong law of small numbers} is the humorous law that proclaims,
    in the words of Richard K.~Guy,

    \bigskip{}

    \begin{quote}
        There aren't enough small numbers to meet the many demands made of them.
    \end{quote}

    \bigskip{}

    In other word, the reason that there are many surprising coincidences in mathematics is
    that small numbers are so few but are used so often.
\end{frame}

%{
%\setbeamertemplate{background}[default]
%\usebackgroundtemplate{\includegraphics[width=\paperwidth]{Zeilberger.jpg}}%
%\begin{frame}[c]
%    \frametitle{A different opinion}
%
%    \begin{columns}
%        \begin{column}{0.5\textwidth}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            Unfortunately, these examples serve as `weapons' in the propaganda efforts of
%            rigorists, fanatical, `purists', who see truth as black and white, and claim that
%            you can't guarantee the truth of a statement no matter how many special cases you
%            have checked.
%
%            \hfill --- \href{https://sites.math.rutgers.edu/~zeilberg/mamarim/mamarimhtml/small.html}{Doron Zeilberger}
%        \end{column}
%    \end{columns}
%\end{frame}
%}

\subsection{Proof by Cases}

\begin{frame}
    \frametitle{Proof by cases}

    To show that
    \begin{equation*}
        (Q_{1} \vee Q_{2} \dots \vee Q_{n})
        \imp
        P
        ,
    \end{equation*}
    it suffices to show that
    \begin{equation*}
        (Q_1 \imp P)
        \wedge
        (Q_2 \imp P)
        \wedge
        \dots
        \wedge
        (Q_n \imp P)
        .
    \end{equation*}

    In other words, if in each possible case $P$ is \tttrue{},
    then $P$ is always \tttrue{}.
\end{frame}

\begin{frame}
    \frametitle{Example 3.2.11}

    Prove that for any integer $n$, the number $(n^3 − n)$ is even.
\end{frame}

\begin{frame}
    \frametitle{\dizzy{} A classical example}

    Prove that there exist two \emph{irrational} numbers $s$ and $t$ such that
    $s^{t}$ is rational.

    Proof: We consider two cases 
    $\sqrt{2}^{\sqrt{2}} \in \dsQ$
    and
    $\sqrt{2}^{\sqrt{2}} \notin \dsQ$.
    (One of these must be \tttrue{}, but we don't care which one.)

    \vspace{4cm}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Prove that for all integers $n$, $n^{2} \ge n$.

    \hint{} How many cases do we have to consider?
\end{frame}

\subsection{Proof of If and Only if}

\begin{frame}
    \frametitle{Exercise 3.2.12}
    Show that 
    \begin{equation}
        \label{eq:3.2.12}
        x y = \frac{{(x + y)^2}}{4}
    \end{equation}
    if and only if $x = y$.

    Proof:
    \only<1>{%
        First we show that $x = y$ implies \eqref{eq:3.2.12}.
    }%
    \only<2>{%
        Then we show that  \eqref{eq:3.2.12} implies $x = y$.
    }%
    \vspace{4cm}
\end{frame}

%\begin{frame}
%    \frametitle{Strangers and friends}
%    
%    \begin{theorem}
%        In any group of 6 people, there are either 3 mutual friends (every pair knows each other) or 3
%        mutual strangers (no pair knows each other).
%    \end{theorem}
%\end{frame}

%\subsection{Common Mistakes}
%
%\begin{frame}
%    \frametitle{Is It Correct?}
%
%    \begin{block}{Statement}
%        If $ab$ is an even number, then $a$ or $b$ is even.
%    \end{block}
%
%    \begin{proof}
%    \begin{figure}[htpb]
%        \centering
%        \begin{overlayarea}{\textwidth}{5cm}
%        \includegraphics<1>[width=\linewidth]{proof1.png}
%        \includegraphics<2>[width=\linewidth]{proof2.png}
%        \includegraphics<3>[width=\linewidth]{proof3.png}
%        \includegraphics<4>[width=\linewidth]{proof4.png}
%        \end{overlayarea}
%        \vspace{-1em}
%    \end{figure}
%    \end{proof}
%\end{frame}

%\subsection{Put It Together}
%
%\begin{frame}
%    \frametitle{Another Proof of Infinitely Many Primes}
%
%    Consider Fermat numbers $F_n = 2^{2^{n}} + 1$ for $n \in \dsN$.
%
%    By induction (we will talk about it later)
%    \begin{equation*}
%        \prod_{k=0}^{n-1} F_{k} = F_{n} - 2 \qquad (n \ge 1).
%    \end{equation*}
%
%    \think{} There for when $n \ne k$,
%    \begin{equation*}
%        \gcd(F_{n}, F_{k}) = 1.
%    \end{equation*}
%
%    \think{} So there are infinitely many primes.
%    
%    \emoji{eye} Proof \href{https://dannysiublog.com/blog/using-fermat-number-to-prove-the-infinity-of-primes/}{link}.
%\end{frame}

%\begin{frame}
%    \frametitle{\zany{} Romeo and Juliet}
%    Consider the following proof --
%    \begin{itemize}
%        \item Everyone loves a lover; Romeo loves Juliet; so everyone loves Juliet.
%    \end{itemize}
%    How to write this proof formally?
%\end{frame}

\end{document}
