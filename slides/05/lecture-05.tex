\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Sequences and Series}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \ac{ec}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 9.1.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item
                            Exercise 9.1: 1--3, 5, 7, 9, 11, 13--15, 20, 24.                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ec} 9.1 Infinite Sequence and Series}

\subsection{Sequences}

\begin{frame}[c]
    \frametitle{Motivational Example: Swedes and Cakes}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In \emoji{sweden}, it is impolite to eat the last piece of food.
            \bigskip{}

            If 
            \begin{itemize}
                \item the first Swede eats $\frac{1}{2}$ of a \emoji{birthday},
                \item the second eats $\frac{1}{4}$ of a \emoji{birthday},
                \item and so on,
            \end{itemize}
            how many \emoji{birthday} do we need?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-cake.jpg}
                \caption{Remember not to eat the whole cake in Sweden!}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}


\begin{frame}[c]
    \frametitle{An Intuitive Answer}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Intuitively, the answer is 
            \begin{equation*}
                \frac{1}{2} 
                + 
                \frac{1}{4}
                +
                \frac{1}{8}
                +
                \frac{1}{16}
                +
                \dots
                =
                1
                .
            \end{equation*}

            \medskip{}

            \think{} How to make this mathematically rigorous?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \begin{tikzpicture}[
                    scale=1.3,
                    ]
                    % Draw the circle
                    \draw[fill=MintGreen] (0,0) circle (2cm);

                    % Initialize angle
                    \newcommand{\currentangle}{0}

                    % Loop to draw each sector
                    \foreach \x [count=\xi] in {1/2, 1/4, 1/8, 1/16, 1/32, 1/64} {
                        % Draw sector
                        \draw (0,0) -- ++(\currentangle:2cm) arc (\currentangle:\currentangle+360*\x:2cm) -- cycle;

                        % Label sector
                        \pgfmathsetmacro{\labelangle}{\currentangle + 540*\x}
                        \node at (\labelangle:1.5cm) {\footnotesize \(\x\)};

                        % Update angle
                        \pgfmathsetmacro{\currentangle}{\currentangle + 360*\x}
                    }
                \end{tikzpicture}
                \caption{Dividing a \emoji{birthday}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Review: Sequences}

    An \alert{infinite sequence} or simply \alert{sequence} is \emph{a list of numbers in a specific order}. 

    For example, the following is a sequence:
    \begin{equation*}
        a_0, a_1, a_2, a_3, \dots
    \end{equation*}
    We can denote this sequence by
    \begin{equation*}
        \{a_{n}\}_{n \in \mathbb{N}},
        \quad 
        \{a_{n}\},
    \end{equation*}

    \pause{}

    The index of the sequence does not have to start with $0$.

    For example, the following is also a sequence:
    \begin{equation*}
        a_{5}, a_{6}, a_{7}, a_{8}, \dots
    \end{equation*}
    We can denote it by
    \begin{equation*}
        \{a_{n}\}_{n \ge 5}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Sequences and Functions}

    Think of a sequence as a function $f:\{n_{0}, n_{0} + 1, \dots\} \mapsto \dsN$
    where $n_{0}$ is the starting index.

    For instance, the following sequence 
    \begin{equation*}
        0, 1, 2, 3, 4, 5, \dots
    \end{equation*}
    corresponds to the function \( f(n) = n \)
    and we can write it as 
    \begin{equation*}
        \{f(n)\}_{n \in \mathbb{N}}
        =
        \{n\}_{n \in \mathbb{N}}
        .
    \end{equation*}

    \pause{}

    \cake{} Can you guess which function the following sequence corresponds to?
    (Assume the index starts from $0$.)

    \begin{itemize}
        \item $1, 1/2, 1/3, 1/4, \dots$
        \item $0, 2, 6, 12, \dots$
    \end{itemize}
\end{frame}

\subsection{Limits of Sequences}

\begin{frame}
    \frametitle{The limit of \(x/(x+1)\)}

        Recall from calculus that
        \[
        \lim_{{x \to \infty}} \frac{x}{x+1} = 1
        \]
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                scale=0.8
                ]
                \begin{axis}[
                    axis lines = middle,
                    xlabel = \( x \),
                    ylabel = \( x/(x+1)\),
                    ymax = 1.1,
                    samples = 100
                    ]
                    \addplot[blue, thick, domain=0:70] {x/(x+1)};
                    \addplot[red, dashed, domain=0:70] {1};
                \end{axis}
            \end{tikzpicture}
            \caption{Example: $\lim_{{x \to \infty}} \frac{x}{x+1} = 1$}
        \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Review: Limits of Functions}

    A real number \( L \) is called the \alert{limit} of a function \( f(x) \)
    as \( x \) approaches \( \infty \), denoted as
    \[
        \lim_{{x \to \infty}} f(x) = L,
    \]
    if for any given number \( \epsilon > 0 \), there exists a \( N > 0 \) such that
    \[
        \abs{ f(x) - L } < \epsilon
    \]
    whenever \( x > N \).
\end{frame}

\begin{frame}
    \frametitle{Example: Limit of \( x/(x+1) \)}
    
    Thus, by
    \[
        \lim_{{x \to \infty}} \frac{x}{x+1} = 1
    \]
    we mean that
    for any given \( \epsilon > 0 \), there exists a \( N > 0 \) such that
    \[
        \abs{ \frac{x}{x+1} - 1} < \epsilon,
        \qquad \text{for all } x > N.
    \]

    \pause{}

    For example, given $\epsilon = 0.1$, we can take $N = 10$.
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.5,
            ]
            \begin{axis}[
                axis lines = middle,
                xlabel = \( x \),
                ylabel = \( x/(x+1)\),
                ymax = 1.01,
                samples = 100
                ]
                \addplot[blue, thick, domain=10:70] {x/(x+1)};
                \addplot[red, dashed, domain=10:70] {1};
            \end{axis}
        \end{tikzpicture}
        \caption{Example: $\abs{x/(x+1)-1} < 0.1$ when $x > 10$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Limit of sequence}

    A real number \( L \) is called the \alert{limit} of an infinite sequence \( \{ a_n \} \), denoted as
    \[
        \lim_{{n \to \infty}} a_n = L
    \]
    or simply \( a_n \to L \),
    if for any given number \( \epsilon > 0 \), there exists an integer \( N \) such that
    \[
        | a_n - L | < \epsilon
    \]
    for all \( n > N \).

    \pause{}

    A sequence \( \{ a_n \} \) \alert{converges} to \( L \) if \( \lim_{{n \to \infty}} a_n = L \).

    If no such \( L \) exists, the sequence is \alert{divergent}.
\end{frame}

\begin{frame}
    \frametitle{Convergence of Functions and Sequences}

    \begin{exampleblock}{\puzzle{} Exercise}
        Prove that if \( a_n = f(n) \) for all $n \in \mathbb{N}$, then
        \begin{equation*}
            \lim_{{x \to \infty}} f(x) = L
            \imp
            \lim_{{n \to \infty}} a_n = L
        \end{equation*}
    \end{exampleblock}

    \hint{} In other words, a sequence converges if and only if its corresponding function converges.

    \cake{} What is the limit of the sequence
    \begin{equation*}
        \left\{ \frac{n}{n+1} \right\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.1}

    For integers $n ≥ 1$ define $a_n = 2^{-n}$. 
    Find $\lim_{n \to \infty} a_n$ if it exists.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[scale=0.9]
            \begin{axis}[
                axis lines = middle,
                xlabel = \( n \),
                ymax = 1.05,
                xmax = 5.5,
                ymin = -0.05,
                legend entries={$\{2^{-n}\}$, $f(x)=2^{-x}$},
                ]
                % Original plot
                \addplot[ycomb, blue, thick, mark=*, domain=0:5, samples=6] {2^(-x)};

                % Red curve for 2^(-x)
                \addplot[red, thick, smooth, domain=0:5, samples=100] {2^(-x)};
            \end{axis}
        \end{tikzpicture}
        \caption{The sequence $\{2^{-n}\}$}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{A function that diverges to infinity}
%
%    The function \( f(x) \) is said to \alert{diverges to \(\infty\)} as \( x \) 
%    approaches \( x_{0} \), denoted as
%    \[
%        \lim_{{x \to x_0}} f(x) = \infty
%    \]
%    or simply \( f(x) \to \infty \),
%    if for any given real number \( M \), there exists a \( \delta > 0 \) such that
%    \[
%        f(x) > M
%    \]
%    whenever \( 0 < | x - x_0 | < \delta \).
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example: \( f(x) = 1/|x| \)}
%
%    Recall from calculus that
%    \[
%    \lim_{{x \to 0}} \frac{1}{|x|} = \infty
%    \]
%    \begin{figure}[htpb]
%        \centering
%        \begin{tikzpicture}[
%            scale=0.8
%            ]
%            \begin{axis}[
%                axis lines = middle,
%                axis equal, % This ensures the aspect ratio is 1
%                xlabel = \( x \),
%                ylabel = \( f(x) \),
%                xtick = {-3, -2, -1, 0, 1, 2, 3},
%                ytick = {1, 2, 3, 4},
%                ymin = 0,
%                ymax = 5,
%                xmin = -4,
%                xmax = 4,
%                samples = 100
%                ]
%                \addplot[blue, domain=-4:-0.1] {1/abs(x)};
%                \addplot[blue, domain=0.1:4] {1/abs(x)};
%            \end{axis}
%        \end{tikzpicture}
%        \caption{Example: A function \( f(x) \) which has a limit \( \infty \) as \( x \to 0 \).}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Review: A Function that Has the Limit of Infinity}

    Given a function \( f(x) \), 
    we say that it \alert{has the limit \(\infty\)} as \( x \to \infty\),
    denoted as
    \[
        \lim_{{x \to \infty}} f(x) = \infty
    \]
    or simply \( f(x) \to \infty \),
    if for any \( M > 0\), 
    there exists \( N > 0\) such that
    \[
        f(x) > M, \qquad \text{for all } x > N,
    \]

    \bomb{} In this case, $f(x)$ is actually \emph{divergent}.
\end{frame}

\begin{frame}
    \frametitle{A Sequence that Has the Limit of Infinity}

    Given a sequence \( \{a_n\} \), 
    we say that $\{a_n\}$ \alert{has the limit \(\infty\)} as \( n \to \infty\),
    denoted as
    \[
        \lim_{{n \to \infty}} a_n = \infty
    \]
    or simply \( a_n \to \infty \),
    if for any \( M > 0\), 
    there exists \( N > 0\) such that
    \[
        a_n > M, \qquad \text{for all } n > N,
    \]

    \bomb{} In this case, $\{a_n\}$ is actually \emph{divergent}.
\end{frame}

\begin{frame}
    \frametitle{Review: L'Hôpital's Rule}

    If $f$ and $g$ are differentiable functions and
    \begin{equation*}
        \lim_{x \to x_{0}} \frac{f(x)}{g(x)}
        = \frac{0}{0} \quad \text{or} \quad \frac{\pm \infty}{\pm \infty},
    \end{equation*}
    then
    \[
        \lim_{{x \to \infty}} \frac{f(x)}{g(x)} = \lim_{{x \to \infty}} \frac{f'(x)}{g'(x)}
    \]
    if \ac{rhs} limits exist, where $x_{0} \in \dsR \cup \{\infty, -\infty\}$.

    \pause{}

    \cake{} How to use L'Hôpital's rule to show that
    \[
        \lim_{{x \to 0}} \frac{\sin(x)}{x} = 1
    \]

    \bomb{} You do need to remember the derivatives of common functions. \emoji{wink}
\end{frame}

\begin{frame}{When the Limit of Derivatives Does not Exist}
    It is possible that $\lim_{{x \to \infty}} \frac{f'(x)}{g'(x)}$ does not exist, 
    yet $\lim_{{x \to \infty}} \frac{f(x)}{g(x)}$ does exist. 

    For example,
    \begin{equation*}
        \lim_{x \to 0} \frac{\sin(x)+x}{x} = 2
    \end{equation*}
    but
    \begin{equation*}
        \lim_{x \to 0} \frac{\cos(x)+1}{1}
        =
        \lim_{x \to 0} \cos(x) + 1
    \end{equation*}
    does not exist.

    \cake{} Can you think of another example?
\end{frame}

\begin{frame}
    \frametitle{Example 9.2}
    For integers $n ≥ 0$ define $a_n = \frac{2n+1}{3n-2}$. 
    Find $\lim_{n \to \infty} a_n$ if it exists.
\end{frame}

\begin{frame}
    \frametitle{Example 9.3}
    For integers $n ≥ 0$ define $a_n = \frac{e^n}{3n+2}$. 
    Find $\lim_{n \to \infty} a_n$ if it exists.
\end{frame}

\begin{frame}
    \frametitle{Example 9.4}
    Let $\{F_{n}\}_{n \in \dsN}$ be Fibonacci sequence,
    where $F_0 = 0$ $F_1 = 1$ and
    \begin{equation*}
        F_n = F_{n-1} + F_{n-2}, \qquad \text{for all } n \ge 2
    \end{equation*}

    Let $a_{n} = F_{n}/F_{n-1}$ for $n \geq 2$.

    \pause{}

    It it well-known that $\lim_{n \to \infty} a_n$ exists. (\puzzle{} Try to prove it!)

    Let's find what it is.
\end{frame}

\begin{frame}
    \frametitle{\zany{} Golden Ratio}
    The follow number is called the \alert{Golden Ratio}:
    \begin{equation*}
        \phi = \frac{1+\sqrt{5}}{2}
        \approx
        1.618
    \end{equation*}

    A golden rectangle has width $a+b$ and height $a$ such that
    \begin{equation*}
        \frac{a+b}{a} = \frac{a}{b} = \phi
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.3\textwidth]{golden-ratio.png}
        \caption{A golden rectangle. Source:
        \href{https://en.wikipedia.org/wiki/Golden\_ratio}{Wikipedia}}
    \end{figure}
\end{frame}

\subsection{Series}

\begin{frame}
    \frametitle{How to Write a Sum}
    We can write a sum using the $\Sigma$ notation as follows:
    \begin{equation*}
        a_{n} + a_{n+1} + \dots + a_{m} = \sum_{i = n}^{m} a_{i}
    \end{equation*}
    This can be read in English as:
    \begin{itemize}
        \item The sum of \( a_i \) from \( i \) equals \( n \) to \( m \).
        \item Summation of \( a_i \) as \( i \) runs from \( n \) to \( m \).
    \end{itemize}
    \pause{}
    \cake{}
    How can we read the following sum?
    \begin{equation*}
        1 + 2 + 3 + \dots + n = \sum_{i=1}^{n} i
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Series}

    An \alert{infinite series} or simply \alert{series} is the sum of 
    an infinite sequence.

    If the sequence is $\{a_{n}\}_{n \in \mathbb{N}}$, the corresponding series can
    be written as
    \begin{equation*}
        \sum_{n=0}^{\infty} a_{n}
    \end{equation*}
    or simply $\sum a_{n}$.

    \dizzy{} How can we possibly add infinity many numbers?
    How to make sense of
    \begin{equation*}
        \sum_{n=1}^{\infty} 2^{-n}
        =
        1
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Partial Sums and Convergent Series}

    The \alert{partial sums} of a sequence $\{a_{n}\}_{n \in \mathbb{N}}$, 
    is another sequence $\{s_n\}_{n \in \mathbb{N}}$ defined by
    \begin{equation*}
        s_n = \sum_{k=0}^{n} a_k = a_{0} + a_{1} + \dots + a_{n}
    \end{equation*}

    \pause{}

    If $\{s_n\}_{n \in \mathbb{N}}$ converges,
    then we say that the series $\sum a_n$ is
    \alert{convergent} and let
    \begin{equation*}
        \sum_{n=0}^{\infty} a_n = \lim_{n \to \infty} s_n
    \end{equation*}

    Otherwise, we say that the series is \alert{divergent}.

    \cake{} Is $\sum_{n=0}^{\infty} 1$ convergent?
\end{frame}

\begin{frame}
    \frametitle{Example of Convergent Series: Geometric Progression}

    For $a \ne 0$ and $\abs{r} < 1$, the
    \alert{geometric progression} $\sum_{n=0}^{\infty} a r^{n}$
    is convergent:
    \begin{equation*}
        a + a r + a r^{2} + a r^{3}
        +
        \dots
        =
        \sum_{n=0}^{\infty} a r^{n} = \frac{a}{1-r}\tag{9.6}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.5}

    \cake{} What is the value of the following geometric progression?
    \begin{equation*}
        \frac{1}{2} + \frac{1}{4} + \dots
        =
        \sum_{n=1}^{\infty} \frac{1}{2^n}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.6}

    Write the repeating decimal $0.\overline{17} = 0.17171717\dots$ as a rational number.
    
    \hint{} Convert it to a geometric progression.
\end{frame}

%\subsection{Continued Fractions}
%
%\begin{frame}
%    \frametitle{Continued Fractions}
%
%    A \alert{continued fraction} is an expression of the form:
%    \[
%        a_0 + \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{1}{a_3 + \cdots}}}
%    \]
%
%    It provides an efficient way to approximate irrational numbers.
%
%    Application includes number theory and optimization.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example: Gold Ratio}
%
%    Show that the golden ratio \(\phi = \frac{1+\sqrt{5}}{2}\) can be represented as:
%    \[
%        \phi = 1 + \cfrac{1}{1 + \cfrac{1}{1 + \cfrac{1}{1 + \cdots}}}
%    \]
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example: Square Root of \(2\)}
%
%    Show that
%    \[
%        \sqrt{2} = 1 + \cfrac{1}{2 + \cfrac{1}{2 + \cfrac{1}{2 + \cdots}}}
%    \]
%
%    \laughing{} If all \emoji{computer} were to disappear,
%    we can still compute $\sqrt{2}$ by \emoji{pencil} using continued fractions.
%\end{frame}
%
%\begin{frame}
%    \frametitle{\tps{}}
%
%    Can you guess what is the number
%    \[
%        1 + \cfrac{3}{2 + \cfrac{3}{2 + \cfrac{3}{2 + \cdots}}}
%    \]
%\end{frame}
%
%\appendix{}
%
%\begin{frame}[c]
%    \frametitle{Assignment}
%
%    \begin{columns}[c]
%        \begin{column}{0.5\textwidth}
%            \exercisepic{\lecturenum}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            \courseassignment{}
%
%            \acf{ec}
%            \begin{itemize}
%                \item Exercise 9.1: 1, 2, 3, 5, 7, 9, 11, 13, 14, 15, 20, 24.
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}
%
%%\begin{frame}
%%    \frametitle{\emoji{clap} The end of graph theory module!}
%%
%%    \begin{figure}[htpb]
%%        \centering
%%        \includegraphics[width=0.8\textwidth]{seven-bridges-map.png}
%%        \caption{Where Graph Theory Started}
%%    \end{figure}
%%\end{frame}

\end{document}
