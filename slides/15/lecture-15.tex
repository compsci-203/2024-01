\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum --- Generating Function (1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c, label=current]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Section 8.1-8.2 until before Example 8.5.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section Section 8.8: 1, 2, 3, 4, 5.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Flajolet and Analytic Combinatorics}

    The classic textbook in generating functions is 
    Analytic Combinatorics (Flajolet and Sedgewick, 2007).
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\textwidth]{Analytic-Combinatorics.png}
        \hfil
        \includegraphics[width=0.35\textwidth]{PhilippeFlajolet.jpg}
        \caption{Philippe Flajolet (1948-2011) and Analytic Combinatorics}
    \end{figure}
\end{frame}

\section{\acs{ac} 8.1 Basic Notion and Terminology}

\subsection{\Acp{gf} are not functions}

\begin{frame}{What is a \acf{gf}}
    The \alert{generating function} 
    of a sequence \(\{a_{n}\}_{n \ge 0} = a_{0},a_{1},\dots\) is the \alert{power series}
    \begin{equation*}
        F(x)=\sum_{n = 0}^{\infty} a_{n} x^{n}
    \end{equation*}

    \only<1>{%
        \bomb{} The choice of the symbol $x$ is arbitrary. The following are the \emph{the
        same} \acp{gf}:
        \begin{equation*}
            \sum_{n = 0}^{\infty} a_{n} x^{n},
            \qquad
            \sum_{n = 0}^{\infty} a_{n} y^{n},
            \qquad
            \sum_{n = 0}^{\infty} a_{n} t^{n},
            \qquad
            \sum_{n = 0}^{\infty} a_{n} \temoji{clown-face}^{n}
        \end{equation*}
    }%

    \only<2>{%
        \astonished{}
        A \acf{gf} such as \(F(x)\) is \emph{not} a function!

        We will \alert{never} ask questions about \acp{gf} like:
        \begin{itemize}
            \item What is the value $F(1/2)$?
            \item What is the radius of convergence of $F(x)$?
            \item What is the codomain of $F(x)$?
            \item \dots
        \end{itemize}
    }%
\end{frame}

\begin{frame}
    \frametitle{Examples of \ac{gf}}

    \cake{}
    What are the \acp{gf} for the following sequences?
    \begin{itemize}
        \setlength\itemsep{3em}
        \item $a_{0} = 1$, and $a_{n} = 0$ for $n \ge 1$?

        \item $b_{0} = 1$, $b_{1} = -1$, and $b_{n} = 0$ for $n \ge 2$?

        \item $c_{n} = 1$ for all $n \ge 0$?

        \item $d_{n} = 2^{n}$ for all $n \ge 0$?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The Advantage of Generating Functions}
    
    Consider the power series
    \begin{equation*}
        F(x) = \sum_{n = 0}^{\infty} n! x^{n}
    \end{equation*}
    \cake{} What is its interval of convergence?

    \pause{}

    \weary{} If we treat it as a function, then it is the same
    as $F(0) = 0$. This is not an interesting function.

    \smiling{} But if we treat $F(x)$ as a \ac{gf}, 
    then we can still use it to solve some combinatorial
    problems.

    \pause{}

    Because \acp{gf} are not functions, we have to define how to do algebra 
    (\emoji{plus}\emoji{minus}\emoji{multiply}\emoji{divide}\dots) with them.
\end{frame}

\subsection{Algebra of \acp{gf}}

\begin{frame}
    \frametitle{Add \Acp{gf}}
    
    Given two \acp{gf} 
    \begin{equation*}
        A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}, 
        \qquad
        B(x) = \sum_{n=0}^{\infty} b_{n} x^{n},
    \end{equation*}
    we \emph{define}
    \begin{equation*}
        A(x)+ B(x) = \sum_{n=0}^{\infty} (a_{n} + b_{n}) x^{n}
        .
    \end{equation*}

    \only<1>{
        \cake{} What is $\sum_{n=0}^{\infty} x^{n} + \sum_{n=0}^{\infty} (-1)^{n} x^{n}$?
    }
    \only<2>{
        \cake{} How to define $A(x)-B(x)$?
    }
\end{frame}

%\begin{frame}
%    \frametitle{Multiply polynomials}
%    Consider how we \emoji{multiply} polynomials.
%
%    \cake{} Can you expand
%    \begin{flalign*}
%        \quad (1-x)(1+x+x^{2}) = &&
%    \end{flalign*}
%\end{frame}

\begin{frame}
    \frametitle{Multiply \acp{gf}}
    Given two \acp{gf} 
    \begin{equation*}
        A(x) = {\color{red} \sum_{n=0}^{\infty} a_{n} x^{n}}, 
        \qquad
        B(x) = {\color{blue} \sum_{n=0}^{\infty} b_{n} x^{n}},
    \end{equation*}
    we \emph{define}
    \begin{equation*}
        \begin{aligned}
            A(x)B(x) 
            &
            = 
            \left( {\color{red} \sum_{n=0}^{\infty} a_{n} x^{n}} \right)
            \left( {\color{blue} \sum_{n=0}^{\infty} b_{n} x^{n}}\right)
            \\
            &
            =
            \sum_{n=0}^{\infty} \left(
                {\color{red} a_{0} x^{0}}
                {\color{blue} \blankveryshort{}} 
                +
                {\color{red} a_{1} x^{1}}
                {\color{blue} \blankveryshort{}} 
                +
                \cdots
                +
                {\color{red} a_{n} x^{n}}
                {\color{blue} \blankveryshort{}} 
            \right)
            \\
            &
            =
            \sum_{n=0}^{\infty} \left(\sum_{k=0}^{n} {\color{red} a_{k} x^{k}}
            {\color{blue} \blankveryshort{}} \right)
            \pause{}
            = 
            \sum_{n=0}^{\infty} 
            \left(
                \sum_{k=0}^{n} 
                \textcolor{red}{a_{k}} 
                \textcolor{blue}{\blankveryshort{}}
            \right) x^{n}
            .
        \end{aligned}
    \end{equation*}

    \cake{} How should we define $A(x)^{n}$?
\end{frame}

\begin{frame}
    \frametitle{Multiplicative Inverse of \ac{gf}}
    For a real number $x$, its \alert{multiplicative inverse},
    denoted by $x^{-1}$ or $\frac{1}{x}$, is the real number $y$ which satisfies
    \begin{equation*}
        x y = 1
    \end{equation*}
    In other words,
    \begin{equation*}
        x y = 1
        \iff \frac{1}{x} = x^{-1} = y
    \end{equation*}

    \pause{}

    The \alert{multiplicative inverse} of $F(x)$, denoted by $F(x)^{-1}$ or $1/F(x)$,
    is the \ac{gf} $G(x)$ which satisfies
    \begin{equation*}
        \blankshort{} = 1
    \end{equation*}
    In other words,
    \begin{equation*}
        \blankshort{} = 1
        \iff \frac{1}{F(x)} = F(x)^{-1} = G(y)
    \end{equation*}

\end{frame}

\begin{frame}{Example 8.1}
    Consider the \ac{gf}
    \begin{equation*}
        F(x) = \sum_{n=0}^{\infty} x^{n} = 1 + x + x^{2} + x^{3} + \cdots
    \end{equation*}
    Show that $F(x)^{-1} = \frac{1}{F(x)} = 1 - x$.
\end{frame}

\begin{frame}
    \frametitle{The Transfer Principle/Uniqueness of Power Series}

    Let $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ and $B(x) = \sum_{n=0}^{\infty} b_{n}
    x^{n}$ be two power series which converge on
    a non-empty neighbourhood $U$ of zero.

    If we have
    \begin{equation*}
        A(x)=B(x), 
        \qquad \text{for all $x \in U$}
    \end{equation*}
    then $a_n = b_n$ for all $n \in \dsN$.

    \pause{}

    Proof: Let $C(x) = \sum_{n=0}^{\infty} (a_{n} - b_{n}) x^{n}$. Then
    \begin{equation*}
        C(x) = 0
        \qquad \text{for all $x \in U$}
    \end{equation*}
    Thus $C^{(n)}(x) = 0$ for all $n \in \dsN$.

    \pause{}
    By Taylor's formula, we have
    \begin{equation*}
        C(x) = 0 + 0 x + 0 x^2 + 0 x^3 + \cdots
    \end{equation*}
    Therefore $a_n - b_n = 0$ for all $n \in \dsN$.
\end{frame}

%\begin{frame}
%    \frametitle{Example: Applying Transfer Principle}
%
%    By (9.6) from \acf{ec}, we have
%    \begin{equation*}
%        \sum_{n=0}^{\infty} x^{n}
%        =
%        \frac{1}{1-x}
%        ,
%        \qquad
%        \text{for all $x \in (-1,1)$}
%    \end{equation*}
%    Thus
%    \begin{equation*}
%        (1-x) \sum_{n=0}^{\infty} x^{n}
%        =
%        1
%        ,
%        \qquad
%        \text{for all $x \in (-1,1)$}
%    \end{equation*}
%
%    \pause{}
%
%    Then by the transfer principle, we have an identity for \acp{gf}:
%    \begin{equation*}
%        (1-x) \sum_{n=0}^{\infty} x^{n}
%        =
%        1
%    \end{equation*}
%    which means
%    \begin{equation*}
%        \frac{1}{\sum_{n=0}^{\infty} x^{n}}
%        =
%        1-x
%        ,
%        \qquad
%        \text{and}
%        \qquad
%        \frac{1}{1-x}
%        =
%        \sum_{n=0}^{\infty} x^{n}
%    \end{equation*}
%\end{frame}

\begin{frame}
    \frametitle{Example 8.2: Derivatives of Power Series}

    By (9.10) from \acf{ec}, we have
    \begin{equation*}
        \sum_{n=0}^{\infty} n x^{n-1}
        =
        \sum_{n=0}^{\infty}
        \dv{x} x^{n}
        =
        \dv{x} \sum_{n=0}^{\infty} x^{n}
        =
        \dv{x}
        \frac{1}{1-x}
        =
        \frac{1}{(1-x)^{2}}
        ,
    \end{equation*}
    for all  $x \in (-1,1)$.

    \pause{}

    Thus, by the transfer principle, we have an identity for \acp{gf}:
    \begin{equation*}
        \sum_{n=0}^{\infty} n x^{n-1}
        =
        \frac{1}{(1-x)^{2}}
    \end{equation*}
    which is the same as
    \begin{equation*}
        \left(\sum_{n=0}^{\infty} n x^{n-1}\right) (1-x)^{2} = 1
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Treating \acp{gf} As Functions}

    From Section~9.5 of \ac{ec}, we have
    \begin{equation*}
        e^{x}
        =
        \sum_{n=1}^{\infty} \frac{x^{n}}{n!}
        ,
        \qquad
        \text{for all  $x \in \dsR$}
    \end{equation*}
    and
    \begin{equation*}
        \log(1+x)
        =
        \sum_{n=1}^{\infty} (-1)^{n+1}\frac{x^{n}}{n}
        ,
        \qquad
        \text{for all  $x \in (-1,1)$}
    \end{equation*}

    \pause{}

    The transfer principle implies that
    \begin{equation*}
        \exp(\log(1+x)) = 1 + x
    \end{equation*}
    holds both as an identity for functions and for \acp{gf}.

    \pause{}

    \hint{} Thus, we will use the function form of the \ac{gf} whenever
    they are available.
\end{frame}

\begin{frame}[label=example-8.7]
    \frametitle{Example 8.7: A Useful Identity}
    Prove the follow identity for \acp{gf}:
    \begin{equation*}
        \frac{1}{(1-x)^{k+1}}
        =
        \sum_{n = 0}^{\infty} \binom{n+k}{k} x^{n}
        ,
        \qquad
        \text{for all $k \ge 1$}
    \end{equation*}
    
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}
    What is the \ac{gf} of
    \begin{equation*}
        \left\{ \sum_{k=1}^{n-1} \frac{1}{k (n-k)} \right\}_{n \ge 1}
    \end{equation*}
    Answer with a function.

    \pause{}

    Answer: Since
    \begin{equation*}
        -\log(1-x)
        =
        \sum_{n=1}^{\infty} \frac{x^{n}}{n}
        ,
        \qquad
        \text{for all  $x \in (-1,1)$}
    \end{equation*}
    we have
    \begin{equation*}
        \log(1-x)^{2}
        =
        \left(\sum_{n=1}^{\infty} \frac{x^{n}}{n}\right)^{2}
        =
        \sum_{n=1}^{\infty} \sum_{k=1}^{n-1} \frac{1}{k (n-k)} x^{n}
        ,
        \qquad
        \text{for all  $x \in (-1,1)$}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}

    What are the \acp{gf} of the following sequences?
    \begin{equation*}
        \{2^{n+1}\}_{n \ge 0},
        \qquad
        \{n 2^{n+1}\}_{n \ge 1},
        \qquad
        \{n^{3}\}_{n \ge 1},
    \end{equation*}
    The answers should be in function form.

    \hint{} For the third one, use \hyperlink{example-8.7}{Example 8.7}.

    \pause{}

    Answer:
    \begin{equation*}
        \frac{2}{1- 2 x},
        \qquad
        \frac{4 x}{(1 - 2 x)^2},
        \qquad
        \frac{x \left(x^2+4 x+1\right)}{(1-x)^4}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}

    What is $b_{n}$ if
    \begin{equation*}
        \sum_{n=0}^{\infty} b_{n} x^n
        =
        \left(\sum_{n=0}^{\infty} \frac{1}{n!} x^{n}\right)
        \left(\sum_{n=0}^{\infty} \frac{1}{n!} x^{n}\right)
    \end{equation*}

    \pause{}

    Answer:
    Since
    \begin{equation*}
        \sum_{n=0}^{\infty} b_{n} x^n
        =
        e^{x} e^{x}
        =
        e^{2x}
        =
        \sum_{n=0}^{\infty} \frac{(2x)^{n}}{n!}
    \end{equation*}
    we have
    \begin{equation*}
        b_n = \frac{2^{n}}{n!},
        \qquad
        \text{ for all $n \in \dsN$}
    \end{equation*}
\end{frame}

\section{\acs{ac} 8.2 Another look at distributing carrots}

\subsection{Example 8.4: Integer Composition}

\begin{frame}
    \frametitle{One \bunny{}}
    
    Let $a_{n}$ be the number of ways to distribute $n$ \carrot{} among \alert{one}
    \bunny{} so that the \bunny{} has $>0$ \carrot{}.

    Thus
    \begin{equation*}
            a_n = 
        \begin{cases}
            \underline{\hspace{0.5cm}} & (n = 0) \\
            \underline{\hspace{0.5cm}} & (n \ge 1) \\
        \end{cases}
    \end{equation*}

    The \ac{gf} of $(a_{n})_{n \ge 0}$ is
    \begin{flalign*}
        \quad
        A(x)
        =
        \sum_{n \ge 0} a_{n} x^n
        =
        &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{Five \bunny{}}
    
    Let $b_{n}$ be the number of ways to distribute $n$ \carrot{} among \alert{five}
    \bunny{}  so that each \bunny{} gets \emph{at least one} \carrot{}.

    Then
    \begin{equation*}
        b_n = 
        \begin{cases}
            {\displaystyle \binom{\blankshort{}}{\blankshort{}}}
            &
            n \ge 5, \\
            \blankshort{} & \text{otherwise}.
        \end{cases}
    \end{equation*}

    \pause{}

    \think{} What is the \ac{gf} of $\{b_{n}\}_{n \ge 0}$?
    \begin{flalign*}
        \quad
        B(x)
        =
        \sum_{n = 0}^{\infty}
        b_{n} x^{n}
        =
        &&
    \end{flalign*}
\end{frame}

\begin{frame}
    \frametitle{A Coincidence?}
    
    Using (Example 8.7)
    \begin{equation*}
        \frac{1}{(1-x)^{k+1}}
        =
        \sum_{n = 0}^{\infty} \binom{n+k}{k} x^{n}
        \qquad
        (\text{for all $k \ge 1$})
        ,
    \end{equation*}
    we have
    \begin{equation*}
        B(x)
        =
        \sum_{n = 5} \binom{n-1}{4} x^{n}
        =
        \sum_{n = 0} \binom{n+4}{4} x^{n+5}
        =
        \frac{x^{5}}{(1-x)^{5}}
        =
        A(x)^{5}
        .
    \end{equation*}
    \astonished{} Is this a coincidence?
\end{frame}

\begin{frame}
    \frametitle{Not a Coincidence!}

    Let's try to see why
    \begin{equation*}
        A(x)^{5}
        =
        \sum_{n \ge 5}
        \sum_{\substack{k_{1},\dots, k_{5} \in \dsN: \\ k_{1}, \dots , k_{5} \ge 1 \\ k_{1} + \dots + k_{5} = n}}
        {\color{red}x^{k_{1}}}{\color{blue}x^{k_{2}}}{\color{orange}x^{k_{3}}}{\color{Teal}x^{k_{4}}}{\color{Maroon}x^{k_{5}}}
    \end{equation*}
    \pause{}
    This implies that
    \begin{equation*}
        A(x)^{5} = \sum_{n=0}^{\infty} b_{n} x^{n} = B(x)
    \end{equation*}
    since $b_n$ is precisely the number of integer solutions of
    \begin{equation*}
        k_{1} + k_{2} + \cdots + k_{5} = n
    \end{equation*}
    with $k_{1}, k_{2}, \dots k_{5} \ge 1$.
\end{frame}

\end{document}
