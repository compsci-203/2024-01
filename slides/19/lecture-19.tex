\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%------------------------------------------------------------------------------
% Loading Optional Packages
%------------------------------------------------------------------------------
\input{../table.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Recurrence Equations (2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 9.4.2
                        \item
                            Section 9.6-9.7
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 9.9: 9, 11, 13, 15, 17.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 9.4 Solving Advancement Operator Equations}

\subsection{Nonhomogeneous Case}

\begin{frame}
    \frametitle{Example 9.14}
    Consider the homogeneous equation
    \begin{equation}
        \label{eq:hom:1}
        (A+2)(A-6) f = 0
    \end{equation}
    \cake{} What is the general solution, let's call it $f_1(n)$, for this equation?
    \[
        f_1(n) = \blankshort{}
    \]

    \think{} What is the general solution for
    \[
        (A+2)(A-6) f = 3^{n}
    \]
\end{frame}

\begin{frame}{Finding One Solution}
    \weary{}
    Unfortunately, no known algorithm guarantees a solution for
    \begin{equation}
        \label{eq:nonhom:1}
        (A + 2)(A - 6) f = 3^n
    \end{equation}
    \smiling{}
    Nevertheless, experience suggests that a solution could closely resemble the right-hand side (RHS):
    \[
        d \cdot 3^n
    \]
    where \(d\) is a constant, independent of \(n\).

    \emoji{pencil} Let's try to find what $d$ should be.
\end{frame}

\begin{frame}{What We Know}
    \smiling{}
    We have found a \alert{general} solution
    \begin{equation*}
        f_1(n) = c_1 \cdot (-2)^{n} + c_2 \cdot 6^{n}
    \end{equation*}
    for the homogeneous linear recurrence
    \begin{equation*}
        (A + 2)(A - 6) f = 0. \tag{\ref{eq:hom:1}}
    \end{equation*}

    \pause{}

    \smiling{}
    We've also found one \alert{particular} solution:
    \[
        f_{2}(n) = -\frac{1}{15} \cdot 3^{n}
    \]
    for the nonhomogeneous linear recurrence
    \begin{equation*}
        (A + 2)(A - 6) f = 3^n. \tag{\ref{eq:nonhom:1}}
    \end{equation*}

    \think{} But what is the general solution of \eqref{eq:nonhom:1}?
\end{frame}

\begin{frame}
    \frametitle{The general solution of \eqref{eq:nonhom:1}}
    \astonished{}
    Therefore, the general solutions of \eqref{eq:nonhom:1} is:
    \[
        f(n) = f_{1}(n) + f_{2}(n) = c_{1} \cdot (-2)^{n} + c_{2} \cdot 6^{n} -
        \frac{1}{15} \cdot 3^{n}
    \]

    \think{} Why is this the case?
\end{frame}

\begin{frame}{\emoji{ramen} A Recipe}
    To solve an nonhomogeneous equation
    \[
        p(A) f = g
    \]
    we can take the following steps:
    \begin{enumerate}
        \item Find the \emph{general} solution \( f_{1} \) for
        \[
            p(A) f = 0
        \]
        \item Find any \emph{particular} solution \( f_{2} \) (typically by guessing) for
        \[
            p(A) f = g
        \]
    \end{enumerate}

    The general solution combines both:
    \[
        f(n) = f_{1}(n) + f_{2}(n)
    \]
\end{frame}

\begin{frame}
    \frametitle{Example 9.15}
    
    Find the solutions to the equation
    \begin{equation*}
        (A + 2)(A − 6) f = 6^n,
    \end{equation*}
    if $f(0) = 1$ and $f(1) = 5$.

    \only<1>{%
        Step 1: \cake{} What is the general solution of
        \begin{equation*}
            (A + 2)(A − 6) f = 0
        \end{equation*}
    }%
    \only<2-3>{%
        Step 2:
        Let's try if $d \alt<3>{n}{}6^{n}$ is a solution for some $d$.
    }%
\end{frame}

\begin{frame}[fragile]{Check Our Solution with \emoji{robot}}
    Use the following
    \href{https://www.wolframalpha.com/input?i=solve+f\%5Bn\%2B2\%5D-4*f\%5Bn\%2B1\%5D-12f\%5Bn\%5D\%3D\%3D6\%5En\%2C+f\%5B1\%5D\%3D\%3D5\%2C+f\%5B0\%5D\%3D\%3D1}{query}
    on Wolfram Alpha
    \begin{small}
        \begin{verbatim}
solve f(n+2)-4*f(n+1)-12 f(n)=6^n, f(1)=5, f(0)=1
        \end{verbatim}
    \end{small}
    we see that the solution is
    \begin{equation*} 
        f(n) = \frac{1}{3} \cdot 2^{n - 6} \left( 4 \cdot 3^n \cdot n + 27 \cdot (-1)^n + 55 \cdot 3^{n + 1} \right) 
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{\laughing{} Can you guess the next problem?}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Prompt ---
            Highly detailed digital painting of a mouse,  a cow, a tiger, a rabbit, a dragon, a snake and a horse on the African prairie by makoto shinkai and krenz cushart.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics<1>[width=\textwidth]{prairie-01.jpg}%
                \includegraphics<2>[width=\textwidth]{prairie-02.jpg}%
                \includegraphics<3>[width=\textwidth]{prairie-03.jpg}%
                \caption*{%
                    Made with
                    \only<1>{%
                        DALL\cdot E in spring 2023
                    }%
                    \only<2>{%
                        DALL\cdot E in autumn 2023
                    }%
                    \only<2>{%
                        DALL\cdot E in spring 2024
                    }%
                }%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 9.16}

    Find \emph{all} solutions to the equation
    \begin{equation*}
        r_{n+1} = r_{n} + n + 1
    \end{equation*}
    with initial conditions \( r_{0} = 1 \).

    \only<1>{%
        Step 1:
        The general solution to the corresponding homogeneous equation is
        \begin{equation*}
            f_{1}(n) = \blankshort{}
        \end{equation*}
    }%
    \only<2>{%
        Step 2:
        Let's try if $d_{1} n +  d_{2}$ is a solution for some $d_{1}$ and $d_{2}$.
    }%
    \only<3>{%
        Step 2:
        Let's try if $d_{1} n^{2} +  d_{2}n$ is a solution for some $d_{1}$ and $d_{2}$.
    }%
    \only<4>{%
        How should we choose $c_{1}$?
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 9.17}

    Find \emph{all} solutions to the equation
    \begin{equation*}
        (A-2)^{2} f = 3^{n} + 2 n
    \end{equation*}

    \only<1>{%
        Step 1:
        The general solution to the corresponding homogeneous equation is
        \begin{equation*}
            f_{1}(n) = \blankshort{}
        \end{equation*}
    }%
    \only<2>{%
        Step 2:
        Let's try if $d_{1} 3^n +  d_{2}n$ is a solution for some $d_{1}$ and $d_{2}$.
    }%
    \only<3>{%
        Step 2:
        Let's try if $d_{1} 3^n +  d_{2}n + d_{3}$ is a solution for some $d_{1}$, $d_{2}$ and $d_{3}$.
    }%
\end{frame}

\begin{frame}
    \frametitle{How to Make a Guess?}

    \begin{table}[htpb]
        \centering
        \begin{tabular}{c | c}
            \hline
            \acs{rhs} of recurrence & Guesses \\
            \hline
            $r^{n}$ & \makecell{$d r^{n}$ \\ $d n r^{n}$} \\
            \hline
            $r_{1}^{n} + r_{2}^{n}$ &
            \makecell{%
                $d_{1} r_{1}^{n} + d_{2} r_{2}^{n}$ \\
                $d_{1} n r_{1}^{n} + d_{2} r_{2}^{n}$ \\
            }%
            \\
            \hline
            $a n + b$ & \makecell{$d_1 n + d_2$ \\ $d_1 n^{2} + d_2 n + d_3$} \\
            \hline
            $r^{n} + b n$ & 
            \makecell{%
                $d_1 r^{n} + d_{2} n + d_{3}$ \\ 
                $d_1 r^{n} + d_{2} n^{2} + d_{3}n + d_{4}$ \\
            }% 
            \\
            \hline
        \end{tabular}
        \caption{An incomplete summary of the guesses.}
    \end{table}
    
    \cake{} What is your guess for $9^{n} + 3 n^{2}$?
\end{frame}

%\begin{frame}{\zany{} Unique solutions for linear recurrences}
%    Consider a linear recurrence of order \( k \) of the form
%    \[
%        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \ldots + c_{k} a_{n} = g(n),
%        \quad \text{for all } n \ge 0,
%    \]
%    with initial conditions \( a_{0}, a_{1}, \ldots, a_{k-1} \).
%
%    \cake{} How many unique solutions exist for this linear recurrence?
%\end{frame}

%\begin{frame}
%    \frametitle{Towers of Hanoi puzzle}
%
%    These disks are to be transferred, one at a time, onto another peg, 
%    such that at no time is a larger disk is on top of a smaller one. 
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=\textwidth]{Tower_of_Hanoi.jpg}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{The recurrent equation for towers of Hanoi}
%    
%    Let $h_n$ be the minimal number of moves needed.
%
%    Find a recursions for $h_{n}$.
%
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Can you solve the recursion
    \begin{equation*}
        h_{n+1} = 2 h_{n} + 1, \qquad \text{for all } n \ge 1
    \end{equation*}
    with the initial condition $h_1 = 1$.

    \hint{} This recursion comes from
    \href{https://en.wikipedia.org/wiki/Tower\_of\_Hanoi}{Towers of Hanoi}
    problem.
\end{frame}

%\section{\zany{} \acs{ac} 9.5 Formalizing our approach to recurrence equations}
%
%\subsection{Homogeneous Equations}
%
%\begin{frame}
%    \frametitle{Polynomial roots and the advancement operator}
%
%    Given a polynomial \( p(A) \) of degree \( k \), if there exist \( r_1, \dots,
%    r_k \) in \( \mathbb{C}\) (complex numbers) such that
%    \[
%        p(A) = (A - r_{1}) \dots (A - r_{k}),
%    \]
%    then these numbers \( r_1, \dots, r_k \) are called the \alert{roots} of \( p(A) \).
%
%    \begin{block}{\emoji{star} The fundamental theorem of algebra}
%        A polynomial of degree \( n \) has \( n \) roots in \( \mathbb{C} \).
%        \href{https://en.wikipedia.org/wiki/Fundamental\_theorem\_of\_algebra}{Proof}.
%    \end{block}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Examples of polynomial roots}
%
%    Some polynomials have \emph{distinct} roots:
%    \begin{itemize}
%        \item \( x^2 + x - 6 = (x + 3)(x - 2) \)
%        \item \( x^2 + 1 = (x + \mathbb{i})(x - \mathbb{i}) \)
%    \end{itemize}
%
%    Some have \emph{multiple} roots:
%    \begin{itemize}
%        \item \( x^2 + 2x + 1 = (x + 1)^{2} \)
%    \end{itemize}
%
%\end{frame}
%
%\begin{frame}
%    \frametitle{High school math}
%
%    Recall that the two roots of
%    \begin{equation*}
%        x^2 + b x+c
%    \end{equation*}
%    are
%    \begin{equation*}
%        \frac{-b \pm \sqrt{b^2-4 c}}{2}
%    \end{equation*}
%
%    \cake{} What are the roots of
%    \begin{equation*}
%        x^2 + 10 x + 21
%    \end{equation*}
%\end{frame}
%
%\subsection{When $p(A)$ has \emph{distinct} roots}
%
%\begin{frame}
%    \frametitle{Theorem 9.21 (AC)}
%
%    Let \( p(A) \) be a polynomial with \emph{distinct} roots \( r_{1}, \dots,
%    r_{n} \). Then, for any constants \( c_1, \dots, c_k \) in \( \mathbb{R} \),
%    the expression
%    \begin{equation*}
%        c_{1} r_{1}^{n} + c_{2} r_{2}^{n} + \cdots + c_{k} r_{k}^{n}
%    \end{equation*}
%    is a solution to \( p(A) f = 0 \).
%
%    In other words, 
%    \begin{equation*}
%        p(A) (c_{1} r_{1}^{n} + c_{2} r_{2}^{n} + \cdots + c_{k} r_{k}^{n}) = 0, 
%        \qquad (\text{for all } n \in \mathbb{N})
%        .
%    \end{equation*}
%
%    Moreover, \alert{every solution} to \( p(A) f = 0 \) can be written in this form.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example 9.10}
%    
%    Can you find the solution for
%    \begin{equation*}
%        t_{n+2} = 3 t_{n+1} - t_n, \qquad \text{ for } n \ge 1,
%    \end{equation*}
%    which satisfies both the initial conditions
%    \begin{equation*}
%        t_1 = 3, \qquad t_2 = 8.
%    \end{equation*}
%\end{frame}
%
%\begin{frame}[fragile]
%    \frametitle{\zany{} \emoji{robot} to help}
%    
%    Such problem can (of course) be solved with WolframAlpha with 
%    the \href{https://www.wolframalpha.com/input?i=solve+t\%5Bn+\%2B+2\%5D+\%3D\%3D+3+t\%5Bn+\%2B+1\%5D+-+t\%5Bn\%5D\%2C+t\%5B1\%5D\%3D\%3D3\%2C+t\%5B2\%5D\%3D\%3D8}{query}
%    \begin{small}
%\begin{verbatim}
%solve t[n+2]==3*t[n+1] - t[n], t[1]==3, t[2]==8
%\end{verbatim}
%    \end{small}
%
%    And the result is
%    \begin{equation*}
%    t(n) = -\frac{2 \sqrt{5} \left(\frac{3}{2} - \frac{\sqrt{5}}{2}\right)^n - 15 \left(\frac{3}{2} + \frac{\sqrt{5}}{2}\right)^n - 7 \sqrt{5} \left(\frac{3}{2} + \frac{\sqrt{5}}{2}\right)^n}{5 (3 + \sqrt{5})}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}[t]
%    \frametitle{\tps{}}
%
%    Consider the following recurrence
%    \begin{equation*}
%        t_n = t_{n-1} + t_{n-2} \qquad (n \ge 2).
%    \end{equation*}
%
%    Write this recurrence with advanced operator $A$ and
%    find all solutions for this recurrence equation.
%\end{frame}
%
%\begin{frame}[t]
%    \frametitle{Initial conditions}
%
%    $t_n$ can be seen as the number of ways to tile a $2 \times n$ chessboard
%    using only dominoes \tikz[scale=0.3]{\drawtile{1}{2}} (rotation allowed).
%    \begin{figure}[htpb]
%    \begin{center}
%        \begin{tikzpicture}[scale=1, transform shape]
%            \drawchessboard{2}{5}
%            \end{tikzpicture}
%    \end{center}
%    \caption{Example of tiling a chessboard with \tikz[scale=0.3]{\drawtile{1}{2}}}
%    \end{figure}
%
%    In other words, we have the following initial conditions:
%    \begin{equation*}
%        t_{0}=1, \qquad t_{1}=1.
%    \end{equation*}
%
%    \puzzle{} Find $(t_{n})_{n \ge 0}$ which that satisfies both these conditions
%    and the recurrence $t_n = t_{n-1} + t_{n-2}$?
%\end{frame}
%
%\subsection{When $p(A)$ has \emph{multiple} roots}
%
%\begin{frame}
%    \frametitle{Example 9.12}
%    
%    Can we find all solutions to
%    \begin{equation*}
%        (A − 2)^2 f = 0.
%    \end{equation*}
%
%    \cake{} Why $c_1 2^n + c_2 2^n$ \emph{probably} does not give all the solutions?
%    
%    \think{} What if we try $c_2 n 2^n$?
%
%    \pause{}
%
%    \cake{} Can you guess the general solution for
%    \begin{equation*}
%        (A-2)^3 f = 0
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Dealing with multiple roots}
%    
%    \cake{} Observing that
%    \begin{equation*}
%        (A-r) (r^n) = 0, \quad
%        (A-r)^2 (n r^n) = 0, \quad
%        (A-r)^3 (n^{2} r^n) = 0, \dots
%    \end{equation*}
%    can you guess that the general solution of
%    \begin{equation*}
%        (A - r)^{p} f = 0
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Example 9.13}
%
%    Can you find the solution for
%    \begin{equation*}
%        (A+5)(A-1)^3 f = 0
%    \end{equation*}
%    which satisfies both the initial conditions
%    \begin{equation*}
%        f_{0} = 1, f_{1} = 2, f_{2} = 4, f_{3} = 4.
%    \end{equation*}
%\end{frame}
%
%%\begin{frame}
%%    \frametitle{\tps{}}
%%
%%    Let $d_{n}$ be the number of strings of length $n$ on the alphabet 
%%    $\{\temoji{apple}, \temoji{banana}, \temoji{cherries}\}$ with even numbers of
%%    \emoji{cherries}.
%%
%%    Can you explain why
%%    \begin{equation*}
%%        d_{n + 1} = 2 d_n + (3^{n-1} - d_{n}),
%%        \qquad (n \ge 1)
%%    \end{equation*}
%%
%%    \hint{} We will learn how to deal with nonhomogeneous linear recurrence in
%%    the next lecture.
%%\end{frame}
%
%%\begin{frame}
%%    \frametitle{Linear Algebra in 5 minutes}
%%    
%%    This section contains what you should remember after a 7-week 
%%    Linear Algebra course.
%%
%%    \begin{figure}[htpb]
%%        \centering
%%        \includegraphics[width=0.5\textwidth]{vector-space.png}
%%    \end{figure}
%%\end{frame}
%%
%%\begin{frame}[c]
%%    \frametitle{Vector space}
%%
%%    \begin{columns}[c]
%%        \begin{column}{0.5\textwidth}
%%            A \alert{vector space}  is a set whose elements, 
%%            often called \alert{vectors}, 
%%            may be added together and multiplied by numbers.
%%        \end{column}
%%        \begin{column}{0.5\textwidth}
%%            \begin{figure}[htpb]
%%                \centering
%%                \includegraphics[width=0.8\textwidth]{vector.png}
%%                \caption*{Vectors}
%%            \end{figure}
%%        \end{column}
%%    \end{columns}
%%\end{frame}
%%
%%\begin{frame}
%%    \frametitle{Linear combination}
%%    
%%    Given $k$ vectors $v_{1}, \dots, v_{k}$, the expression
%%    \begin{equation*}
%%        c_1 v_{1} + c_2 v_{2} + \ldots + c_k v_{k}
%%    \end{equation*}
%%    is called a \alert{linear combination} of these vector,
%%    and $c_{1}, \dots, c_{k}$ are the \alert{weights}.
%%
%%    If the linear combination of 
%%    \begin{equation*}
%%        c_1 v_{1} + c_2 v_{2} + \ldots + c_k v_{k}
%%        =
%%        \mathbf{0}
%%    \end{equation*}
%%    \emph{only} when the weights are all $0$,
%%    they are called \alert{linearly independent}.
%%\end{frame}
%%
%%\begin{frame}
%%    \frametitle{Dimensions}
%%
%%    A vector space $W$ has \alert{dimension} $k$
%%    if we can find at most $k$ \emph{linearly independent}
%%    vectors in $W$.
%%
%%    In such a case, any vector in $W$ is the \emph{linear combination}
%%    of these $k$ vectors.
%%\end{frame}
%
%\begin{frame}{Theorem 9.18}
%    The set of all solutions to an \emph{order-\(k\)} homogeneous linear equation
%    \begin{equation*}
%        \label{eq:9.5.1}
%        (c_0 A^k + c_1 A^{k-1} + c_2 A^{k-2} + \ldots + c_k) f = 0 
%        \tag{9.5.1}
%    \end{equation*}
%    forms a \(k\)-dimensional vector space \(V\).
%
%    \begin{block}{\hint{} Implication}
%        Every solution of \eqref{eq:9.5.1} is a linear combination of \(k\)
%        linearly independent solutions to \eqref{eq:9.5.1}.
%    \end{block}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Review: Theorem 9.21 (\ac{ac})}
%
%    Let \( p(A) \) be a polynomial with \emph{distinct} roots \( r_{1}, \dots,
%    r_{n} \). Then, for any constants \( \temoji{cucumber}_1, \dots, \temoji{cucumber}_k \) in \( \mathbb{R} \),
%    the expression
%    \begin{equation*}
%        \temoji{cucumber}_{1} r_{1}^{n} + \temoji{cucumber}_{2} r_{2}^{n} + \cdots + \temoji{cucumber}_{k} r_{k}^{n}
%    \end{equation*}
%    is a solution to \( p(A) f = 0 \).
%
%    In other words, 
%    \begin{equation*}
%        p(A) (\temoji{cucumber}_{1} r_{1}^{n} + \temoji{cucumber}_{2} r_{2}^{n} + \cdots + \temoji{cucumber}_{k} r_{k}^{n}) = 0, 
%        \qquad (\text{for all } n \in \mathbb{N})
%        .
%    \end{equation*}
%
%    Moreover, \alert{every solution} to \( p(A) f = 0 \) can be written in this form.
%\end{frame}
%
%\begin{frame}
%    \frametitle{Proof of theorem 9.21 for distinct roots}
%
%    First, note that $r_{1}^n, \dots, r_{k}^n$ are solutions to \( p(A) f = 0 \).
%
%    \begin{block}{Linear independence}
%        When $r_1, \dots, r_k$ are distinct roots, the functions \( r_{1}^n, \dots, r_{k}^n \) are linearly independent.
%    \end{block}
%
%    \begin{block}{Forming a basis}
%        By Theorem 9.18,
%        these functions form a basis for the $k$-dimensional vector space of all solutions.
%    \end{block}
%
%    \begin{block}{General solution}
%        Any solution in this space can be represented as a linear combination:
%        \[
%            \temoji{cucumber}_{1} r_{1}^{n} + \temoji{cucumber}_{2} r_{2}^{n} + \cdots + \temoji{cucumber}_{k} r_{k}^{n}
%        \]
%        where \( \temoji{cucumber}_{1}, \dots, \temoji{cucumber}_{k} \) are arbitrary constants.
%    \end{block}
%
%\end{frame}
%
%%\begin{frame}[t]{Example 9.9 again}
%%    Consider
%%    \begin{equation}
%%        \label{eq:homogeneous:1}
%%        p(A) f 
%%        = (A^{2}+A-6) f 
%%        = (A+3)(A-2)f 
%%        = 0
%%    \end{equation}
%%
%%    Its solution space has dimension $\underline{\hspace{1cm}}$.
%%
%%    Note that $2^{n}$ and $(-3)^{n}$ are two linearly independent solutions.
%%
%%    Thus
%%    \begin{itemize}
%%        \item Then \(c_{1} 2^{n} + c_{2} (-3)^{n}\) are also solutions of \cref{eq:homogeneous:1}.
%%        \item \alert{All} solutions are of this form.
%%    \end{itemize}
%%\end{frame}

\section{\acs{ac} 9.6 Using Generating Functions to Solve Recurrences}

\begin{frame}
    \frametitle{Example 9.24: Revisiting an old problem}

    Recall in Example 9.9, we have solved
    \begin{equation*}
        (A^{2} + A - 6) r = 0
    \end{equation*}
    subject to the initial conditions \( r_{0} = 1 \) and \( r_{1} = 3 \).

    \astonished{} This can also be done by using the \ac{gf}
    \begin{equation*}
        \sum_{n = 0}^{\infty} r_{n} x^{n} = \frac{1 + 4x}{1 + x - 6x^2}
    \end{equation*}
\end{frame}


\begin{frame}
    \frametitle{Extract the coefficients}
    
    By a partial fraction decomposition, we have
    \begin{equation*}
        \frac{1 + 4x}{(1 + 3x)(1 - 2x)}
        = \frac{6}{5(1 - 2x)} - \frac{1}{5(1 + 3x)}
    \end{equation*}
    
    \cake{} How find a closed form for \( r_n \) from this?
\end{frame}

\begin{frame}
    \frametitle{Example 9.25}

    Assume that $r_{0}=2$, $r_{1}=1$ and for $n \ge 2$
    \begin{equation*}
        r_{n} − r_{n−1} − 2 r_{n−2} = 2^n
    \end{equation*}
    \think{} How to show that
    \begin{equation*}
        \sum_{n=0}^{\infty} r_{n} x^{n}
        =
        \frac{6x^2 − 5x + 2}{(1 − 2x)(1 − x − 2x^2)}
    \end{equation*}

    \puzzle{} Exercise: Find a closed form for \( r_n \) from this.
    (Verify your answer \href{https://bit.ly/3Swxk20}{here}.)
\end{frame}

\section{\acs{ac} 9.7 Solving a Nonlinear Recurrence}

\begin{frame}[t]
    \frametitle{Full Binary Tree}

    A \alert{full binary tree} is a tree structure in which each node has either
    two children, or no children.

    We call a node without children a \alert{leaf} --- \emoji{leaves}.

    Otherwise we call it an \alert{internal node} --- \tikz{\node[tree node] {}}.

    \begin{figure}
        \centering
        % Tree 1
            \begin{tikzpicture}[
                scale=0.3,
                sibling distance=2.5cm,
                every node/.style={tree node},
                ]
                \node[tree leaf] {};
            \end{tikzpicture}
        \hfil
        % Tree 2
            \begin{tikzpicture}[
                scale=0.3,
                sibling distance=2.5cm,
                every node/.style={tree node},
                ]
                \node {}
                    child {node[tree leaf] {}}
                    child {node[tree leaf] {}};
            \end{tikzpicture}
        \hfil
        % Tree 3
        \begin{tikzpicture}[
            scale=0.3,
            sibling distance=2.5cm,
            every node/.style={tree node},
            ]
            \node {}
                child {node {}
                    child {node[tree leaf] {}}
                    child {node[tree leaf] {}}
                }
                child {node[tree leaf] {}};
        \end{tikzpicture}
        \hfil
        % Tree 4
        \begin{tikzpicture}[
            scale=0.3,
            sibling distance=2.5cm,
            every node/.style={tree node},
            ]
            \node {}
                child {node[tree leaf] {}}
                child {node {}
                    child {node[tree leaf] {}}
                    child {node[tree leaf] {}}
                };
        \end{tikzpicture}
        \caption{Full binary tree of with $1$, $2$ or $3$ \emoji{leaves}}
    \end{figure}

    \pause{}

    Let $c_n$ is the number of full binary trees with $n$ \emoji{leaves}.

    \think{} How to find a closed form for \( c_n \)?
\end{frame}

\begin{frame}
    \frametitle{A Recurrence}
    
    Note that $\{c_n\}_{n \ge 0}$ satisfies
    \begin{equation*}
        c_{n} = \sum_{k=1}^{n-1} c_{k} c_{n-k},
        \qquad \text{for all } n \ge 2.
    \end{equation*}
    \begin{figure}
        \begin{tikzpicture}[
            scale=0.6,
            sibling distance=4.5cm,
            every node/.style={tree node},
            ]
            \node {}
                child {%
                    node[rectangle, draw=none, fill=none, inner sep=0pt]
                    {\emoji{leaves}\emoji{leaves}\dots\emoji{leaves}\emoji{leaves}}
                }%
                child {%
                    node[rectangle, draw=none, fill=none, inner sep=0pt]
                    {\emoji{leaves}\emoji{leaves}\dots\emoji{leaves}\emoji{leaves}}
                };
            \node[draw=none, fill=none] at (-2.5,-2.5) {$k$};
            \node[draw=none, fill=none] at (2.5,-2.5) {$n-k$};
        \end{tikzpicture}
        \caption{A full tree of with $n \ge 2$ leaves}
    \end{figure}
    \pause{}
    Since $c_{0} = 0$, the recursion is actually
    \begin{equation*}
        c_{n} = \sum_{k=0}^{n} c_k c_{n-k}, \qquad \text{for all } n \ge 2.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Step 1: Find the generating function}
    
    Let $C(x) = \sum_{n = 0}^{\infty} c_{n} x^{n}$.
    Then
    \begin{equation*}
        c_{n} = \sum_{k=0}^{n} c_k c_{n-k}, \qquad \text{for all } n \ge 0
    \end{equation*}
    implies
    \begin{equation*}
        C(x) =
        \only<1>{%
            x + C(x)^{2}
        }%
        \only<2>{%
            \frac{1 \pm (1-4 x)^{1/2}}{2}
        }%
    \end{equation*}

    \only<1>{%
        \weary{} This is \emph{not} a linear recurrence equation. 
        There's no general method to solve it.
    }%
\end{frame}

\begin{frame}
    \frametitle{Step 2: Applying Newton's binomial theorem}
    
    \think{}
    How can we apply Newton's binomial theorem to
    \begin{equation*}
        C(x) = \frac{1 \pm (1-4 x)^{1/2}}{2}
    \end{equation*}
    \pause{}%
    \think{} 
    Can we further simplify with $C(x)$ with Lemma 9.27
    \begin{equation}
        \binom{1/2}{k}
        =
        \frac{(-1)^{k-1}}{k}    
        \frac{\binom{2k-2}{k-1}}{2^{2k-1}}
    \end{equation}

    \puzzle{} Exercise: Try to prove Lemma 9.27.
\end{frame}

\begin{frame}
    \frametitle{Step 3: Solving the recurrence}
    \dizzy{} In the equation
    \begin{equation*}
        C(x)
        =
        \frac{1}{2}
        \pm
        \frac{1}{2}
        \mp
        \sum_{n=1}^{\infty} \frac{\binom{2n-2}{n-1}}{n} x^{n},
    \end{equation*}
    how is it possible that there are $\pm$ and $\mp$?

    \pause{}
    \smiling{}
    Since $c_{n} \ge 0$,
    we actually have
    \begin{equation*}
        C(x)
        =
        \sum_{n=1}^{\infty} \frac{\binom{2n-2}{n-1}}{n} x^{n}.
    \end{equation*}
    Thus
    \begin{equation*}
        c_{n} 
        = 
        \frac{1}{n}
        \binom{2n-2}{n-1}
        ,
        \qquad \text{for all } n \ge 1.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Catalan Numbers}
    
    Recall that the Catalan numbers $C(n)$ satisfy
    \begin{equation*}
        C(n) = \frac{1}{n+1} \binom{2n}{n}, \qquad \text{for all } n \ge 0.
    \end{equation*}
    Thus
    \begin{equation*}
        c_{n} 
        = 
        \frac{1}{n}
        \binom{2n-2}{n-1}
        ,
        \qquad \text{for all } n \ge 1
    \end{equation*}
    implies that
    \begin{equation*}
        C(n) = c_{n+1}.
    \end{equation*}

    \astonished{}
    So, $C(n)$ is the number of full binary trees with $n+1$ \emoji{leaves}.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Recall that the Catalan numbers satisfy
    \begin{equation}
        C(n+1) = \sum_{k=0}^{n} C(k) C(n-k), \qquad \text{for all } n \ge 0.
    \end{equation}
    Can you prove this using
    \begin{equation*}
        c_{n} = \sum_{k=0}^{n} c_k c_{n-k}, \qquad \text{for all } n \ge 0
    \end{equation*}
    and that $C(n) = c_{n+1}$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    A \alert{general tree}  is a tree in which each node can have any number of children.
    \begin{figure}
        \centering
        % Tree 1
            \begin{tikzpicture}[
                scale=0.3,
                sibling distance=2.5cm,
                every node/.style={tree node},
                ]
                \node {};
            \end{tikzpicture}
        \hfil
        % Tree 2
            \begin{tikzpicture}[
                scale=0.3,
                sibling distance=2.5cm,
                every node/.style={tree node},
                ]
                \node {}
                    child {node {}};
            \end{tikzpicture}
        \hfil
        % Tree 3
        \begin{tikzpicture}[
            scale=0.3,
            sibling distance=2.5cm,
            every node/.style={tree node},
            ]
            \node {}
                child {node {}}
                child {node {}};
        \end{tikzpicture}
        \hfil
        % Tree 4
        \begin{tikzpicture}[
            scale=0.3,
            sibling distance=2.5cm,
            every node/.style={tree node},
            ]
            \node {}
                child {node {}
                    child {node {}}
                };
        \end{tikzpicture}
        \caption{General trees with $1$, $2$ or $3$ nodes}
    \end{figure}
    
    Let $g_{n}$ be the number of general trees with $n$ nodes. 
    Let $G(x) = \sum_{n=0}^{\infty} g_n x^n$.

    \zany{} Can you see why
    \begin{equation*}
        G(x) 
        = 
        x
        +
        x G(x)
        +
        x G(x)^{2}
        +
        \dots
        =
        \frac{x}{1 - G(x)}
    \end{equation*}

    \cake{} Show that $g_{n} = c_{n}$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    Let $\{t_{n}\}_{n=0}^{\infty}$ be a sequence of nonnegative integers.
    Let
    \begin{equation*}
        T(x) = \sum_{n=0}^{\infty} t_n x^n
    \end{equation*}

    Suppose that
    \begin{equation*}
        T(x) = 1 + x T(x)^{2}
    \end{equation*}
    Find a closed form of $t_{n}$.

    \pause{}

    Answer: You should have
    \begin{equation*}
        t_{n} = C(n) = \frac{1}{n+1}\binom{2n}{n}.
    \end{equation*}
\end{frame}

\end{document}
