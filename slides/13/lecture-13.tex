\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\input{../venn.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Inclusion-Exclusion (1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c, label=current]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Section 7.1-7.2.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 7.7: 1, 3, 5, 7, 9, 11, 13.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 7.1 Introduction of Inclusion-Exclusion Formula}

\begin{frame}
    \frametitle{Counting The Number of Students}
    
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[xscale=0.9, yscale=0.7]
                \draw[fill=white] (0,0) rectangle (10, 8);
                \only<1>{%
                    \foreach \x in {1,2,...,9} {%
                        \foreach \y in {1,2,...,7} {%
                            \node at (\x, \y) {\large{}\student{}};
                        }%
                    }%
                }%
                \only<2->{%
                    \node at (1,1) {$63$ \student{}};
                    \draw[fill=LightPink] (4.2,4) 
                        circle [x radius=4,y radius=3.5];
                }%
                \only<2>{%
                    \begin{scope}
                        \draw[clip] (4.2,4) 
                            circle [x radius=4,y radius=3.5];
                        \foreach \x in {1,2,...,7} {%
                            \foreach \y in {1,2,...,7} {%
                                \node at (\x, \y) {\large{}\emoji{computer}};
                            }%
                        }%
                    \end{scope}
                }%
                \only<3->{%
                    \node at (1,4) {$47$ \emoji{computer}};
                    \draw[fill=SkyBlue] (5.7,4) 
                        circle [x radius=4,y radius=3.5];
                }%
                \only<3>{%
                    \begin{scope}
                        \draw[clip] (5.7,4) 
                            circle [x radius=4,y radius=3.5];
                        \foreach \x in {2,...,9} {%
                            \foreach \y in {1,2,...,7} {%
                                \node at (\x, \y)
                                    {\large{}\emoji{man-student-medium-skin-tone}};
                            }%
                        }%
                    \end{scope}
                }%
                \only<4->{%
                    \begin{scope}
                        \draw[clip] (4.2,4) 
                            circle [x radius=4,y radius=3.5];
                        \draw[clip] (5.7,4) 
                            circle [x radius=4,y radius=3.5];
                        \draw[fill=LightYellow] (0,0) rectangle (10, 8);
                        \only<4>{%
                            \begin{scope}
                                \foreach \x in {1,...,9} {%
                                    \foreach \y in {1,2,...,7} {%
                                        \node at (\x, \y)
                                            {$\temoji{man-student-medium-skin-tone}_{\temoji{computer}}$};
                                    }%
                                }%
                            \end{scope}
                        }%
                    \end{scope}
                    \node at (9,4) {$51$ \emoji{man-student-medium-skin-tone}};
                }%
                \only<5>{%
                    \node at (4.5, 4) {$45$
                        \emoji{computer}\emoji{man-student-medium-skin-tone}};
                }%
            \end{tikzpicture}
        \end{center}
        \caption{%
            \only<1>{%
                Assume that we have $63$ \student{} in total
            }%
            \only<2>{%
                Among them $47$ of are \emoji{computer} majors
            }%
            \only<3>{%
                $51$ are male
            }%
            \only<4>{%
                $45$ are male \emoji{computer} student
            }%
            \only<5>{%
                How many \emoji{woman-student-medium-skin-tone} are there who do not major
                in \emoji{computer}?
            }%
        }%
    \end{figure}
\end{frame}

\section{\acs{ac} 7.2 The Inclusion-Exclusion Formula}

\begin{frame}
    \frametitle{Some Notations}
    
    Let $X$ be a set, e.g., all \student{} in the class.

    Let 
    $\scP = \{P_1 , P_2 , \ldots, P_m \}$ be a family of $m$ properties, e.g.,
    \begin{itemize}
        \item $P_1$ --- male
        \item $P_2$ --- \emoji{computer} major
    \end{itemize}

    \pause{}

    For $Y \subseteq [m]$, let $N(Y)$ denote the number of elements of $X$
    which satisfy property $P_i$ for all $i \in Y$. 

    \bomb{} We let $N(\emptyset) = |X|$.

    \pause{}

    \cake{} In the above example, what do each of the following mean?
    \begin{equation*}
        N(\emptyset),
        \qquad
        N(\{1\}),
        \qquad
        N(\{2\}),
        \qquad
        N(\{1, 2\})
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{The \ac{cs} student example}
    
    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[xscale=0.7, yscale=0.5]
                \draw[fill=white] (0,0) rectangle (10, 8);
                \node at (1,1) {$63$ \student{}};
                \draw[fill=LightPink] (4.2,4) 
                    circle [x radius=4,y radius=3.5];
                \node at (1,4) {$47$ \emoji{computer}};
                \draw[fill=SkyBlue] (5.7,4) 
                    circle [x radius=4,y radius=3.5];
                \begin{scope}
                    \draw[clip] (4.2,4) 
                        circle [x radius=4,y radius=3.5];
                    \draw[clip] (5.7,4) 
                        circle [x radius=4,y radius=3.5];
                    \draw[fill=LightYellow] (0,0) rectangle (10, 8);
                \end{scope}
                \node at (9,4) {$51$ \emoji{man-student-medium-skin-tone}};
                \node at (4.5, 4) {$45$
                    $\temoji{computer}_\temoji{man-student-medium-skin-tone}$};
            \end{tikzpicture}
        \end{center}
        \caption{%
            The number of \emoji{computer} majors and \emoji{man-student-medium-skin-tone}
            in the class
        }%
    \end{figure}

    \cake{} Given the notations we just defined, what is the number
    \begin{equation*}
        N(\emptyset) - N(\{1\}) - N(\{2\}) + N(\{1, 2\})
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Three Properties}

    Let $X$ be the set of \student{} in the class.
    
    Let $P_{1}, P_{2}, P_{3}$ be the properties of owing a \lion{}, \tiger{} or \panda{}.

    \think{} What is the number of \student{} who have none of these?

    \vspace{-1em}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \begin{venndiagram3sets}[
                    tikzoptions={scale=0.8},
                    labelA={$P_1$}, 
                    labelB={$P_2$}, 
                    labelC={$P_3$}]
                    \setkeys{venn}{shade=LightPink}
                    \fillA 

                    \setkeys{venn}{shade=SkyBlue}
                    \fillB 

                    \setkeys{venn}{shade=LightYellow}
                    \fillC

                    \setkeys{venn}{shade=LightPink!50!SkyBlue}
                    \fillACapB

                    \setkeys{venn}{shade=SkyBlue!50!LightYellow}
                    \fillBCapC

                    \setkeys{venn}{shade=LightPink!50!LightYellow}
                    \fillACapC

                    \setkeys{venn}{shade=LightPink!33!LightYellow!33!SkyBlue}
                    \fillACapBCapC
                \end{venndiagram3sets}
                \caption{A Venn diagram for three properties}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Four Properties}
    
    Let $X$ be the set of \student{} in the class.
    
    Let $P_{1}, \dots P_{4}$ be the properties of owing a \lion{}, \tiger{}, \panda{} or
    \unicorn{}.

    \think{} What is the number of \student{} who have none of these?

    \begin{figure}[htpb]
        \tikzset{
            set/.style ={ 
                ellipse, 
                minimum width=3.5cm, 
                minimum height=2cm,
                draw,
        }}
        \centering
        \begin{tikzpicture}
            \foreach \x/\y/\a/\l in {%
                .7/0/60/$P_1$, .3/1/60/$P_2$, -.7/0/-60/$P_3$, -.3/1/-60/$P_4$
            }%
            \node[set, rotate=\a, label={[label distance=1cm]above:\l}] at (\x,\y) {};
        \end{tikzpicture}
        \caption{A Venn diagram for four properties is quite complicated}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 7.7 --- The Inclusion-Exclusion Formula}

    The number of elements of $X$ which satisfy \emph{none} of the properties in $\scP =
    \{P_{1}, P_{2}, \ldots, P_{m}\}$ is given by
    \begin{equation*}
        \label{eq:7:2:1}
        \sum_{Y \subseteq [m]} (−1)^{|Y|} N(Y),  \tag{7.2.1}
    \end{equation*}
    where $[m] = \{1, \dots, m\}$.

    \hint{} Proof by induction!

    \cake{} What is \eqref{eq:7:2:1} when 
    \foreach \n in {1,...,3}{%
        \only<\n>{$m = \n$?}
    }%
\end{frame}

\begin{frame}
    \frametitle{Proof of Theorem 7.7: Base Case}
    
    \cake{} When $m = 1$, what is the following formula?
    \begin{equation*}
        \sum_{Y \subseteq [m]} (−1)^{|Y|} N(Y),  \tag{7.2.1}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Proof of Theorem 7.7: Induction Step}

    \only<1-2>{%
        Assume the formula \eqref{eq:7:2:1} holds when $m = k$ for some $k \ge 1$.

        We will show that it also holds when $m = k + 1$ for some $k \ge 1$ ---
        \begin{equation*}
            \sum_{Y \subseteq [k+1]} (−1)^{|Y|} N(Y)
        \end{equation*}
        is the number of elements of $X$ which \reject{} all of the properties in
        $\scP = \{P_{1}, P_{2}, \ldots, P_{k}, P_{k+1}\}$.
    }%
    \only<2->{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[xscale=0.7, yscale=0.5]
                \only<1>{%
                    \node at (-0.5, 1.5) {$X$};
                }%
                \onslide<2, 3, 5->{%
                    \draw[fill=LightYellow] (0,0) rectangle (5, 3);
                    \node at (2.5, 1.5) {$X':$ Those \accept{} $P_{k+1}$};
                }%
                \onslide<2, 4->{%
                    \draw[fill=SkyBlue] (5,0) rectangle (10, 3);
                    \node at (7.5, 1.5) {$X'':$ Those \reject{} $P_{k+1}$};
                }%
            \end{tikzpicture}
            \caption{A split of $X$}
        \end{figure}
    }
    \only<3-5>{%
        Let $Y$ be a subset of $[k]$.

    }%
    \only<3>{%
        Let $N'(Y)$ the number of elements of $X'$ which \accept{} $P_{i}$ for
        all $i \in Y$.

        Let $X_{0}'$ be the set of elements of $X'$ which \reject{} all of $\{P_{1},
            \dots, P_{k}\}$.

        Then by the induction hypothesis, 
        \begin{equation*}
            \abs{X_{0}'} 
            =
            \sum_{Y \subseteq [k]} (−1)^{|Y|} N'(Y)
        \end{equation*}
    }%
    \only<4>{%
        Let $N''(Y)$ the number of elements of $X''$ which \accept{} $P_{i}$ for
        all $i \in Y$.

        Let $X_{0}''$ be the set of elements of $X''$ which \reject{} all of $\{P_{1},
            \dots, P_{k}\}$.

        Then by the induction hypothesis, 
        \begin{equation*}
            \abs{X_{0}''} 
            =
            \sum_{Y \subseteq [k]} (−1)^{|Y|} N''(Y)
        \end{equation*}
        \hint{} This is the number we are trying to compute.
    }%
    \only<5-5>{%
        Note that $N(Y) = N'(Y) + N''(Y)$.
    }%
    \only<6->{%
        Thus, we have
        \begin{equation*}
            \begin{aligned}
                \abs{X_0''} 
                & = \sum_{Y \subseteq [k]} (-1)^{|Y|}N''(Y)  \\
                & = \sum_{Y \subseteq [k]} (-1)^{|Y|}(N(Y) - N'(Y)) \\
                \onslide<7>{%
                & = \sum_{Y \subseteq [k]} (-1)^{|Y|}N(Y) 
                + \sum_{Y \subseteq [k]} (-1)^{|Y|+1}N(Y \cup \{k + 1\}) \\
                & = \sum_{Y \subseteq [k+1]} (-1)^{|Y|}N(Y).
                }%
        \end{aligned}
        \end{equation*}
    }%
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    How many integers in $[100]$ 
    are divisible by \emph{none} of $2$, $3$, and $5$?

    \vspace{2em}

    \begin{columns}[c, totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            \hint{} 
            Apply inclusion-exclusion formula
            to $X = [100]$ and
            \begin{itemize}
                \item $P_1$ --- divisible by $2$
                \item $P_2$ --- divisible by $3$
                \item $P_3$ --- divisible by $5$
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \begin{venndiagram3sets}[
                    tikzoptions={scale=0.8},
                    labelA={$P_1$}, 
                    labelB={$P_2$}, 
                    labelC={$P_3$}]
                    \setkeys{venn}{shade=LightPink}
                    \fillA 

                    \setkeys{venn}{shade=SkyBlue}
                    \fillB 

                    \setkeys{venn}{shade=LightYellow}
                    \fillC

                    \setkeys{venn}{shade=LightPink!50!SkyBlue}
                    \fillACapB

                    \setkeys{venn}{shade=SkyBlue!50!LightYellow}
                    \fillBCapC

                    \setkeys{venn}{shade=LightPink!50!LightYellow}
                    \fillACapC

                    \setkeys{venn}{shade=LightPink!33!LightYellow!33!SkyBlue}
                    \fillACapBCapC
                \end{venndiagram3sets}
                \caption{Venn diagram for three properties}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Example 7.3}
    
    \think{} How to use inclusion-exclusion formula to find the number of integer solutions to the equation
    \begin{equation*}
        x_1 + x_2 + x_3 + x_4 = 97
    \end{equation*}
    such that $x_1, \dots, x_4 \ge 0$, $x_3 \le 7$, and $x_4 \le 8$?
\end{frame}

\end{document}
