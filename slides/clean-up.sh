#!/bin/bash

for dir in */ ; do
    cd "$dir"
    rm -f *.fdb_latexmk *.log *.out *.toc *.aux *.fls *.nav *.snm 
    cd ..
done
