\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\usepackage{tikz}

\title{Lecture \lecturenum{} --- Graph Colouring (2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 5.4.2
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 5.9: 19-23.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 5.4.2 Cliques and Chromatic Number}

\subsection{Cliques and Clique Number}

%\begin{frame}
%    \frametitle{Warming up \emoji{hot-face}}
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.6\textwidth]{bunny-basket-ball.png}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Chromatic Number of Subgraph}

    Recall that if $H$ is a subgraph of $G$, then $\chi(G) \ge \chi(H)$.
    
    \cake{} What is $\chi(G)$ for the following graph $G$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{clique-1.pdf}
        \caption{A graph $G$}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Cliques}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{Clique} ---
            a narrow exclusive circle or group of persons.

            \begin{flushright}
                --- \itshape{Merriam-Webster Dictionary}
            \end{flushright}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{clique-tv.jpg}
                \caption{Clique is also a \emoji{uk} \emoji{tv} show}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Review: Induced subgraphs}

    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is called \alert{an induced subgraph}
    if $xy \in E$ and $x, y \in W$ implies that $xy \in F$.

    \begin{figure}[htpb]
        \centering
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph.pdf}}
            \caption{A subgraph which is \emph{not} an induced subgraph}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.49\textwidth}
            \centering
            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph-induced.pdf}}
            \caption{A subgraph which is also an induced subgraph}
        \end{subfigure}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cliques in graphs}

    A subset of vertices $S$ is a \alert{clique} if the \emph{induced
    subgraph} on $S$ is a complete graph.

    The \alert{clique number} of $G$, denoted by $\omega(G)$, 
    is the size of the \emph{largest clique} in $G$.
    
    \cake{} What is $\omega(G)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\textwidth]{clique-number-01.pdf}
        \caption{A graph $G$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A lower bound of the chromatic number}
    Since $\omega(G) = m$ means $G$ has $K_{m}$ as a subgraph,
    we have $\chi(G) \ge m = \omega(G)$.

    \cake{} What is the $\chi(G)$ and $\omega(G)$ for the following graphs?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.45\linewidth]{clique-1.pdf}
        \includegraphics<2>[width=0.45\linewidth]{clique-number-02.pdf}
        \caption{A graph $G$}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Can $\chi(G)=\omega(G)$?}
%
%    \cake{} What is $\omega(C_{2n})$ and $\chi(C_{2n})$?
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.5\textwidth]{c6.pdf}
%        \caption{$C_6$}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Can $\chi(G)>\omega(G)$?}
%
%    \cake{} What is $\omega(C_{2n+1})$ and $\chi(C_{2n+1})$?
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.5\textwidth]{c5.pdf}
%        \caption{$C_5$}
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    A conversation between a \student{} and ChatGPT \emoji{robot}:

    \begin{itemize}
        \item[\emoji{student}] Let $G$ be a bipartite graph.
        What are the values of $ω(G)$ and $χ(G)$?
        \item[\emoji{robot}] $2$ and $2$.
    \end{itemize}

    Do you agree with the \emoji{robot}? Why or why not?

    \hint{} Is $K_{1}$ bipartite? What about $I_{3}$?
\end{frame}

\begin{frame}
    \frametitle{Proposition 5.25 (\acs{ac}) --- Can $\chi(G) \gg \omega(G)$?}

    For every $t \ge 3$, there exists a graph $G_t$ so that $\chi(G_t) = t$ and
    $\omega(G_t) = 2$.

    \only<1>{%
        In other words, there exist \triangular{}/$C_3$-free graphs
        with \emph{arbitrarily large} chromatic number.
    }%

    \only<+>{%
        \astonished{} The there can be a huge difference between $\chi(G)$ and $\omega(G)$.
    }%
    \only<+>{%
        \cake{}
        Example: Can you think of a \triangular{}-free graph with chromatic number $3$?
        We will use it a $G_{3}$.
    }
    \only<+>{%
        Example: The following is a \triangular{}-free graph with chromatic number $4$.
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.5\linewidth]{../images/graphs/g4-triangle-free-coloured.pdf}
            \caption{A possible choice of $G_{4}$}
        \end{figure}
    }%
\end{frame}

\subsection{Proof of Proposition 5.25 (\acs{ac}) by J.~Kelly and L.~Kelly}

\begin{frame}
    \frametitle{A seating problem}

    There are 
    $\alt<2>{2 \times}{} 5 + 1 = \alt<2>{11}{6}$ 
    \bunny{} in the classroom.

    The are $5$ \emoji{chair} in the classroom.

    \cake{} Can you show that it is impossible that
    \begin{itemize}
        \item each \bunny{} sits on a \emoji{chair},
        \item no \emoji{chair} has more than $\alt<2>{2}{1}$
            \bunny{} on it.
    \end{itemize}

    \only<2>{%
        \hint{} Proof by contradiction.
    }
\end{frame}

\begin{frame}
    \frametitle{Proposition 5.24 (\acs{ac}) --- Generalized \emoji{bird} Hole Principle}

    \begin{block}{Review: Pigeon-hole Principle}
        If $n + 1$ \emoji{bird} fly into $n$ \emoji{wastebasket},
        then there exists a \emoji{wastebasket} with least $2$ \emoji{bird}.
    \end{block}

    \pause{}

    \begin{block}{New: Generalized Pigeon-hole Principle}
        If $(m-1) n + 1$ \emoji{bird} fly into $n$ \emoji{wastebasket}, 
        then there exists a \emoji{wastebasket} with least $m$ \emoji{bird}.
    \end{block}

    \cake{} How to get the Pigeon-hole Principle from the generalized version?

    Proof by contradiction:
\end{frame}

\begin{frame}
    \frametitle{Proof of Proposition 5.25 (\acs{ac}) by Kelly and Kelly --- A Sketch}
    
    Base case --- We let $G_3 = C_5$, which is \triangular{} free and has chromatic number $3$.

    \only<1>{%
        \cake{}
        The choices of $G_3$ are plenty. Can you think of another one?
    }%

    \pause{}

    Induction step ---
    \begin{itemize}[<+->]
        \item[\emoji{screwdriver}] Given a \triangular{}-free graph $G_{t}$ with $\chi(G_t) = t$,
            we construct a $G_{t+1}$ following a specific procedure.
        \item[\emoji{mag}] Verify $G_{t+1}$ is \triangular{}-free.
        \item[\emoji{paintbrush}] Verify $\chi(G_{t+1}) \le t + 1$ by finding a proper colouring with only
            $t+1$ colours.
        \item[\think{}] Prove $\chi(G_{t+1}) > t$ by contradiction.
        \item[\emoji{tada}] Conclude that $\chi(G_{t+1}) = t + 1$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\emoji{screwdriver} Construct $G_{t+1}$}
    Let $n_{t}$ be the number of vertices in $G_{t}$ 
    and let $m_{t} = (n_t - 1) t + 1$.

    To get $G_{t+1}$, we
    \begin{itemize}[<+->]
        \item start with $m_{t}$ vertices,
        \item for each $n_t$-subset of the $m_{t}$ vertices, attach a copy of $G_{t}$.
    \end{itemize}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8,
            every node/.style={%
                circle, 
                thick,
                fill=white, 
                inner sep=0pt, 
                minimum width=6pt
            }
            ]
            % Upper nodes
            \foreach \x in {1,...,13}
            \node[draw=BrightRed] (u\x) at (\x-1,2.5) {};

            % Set some parameters for drawing the C_5
            \pgfmathsetmacro{\angleInc}{360 / max(5, 2)}
            \pgfmathtruncatemacro{\maxN}{5 - 1}
            \pgfmathsetmacro{\radius}{1}

            % Define the style for the nodes
            % Lower nodes, define nodes for three C_5 graphs
            % Lower nodes, define nodes for three C_5 graphs
            \foreach \l/\x in {1/1, 2/4, 3/11} { % Adjusted xshift values for separation
                \begin{scope}[
                    xshift=\x cm % Ensure units are consistent and clear
                    ]
                    % Vertices
                    \foreach \i in {0,...,\maxN} {
                        \pgfmathsetmacro{\angle}{\i * \angleInc + 100}
                        \node[draw=DaylightBlue] (\l\i) at (\angle:\radius) {};
                    }

                    % Edges
                    \foreach \i in {0,...,\maxN} {
                        \pgfmathtruncatemacro{\next}{mod(\i+1, 5)}
                        \draw[ultra thick] (\l\i) -- (\l\next);
                    }
                \end{scope}
            }%
            \only<2->{%
                \foreach \i in {0,...,\maxN} {
                    \pgfmathtruncatemacro{\j}{\i+1}%
                    \only<2>{%
                        \draw[MintGreen, ultra thick] (1\i) -- (u\j);
                    }%
                    \only<3->{%
                        \draw[thin] (1\i) -- (u\j);
                    }%
                }%
            }%
            \only<3->{%
                \foreach \i in {0,...,\maxN} {
                    \pgfmathtruncatemacro{\j}{\i+2}%
                    \only<3>{%
                        \draw[MintGreen, ultra thick] (2\i) -- (u\j);
                    }%
                    \only<4->{%
                        \draw[thin] (2\i) -- (u\j);
                    }%
                }%
            }%
            \only<4>{%
                \foreach \i in {0,...,\maxN} {
                    \pgfmathtruncatemacro{\j}{\i+9}%
                    \only<4>{%
                        \draw[MintGreen, ultra thick] (3\i) -- (u\j);
                    }%
                }%
            }%
        \end{tikzpicture}
        \caption{%
            Example: $G_{4}$'s sketch. Note that there are $13$ \emoji{o} 
            and $\binom{13}{5}$ $C_{5}$
        }%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{\emoji{mag} Verify $G_{t+1}$ is \triangular{}-free}

    Is it possible that $G_{t+1}$ has a \triangular{} which consists of
    \begin{itemize}[<+->]
        \item vertices from a single copy of $G_{t}$
        \item vertices from multiple copies of $G_{t}$
        \item at least of two $\temoji{o}$
        \item one $\temoji{o}$ and two vertices from a copy of $G_{t}$
    \end{itemize}
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8,
            every node/.style={%
                circle, 
                thick,
                fill=white, 
                inner sep=0pt, 
                minimum width=6pt
            }
            ]
            % Upper nodes
            \foreach \x in {1,...,13}
            \node[draw=BrightRed] (u\x) at (\x-1,2.5) {};

            % Set some parameters for drawing the C_5
            \pgfmathsetmacro{\angleInc}{360 / max(5, 2)}
            \pgfmathtruncatemacro{\maxN}{5 - 1}
            \pgfmathsetmacro{\radius}{1}

            % Define the style for the nodes
            % Lower nodes, define nodes for three C_5 graphs
            % Lower nodes, define nodes for three C_5 graphs
            \foreach \l/\x in {1/1, 2/4, 3/11} { % Adjusted xshift values for separation
                \begin{scope}[
                    xshift=\x cm % Ensure units are consistent and clear
                    ]
                    % Vertices
                    \foreach \i in {0,...,\maxN} {
                        \pgfmathsetmacro{\angle}{\i * \angleInc + 100}
                        \node[draw=DaylightBlue] (\l\i) at (\angle:\radius) {};
                    }

                    % Edges
                    \foreach \i in {0,...,\maxN} {
                        \pgfmathtruncatemacro{\next}{mod(\i+1, 5)}
                        \draw[ultra thick] (\l\i) -- (\l\next);
                    }
                \end{scope}
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+1}%
                    \draw[thin] (1\i) -- (u\j);
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+2}%
                    \draw[thin] (2\i) -- (u\j);
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+9}%
                    \draw[thin] (3\i) -- (u\j);
            }%
        \end{tikzpicture}
        \caption{%
            Sketch of $G_{4}$
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\emoji{paintbrush} An Upper Bound of $\chi(G_{t+1})$}

    We can colour each copy of $G_{t}$ with $t$ colours properly.

    Then we give each \emoji{o} a new colour.

    This is a proper colouring.

    So we have $\chi(G_{t+1}) \le t+1$.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8,
            every node/.style={%
                circle, 
                thick,
                fill=white, 
                inner sep=0pt, 
                minimum width=6pt
            }
            ]
            % Upper nodes
            \foreach \x in {1,...,13}
            \node[draw=BrightRed, fill=JuliaOrange] (u\x) at (\x-1,2.5) {};

            % Set some parameters for drawing the C_5
            \pgfmathsetmacro{\angleInc}{360 / max(5, 2)}
            \pgfmathtruncatemacro{\maxN}{5 - 1}
            \pgfmathsetmacro{\radius}{1}

            % Define the style for the nodes
            % Lower nodes, define nodes for three C_5 graphs
            % Lower nodes, define nodes for three C_5 graphs
            \foreach \l/\x in {1/1, 2/4, 3/11} { % Adjusted xshift values for separation
                \begin{scope}[
                    xshift=\x cm % Ensure units are consistent and clear
                    ]
                    % Vertices
                    \foreach \i in {0,...,\maxN} { % Assuming \maxN is defined somewhere in your document
                        \pgfmathsetmacro{\angle}{\i * \angleInc + 100}
                        % Determine the fill color based on the value of \i
                        \ifnum\i=1
                            \def\mycolor{red}
                        \else
                            \ifnum\i=2
                                \def\mycolor{blue}
                            \else
                                \ifnum\i=3
                                    \def\mycolor{red}
                                \else
                                    \ifnum\i=4
                                        \def\mycolor{blue}
                                    \else
                                        \ifnum\i=0
                                            \def\mycolor{green}
                                        \else
                                            \def\mycolor{black} % Default color if needed
                                        \fi
                                    \fi
                                \fi
                            \fi
                        \fi

                        % Now use the determined color for filling the node
                        \node[draw=DaylightBlue, fill=\mycolor] at (\angle:\radius) {};
                    }

                    % Edges
                    \foreach \i in {0,...,\maxN} {
                        \pgfmathtruncatemacro{\next}{mod(\i+1, 5)}
                        \draw[ultra thick] (\l\i) -- (\l\next);
                    }
                \end{scope}
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+1}%
                    \draw[thin] (1\i) -- (u\j);
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+2}%
                    \draw[thin] (2\i) -- (u\j);
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+9}%
                    \draw[thin] (3\i) -- (u\j);
            }%
        \end{tikzpicture}
        \caption{Example: $\chi(G_{4}) \le 4$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\think{} A Lower Bound of the Chromatic Number}

    Assume that there is a proper colouring of $G_{t+1}$ with $t$ colours.

    Then generalized \emoji{bird}-hole Principle implies
    that among $m_t = (n_t-1)t +1$ $\temoji{o}$, at least $n_{t}$ will have the same colour.

    These $n_{t}$ vertices are connected to a copy of $G_{t}$.
    
    \smiling{} This is a contradiction.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            scale=0.8,
            every node/.style={%
                circle, 
                thick,
                fill=white, 
                inner sep=0pt, 
                minimum width=6pt
            }
            ]
            % Upper nodes
            \foreach \x in {1,...,13}
            \node[draw=BrightRed] (u\x) at (\x-1,2.5) {};

            % Set some parameters for drawing the C_5
            \pgfmathsetmacro{\angleInc}{360 / max(5, 2)}
            \pgfmathtruncatemacro{\maxN}{5 - 1}
            \pgfmathsetmacro{\radius}{1}

            % Define the style for the nodes
            % Lower nodes, define nodes for three C_5 graphs
            % Lower nodes, define nodes for three C_5 graphs
            \foreach \l/\x in {1/1, 2/4, 3/11} { % Adjusted xshift values for separation
                \begin{scope}[
                    xshift=\x cm % Ensure units are consistent and clear
                    ]
                    % Vertices
                    \foreach \i in {0,...,\maxN} { % Assuming \maxN is defined somewhere in your document
                        \pgfmathsetmacro{\angle}{\i * \angleInc + 100}
                        % Determine the fill color based on the value of \i
                        \ifnum\i=1
                            \def\mycolor{red}
                        \else
                            \ifnum\i=2
                                \def\mycolor{blue}
                            \else
                                \ifnum\i=3
                                    \def\mycolor{red}
                                \else
                                    \ifnum\i=4
                                        \def\mycolor{blue}
                                    \else
                                        \ifnum\i=0
                                            \def\mycolor{green}
                                        \else
                                            \def\mycolor{black} % Default color if needed
                                        \fi
                                    \fi
                                \fi
                            \fi
                        \fi

                        % Now use the determined color for filling the node
                        \node[draw=DaylightBlue, fill=\mycolor] at (\angle:\radius) {};
                    }

                    % Edges
                    \foreach \i in {0,...,\maxN} {
                        \pgfmathtruncatemacro{\next}{mod(\i+1, 5)}
                        \draw[ultra thick] (\l\i) -- (\l\next);
                    }
                \end{scope}
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+1}%
                    \draw[thin] (1\i) -- (u\j);
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+2}%
                    \draw[thin] (2\i) -- (u\j);
            }%
            \foreach \i in {0,...,\maxN} {
                \pgfmathtruncatemacro{\j}{\i+9}%
                    \draw[thin] (3\i) -- (u\j);
            }%
        \end{tikzpicture}
        \caption{Example: $\chi(G_{4}) \ge 4$ because no matter how you colour $13$
        \emoji{o} with $3$ colours, there will be five vertices with the same colour.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\emoji{tada} The Chromatic Number of $G_{t+1}$}
    
    We have proved that
    \begin{equation*}
        \chi(G_{t+1}) \ge t+1,
        \qquad
        \text{and}
        \qquad
        \chi(G_{t+1}) \le t+1.
    \end{equation*}
    It follows that $\chi(G_{t+1}) = t+1$. 

    We have also shown that $G_{t+1}$ is \triangular{}-free.

    \pause{}

    Thus we have finished the induction step --- 

    Given a graph $G_{t}$ which is
    \triangular{}-free and $\chi(G_{t}) = t$, we can construct a graph $G_{t+1}$ which is
    also \triangular{}-free and $\chi(G_{t+1}) = t+1$.
\end{frame}

\subsection{Proof of Proposition 5.25 (\acs{ac}) by J.~Mycielski}

\begin{frame}
    \frametitle{Proof of Proposition 5.25 (\acs{ac}) by J.~Mycielski --- A
    sketch}
    
    Base case --- Let $G_3 = C_5$.

    Induction step ---
    \begin{itemize}
        \item[\emoji{screwdriver}] Given a \triangular{}-free graph $G_{t}$ with $\chi(G_t) = t$,
            we construct a $G_{t+1}$ following a specific procedure.
        \item[\emoji{mag}] Verify $G_{t+1}$ is \triangular{}-free.
        \item[\emoji{paintbrush}] Verify $\chi(G_{t+1}) \le t + 1$ by finding a proper colouring with only
            $t+1$ colours.
        \item[\think{}] Prove $\chi(G_{t+1}) > t$ by contradiction.
        \item[\emoji{tada}] Conclude that $\chi(G_{t+1}) = t + 1$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{\emoji{screwdriver} Construct $G_{t+1}$}
    
    Let $n_{t}$ be the number of vertices in $G_{t}$.

    Given $G_{t}$ we construct $G_{t+1}$ as follows ---

    \begin{itemize}
        \item Add new vertices $y_{1}, \ldots, y_{n_{t}}$ to $G_{t}$.
        \item Add another vertex $z$ and connect it to all $y_{i}$'s.
        \item Connect $y_{j}$ to $x_{i}$ if $x_{j}$ is adjacent to $x_{i}$.
    \end{itemize}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.4\linewidth]{../images/graphs/g4-triangle-free-skeleton.pdf}
        \includegraphics<2>[width=0.4\linewidth]{../images/graphs/g4-triangle-free.pdf}
        \caption{Example: Construct $G_4$}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{\emoji{mag} Verify $G_{t+1}$ Is \triangular{}-free}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Is it possible that $G_{t+1}$ has a \triangular{} which contains
            \begin{itemize}
                \item $z$
                \item $x_i, x_j, x_k$
                \item $y_i, y_j, y_k$
                \item $x_i, y_j, y_k$
                \item $x_i, x_j, y_k$
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \includegraphics[width=\linewidth]{g4-triangle-free.pdf}
                \caption{Example: $G_4$ is \triangular{}-free}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\emoji{paintbrush} An Upper Bound of $\chi(G_{t+1})$}

    It is easy to see that $\chi(G_{t+1}) \le t+1$ by finding a proper colouring
    with $t+1$ colours.

    \pause{}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{../images/graphs/g4-triangle-free-coloured.pdf}
        \caption{Example: $\chi(G_4) \le 4$}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{\think{} A Lower Bound of $\chi(G_{t+1})$}

    To show that $\chi(G_{t+1}) \ge t+1$, use proof by contradiction ---
    \begin{itemize}
        \item Assume $\chi(G_{t+1}) \le t$.
        \item Then $G_{t+1}$ has a proper colouring $\phi$ using only $t$
            colours.
        \item By modifying $\phi$, we can get a new colouring $\phi'$.
        \item $\phi'$ is not necessarily a proper colouring of $G_{t+1}$.
        \item However, $\phi'$ is going to be a proper colouring of $G_{t}$ with
            only $t-1$ colours.
        \item This contradicts that $\chi(G_t) = t$.
    \end{itemize}

    \emoji{sweat} The details is in how to modify $\phi$.
\end{frame}

\end{document}
