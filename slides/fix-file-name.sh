#!/bin/bash

for dir in */ ; do
    cd "$dir"
    mv lecture-*.tex "lecture-${dir%/}.tex"
    cd ..
done
