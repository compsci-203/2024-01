\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\input{../graphs/interval-1.tex}
\input{../graphs/interval-2.tex}
\input{../graphs/k3-3.tex}

\title{Lecture \lecturenum{} -- Graph Colouring (3)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 5.4.3
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 5.9: 25, 26, 27.
                    \end{itemize}
            \end{itemize}
            \href{http://discrete.openmathbooks.org/dmoi3}{\acf{dm}},
            \begin{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 4.4: 9, 10.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 5.4.3 Can We Determine Chromatic Number?}

\subsection{First-Fit Algorithm}

\begin{frame}
    \frametitle{First-fit algorithm}

    \weary{}
    Deciding if $\chi(G) \le k$ is
    \href{https://en.wikipedia.org/wiki/NP-complete}{NP-complete}
    i.e., very hard, except for the cases $k \in \{0,1,2\}$.

    \only<1>{\puzzle{} Why it is easy for these $k$?}

    \pause{}

    But we do have a simple recursive algorithm called \alert{first-fit} to find a proper colouring $\phi$:
    \begin{enumerate}[<+->]
        \item Fix an ordering of the vertices $(v_1 , v_2 , \dots, v_n)$.
        \item Let $\phi(v_{1}) = 1$.
        \item Given of $\phi(v_1), \ldots, \phi(v_i)$, let
            $\phi(v_{i+1})$
            be the smallest integer (colour) \emph{not} on $v_{i+1}$'s neighbours.
        \item Repeat step 3 until each vertex has a colour.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example: First-fit algorithm}

    Let's find a \emph{proper} colouring of the following graph via first-fit using the
    colours: \colorCircles{}

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.8\textwidth]{graph-to-colour-1.pdf}
        \includegraphics<2>[width=0.6\textwidth]{graph-to-colour-2.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\weary{} The Problem with First-Fit}

    \only<1>{%
        How many colours does the first-fit algorithm need for
        the same graph but different ordering of vertices?

        Let use colours: \colorCircles{}
    }%

    \begin{figure}
        \begin{center}
            \begin{tikzpicture}
                \draw (0,0)
                    pic[
                    style={graph edge},
                    every node/.style={graph node small},
                    xscale=2,
                    yscale=1,
                    ]
                    {interval-1};
                \only<2>{%
                    \foreach \y/\fillc in {0/BrightRed, 1/MintGreen, 2/DaylightBlue} {%
                            \foreach \x in {0, 2} {%
                                \node[graph node small, fill=\fillc] at (\x,\y) {};
                            }%
                        }%
                    }%
                \end{tikzpicture}
            \hfil
            \begin{tikzpicture}
                \draw (0,0)
                    pic[
                    style={graph edge},
                    every node/.style={graph node small},
                    xscale=2,
                    yscale=1,
                    ]
                    {interval-2};
                \only<2>{%
                    \foreach \x/\fillc in {0/BrightRed, 2/MintGreen} {%
                            \foreach \y in {0, 1, 2} {%
                                \node[graph node small, fill=\fillc] at (\x,\y) {};
                            }%
                        }%
                    }%
                \end{tikzpicture}
        \end{center}
        \caption{The same graph $G$ but with different ordering of vertices.}
    \end{figure}

    \pause{}

    \bomb{} Different ordering of vertices may cause first-fit to use different number of
    colours!

    \think{} For what kind of $G$ does the first-fit algorithm always use $\chi(G)$ colours?
\end{frame}

\subsection{Maximum Degrees and Chromatic Numbers}

\begin{frame}
    \frametitle{The Maximum Degree}

    Let $\Delta(G)$ be the maximum degree in $G = (V, E)$, i.e.,
    \begin{equation*}
        \Delta(G) = \max_{v \in V} \deg_G(v)
    \end{equation*}

    \cake{} What is $\Delta(G)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{lollipop-5.pdf}
        \caption{A \emoji{lollipop} graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 4.4.5 (\acs{dm}): Brook's Theorem}

    Any graph $G$ satisfies $χ(G) \le ∆(G)$,
    \emph{unless} $G$ is a complete graph or an odd cycle,
    in which case $χ(G) = ∆(G) + 1$.

    \begin{figure}
        \centering
        \includegraphics<1>[width=0.5\textwidth]{c7.pdf}
        \includegraphics<2>[width=0.5\textwidth]{k5.pdf}
        \includegraphics<3>[width=0.7\textwidth]{lollipop-5.pdf}
        \caption{%
            \only<1>{%
                An odd cycle $C_{7}$
            }%
            \only<2>{%
                A complete graph $K_{5}$
            }%
            \only<3>{%
                Not an odd cycle, not a complete graph.
            }%
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A Simple Upper Bound}

    We will prove a simpler upper bound: For any graph $G$, $\chi(G) \le \Delta(G) + 1$.

    \hint{} This follows from the observation that the first-fit algorithm uses at most $\Delta(G) + 1$ colours.

    \begin{figure}[htpb]
        \begin{center}
            \colorbox{white}{%
                \begin{tikzpicture}[
                    every path/.style={graph edge},
                    ]
                    \foreach \x/\fillc/\vlabel
                        in {%
                            1/BrightRed/$v_1$,
                            2/MintGreen/$v_2$,
                            3/DaylightBlue/$v_3$,
                            7/LavenderDream/$v_i$,
                            8/white/$v_{i+1}$,
                            11/white/$v_{n}$
                        }% 
                        {%
                            \node[graph node small, fill=\fillc] (v\x) at (\x, 0) {};
                            \node[fill=none, draw=none] at (\x, -0.5) {\vlabel};
                        }
                        \draw (v1) -- (v2) -- (v3);
                        \draw (v1) to [out=45, in=135] (v3);
                        \foreach \x in {1, 4, 5, 6} {
                            \draw (v7)  to [out=135, in=45] (\x, 0.22);
                        }
                        \foreach \x in {8, 9, 10} {
                            \draw (v7)  to [in=135, out=45] (\x, 0.22);
                        }
                    \end{tikzpicture}
            }%
        \end{center}
        \caption{\cake{} What is the maximum number of neighbours can $v_{i}$ have on its left?}
    \end{figure}
\end{frame}

\subsection{Interval Graphs}

\begin{frame}
    \frametitle{The Screening Schedule}

    The screening schedule of $6$ films at a film festival are shown below.

    \begin{figure}[htpb]
        \footnotesize
        \begin{tikzpicture}[
            every node/.style={midway},
            style={graph edge, draw=black},
            xscale=1.3,
            yscale=1.2,
            auto
            ]
            \input{../graphs/interval-4.tex}
        \end{tikzpicture}
        \caption{The screening schedule}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Interval Graph}

    Given a list of intervals $(S_{v})_{v \in V}$, the corresponding \emph{interval graph} is the
    graph with vertex set $V$ and edge set
    \begin{equation*}
        E = \{uv : S_{u} \cap S_{v} \ne \emptyset\}
    \end{equation*}

    \vspace{-2em}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \footnotesize
                \begin{tikzpicture}[
                    every node/.style={midway},
                    style={graph edge, draw=black},
                    yscale=0.6,
                    xscale=0.9,
                    auto
                    ]
                    \input{../graphs/interval-4.tex}
                \end{tikzpicture}
                \caption{Intervals}
            \end{figure}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.6\textwidth]{film-festival-1.pdf}
                \caption{An interval graph $G$}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 5.28 (\acs{ac}): Interval Graphs Are Perfect}

    If $G = (V, E)$ is an interval graph, then $\chi(G) = \omega(G)$.
    
    \only<1>{%
        \think{} Are all perfect graphs interval graphs?
    }%

    \pause{}

    Proof: We have know from before that $\chi(G) \ge \omega(G)$.

    To prove that $\omega(G) \ge \chi(G)$, we use the first-fit algorithm,
    with the vertex sorted in the order of the left end of their intervals.
\end{frame}

%\subsection{Independent Number}
%
%\begin{frame}
%    \frametitle{The Screening Schedule}
%
%    Let's consider screening schedule again.
%
%    \cake{}
%    What is the max number of films we can watch in their entirety?
%
%    \begin{figure}[htpb]
%        \footnotesize
%        \begin{tikzpicture}[
%            every node/.style={midway},
%            style={graph edge, draw=black},
%            xscale=1,
%            yscale=1,
%            auto
%            ]
%            \input{../graphs/interval-4.tex}
%        \end{tikzpicture}
%        \caption{The screening schedule}
%    \end{figure}
%
%    \think{} How can we turn this into a problem about interval graphs?
%\end{frame}
%
%\begin{frame}
%    \frametitle{Review: Independent Graphs}
%    Recall that an independent graph \( I_n \) is a graph with \( n \) vertices and no edges.
%
%    \begin{figure}[htpb]
%        \centering
%        \begin{tikzpicture}[
%            font=\footnotesize,
%            every node/.style={graph node, minimum size=1em},
%            style={graph edge},
%            scale=0.8]
%            % Vertices
%            \foreach \i in {1,...,8}
%            {
%                \node[label={\i*45-90: \( v_{\i} \)}] (\i) at
%                    (\i*45-90:2cm) {};
%            }
%        \end{tikzpicture}
%        \caption{Example: \( I_{8} \)}
%    \end{figure}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Review: Induced subgraphs}
%
%    A \emph{subgraph} $H = (W,F)$ of $G = (V, E)$ is called \alert{an induced subgraph}
%    if $xy \in E$ and $x, y \in W$ implies that $xy \in F$.
%
%    \begin{figure}[htpb]
%        \centering
%        \begin{subfigure}{0.49\textwidth}
%            \centering
%            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph.pdf}}
%            \caption{A subgraph which is \emph{not} an induced subgraph}
%        \end{subfigure}
%        \hfill
%        \begin{subfigure}{0.49\textwidth}
%            \centering
%            \colorbox{white}{\includegraphics[width=\linewidth]{subgraph-induced.pdf}}
%            \caption{A subgraph which is also an induced subgraph}
%        \end{subfigure}
%    \end{figure}
%\end{frame}
%
%\begin{frame}[c]
%    \frametitle{Independence Number}
%
%    \begin{columns}[c]
%        \begin{column}{0.6\textwidth}
%            \setlength{\parskip}{1em}  % Enhance paragraph spacing
%
%            The size of the largest induced $I_{k}$ in $G$, denoted by $\alpha(G)$, 
%            is called the \alert{independent number} of $G$.
%
%            So the number of the ``maximum number of films to we can watch'' is the same
%            asking: what is $\alpha(G)$?
%        \end{column}
%        \begin{column}{0.4\textwidth}
%            \begin{figure}[htpb]
%                \centering
%                \includegraphics<1>[width=0.7\textwidth]{film-festival-2.pdf}
%                \includegraphics<2>[width=0.7\textwidth]{film-festival-3.pdf}
%                \includegraphics<3>[width=0.7\textwidth]{film-festival-4.pdf}
%                \caption{The interval $G$ has an induced
%                \only<1>{$I_2$}\only<2>{$I_3$}\only<3>{$I_4$}}
%            \end{figure}  % This was missing
%        \end{column}
%    \end{columns}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Independence number and chromatic number}
%
%    Lemma: If a graph $G$ has $n \ge 1$ vertices, then
%    \begin{equation*}
%        \alpha(G) \ge \frac{n}{\chi(G)}
%    \end{equation*}
%\end{frame}
%
%\begin{frame}
%    \frametitle{\tps{}}
%
%    \cake{} Can You Draw the Interval Graph $G$?
%
%    \think{} What are, $\alpha(G)$,  $\omega(G)$ and $\chi(G)$?
%
%    \bigskip
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{Interval_graph.svg.png}
%    \end{figure}
%\end{frame}

%\begin{frame}
%    \frametitle{The clique number of an interval graph}
%
%    Try colouring this graph with first-hit.
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.51\linewidth]{interval-2.pdf}
%        \includegraphics[width=0.51\linewidth]{interval-1.pdf}
%    \end{figure}
%\end{frame}

\subsection{Perfect Graphs}

%\begin{frame}
%    \frametitle{What is a perfect sunset?}
%
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.8\textwidth]{bunny-sunset.jpg}
%    \end{figure}
%\end{frame}
%
\begin{frame}
    \frametitle{Perfect Graph}

    A graph $G$ is said to be \alert{perfect} if $\chi(H) = \omega(H)$ for every
    \emph{induced} subgraph $H$.

    \cake{} Can you pick \only<1>{an}\only<2>{another}\only<3>{a third} 
    induced subgraph $H$ and find $\chi(H)$ and $\omega(H)$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{graph-perfect-example-1.pdf}
        \caption{Example: A perfect graph $G$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 5.28 (\ac{ac})}
    
    \think{} Can you see why every interval graph is perfect?
    
    \vspace{-1em}

    \input{../graphs/interval-3.tex}
\end{frame}

\subsection{Strong Perfect Graph Theorem}

\begin{frame}
    \frametitle{Definition: Complement graphs}

    % Introduction to the concept of complement graphs
    Given a graph \( G \), its \alert{complement graph} is the graph obtained by removing
    all existing edges and adding edges where none existed, which we denote by
    $\overline{G}$.

    \begin{figure}[htpb]
        \centering
        % Use subfigures to label each graph
        \hfill
        \begin{subfigure}{0.4\textwidth}
            \centering
            \includegraphics[width=\linewidth]{graph-complement-example-1.pdf}
            \caption{A graph \( G \)}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.4\textwidth}
            \centering
            \includegraphics[width=\linewidth]{graph-complement-example-2.pdf}
            \caption{The complement of \( G \)}
        \end{subfigure}
        \hfill
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Definition: Hole}

    A \alert{hole} in a $G$ is an induced subgraph which is a cycle of length at least $4$.

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}[
                every node/.style={graph node},
                every path/.style={graph edge},
                scale=1.5
                ]
                \only<1>{%
                    \drawCnInner{5};
                    \node (6) at (-1.5,-0.5) {6};
                    \node (7) at (1,1.2) {7};
                    \draw (2) -- (6) -- (3);
                    \draw (7) -- (1);
                }%
                \only<2>{%
                    \drawCnInner{7};
                    \node (8) at (-2.5,-0.5) {8};
                    \node (9) at (1,1.7) {8};
                    \draw (2) -- (8) -- (3);
                    \draw (9) -- (1);
                    \draw (8) -- (4);
                }%
            \end{tikzpicture}
        \end{center}
        \caption{Example: A graph with a hole of size \only<1>{$5$}\only<2>{$7$}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Strong Perfect Graph Theorem}

    A graph is \emph{perfect} if and only if neither itself nor its complement contain an odd holes 
    of size $5$.

    \only<1>{%
        \cake{} Does the graph contain a hole of size $5$?
    }%
    \only<2>{%
        \cake{} Does the graph contain a odd hole of size $7$ or larger? 
    }%
    What about its complement?

    \begin{figure}[htpb]
        \begin{center}
            \colorbox{white}{
                \begin{tikzpicture}[
                    every node/.style={graph node},
                    style={graph edge},
                    scale=1.2,
                    ]
                    \drawCnInner{6};
                    \draw (3) -- (6);
                    \tikzset{every path/.style={draw=DaylightBlue!30}};
                \draw (4) -- (5);
                \draw (4) -- (1) -- (5);
                \draw (4) -- (2) -- (5);
            \end{tikzpicture}
        }
    \end{center}
    \caption{Is this a perfect graph?}%
\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Cycle of Length $4$}

    \cake{}
    Is the graph $C_4$ perfect?
    
    \cake{} Is $C_4$ an interval graph?

    \cake{} Are all perfect graphs interval graphs?

    \begin{figure}[htpb]
        \centering
        \drawCn{4}
        \caption{The graph $C_4$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Complete Graphs}

    \cake{} Is the complete graph $K_{n}$ perfect?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={graph node small},
            style={graph edge},
            scale=1.3,
            ]
            \drawKnInner{7};
        \end{tikzpicture}
        \hfil
        \begin{tikzpicture}[
            every node/.style={graph node small},
            style={graph edge},
            scale=1.3,
            ]
            \drawInInner{7};
        \end{tikzpicture}
        \caption{%
            $K_{7}$ and its complement
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Definition: Complete Bipartite Graph}
    
    A \alert{complete bipartite graph} $K_{m, n}$ consists of two sets of vertices of
    sizes $m$ and $n$. Each vertex in one set is connected to every vertex in the other
    set, and no edges exist within the same set.

    \begin{figure}[htpb]
        \centering
        \drawKmn{3}{3}
        \hfil
        \drawKmn{2}{3}
        \caption{%
            $K_{2,2}$ and $K_{2,3}$
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Complete Bipartite Graphs}

    \cake{} Is the complete bipartite graph $K_{m, n}$ perfect?

    \begin{figure}[htpb]
        \centering
        \drawKmn{3}{4}
        \hfil
        \drawKmnComplement{3}{4}
        \caption{%
            $K_{3, 4}$ and its complement
        }%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Can you prove that the complete graph $K_n$ 
    is perfect using only the definition of a perfect graph?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Can you prove that the complete graph $K_n$
    is perfect by showing that it is an interval graph?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Is the following graph perfect?

    \begin{figure}[htpb]
        \begin{center}
            \colorbox{white}{
                \begin{tikzpicture}[
                    every node/.style={graph node},
                    style={graph edge},
                    scale=1.2,
                    ]
                    \drawCnInner{6};
                    \draw[draw=white, ultra thick] (1) -- (5) -- (2) -- (3) -- (4) -- (1);
                    \draw (3) -- (6);
                    \draw (2) -- (6);
                \end{tikzpicture}
        }
    \end{center}
    \caption{Is this a perfect graph?}%
\end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}: Bipartite Graph}
    
    \sweat{} Show that every bipartite graph $G$ is perfect.

    \hint{} Show that
    \begin{itemize}
        \item $G$ can not contain any odd cycles.
        \item $\overline{G}$ can not contain any odd holes of size at least $5$.
    \end{itemize}

    \temoji{bomb} Holes of size $3$ or $4$ are possible in $\overline{G}$, but not size $5$ and
    above.

    \emoji{wink} Give it a try by yourself. The solution of this one will not be given.
\end{frame}

\begin{frame}[c]
    \frametitle{Maria Chudnovsky}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            The theorem was proved by
            \href{https://en.wikipedia.org/wiki/Maria\_Chudnovsky}{Maria Chudnovsky},
            Neil Robertson, Paul Seymour, and
            Robin Thomas (2006).

            Maria Chudnovsky won a MacArthur Fellowship (2012), which comes
            with 500,000 \emoji{dollar}.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{Chudnovsky.jpg}
                \caption{Maria Chudnovsky}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
