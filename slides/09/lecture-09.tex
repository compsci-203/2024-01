\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\input{../language.tex}

\title{Lecture \lecturenum --- Strings, Sets, and Binomial Coefficients (1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Chapter 1
                        \item
                            Section 2.1-2.3
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 2.9: 1, 3, 5, 7, 9, 11, 13, 21
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 1 An Introduction to Combinatorics}

\begin{frame}
    \frametitle{What Is Combinatorics}
    
    \alert{Combinatorics} is a branch of mathematics mostly about counting.

    Some sub-fields of combinatorics include:
    \begin{itemize}[<+->]
        \item[\emoji{abacus}] Enumerative combinatorics: counting the number of certain objects
        \item[\emoji{mag}] Analytic combinatorics: counting by using \alert{generating functions} 
        \item[\emoji{infinity}] Extremal combinatorics: how large does something
            have to be to guarantee it has a certain property
        \item[\emoji{spider-web}] Graph theory: how to model networks in math
        \item[\emoji{game-die}] Probabilistic combinatorics: how to use probability theory
            to solve combinatorial problems
    \end{itemize}
\end{frame}


%\begin{frame}
%    \frametitle{What We Are Going To Cover}
%    
%    Topics we will see:
%    \begin{itemize}[<+->]
%        \item[\emoji{spider-web}] Discrete Structures: Graphs, digraphs, strings, patterns, and partitions.
%        \item[\emoji{abacus}] Enumeration: Permutations, combinations, inclusion/exclusion, generating functions,
%            recurrence relations
%        \item[\emoji{laptop}] Algorithms and Optimization: 
%            Eulerian circuits, hamiltonian cycles, planarity
%            testing, graph coloring, spanning trees, bipartite
%    \end{itemize}
%\end{frame}

\subsection{1.1 Enumeration}

\begin{frame}
    \frametitle{The Number of Necklaces}

    A \emoji{prayer-beads} consists of six beads with three different colours.

    \cake{} Which three of the four \emoji{prayer-beads} below are the same?

    \begin{figure}[h]
        \centering
        % Use the new command with different colors for each circle
        \ColoredCircle{SeaGreen}{BrightRed}{DaylightBlue}{DaylightBlue}{BrightRed}{BrightRed}
        \ColoredCircle{DaylightBlue}{DaylightBlue}{BrightRed}{BrightRed}{SeaGreen}{BrightRed}
        \ColoredCircle{BrightRed}{SeaGreen}{BrightRed}{BrightRed}{DaylightBlue}{DaylightBlue}
        \ColoredCircle{SeaGreen}{BrightRed}{DaylightBlue}{BrightRed}{DaylightBlue}{BrightRed}
        \caption{Four \emoji{prayer-beads}}
        \label{fig:necklace}
    \end{figure}

    \puzzle{} How many different \emoji{prayer-beads} of six beads can be formed using
    three \textcolor{BrightRed}{red}, 
    two \textcolor{DaylightBlue}{blue},
    one \textcolor{SeaGreen}{green}?
\end{frame}

\subsection{1.2 Graph Theory}

\begin{frame}
    \frametitle{Friends in a Forest}

    What is the maximum number of animals among which there are no friends?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={inner sep=2pt},
            xscale=3,
            yscale=1.8
            ]
            % Loop to create 20 nodes at random positions
            \foreach \n in {1,...,15}{
                \node (node\n) at (\n*24:1) {};
            }

            % Seed the random number generator
            \pgfmathsetseed{1247}

            % In your tikzpicture environment:
            \foreach \i in {1,...,14} {
                \foreach \j in {\i,...,15} {
                    \pgfmathsetmacro{\randNum}{rnd}
                    \ifdim \randNum pt > 0.9pt
                        \draw[thick] (node\i) -- (node\j);
                    \fi
                }
            }

            \node at (node1) {\emoji{dog-face}};
            \node at (node2) {\emoji{cat-face}};
            \node at (node3) {\emoji{mouse-face}};
            \node at (node4) {\emoji{lion}};
            \node at (node5) {\emoji{tiger-face}};
            \node at (node6) {\emoji{bear}};
            \node at (node7) {\emoji{elephant}};
            \node at (node8) {\emoji{monkey-face}};
            \node at (node9) {\emoji{horse-face}};
            \node at (node10) {\emoji{hamster}};
            \node at (node11) {\emoji{cow-face}};
            \node at (node12) {\emoji{pig-face}};
            \node at (node13) {\emoji{chicken}};
            \node at (node14) {\emoji{owl}};
            \node at (node15) {\emoji{fish}};
            %\node at (node16) {\emoji{frog}};
            %\node at (node17) {\emoji{panda}};
            %\node at (node18) {\emoji{koala}};
            %\node at (node19) {\emoji{penguin}};
            %\node at (node20) {\emoji{polar-bear}};
        \end{tikzpicture}
        \caption{Friendship Graph}
    \end{figure}

    \hint{} This is to ask what is the \alert{independent number} of the graph.
\end{frame}

\subsection{1.4 Number Theory}

\begin{frame}
    \frametitle{Number Theory}

    \alert{Number theory}  studies the properties of positive integers.

    Some times a problem in number theory can also be seen a combinatorial problem.

    \begin{exampleblock}{Example: Integer Partition}
        In how many ways can we write $4$ as a sum of positive integers in
        non-increasing order, e.g.,
        \begin{equation*}
            \begin{aligned}
                4   & = 4 \\
                    & = 3 + 1 \\
                    & = \blankshort{}
            \end{aligned}
        \end{equation*}
        \vspace{1cm}
    \end{exampleblock}
\end{frame}

\subsection{1.5 Geometry}

\begin{frame}
    \frametitle{A game of \emoji{crown}}

    We draw some infinitely long lines as boundaries to divide an infinitely
    large continent into a number kingdoms.

    \think{} How many kingdoms do we get with three lines?

    \begin{figure}
        \centering
        \input{../tikz-snippet/kindom.tex}
        \caption{Three Lines}
    \end{figure}
\end{frame}

\subsection{1.6 Optimization}

\begin{frame}
    \frametitle{Cars and Ex-partners}

    A group of animals plans a road trip.
    Ex-partners must go in separate \emoji{car}.

    \think{} What's the minimum number of \emoji{car} needed?

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            every node/.style={inner sep=2pt},
            xscale=3,
            yscale=1.8
            ]
            % Loop to create 20 nodes at random positions
            \foreach \n in {1,...,10}{
                \node (node\n) at (\n*36:1) {};
            }

            % Seed the random number generator
            \pgfmathsetseed{3247}

            % In your tikzpicture environment:
            \foreach \i in {1,...,9} {
                \foreach \j in {\i,...,10} {
                    \pgfmathsetmacro{\randNum}{rnd}
                    \ifdim \randNum pt > 0.8pt
                        \draw[thick] (node\i) -- (node\j);
                    \fi
                }
            }

            \node at (node1) {\emoji{dog-face}};
            \node at (node2) {\emoji{cat-face}};
            \node at (node3) {\emoji{mouse-face}};
            \node at (node4) {\emoji{lion}};
            \node at (node5) {\emoji{tiger-face}};
            \node at (node6) {\emoji{bear}};
            \node at (node7) {\emoji{elephant}};
            \node at (node8) {\emoji{monkey-face}};
            \node at (node9) {\emoji{horse-face}};
            \node at (node10) {\emoji{hamster}};
            %\node at (node11) {\emoji{cow-face}};
            %\node at (node12) {\emoji{pig-face}};
            %\node at (node13) {\emoji{chicken}};
            %\node at (node14) {\emoji{owl}};
            %\node at (node15) {\emoji{fish}};
            %\node at (node16) {\emoji{frog}};
            %\node at (node17) {\emoji{panda}};
            %\node at (node18) {\emoji{koala}};
            %\node at (node19) {\emoji{penguin}};
            %\node at (node20) {\emoji{polar-bear}};
        \end{tikzpicture}
        \caption{Ex-partner Graph}
    \end{figure}

    \hint{} This is to ask what is the \alert{chromatic number} of the graph.
\end{frame}

\section{\acs{ac} 2 Strings, Sets, and Binomial Coefficients}

\subsection{2.1 Strings}

\begin{frame}
    \frametitle{Strings}

    A function $s:[n] \to X$ is called an \alert{$X$-string of length $n$} or simply a
    \alert{string/word}.

    We normally write a string as $s = x_1 x_2 \dots x_n$ or $s = (x_1, x_2, \dots x_n)$.

    \pause{}
    
    The set $X$ is called the \alert{alphabet}.

    The elements in $X$ called \alert{characters/letters}.

    \pause{}
    
    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{words.jpg}
        \caption{English words are strings with the alphabet $X = \{a, b, \dots z, A, B, \dots, Z\}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Examples of Strings}

    Often we keep the alphabet implicit.

    \cake{} What are a possible alphabet for each of the following strings?
    \begin{itemize}[<+->]
        \item $010010100010110011101$ --- a bit/binary string.

        \item $201002211001020$ --- a ternary string.

        \item
            \emoji{apple}\emoji{banana}\emoji{apple}\emoji{banana}\emoji{cherries}\emoji{banana}\emoji{peach}
            --- my breakfasts of a week

        \item \texttt{KSF762} --- an European vehicle license plate

        \item $(34, 53, 3, 43, 54, 64, 7)$.
    \end{itemize}
\end{frame}

%\begin{frame}
%    \frametitle{Strings are functions}
%    
%    A string of length \(n\) on the alphabet \(\mathcal A\) is a function from \([n]\) to \(\mathcal A\).
%
%    \begin{example}
%        The string ababcbd can be seen as the function $f:[7]\mapsto\{\text{a, b, c, d}\}$
%        where
%        \begin{equation*}
%            f = 
%            \begin{pmatrix}
%                1 & 2 & 3 & 4 & 5 & 6 & 7 \\
%                  &   &   &   &   &   &  
%            \end{pmatrix}
%        \end{equation*}
%    \end{example}
%\end{frame}

%\begin{frame}
%    \frametitle{Notations for strings}
%
%    The string \(\left( 2,5,8,11 \right) \) can be seen as a function \(f:[4]\to[12]\) defined by \(f(n)=3n-1\).
%
%    Such a string is often written as \( (a_1,a_{2},a_{3},a_{4}, \dots, a_{m})\) with
%    \(a_{n}=3n-1\) for $n \in [m]$.
%
%    \cake{} The following string
%    \begin{equation*}
%        (2, 5, 10, 17)
%    \end{equation*}
%    is defined by $a_{n} = \blankshort{}$ for $n \in [4]$.
%\end{frame}

\begin{frame}[c]
    \frametitle{Example: My breakfast}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            I eat one type of fruit among $\{\temoji{apple}, \temoji{banana},
            \temoji{cherries}\}$ for breakfast.

            \cake{} How many different ways can I arrange breakfast for a week?

            \hint{} This is to ask for the number of words of length $7$ from an $3$
            letter alphabet.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{apple-banana-cherry.jpg}
            \end{figure}
        \end{column}
    \end{columns}

    \pause{}

    \medskip{}

    \think{} What is the number of $X$-strings of length $n$?
\end{frame}

\begin{frame}
    \frametitle{Strings and CPU}
    
    An instruction for a $64$-bit CPU is a bit string of length $64$.

    \cake{} What is the number of such strings?

    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{cpu.jpg}
    \end{figure}

\end{frame}


\begin{frame}[c]
    \frametitle{An Ancient Chinese Combinatorial Problem}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            A hexagram consists of six lines where each line can be one
            of two states: solid or dashed.

            \bigskip{}

            The ancient Chinese divination book I Ching (易经)
            says that there are $64$ hexagrams.

            \bigskip{}

            \cake{} Is I Ching correct?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{hexagram.jpg}
                \caption{A hexagram}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\zany{} How I Ching Works}

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.5\textwidth]{yarrow_stalks.jpg}
            \caption{Fifty yarrow stalks used for I Ching divination}
        \end{figure}
    }%
    \only<2>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\textwidth]{hexagram-64.png}
            \caption{The 64 hexagrams}
        \end{figure}
    }%
\end{frame}

%\begin{frame}
%    \frametitle{The number of strings}
%
%    There is a bijection between the set of strings of length
%    $7$ and my breakfast of a week, e.g.,
%    \begin{itemize}
%        \item 
%            \emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}
%            --- Having \emoji{apple} everyday.
%        \item 
%            \emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{apple}\emoji{cherries}\emoji{cherries}
%            --- \emoji{apple} for weekdays and \emoji{cherries} for weekends.
%        \item 
%            \emoji{apple}\emoji{banana}\emoji{cherries}\emoji{apple}\emoji{banana}\emoji{cherries}\emoji{apple}
%            --- Rotating \emoji{apple} and \emoji{banana} and \emoji{cherries}.
%    \end{itemize}
%
%    \cake{} What is the number of words of length $7$ from an $3$ letter
%    alphabet?
%
%    \cake{} What is the number of words of length $n$ from an $m$ letter
%    alphabet?
%\end{frame}

\begin{frame}[c]
    \frametitle{Example: My lunch}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            My sandwich consists of
            \begin{enumerate}
                \item One bread: \emoji{croissant}, \emoji{bread}, \emoji{bagel}
                \item One protein: \emoji{mushroom}, \emoji{avocado}
                \item One sauce: \emoji{tomato}, \emoji{hot-pepper}
            \end{enumerate}

            \cake{} How many different types of sandwiches can I make?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bagel.jpg}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}


\begin{frame}
    \frametitle{The number of strings with restrictions}
    The \emoji{sandwich} problem is to ask for the number of elements in the Cartesian
    product
    \begin{equation*}
        \{\temoji{croissant}, \temoji{bread}, \temoji{bagel}\}
        \times
        \{\temoji{mushroom}, \temoji{avocado}\}
        \times
        \{\temoji{tomato}, \temoji{hot-pepper}\}
    \end{equation*}

    \pause{}

    \think{} If we require a string $s$ to satisfy
    \begin{equation*}
        s(i) \in X_{i} \subseteq X \qquad \text{for all } i \in [n]
    \end{equation*}
    what is the number of such $s$?
\end{frame}

%\begin{frame}
%    \frametitle{Strings with restrictions}
%
%    In \emoji{sweden}, 
%    a vehicle plate has three letters, 
%    followed by three digits.
%    The letters I, Q, V are not allowed.
%    \begin{figure}
%        \centering
%        \includegraphics[width=0.4\textwidth]{license.jpg}
%    \end{figure}
%
%    \cake{} How many such vehicle plates can there be?
%\end{frame}

\begin{frame}
    \frametitle{Example 2.3: The Number of Passwords}

    How many passwords satisfy
    \begin{itemize}
        \item The first letter is an upper-case letter
        \item The second to the sixth character must be a letter or a digit
        \item The seventh must be either @ or .
        \item The eighth through twelfth positions allow lower-case letters, *, \%, and \#
        \item The thirteenth position must be a digit
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The number of passwords --- solution}

    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{password.jpg}
    \end{figure}


    So the number of possible password is
    \[
        26 \times 62^5 \times 2 \times 29^{5} \times 10
        =
        9771287250890863360
        \approx
        9.77 \times 10^{18}.
    \]
\end{frame}

%\begin{frame}
%    \frametitle{How good is your intuition with large numbers?}
%    
%    If a \emoji{robot} can guess $10^3$ password per second, 
%    how many years will it take to try all 
%    $9.77 \times 10^{18}$ passwords?
%
%    \begin{enumerate}
%        \item Less than $10^3$.
%        \item Between than $10^3$ to $10^6$.
%        \item Between than $10^6$ to $10^9$.
%        \item More than $10^9$.
%    \end{enumerate}
%\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} A bad password}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            People often choose a password by substituting, e.g.,
            \begin{equation*}
                \text{Trobador}
                \Rightarrow
                \text{Tr0b4dor}
            \end{equation*}
            The number of such password is only $(26+10)^{8} \approx 2.8 \times 10^{12}$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{troubadour.jpg}
                \caption*{A trobador}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\zany{} A good password}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            But if you choose four random English words, e.g.,
            \begin{center}
                correct horse battery staple
            \end{center}
            then you have about $20,000^4 \approx 1.6 \times 10^{17}$.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{correct-horse-battery-staple.jpg}
                \caption*{However ridiculous your password is, AI can make sense of it!
                \laughing{}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{\laughing{} My WiFi Password}

    Can you guess what is my WiFi password?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{wifi-password.jpg}
    \end{figure}
\end{frame}

\subsection{2.2 Permutations}

\begin{frame}[c]
    \frametitle{Letters from a bag}

    \begin{columns}[c]
        \column{0.5\textwidth}

        \colmedskip{}

        Put the 26 letters of English alphabet in a bag.

        Take six letters out one by one, without replacement.

        This makes a string of length six.

        \cake{} Could this string be \alert{yellow}?

        \column{0.5\textwidth}

        \begin{figure}
            \centering
            \includegraphics[width=\textwidth]{letter.jpg}
        \end{figure}


    \end{columns}

\end{frame}

\begin{frame}
    \frametitle{What is a permutation?}

    An $X$-string $s = x_1 x_2 \dots x_n$ is a \alert{permutation} if
    all $n$ letter in $s$ are \emph{distinct}.

    \cake{} How many $X$-permutations of length $n$ are there if $\abs{X} < n$?

    \pause{}
    
    \cake{} Which of the following strings are permutations?

    \begin{itemize}
        \item $(12, 7, 8, 6, 4, 9, 11)$
        \item $(X, y, a, A, D, 7, B, E, 9)$
        \item $(5, b, 7, 2, 4, 9, A, 7, 6, X)$
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Factorials}
    
    For $n \in \dsN$, we define 
    \begin{equation*}
        n!
        =
        \begin{cases}
            1 & n = 0 \\
            n (n-1)! & n > 0
        \end{cases}
    \end{equation*}

    We call $n!$ the \alert{factorial of $n$}.

    \pause{}
    For integers $m \ge n \ge 0$, we define
    \begin{equation*}
        P(m, n) 
        = 
        \frac{m!}{m-n}!
        =
        m (m-1) \dots (m-n+1)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Proposition 2.6 --- The Number of Permutations}

    If $X$ is an $m$-element set and $n$ is a positive integer with $m ≥ n$, 
    then the number of $X$-strings of length $n$ that are permutations is $P(m, n)$.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    In \emoji{sweden}, 
    a vehicle plate has three letters, 
    followed by three digits.
    The letters I, Q, V are not allowed.

    In addition, the three letters must be \emph{distinct}.
    
    \begin{figure}
        \centering
        \includegraphics[width=0.4\textwidth]{license.jpg}
    \end{figure}

    \cake{} How many such vehicle plates can there be?
\end{frame}

%\begin{frame}
%    \frametitle{An election}
%
%    \begin{columns}
%        \column{0.5\textwidth}
%
%        A group of 40 students holds elections to select a prime minister,
%        a deputy prime minister,
%        and a minister for finance. 
%
%        Assuming that a student \alert{cannot} hold more than one position, how many different outcomes are possible?
%
%        \column{0.5\textwidth}
%
%        \begin{figure}
%            \centering
%            \includegraphics[width=0.5\textwidth]{vote.jpg}
%        \end{figure}
%    \end{columns}
%\end{frame}

\subsection{2.3 Combinations}

\begin{frame}
    \frametitle{Combinations}

    Let $X$ be a finite set and $k$ be an integer with $0 \le k \le \abs{X}$.

    Then a $k$-element subset of $X$ is called a \alert{combination} of size $k$.

    When $\abs{X} = n$, we write the number of the number of such combinations
    \begin{equation*}
        \binom{n}{k}
    \end{equation*}
    which reads as $n \text{ choose } k$.

    We can also write this as $C(n,k)$.

    \hint{} Numbers like $\binom{n}{k}$ are called \alert{binomial coefficients}.
\end{frame}

\begin{frame}
    \frametitle{Why it is hard to choose food?}

    A restaurant offers
    \emoji{hamburger}\emoji{pizza}\emoji{fries}.

    We want to order 2 different types of foods.

    \cake{} If we care about the \emph{order} of the food, in how many ways
    can we do this?

    \pause{}

    What if we do \alert{not} care about the order?
\end{frame}

%\begin{frame}
%    \frametitle{The number of combinations}
%
%    The restaurant problem is the same as asking if we can choose \( n \) different
%    letters from an \( m \)-letter alphabet, regardless of their order, in how many ways can we do it.
%    
%    This is called the number of \alert{combinations} of \( n \) letters taken from
%    an \( m \)-letter alphabet,
%    denoted by \( C(m,n) \).
%
%    How can we compute \( C(m,n) \)?
%\end{frame}

\begin{frame}[t]
    \frametitle{Proposition 2.9 --- The Number of Combinations}

    For \(0 \le n \le m\)
    \[
        C(m,n)
        = 
        \frac{P(m,n)}{n!}
        =
        \frac{m(m-1)\dotsm(m-n+1)}{n!}
        =
        \frac{m!}{n!(m-n)!}
    \]

    \pause{}

    \begin{exampleblock}{\zany{} An assuming corollary}
        The proposition implies that
        \[
            \frac{m!}{n!(m-n)!}
        \] 
        is an integer for any integers $m \ge n \ge 0$.

        This is not easy to prove without proposition 2.9.
    \end{exampleblock}
\end{frame}

%\begin{frame}
%    \frametitle{Binomial Coefficients}
%
%    Another way to write $C(m,n)$ is
%    \[
%        \binom{m}{n},
%    \]
%    which reads ``\(m\) choose \(n\)''.
%
%    This number is called a \alert{binomial coefficient}. 
%
%    We will see where the name comes soon!
%\end{frame}

\begin{frame}{Proposition 2.10 --- A combinatorial identity}
    For all integers $m, n$ with \(0 \le n \le m\)
    \[
        \binom{m}{n}=\binom{m}{m-n}.
    \]
    \only<1>{Proof with Proposition 2.9:}%
    \only<2>{Proof with a combinatorial argument:}
\end{frame}

%%\begin{frame}
%%    \frametitle{Election --- revisited}
%%
%%    \begin{columns}
%%        \column{0.5\textwidth}
%%
%%        A group of 40 students holds elections to select a parliament of three members. 
%%
%%        Assuming that there is \alert{no} difference among the roles of each
%%        member of parliament, how many different outcomes are possible?
%%
%%        \column{0.5\textwidth}
%%
%%        \begin{figure}
%%            \centering
%%            \includegraphics[width=0.5\textwidth]{vote.jpg}
%%        \end{figure}
%%    \end{columns}
%%\end{frame}
%
%\appendix{}
%
%\begin{frame}[c]
%    \frametitle{Assignment}
%
%    \begin{columns}[c]
%        \begin{column}{0.5\textwidth}
%            \exercisepic{\lecturenum}
%        \end{column}
%        \begin{column}{0.5\textwidth}
%            \courseassignment{}
%
%            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
%            \begin{itemize}
%                \item[\emoji{pencil}] Section 2.9: 1, 3, 5, 7, 9, 11, 13, 21.
%            \end{itemize}
%        \end{column}
%    \end{columns}
%\end{frame}

\end{document}
