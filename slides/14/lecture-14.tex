\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\input{../venn.tex}
\input{../table.tex}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Inclusion-Exclusion (2)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item Section 7.3-7.4.
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Section 7.7: 15, 17, 
                            19\footnote{Use a \emoji{computer}}
                            21,
                            22\footnote{See
                            \href{https://en.wikipedia.org/wiki/Derangement\#Counting\_derangements}{Wikipedia}},
                            27.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{Review of Notations}

\begin{frame}
    \frametitle{Example 7.4}
    
    Let $X$ be the set of all functions from $[n]$ to $[m]$.

    Let $P_{i}$ be the property that there is \emph{no} of $j \in [n]$ with
    \begin{equation*}
        f(j) = i.
    \end{equation*}

    \cake{} Which ones of $\{P_{1}, P_{2}, \ldots, P_{5}\}$ does the following function satisfy?

    \begin{center}
        \begin{tabular}{ c | c c c c c}
            $i$ & $1$ & $2$ & $3$ & $4$ & $5$
            \\
            \hline
            $f(i)$  & $2$ & $3$ & $2$ & $2$ & $3$
        \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Example 7.5: Permutations}
    
    Let $X$ be the set of bijective functions from $[n]$ to $[n]$.

    The elements of $X$ are called \alert{permutations} of $[n]$.

    For example, the following function $\sigma$ is permeation of $[5]$

    \begin{center}
        \begin{tabular}{ c | c c c c c}
            $i$ & $1$ & $2$ & $3$ & $4$ & 5
            \\
            \hline
            $\sigma(i)$  & $2$ & $4$ & $3$ & $1$ & $5$
        \end{tabular}
    \end{center}
    
    \only<2>{%
        \hint{} We can also view a permutation of $[n]$ as a string length $n$ with
        alphabet $[n]$ which does not duplicate characters.

        \cake{} Can you give some examples of permutations of $[5]$?
    }%

    \only<3>{%
        Let $P_i$ be the property of $\sigma$ that $\sigma(i) = i$.

        \cake{} Which of $P_1, P_{2}, \ldots, P_{5}$ does $\sigma$ satisfy?
    }%
\end{frame}

\section{\acs{ac} 7.3 Enumerating Surjections}

\begin{frame}
    \frametitle{Review: Surjective (Onto) Functions}

    When a function's range contains the codomain, we
    say that is a \alert{surjective/onto function} or a \alert{surjection}.

    \begin{figure}[htpb]
        \centering
        \input{../tikz-snippet/surjection.tex}
        \caption{\cake{} Which of the functions are surjective?}
    \end{figure}

    \pause{}
    
    \begin{table}[htpb]
        \centering
        \begin{tabular}{ c | c c c c c}
            $i$ & $1$ & $2$ & $3$ & $4$ & $5$
            \\
            \hline
            $f(i)$  & $2$ & $4$ & $3$ & $2$ & $4$
        \end{tabular}
        \hfil
        \begin{tabular}{ c | c c c c c}
            $i$ & $1$ & $2$ & $3$ & $4$ & $5$
            \\
            \hline
            $f(i)$  & $1$ & $3$ & $2$ & $4$ & $1$
        \end{tabular}
        \caption{\cake{} Which of the functions $f:[5] \mapsto [4]$ are surjective?}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{5 Books and 3 Students}

    We want to give $5$ \emph{distinct} 
    \book{} to to $3$ \emph{distinct} \student{}
    such that each \student{} gets at least one \book{}.  

    \think{} In how many ways can we do this?

    \only<1>{
        \cake{} What is the difference between this problem and the one about giving $10$
        \carrot{} to $3$ \bunny{}?
    }

    \pause{}

    This is to ask for the number of surjections from $[5]$ to $[3]$.

    \only<2>{%
        A valid distribution of books could be:
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[scale=0.8]
                \drawFunction{5}{3}{1,2,1,3,3};
            \end{tikzpicture}
            \caption{A surjection from $[5]$ to $[3]$}
        \end{figure}
    }%
    \only<3>{%
        An invalid distribution of books could be:
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[scale=0.8]
                \drawFunction{5}{3}{1,3,1,1,3};
            \end{tikzpicture}
            \caption{Not a surjection from $[5]$ to $[3]$}
        \end{figure}
    }%
\end{frame}

\begin{frame}
    \frametitle{The Surjection Numbers}

    Let $S(n, m)$ be the number of surjections from $[n]$ to $[m]$.
    This is called the \alert{surjection number}.

    \only<1-4>{%
        Let $X$ be the set of all functions from $[n]$ to $[m]$.
    }%

    Let $P_{i}$ be the property of $f \in X$ that 
    $i$ is \emph{not} in the range of $f$.

    \pause{}

    \only<2-4>{%
        \cake{} Which $P_i$ does the following function $f$ satisfy?
    }%

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[scale=0.8]
            \only<+>{\drawFunction{5}{3}{1,3,1,1,3}};
            \only<+>{\drawFunction{5}{3}{2,2,2,2,2}};
            \only<+->{\drawFunction{5}{3}{1,2,1,3,3}};
        \end{tikzpicture}
        \caption{A \alt<4>{}{non-}surjective function $f:[5] \mapsto [3]$}
    \end{figure}

    \only<+>{%
        \hint{} So $S(n,m)$ is the number of functions from $[n]$ to $[m]$ 
        which satisfies \blankshort{} of $P_{i}$'s.
    }%
\end{frame}

\begin{frame}
    \frametitle{Examples of $N(Y)$}

    The following function $f:[5]\mapsto[3]$ satisfies \only<1>{$P_{2}$}\only<2>{$P_{1}, P_{3}$}.

    \cake{} What is \only<1>{$N(\{2\})$}\only<2>{$N(\{1, 3\})$}, i.e., the number of such functions?
    
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[scale=0.8]
            \only<1>{\drawFunction{5}{3}{1,3,1,1,3}}
            \only<2>{\drawFunction{5}{3}{2,2,2,2,2}}
        \end{tikzpicture}
        \caption{A function $f:[5] \mapsto [3]$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lemma 7.8: $N(Y)$}
    %Let $S(n, m)$ be the number of surjections from $[n]$ to $[m]$.

    Let $X$ be the set of all functions from $[n]$ to $[m]$.

    Let $P_{i}$ be the property of $f \in X$ that 
    $i$ is \emph{not} in the range of $f$.

    For $Y \subseteq [m]$ with $\abs{Y} = k$, we have
    \begin{equation*}
        N(Y) = (\blankshort{})^{n}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 7.9: Surjection Numbers}

    The number of surjections from $[n]$ to $[m]$ is
    \begin{equation*}
        S(n,m)
        =
        \sum_{k=0}^{m} (-1)^{k} \binom{m}{k}(m-k)^{n}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise: 4 Books and 3 Students}

    We want to give $4$ \emph{distinct} 
    \book{} to to $3$ \emph{distinct} \student{}
    such that \emph{at most one} \student{} gets no books.

    \pause{}

    Answer:
    \begin{equation*}
        S(4, 3) + 3 \cdot S(4, 2)
        =
        78
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example: Distinct Books and Identical Students}
    
    We want to give $5$ \emph{distinct} \book{}
    to three \student{} who are identical triplets such that each \student{} gets at least
    one book.

    \bomb{} We cannot distinguish the three \student{}.

    \think{} In how many ways can we do this?

    \pause{}

    For example, one way to do this by splitting the books into subsets as follows:
    \begin{equation*}
        \{\{1, 3, 5\}, \{2\}, \{4\}\}
    \end{equation*}
    \hint{}
    In how many ways can we turn the above partition of \book{} into a surjection from $[5]$ to $[3]$?
\end{frame}

\begin{frame}
    \frametitle{Stirling Number of the Second Kind}

    \alert{Stirling numbers of the second kind},
    denoted by \(\left\{ {n \atop k} \right\}\)
    is the number of ways to partition a set of $n$ elements into $k$ non-empty subsets.

    The above example is asking for $\left\{ {5 \atop 3} \right\}$.

    \pause{}

    Using the argument in the example above, we can see that
    \begin{equation*}
        \left\{ {n \atop k} \right\}
        =
        \frac{S(n, k)}{k!}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example: Stirling Number of the Second Kind}

    From the table below,
    we see that 
    \only<1>{%
        \begin{equation}
            \left\{ {n \atop k} \right\}=0,
            \qquad
            \text{for all $k > n \geq 1$}
        \end{equation}
    }%
    \only<2>{%
        \begin{equation*}
            \left\{ {n \atop 1} \right\}=\left\{ {n \atop n} \right\}=1
            \qquad
            \text{for all $n \geq 1$}
        \end{equation*}
    }%
    \cake{}
    Why is this?


    \begin{table}[htpb]
        \centering
        \begin{tabular}{ c|c c c c c }
            \hline
            \diagbox[height=2em]{$k$}{$n$} & 1 & 2 & 3 & 4 & 5 \\ \hline
            1 & 1 & 0 & 0 & 0 & 0 \\ \hline
            2 & 1 & 1 & 0 & 0 & 0 \\ \hline
            3 & 1 & 3 & 1 & 0 & 0 \\ \hline
            4 & 1 & 7 & 6 & 1 & 0 \\ \hline
            5 & 1 & 15 & 25 & 10 & 1 \\ \hline
        \end{tabular}
        \caption{$\left\{ {n \atop k} \right\}$ --- Stirling numbers of the second kind}
    \end{table}

    \emoji{eye} See \href{https://oeis.org/A008277}{OEIS} find more of these numbers.
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise: 4 Books and 3 Students}

    We want to give $4$ \emph{distinct} 
    \book{} to to $3$ \student{} who \emph{identical triplets} 
    such that \emph{at most one} \student{} gets no books.

    \bomb{} We cannot distinguish the three \student{}.

    \pause{}

    Answer:
    \begin{equation*}
        \left\{ {4 \atop 3} \right\}
        +
        \left\{ {4 \atop 2} \right\}
        =
        13
    \end{equation*}
\end{frame}

\section{\acs{ac} 7.4 Derangement}

\begin{frame}[c]
    \frametitle{A \colorbox{white}{\emoji{mortar-board}} puzzle}
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colmedskip{}

            Suppose that $4$ \student{} get very excited in their graduation ceremony and throw
            their \emoji{mortar-board} into the air.

            \think{} In how many ways can they
            \begin{itemize}
                \item each gets exactly one \emoji{mortar-board} back,
                \item but no one get their own \emoji{mortar-board}.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{bunny-mortar-board.jpg}
                \caption{A very exciting graduation ceremony}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

%\begin{frame}
%    \frametitle{Review: Injective (One-to-one) Function}
%
%    When each element of the codomain is the image at most one
%    element of the domain, 
%    we say that a function is an
%    \alert{injection}.
%
%    \begin{figure}[htpb]
%        \centering
%        \input{../tikz-snippet/injection.tex}
%        \caption{\cake{} Which of the functions are injective?}
%    \end{figure}
%
%    \pause{}
%    
%    \begin{table}[htpb]
%        \centering
%        \begin{tabular}{ c | c c c c}
%            $i$ & $1$ & $2$ & $3$ & $4$
%            \\
%            \hline
%            $f(i)$  & $2$ & $5$ & $3$ & $1$
%        \end{tabular}
%        \hfil
%        \begin{tabular}{ c | c c c c }
%            $i$ & $1$ & $2$ & $3$ & $4$
%            \\
%            \hline
%            $f(i)$  & $2$ & $5$ & $2$ & $1$
%        \end{tabular}
%        \caption{\cake{} Which of the functions $f:[4] \mapsto [5]$ are injective?}
%    \end{table}
%\end{frame}

\begin{frame}
    \frametitle{Derangements}

    \only<1>{%
        The way to give \emoji{mortar-board} can be seen as a \emph{bijective} 
        function $\sigma: [4] \mapsto [4]$
        because each \student{} gets one \emoji{mortar-board} back.

        In other words $\sigma$ must be a \emph{permutation} of $[4]$.
    }%

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[scale=1]
            \only<1>{\drawFunction{4}{4}{3,1,2,4}};
            \only<2->{\drawFunction{4}{4}{4,3,1,2}};
        \end{tikzpicture}
        \caption{%
            \only<1>{%
                A bijection from $[4]$ to $[4]$
            }%
            \only<2>{%
                A bijection from $[4]$ to $[4]$ that satisfies the requirement
            }%
            \only<3>{%
                A derangement
            }%
        }%
    \end{figure}
    \only<1>{%
        \cake{} Do the above permutation/bijection our requirement?
    }%

    \only<2->{%
        To satisfy the requirement that 
        \emph{no one get their own \emoji{mortar-board}},
        we must have $\sigma(i) \ne \blankshort{}$ for all $i \in [4]$.
    }%

    \only<3>{%
        A permutation $\sigma:[n] \mapsto [n]$
        which satisfies
        \begin{equation*}
            \sigma(i) \ne \blankshort{}
            ,
            \qquad
            \text{for all $i \in [n]$}
        \end{equation*}
        is called a \alert{derangement}.

        We use $d_{n}$ to denote the number of derangements on $[n]$.
    }
\end{frame}

\begin{frame}
    \frametitle{Example: Derangements}

    \cake{} Which ones of the following are derangements?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            \begin{center}
                \begin{tabular}{ c | c c c c }
                    $i$ & $1$ & $2$ & $3$ & $4$
                    \\
                    \hline
                    $\sigma(i)$  & $2$ & $1$ & $3$ & $4$
                \end{tabular}
            \end{center}
            \begin{center}
                \begin{tabular}{ c | c c c c }
                    $i$ & $1$ & $2$ & $3$ & $4$
                    \\
                    \hline
                    $\sigma(i)$  & $2$ & $5$ & $4$ & $3$
                \end{tabular}
            \end{center}
        \end{column}
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            \begin{center}
                \begin{tabular}{ c | c c c c }
                    $i$ & $1$ & $2$ & $3$ & $4$
                    \\
                    \hline
                    $\sigma(i)$  & $2$ & $4$ & $3$ & $1$
                \end{tabular}
            \end{center}
            \begin{center}
                \begin{tabular}{ c | c c c c }
                    $i$ & $1$ & $2$ & $3$ & $4$
                    \\
                    \hline
                    $\sigma(i)$  & $2$ & $4$ & $1$ & $3$
                \end{tabular}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Properties of Permutations}

    Let $X$ be the set of all permutations of $[n]$.

    Let $P_{i}$ denote the property of $\sigma$ that $\sigma(i) = i$.

    The number $i$ is called a \alert{fixed point} of $\sigma$.

    \pause{}

    \cake{} 
    Which of $P_1, P_{2}, \dots, P_{4}$ does $\sigma$ satisfy?

    \begin{table}[htpb]
        \centering
        \begin{tabular}{ c | c c c c }
            $i$ & $1$ & $2$ & $3$ & $4$
            \\
            \hline
            $\sigma(i)$  & $2$ & $1$ & $3$ & $4$
        \end{tabular}
        \hfil
        \begin{tabular}{ c | c c c c }
            $i$ & $1$ & $2$ & $3$ & $4$
            \\
            \hline
            $\sigma(i)$  & $2$ & $4$ & $3$ & $1$
        \end{tabular}
        \caption{Examples of fixed points}
    \end{table}

    \pause{}

    \cake{} If $\sigma$ is a derangement, 
    which of $P_1, P_{2}, \dots, P_{n}$ does $\sigma$ satisfy?
\end{frame}

\begin{frame}
    \frametitle{Compute $N(Y)$ When $\abs{Y} = 1$}

    \cake{} 
    If $n=4$, what is $N(\{1\})$?

    \hint{} 
    How many permutation $\sigma$ are there with $\sigma(1) = 1$?
    \begin{center}
        \begin{tabular}{ c | c c c c }
            $i$ & $1$ & $2$ & $3$ & $4$
            \\
            \hline
            $\sigma(i)$  & $1$ & \txtq{} & \txtq{}  & \txtq{}
        \end{tabular}
    \end{center}

    \pause{}

    \cake{} What about $N(\{2\}), N(\{3\}), N(\{4\})$?

    \pause{}

    \cake{} What is $N(i)$ for $i \in [n]$ in general?
\end{frame}

\begin{frame}
    \frametitle{Compute $N(Y)$ When $\abs{Y} = 2$}

    \cake{} 
    If $n=4$, what is $N(\{1, 4\})$?

    \hint{} 
    How many permutation $\sigma$ are there with $\sigma(1) = 1, \sigma(4) = 4$?
    \begin{center}
        \begin{tabular}{ c | c c c c }
            $i$ & $1$ & $2$ & $3$ & $4$
            \\
            \hline
            $\sigma(i)$  & $1$ & \txtq{} & \txtq{}  & $4$
        \end{tabular}
    \end{center}

    \pause{}

    \cake{} What about $N(\{1, 2\}), N(\{1, 3\}), N(\{2, 3,\}), \ldots$?

    \pause{}

    \cake{} What is $N(\{i, j\})$ for $i, j \in [n]$ with $i \ne j$ in general?
\end{frame}

\begin{frame}
    \frametitle{Lemma 7.10. (\acs{ac}) --- A Formula for $N(Y)$}

    For each subset $Y \subseteq [n]$ with $\abs{Y}=k$, we have
    \begin{equation*}
        N(Y) = \blankshort{}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 7.11 --- The Number of Derangements}
    For each positive integer $n$,
    the number $d_n$ of derangements of $[n]$ satisfies
    \begin{equation*}
        d_n=\sum_{k=0}^n (-1)^k\binom{n}{k}(n-k)!.
    \end{equation*}
\end{frame}

\begin{frame}[label=current]
    \frametitle{Theorem 7.12 --- The Probability of Getting a Derangement}

    Let $d_n$ denote the number of derangements of $[n]$.
    Then
    \begin{equation*}
        \lim_{n \to \infty} \frac{d_{n}}{n!} = e^{-1}
    \end{equation*}

    \zany{} This means when there are many many \emoji{mortar-board}, 
    the probability of that no \student{} gets their own \emoji{mortar-board} 
    back is about $\frac{1}{e}$.

    \pause{}

    \hint{} Which function does the following power series define?
    \begin{equation*}
        \sum_{n=0}^{\infty} \frac{x^{n}}{n!}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Suppose instead of requiring all $4$ \student{} to \emph{not} to have their \emoji{mortar-board}, 
    we want precisely $2$ of them to \emph{get} their own \emoji{mortar-board}.

    \cake{} In how many ways can we do this?

    \pause{}

    \hint{} In how many ways can you choose the two \student{} who get their
    \emoji{mortar-board} back?
    In how many ways can you give the other two \emoji{mortar-board}?
\end{frame}

\end{document}
