\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

% Some graphs
\input{../graphs/k4-planar.tex}
\input{../graphs/k4.tex}
\input{../graphs/k3-3.tex}
\input{../graphs/petersen.tex}

\title{Lecture \lecturenum{} --- Planar Graphs}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 5.5
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 5.9: 29, 31, 33, 34, 35..
                    \end{itemize}
            \end{itemize}
            \href{http://discrete.openmathbooks.org/dmoi3}{\acf{dm}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 4.3
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 4.3: 1, 2, 3, 5, 8, 12, 14, 15.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 5.5 Planar Graphs}

\subsection{Definitions of Planar Graphs}

\begin{frame}[c]
    \frametitle{Homes and Utilities}

    \think{} Can we connect three homes to three types of utilities so that the pipes and
    lines do not cross each other?

    \bigskip{}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            [
            scale=1.50,
            inner sep=1.5mm,
            vertex/.style={opacity=0,text opacity=1,font=\huge},
            every path/.style={lightgray,ultra thick},
            ]
            \node (water) at (0,1) [vertex] {\emoji{droplet}};
            \node (electricity) at (0,2) [vertex] {\emoji{zap}};
            \node (natural gas) at (0,3) [vertex] {\emoji{fire}};
            \node (home 1) at (4,3) [vertex] {\emoji{house}};
            \node (home 2) at (4,2) [vertex] {\emoji{hut}};
            \node (home 3) at (4,1) [vertex] {\emoji{house-with-garden}};
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Planar graphs}
    
    A \alert{planar graph} is a graph that \emph{can be} drawn on a plane in a way that 
    its edges intersect only at their endpoints.

    Such a drawing is called a \alert{planar embedding}.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (0,0) 
                pic[
                style={graph edge},
                every node/.style={graph node small, thin},
                scale=1.3,
                ]
                {k4};
        \end{tikzpicture}
        \hfil
        \begin{tikzpicture}
            \draw (0,0) 
                pic[
                style={graph edge},
                every node/.style={graph node small, thin},
                scale=1.8,
                ]
                {k4planar};
        \end{tikzpicture}
        \caption{\cake{} Is $K_{4}$ planar?}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The utility problem}

    The answer to the problem of the \emoji{fire} \emoji{zap} \emoji{droplet}
    is \emph{no}.

    This is because $K_{3,3}$ is \emph{not} planar.

    \smiling{} We will see why later.
    
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \draw (0,0) 
                pic[
                style={graph edge},
                every node/.style={graph node small},
                yscale=1.5,
                xscale=3.5,
                ]
                {k3-3};
        \end{tikzpicture}
        \caption{$K_{3,3}$ -- Not a planar graph}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A Trivial Check}
    
    If $H$ is \emph{not} planar, then any graph $G$ containing $H$ as a subgraph
    is \emph{not} planar.

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \node[graph node small] (n1) at (-2,1.5) {};
            \draw[graph edge] (n1) -- (0,0);
            \draw[graph edge] (n1) -- (0,3);
            \draw (0,0) 
                pic[
                style={graph edge},
                every node/.style={graph node small},
                yscale=1.5,
                xscale=3.5,
                ]
                {k3-3};
        \end{tikzpicture}
        \caption{\cake{} Is this graph planar?}%
    \end{figure}
\end{frame}

\subsection{Euler's Formula}

\begin{frame}[c]
    \frametitle{\zany{} Faces}
    
    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \alert{Face}:
            The front part of the head that in humans.

            \begin{flushright}
            \emph{Merriam-Webster Dictionary}
            \end{flushright}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.85\textwidth]{poker-face.jpg}
                \caption{Poker Face is a murder mystery \emoji{tv} show}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Faces of a planar graph}

    A \alert{face} of a planar drawing of a planar graph is a region bounded by edges and
    vertices and not containing any other vertices or edges.

    \begin{columns}[c, totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            \cake{} How many faces does this graph have?
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \begin{tikzpicture}[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=1.6,
                    ]
                    \clip (0.7,0.7) rectangle (3.5,3.6);
                    \foreach \i in {1,2}{
                        \foreach \j in {1,2,3}{
                            \pgfmathsetmacro\k{int(3*(\i-1)+\j)}
                            \node (\k) at (\i,\j) {};
                        }
                    }
                    \foreach \j in {1,2,3}{
                        \pgfmathsetmacro\k{int(\j+3)}
                        \draw (\j) -- (\k);
                    }
                    \foreach \i in {1,2}{
                        \foreach \j in {1,2}{
                            \pgfmathsetmacro\s{int(3*(\i-1)+\j)}
                            \pgfmathsetmacro\t{int(\s+1)}
                            \draw (\s) -- (\t);
                        }
                    }
                    \draw (1) -- (5);
                    \draw (4) to [controls=+(0:2) and +(45:2)] (3);
                \end{tikzpicture}
                \caption{Example: A planar graph}
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}


\begin{frame}
    \frametitle{Faces, Edges, and Vertices}

    Let $f$, $n$, $m$ be the number faces, vertices, and edges of a graph.

    \cake{} What are these numbers for the following graph?

    \only<1>
    {
        \begin{figure}
            \centering
            \begin{tikzpicture}
                \draw (0,0) 
                    pic[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=2.3,
                    ]
                    {k4planar};
            \end{tikzpicture}
            \caption{$K_4$}
        \end{figure}
    }
    \only<2>
    {
        \begin{figure}
            \centering
            \begin{tikzpicture}[
                scale=2,
                rotate=75,
                every node/.style={graph node small},
                every path/.style={graph edge},
                ]
                \node (1) at (0,0) {};
                \node (2) at (0,1) {};
                \node (3) at (1,0) {};
                \node (4) at (1,1) {};
                \node (5) at (0.5,0.5) {};
                \node (6) at (2,0) {};
                \draw (1) -- (2) -- (4) -- (3) -- (5) -- (1);
                \draw (1) -- (3) -- (6) -- (4);
                \draw (5) -- (2);
            \end{tikzpicture}
            \caption{A planar graph}
        \end{figure}
}
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 5.32 (\acs{ac}) --- Euler's Formula}

    Let $G$ be a connected planar graph with $n$ vertices and $m$ edges. 
    Every planar drawing of $G$ has $f$ faces, where $f$ satisfies 
    \begin{equation*}
        n - m + f = 2.
    \end{equation*}

    Proof by induction on $m$.

    Base case: $m = 0$.

    \cake{} What are $n$ and $f$ in this case?

\end{frame}

\begin{frame}[t]
    \frametitle{Induction step}

    Assume the theorem holds for all planar graphs with $m \ge 0$ edges.

    Now consider a connected planar graph $G$ with $m+1$ edges. 

    Let $G'$ be $G$ with one edge removed.

    \only<1>{Case 1: $G'$ is connected.}

    \only<2>{Case 2: $G'$ is not connected.}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    If a connected planar graph $G$ has $n$ vertices and $n-1$ edges,
    what kind of graph is $G$?

    \hint{} User Euler's formula to find the number of faces. 
    That will give you the answer.
\end{frame}

\subsection{Is This Planar? --- A Simple Check}

\begin{frame}
    \frametitle{Edges and faces}

    Given a graph $G$, 
    Let $p$ denote the number of tuples of the form $(e, F)$,
    where $e$ is an edge and a face $F$ is a face adjacent to $e$.


    \cake{} What are the number $p, m, f$ in the following $G$?
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}[
            style={graph edge},
            every node/.style={graph node small},
            scale=2,
            rotate=-45,
            ]
            \node (1) at (0, 0) {$v_1$};
            \node (2) at (1, 0) {$v_2$};
            \node (3) at (1, 1) {$v_3$};
            \node (4) at (0, 1) {$v_4$};
            \node (5) at (1, 2) {$v_5$};
            \draw (1) -- node[edge label] {$e_1$} (2);
            \draw (2) -- node[edge label] {$e_2$} (3);
            \draw (3) -- node[edge label] {$e_3$} (4);
            \draw (4) -- node[edge label] {$e_4$} (1);
            \draw (3) -- node[edge label] {$e_5$} (1);
            \draw (3) -- node[edge label] {$e_6$} (5);
            \node[draw=none, fill=none] (F1) at (0.75, 0.25) {$F_1$};
            \node[draw=none, fill=none] (F2) at (0.25, 0.75) {$F_2$};
            \node[draw=none, fill=none] (F3) at (0.5, 1.5) {$F_3$};
        \end{tikzpicture}
        \caption{A planar graph $G$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{An Inequality}
    
    Given any planar embedding of a planar graph $G$ with at least $3$ edges, we have
    \begin{equation*}
        3 \cdot f \le p \le 2 \cdot m
    \end{equation*}

    \only<1>{%
        Case 1: When $G$ is acyclic, 
        there is only one face (the outer face) and
        every edge is adjacent to it.
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}[
                    xscale=2,
                    style={graph edge},
                    every node/.style={graph node small},
                ]
                \node (1) at (0,0) {$v_1$};
                \node (2) at (1,1) {$v_2$};
                \node (3) at (2,0) {$v_3$};
                \node (4) at (3,1) {$v_4$};
                \draw (1) -- (2) -- (3) -- (4);
            \end{tikzpicture}
            \caption{Example: $f = 1$, $p = 3$, $m = 3$}
        \end{figure}
    }%
    \only<2>{%
        Case 2: When there are cycles in $G$, every edge is adjacent to at most $2$ faces,
        and every face is bounded by at least $3$ edges.
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \draw (0,0) 
                    pic[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=1.5,
                    ]
                    {k4planar};
            \end{tikzpicture}
            \caption{Example: $f = 4$, $p = 12$, $m = 6$}
        \end{figure}
    }%
\end{frame}

\begin{frame}[t]
    \frametitle{Theorem 5.33 (\acs{ac})}

    A planar graph with $n \ge 3$ vertices has at most $3n − 6$ edges.

    \only<1-2>{%
        Proof: 
    }%
    \only<1>{%
        If the graph has at $m \le 2$ edges, then
        \begin{equation*}
            m \le 2 < 3 \le 3n-6
        \end{equation*}
        since $n \ge 3$. Thus, the statement is true in this case.
    }%
    \only<2>{%
        Assuming that there are $m \ge 3$ edges, then we can use
        \begin{equation*}
            3 \cdot f \le p \le 2 \cdot m
        \end{equation*}
        and Euler's formula
        \begin{equation*}
            n - m + f = 2
        \end{equation*}
    }%
    \only<3>{%
       \cake{} Why does this implies $K_{5}$ is \emph{not} planar?

        \vspace{1.5cm}

        \cake{} What about $K_{n}$ with $n > 5$? Are they planar?

        \vspace{1.5cm}

        \cake{} Does the theorem apply to $K_{3,3}$?

        \vspace{1.5cm}
    }
\end{frame}

\begin{frame}[t]
    \frametitle{Proof that $K_{3,3}$ Is Not Planar}

    We can show $K_{3,3}$ is not planar via proof by contradiction.

    \begin{columns}
        \begin{column}{0.3\textwidth}
            \begin{figure}[htpb]
                \begin{tikzpicture}
                    \draw (0,0) 
                        pic[
                        style={graph edge},
                        every node/.style={graph node small},
                        scale=1.8,
                        ]
                        {k3-3};
                \end{tikzpicture}
                \caption{%
                    \cake{} $K_{3,3}$
                }%
            \end{figure}
        \end{column}
        \begin{column}{0.7\textwidth}
        \end{column}
    \end{columns}
\end{frame}


\subsection{Kuratowski's Theorem}

%\begin{frame}
%    \frametitle{Elementary subdivision}
%
%    Given $G$, we get a \alert{elementary subdivision} of $G$ by splitting one edge into two
%    with a new vertex.
%    
%    \begin{figure}[htpb]
%        \centering
%        \includegraphics[width=0.45\linewidth]{k4.pdf}
%        \includegraphics[width=0.45\linewidth]{k4-subdivision.pdf}
%        \caption{$K_{4}$ and its subdivision}%
%    \end{figure}
%\end{frame}

\begin{frame}
    \frametitle{Elementary Subdivision}

    Given $G$, we get a \alert{elementary subdivision} of $G$ by splitting one edge into two
    with a new vertex.
    
    \begin{figure}
        \begin{center}
            \begin{tikzpicture}[
                style={graph edge},
                every node/.style={graph node small},
                scale=3,
                ]
                \pgfmathsetmacro\x{0.5*cos(pi* (1+1/6) r)};
                \draw (0,0) 
                    pic[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=3,
                    ]
                    {k4planar};
                \only<1->{\node[fill=pink] (5) at (0,-0.5) {5};}
                \only<2>{\node[fill=pink] (6) at (\x,-0.5) {6};}
                \end{tikzpicture}
        \end{center}
        \caption{Example: $K_4$ subdivided \only<1>{once}\only<2>{twice}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Homeomorphism}

    Two graphs $G_{1}$ and $G_{2}$ are \alert{homeomorphic} 
    if they can be constructed
    from the same graph $G$ by a sequence (possibly zero) of subdivisions.

    \pause{}

    \cake{} Which of the following graphs are homeomorphic to each other?

    \begin{figure}
        \hfill
        \begin{subfigure}{0.32\textwidth}
            \begin{center}
                \begin{tikzpicture}[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=1.5,
                    ]
                    \newcommand*{\nv}{4};
                    \foreach \i in {1,2,...,\nv}
                    {
                        \pgfmathsetmacro{\x}{cos(2*\i*pi/\nv r)};
                        \pgfmathsetmacro{\y}{sin(2*\i*pi/\nv r)};
                        \node (\i) at (\x,\y) {};
                    }
                    \pgfmathsetmacro{\k}{int(\nv-1)};
                    \foreach \i in {1,...,\k}
                    {
                        \pgfmathsetmacro{\j}{int(\i+1)};
                        \draw (\i) -- (\j);
                    }
                    \draw (\nv) -- (1);
                    \draw (3) -- (1);
                    \draw (2) -- (4);
                    \node (5) at (0.5, 0) {};
                \end{tikzpicture}
            \end{center}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.32\textwidth}
            \begin{center}
                \begin{tikzpicture}[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=1.5,
                    ]
                    \newcommand*{\nv}{5};
                    \foreach \i in {1,2,...,\nv}
                    {
                        \pgfmathsetmacro{\x}{cos(2*\i*pi/\nv r)};
                        \pgfmathsetmacro{\y}{sin(2*\i*pi/\nv r)};
                        \node (\i) at (\x,\y) {};
                    }
                    \pgfmathsetmacro{\k}{int(\nv-1)};
                    \foreach \i in {1,...,\k}
                    {
                        \pgfmathsetmacro{\j}{int(\i+1)};
                        \draw (\i) -- (\j);
                    }
                    \draw (\nv) -- (1);
                    \draw (1) -- (3);
                    \draw (4) -- (2);
                \end{tikzpicture}
            \end{center}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.32\textwidth}
            \begin{center}
                \begin{tikzpicture}[
                    style={graph edge},
                    every node/.style={graph node small},
                    scale=1.5,
                    ]
                    \newcommand*{\nv}{5};
                    \foreach \i in {1,2,...,\nv}
                    {
                        \pgfmathsetmacro{\x}{cos(2*\i*pi/\nv r)};
                        \pgfmathsetmacro{\y}{sin(2*\i*pi/\nv r)};
                        \node (\i) at (\x,\y) {};
                    }
                    \pgfmathsetmacro{\k}{int(\nv-1)};
                    \foreach \i in {1,...,\k}
                    {
                        \pgfmathsetmacro{\j}{int(\i+1)};
                        \draw (\i) -- (\j);
                    }
                    \draw (\nv) -- (1);
                    \draw (1) -- (3);
                    \draw (2) -- (5);
                    \draw (3) -- (5);
                \end{tikzpicture}
            \end{center}
        \end{subfigure}
        \hfill
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 5.34 (Kuratowski's Theorem)}

    A graph is planar if and only if it does not contain a 
    \emph{subgraph} homeomorphic to
    either $K_5$ or $K_{3,3}$.

    \begin{figure}
        \begin{center}
            \begin{tikzpicture}[
                style={graph edge},
                every node/.style={graph node small},
                ]
                \draw (0,0) 
                    pic[
                    style={graph edge},
                    every node/.style={graph node small},
                    xscale=4,
                    yscale=1.7,
                    ]
                    {k3-3};
                \node (7) at ($(1)!0.5!(2)$) {};
                \draw (1) -- (3);
            \end{tikzpicture}
            \caption{Is this graph planar?}%
        \end{center}
        \hfill
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example: Kuratowski's Theorem}
    
    The following graph is not planar because it has a subgraph homeomorphic to
    $K_{3,3}$.

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.6\textwidth]{kuratowski-example-01.pdf}
        \includegraphics<2>[width=0.6\textwidth]{kuratowski-example-02.pdf}
        \includegraphics<3>[width=0.6\textwidth]{kuratowski-example-03.pdf}
        \includegraphics<4>[width=0.5\textwidth]{kuratowski-example-04.pdf}
        \caption{Example of Kuratowski's Theorem}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Show that Petersen graph is not planar.

    \hint{} Find a \emph{subgraph} which is homeomorphic to either $K_{3,3}$.

    \begin{figure}
        \hfill
        \begin{subfigure}{0.45\textwidth}
            \begin{center}
                \begin{tikzpicture}
                    \draw (0,0) 
                        pic[
                        style={graph edge},
                        every node/.style={graph node small},
                        scale=2,
                        ]
                        {petersen};
                \end{tikzpicture}
            \end{center}
            \caption{Peterson graph}
        \end{subfigure}
        \hfill
        \begin{subfigure}{0.45\textwidth}
            \begin{center}
                \begin{tikzpicture}
                    \draw (0,0) 
                        pic[
                        style={graph edge},
                        every node/.style={graph node small},
                        scale=1.5,
                        ]
                        {k3-3};
                \end{tikzpicture}
            \end{center}
            \caption{$K_{3,3}$}%
        \end{subfigure}
        \hfill
    \end{figure}
\end{frame}

\subsection{Four colour theorem}

\begin{frame}
    \frametitle{Theorem 5.37 (\acs{ac}) --- Four Colour Theorem}
    
    Every planar graph has chromatic number at most four.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{graph-four-colour-example-1.pdf}
        \caption{Example: A $4$-coloured planar graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Map colouring}

    The Four Colour Theorem implies that every map can be coloured with at most four colours.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{map.pdf}
        \caption{Colour \emoji{us} map with four colours}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{History of the four colour theorem}

    First conjectured in 1852 by Francis Guthrie.

    First proved with \emoji{laptop} 1976 by Kenneth Appel.

    A simpler proof with \emoji{laptop} in 1997 by Robertson, Sanders, Seymour, and Thomas.

    In 2005, the theorem was also proved by Georges Gonthier with
    \href{https://en.wikipedia.org/wiki/Coq}{Coq}.
\end{frame}

\begin{frame}
    \frametitle{Outerplanar Graph}
    
    An \alert{outerplanar graph} is a graph that has a planar drawing for which
    all vertices belong to the outer face of the drawing.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{Triangulation_3-coloring.png}
        \caption{Example: An outerplanar graph}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Prove that an outerplanar graph is three-colourable.

    \hint{} Given an outerplanar graph, add a vertex in the outer face
    and connect it to all other vertices. What do you get?

    \begin{figure}[htpb]
        \begin{center}
            \begin{tikzpicture}
                \node[fill=white, inner sep=20pt] (wrap) at (0,0) {
                        \begin{tikzpicture}[
                            scale=1, 
                            every node/.style={graph node small, inner sep=0pt},
                            every path/.style={graph edge},
                            ]
                            \begin{scope}[
                                rotate=-45
                                ]
                                \coordinate (c1) at (0,0);
                                \coordinate (c2) at (0.5,1);
                                \coordinate (c3) at (2,0);
                                \coordinate (c4) at (2,2);
                                \coordinate (c5) at (3,1);
                                \coordinate (c6) at (0,2);
                                \coordinate (c7) at (0.75,2);
                                \draw[fill=LightGrey] (c1) -- (c2) -- (c3) -- cycle;
                                \draw[fill=LightGrey] (c1) -- (c2) -- (c6) -- cycle;
                                \draw[fill=LightGrey] (c7) -- (c2) -- (c6) -- cycle;
                                \draw[fill=LightGrey] (c4) -- (c2) -- (c3) -- cycle;
                                \draw[fill=LightGrey] (c4) -- (c5) -- (c3) -- cycle;
                                \node[fill=green] (1) at (c1) {1};
                                \node[fill=DaylightBlue] (2) at (c2) {2};
                                \node[fill=Salmon] (3) at (c3) {3};
                                \node[fill=green] (4) at (c4) {4};
                                \node[fill=DaylightBlue] (5) at (c5) {5};
                                \node[fill=Salmon] (6) at (c6) {6};
                                \node[fill=green] (7) at (c7) {7};
                            \end{scope}
                            \node (z) at (5, 0.5) {};
                            \node[fill=none, draw=none] (zl) at (5, 0.1) {\small Added vertex};
                        \end{tikzpicture}
                };
            \end{tikzpicture}
        \end{center}
        \caption{An outerplanar graph and one added vertex}%
    \end{figure}
    
\end{frame}

\end{document}
