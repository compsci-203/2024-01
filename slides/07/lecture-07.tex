\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}
\usepackage{pgfplots}

%\includeonlyframes{current}

\title{Lecture \lecturenum{} --- Alternating Series}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \ac{ec}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 9.3 starting from integral test.
                        \item
                            Section 9.3
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item
                            Exercise 9.3: 1, 3, 5, 7.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ec} 9.2 Test of Convergence (Continued)}

\subsection{Integral Test}

\begin{frame}
    \frametitle{Integral Test}

    Let $\{a_n\}_{n \ge 1}$ be a sequence with positive terms.

    Let $f$ be a decreasing function with domain $[1, \infty)$ 
    and codomain $[0, \infty)$ such that
    \begin{equation*}
        f(n)=a_n,
        \qquad
        \text{for all } n \ge 1
    \end{equation*}

    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines = middle,
                    xlabel = \( n \),
                    ymin = 0,
                    ymax = 1.1,
                    xmin = 0,
                    xmax = 7,
                    width = 10cm,  % Adjust the width as needed
                    height = 5.5cm,  % Adjust the height as needed
                    legend entries={$f(x)$, $\{a_n\}$},
                    legend pos = north east,
                    ]
                    \addplot[thick, draw=red, domain=1:7, samples=100] {x^(-0.7)};
                    \addplot[ycomb, draw=blue, thick, mark=*, domain=1:6, samples=6] {x^(-0.7)};
                \end{axis}
                \end{tikzpicture}
                \caption{Example: $f(x)$ and $a_n$}
            \end{figure}
    }

    \only<2>{%
        Then
        \begin{equation*}
            \sum_{n=1}^{\infty} a_n
            \qquad
            \text{and}
            \qquad
            \int_{1}^{\infty} f(x) \, \dd{x}
        \end{equation*}
        both converge or both diverge.
    }
\end{frame}

\begin{frame}
    \frametitle{Proof of Integral Test: \alt<2>{Convergent}{Divergent} Case}

    Let $s_{n} = \sum_{k=1}^{n} a_k$ and $s_{n}'= \int_{1}^{n} f(x) \, \dd{x}$.
    Then \alt<2>{$s_{n} - a_{1} \le s_{n}'$}{$s_{n-1} \ge s_{n}'$}.
    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                axis lines = middle,
                xlabel = \( n \),
                ymin = 0,
                ymax = 1.1,
                xmin = 0,
                xmax = 7,
                width = 10cm,  % Adjust the width as needed
                height = 5.5cm,  % Adjust the height as needed
                domain=1:6,
                ]
                \only<1>{%
                    \addplot[const plot,
                        draw=blue,
                        fill=DaylightBlue, samples=6]
                        {x^(-0.7)} \closedcycle;
                    \addplot[draw=red, draw=red, fill=BrightRed, samples=100] {x^(-0.7)} \closedcycle;
                }
                \only<2>{%
                    \addplot[draw=red, draw=red, fill=BrightRed, samples=100] {x^(-0.7)} \closedcycle;
                    \addplot[const plot mark right,
                        draw=blue,
                        fill=DaylightBlue, samples=6]
                        {x^(-0.7)} \closedcycle;}
                \addplot[ycomb, draw=blue, thick, mark=*, samples=6] {x^(-0.7)};
            \end{axis}
        \end{tikzpicture}
        \caption{The blue area is $s_{\alt<2>{6}{5}}\only<2>{-a_1}$  and the red area is $s_{6}'$}
    \end{figure}

    \only<1>{%
        \cake{} Which test tells us $s_{n}' \to \infty$ imply $s_n \to \infty$?
    }
    \only<2>{%
        \cake{} Which test tells us $s_{n}'$ converges imply $s_n$ converges?
    }
\end{frame}

\begin{frame}
    \frametitle{Example 9.10}
    Show that the $p$-series $\sum_{n=1}^{\infty} \frac{1}{n^{p}}$ converges if
    $p>1$ and diverges if $p=1$ using the integral test.

    \pause{}

    \cake{} Using the argument for integral test, we can show that
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{1}{n} = \infty.
    \end{equation*}
    This is called the \alert{harmonic series}.

    \pause{}

    Why does this imply that
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{1}{n^{p}}
        =
        \infty
    \end{equation*}
    for $p \in [0,1)$?

\end{frame}

\subsection{$p$-series Test}

\begin{frame}
    \frametitle{$p$-series Test}

    The series $\sum_{n=1}^{\infty} \frac{1}{n^{p}}$ converges if and only if $p > 1$.

    \pause{}

    The test follows from Example 9.10 for $p \ge 0$.

    \cake{} How to show that $\sum_{n=1}^{\infty} \frac{1}{n^{p}}$ 
    diverges when $p < 0$?
\end{frame}

\subsection{Comparison Test for Series}

\begin{frame}{Comparison Test for Series}
    % Given Series
    Let \( \sum a_n \) and \( \sum b_n \) be two series
    such there exists $N$ such that
    \begin{equation*}
        0 \leq a_n \leq b_n, \quad \forall n > N
    \end{equation*}

    \only<1-2>{%
        % Implication for Convergence
        If \( \sum b_n \) converges, then \( \sum a_n \) also converges.
    }

    \only<2>{%
        Example:
        \begin{equation*}
            a_n = \frac{1}{n(n+1)},
            \qquad
            b_n = \frac{1}{n^{2}}.
        \end{equation*}
    }

    \only<3>{
        % Implication for Divergence
        If \( \sum a_n \) diverges, then \( \sum b_n \) also diverges.

        Example:
        \begin{equation*}
            a_n = \frac{1}{n},
            \qquad
            b_n = \frac{1}{n- \frac{1}{10}}.
        \end{equation*}
    }
\end{frame}

\subsection{Limit Comparison Test}

\begin{frame}{Limit Comparison Test}
    % Given Series
    Let \( \sum a_n \) and \( \sum b_n \) be two series with positive terms.
    Let
    \begin{equation*}
        L = \lim_{n \to \infty} \frac{a_{n}}{b_n}
    \end{equation*}

    \begin{itemize}[<+->]
        \item[\smiling{}] If \(0 < L < \infty\), then \( \sum a_n \) and \( \sum b_n \) both
            converge or both diverge.
        \item[\smiling{}] If \(L=0\) and \( \sum b_n \) converges,
            then \( \sum a_n \) converges.
        \item[\weary{}] If \(L=\infty\) and \( \sum b_n \) diverges,
            then \( \sum a_n \) diverges.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example 9.11}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{1}{n^{n}}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.12}
    \cake {} Is the following series convergent? Why?
    \begin{equation*}
        \sum_{n=1}^{\infty}
        \frac{n+3}{n 2^{n}}
    \end{equation*}
\end{frame}

\subsection{Telescope Series Test}

\begin{frame}
    \frametitle{Telescope Series Test}
    Let $\{b_n\}$ be a sequence.
    Let
    \begin{equation*}
        a_n = b_{n} - b_{n+1}.
    \end{equation*}
    Then $\sum a_n$ converges
    if and only if $b_n \to L \in \dsR$,
    in which case
    \begin{equation*}
        \sum_{n=1}^{\infty} a_n
        =
        b_1 - L
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.13}
    How to to show that
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{1}{n(n+1)}
        =
        1
        .
    \end{equation*}
\end{frame}

\subsection{Properties of Convergent Series}

\begin{frame}
    \frametitle{Properties of Convergent Series}

    Let $\sum a_n$ and $\sum b_n$ be convergent series,
    and let $c$ be a number.
    Then
    \begin{enumerate}
        \item $\sum(a_n \pm b_n)$ is convergent and $\sum (a_n \pm b_n) = \sum a_n \pm \sum b_n$.
        \item $\sum c \cdot a_n$ is convergent and $\sum c \cdot a_n = c \sum a_n$.
    \end{enumerate}

    \hint{} The proof follows from similar properties of limits of functions.
\end{frame}

\section{\acs{ec} 9.3 Alternating Series}

\subsection{Alternative Series Test}

\begin{frame}
    \frametitle{The First Example}

    We have seen that
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{1}{n} =
        \frac{1}{1} + \frac{1}{2} + \frac{1}{3} + \cdots
        =
        \infty
    \end{equation*}

    What if the signs of the terms alternate between positive and negative?
    \begin{equation*}
        \sum_{n=1}^{\infty} (-1)^{n-1}\frac{1}{n} =
        \frac{1}{1} - \frac{1}{2} + \frac{1}{3} - \frac{1}{4} + \cdots
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{A numeric experiment}

    Numeric computation suggests that
    \begin{equation*}
        \sum_{n=1}^{\infty} (-1)^{n-1}\frac{1}{n} \approx 0.693142
    \end{equation*}
    \cake{} Can you guess what this number is?
    \href{https://www.wolframalpha.com/input?i=0.693142}{Wolfram Alpha}

    \begin{figure}[htpb]
        \centering
        %\colorbox{white}{\includegraphics[width=0.5\textwidth]{alternating-series-01.pdf}}
        \begin{tikzpicture}
            \begin{axis}[
                axis lines = middle,
                xlabel={$n$},
                xmin=0, xmax=100,
                ymin=0.3, ymax=1,
                height=5cm,
                width=10cm,
                ]

                % Calculate the first few partial sums
                \addplot[
                    ycomb,
                    color=blue,
                    mark=*,
                    ]
                    coordinates {
                        (1, 1.0) (4, 0.583) (7, 0.76) (10, 0.646) (13, 0.73) (16, 0.663) (19, 0.719) (22, 0.671) (25, 0.713) (28, 0.676) (31, 0.709) (34, 0.679) (37, 0.706) (40, 0.681) (43, 0.705) (46, 0.682) (49, 0.703) (52, 0.684) (55, 0.702) (58, 0.685) (61, 0.701) (64, 0.685) (67, 0.701) (70, 0.686) (73, 0.7) (76, 0.687) (79, 0.699) (82, 0.687) (85, 0.699) (88, 0.687) (91, 0.699) (94, 0.688) (97, 0.698) (100, 0.688)
                    };
                \addplot[domain=0:100, color=gray, dashed] {0.693142};
            \end{axis}
        \end{tikzpicture}
        \caption*{Numeric values of $\sum_{n=1}^{k} (-1)^{n-1}\frac{1}{n}$}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Alternating Series}

    A series $\sum_{n=1}^{\infty} a_n$ is called an \alert{alternating series}
    if the sign of $a_n$ alternates between positive and negative.

    \cake{} Is the following alternating series convergent?
    \only<1>{%
        \begin{figure}[htpb]
            \centering
            %\colorbox{white}{\includegraphics[width=0.5\textwidth]{alternating-series-01.pdf}}
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines = middle,
                    xlabel={$n$},
                    xmin=0,
                    domain=0:22,
                    ymin=0.1, ymax=0.8,
                    height=5cm,
                    width=10cm,
                    ]

                    % Calculate the first few partial sums
                    \addplot[
                        ycomb,
                        color=blue,
                        mark=*,
                        ]
                        coordinates {
                            (1, 0.707) (2, 0.207) (3, 0.561) (4, 0.311) (5, 0.487) (6, 0.362) (7, 0.451) (8, 0.388) (9, 0.433) (10, 0.401) (11, 0.423) (12, 0.408) (13, 0.419) (14, 0.411) (15, 0.417) (16, 0.413) (17, 0.415) (18, 0.413) (19, 0.415) (20, 0.414) (21, 0.414)
                        };
                    \addplot[color=gray, dashed] {0.414};
                \end{axis}
            \end{tikzpicture}
            \caption*{Numeric values of $\sum_{n=1}^{k} (-1)^{n-1}\frac{1}{2^{n}}$}
        \end{figure}
    }
    \only<2>{%
        \begin{figure}[htpb]
            \centering
            %\colorbox{white}{\includegraphics[width=0.5\textwidth]{alternating-series-01.pdf}}
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines = middle,
                    xlabel={$n$},
                    xmin=0,
                    domain=0:202,
                    ymin=0.2, ymax=1,
                    height=5cm,
                    width=10cm,
                    ]

                    % Calculate the first few partial sums
                    \addplot[
                        ycomb,
                        color=blue,
                        mark=*,
                        ]
                        coordinates {
                            (1, 1.0) (10, 0.451) (19, 0.718) (28, 0.511) (37, 0.687) (46, 0.532) (55, 0.672) (64, 0.543) (73, 0.663) (82, 0.55) (91, 0.657) (100, 0.555) (109, 0.653) (118, 0.559) (127, 0.649) (136, 0.562) (145, 0.646) (154, 0.565) (163, 0.644) (172, 0.567) (181, 0.642) (190, 0.569) (199, 0.64)
                        };
                    \addplot[color=gray, dashed] {0.603};
                \end{axis}
            \end{tikzpicture}
            \caption*{Numeric values of $\sum_{n=1}^{k} (-1)^{n-1}\frac{1}{\sqrt{n}}$}
        \end{figure}
    }
\end{frame}

\begin{frame}
    \frametitle{Alternating Series Test}
    Let $\sum a_{n}$ be an alternating series.

    If the sequence $\{\abs{a_n}\}_{n=1}$ is monotone decreasing and $a_n \to 0$,
    then $\sum_{n=1}^{\infty} a_n$ is convergent.

\end{frame}

\begin{frame}
    \frametitle{Example 9.14}

    Is the following series convergent?
    \begin{equation*}
        \sum_{n=2}^{\infty} \frac{(-1)^{n}}{\log n}
    \end{equation*}
\end{frame}

\subsection{Absolute and Conditional Convergence}

\begin{frame}
    \frametitle{A Paradox?}

    Consider alternating series
    \begin{equation*}
        S = 1 - 1 + 1 - 1 \cdots
    \end{equation*}

    We can write it as
    \begin{equation*}
        S_1 = 1 - (1 - 1) - (1 - 1) \cdots = 1
    \end{equation*}
    and
    \begin{equation*}
        S_2 = (1 - 1) + (1 - 1) \cdots = 0
    \end{equation*}

    \astonished{} Does this mean that mathematics contains a paradox and is pure
    \emoji{shit}?
\end{frame}

\begin{frame}
    \frametitle{A Paradox?}

    Consider the \emph{convergent} alternating series.
    \begin{equation*}
        S = 1 - \frac{1}{2} + \frac{1}{3} - \frac{1}{4} + \frac{1}{5} - \frac{1}{6} +
        \frac{1}{7} - \frac{1}{8} + \dots = \log 2.
    \end{equation*}
    What happens when we rearrange $S$'s terms as below?
    \begin{equation*}
        S_1
        =
        \left(1 - \frac{1}{2}\right) - \frac{1}{4}
        + \left(\frac{1}{3} - \frac{1}{6}\right) - \frac{1}{8}
        + \left(\frac{1}{5} - \frac{1}{10}\right) - \frac{1}{12} + \dots
    \end{equation*}

    \pause{}

    \astonished{} Does this mean that mathematics contains a paradox and is pure
    \emoji{shit}?
\end{frame}

\begin{frame}
    \frametitle{Conditional and Absolute Convergence}

    A series $\sum a_n$ is said to be \alert{conditionally convergent} if
    it is convergent but $\sum \abs{a_n}$ is \emph{not}.
    For examples:
    \begin{equation*}
        \sum \frac{(-1)^{n-1}}{n}
        ,
        \qquad
        \sum \frac{(-1)^{n-1}}{\sqrt{n}}
    \end{equation*}

    \pause{}

    A series $\sum a_n$ is said to be \alert{absolute convergent} if
    $\sum \abs{a_n}$ is convergent.
    For examples:
    \begin{equation*}
        \sum \frac{1}{n^{3}}
        ,
        \qquad
        \sum -\frac{1}{2^{n}}
    \end{equation*}
\end{frame}


\begin{frame}{Example 9.14}
    Is the series $\sum_{n=1}^{\infty} \frac{(-1)^{n-1}}{n}$ divergent, \emph{conditionally} convergent or
    \emph{absolute} convergent?
\end{frame}

\begin{frame}
    \frametitle{Absolute Convergence Test}

    If $\sum \abs{a_n}$ is convergent,
    then $\sum a_n$ is convergent.

    \cake{} What is the contrapositive of this statement?
\end{frame}

\begin{frame}
    \frametitle{Riemann's Rearrangement Theorem}

    Given a conditionally convergent series such as \alert{Riemann's Rearrangement
    Theorem} we can rearrange its terms so that it converges to any number or diverges.

    \only<1>{%
        For example, the terms of the conditionally convergent series
        \begin{equation*}
            1 - \frac{1}{2} + \frac{1}{3} - \frac{1}{4} + \frac{1}{5} - \frac{1}{6} +
            \frac{1}{7} - \frac{1}{8} + \dots = \log 2
        \end{equation*}
        can be rearranged to get
        \begin{equation*}
            1 - \frac{1}{2} - \frac{1}{4} + \frac{1}{3} - \frac{1}{6} - \frac{1}{8}
            + \cdots
            =
            \frac{1}{2} \log 2
        \end{equation*}

        \cake{} How do we rearrange the terms of $S$ so that it diverges?
    }%

    \only<2>{%
        On the other hand, if $\sum a_{n}$ converges absolutely,
        then any rearrangement of this series converges to the \emph{same} limit.

        For example, since
        \begin{equation*}
            1 + \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16} + \cdots
            =
            1
        \end{equation*}
        converges absolutely,
        we have
        \begin{equation*}
            \frac{1}{2} + 1 + \frac{1}{8} + \frac{1}{4} + \frac{1}{32} + \frac{1}{16} + \cdots
            =
            1
        \end{equation*}
    }%
\end{frame}

\end{document}
