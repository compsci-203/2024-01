\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\title{Lecture \lecturenum{} --- Recurrence Equations (1)}

\begin{document}

\maketitle

\lectureoutline{}

\begin{frame}[c]
    \frametitle{Required Reading and Exercises}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \exercisepic{\lecturenum}
        \end{column}
        \begin{column}{0.5\textwidth}
            \href{https://www.rellek.net/book/app-comb.html}{\acf{ac}}
            \begin{itemize}
                \item \coursereading{}
                    \begin{itemize}
                        \item
                            Section 9.1-9.3
                        \item
                            Section 9.4.1
                    \end{itemize}
                \item \courseassignment{}
                    \begin{itemize}
                        \item Exercise 9.9: 1, 3, 5, 7.
                        \item Exercises in this slide deck.
                    \end{itemize}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{\acs{ac} 9.1 Introduction to Recurrence Equations}

\subsection{Fibonacci Numbers}

\begin{frame}[c]
    \frametitle{Fibonacci's Puzzle}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \colbigskip{}

            \only<1>{%
                You have no pets in month $0$.

                Then at month $1$, you get a pair of baby \bunny{}.

                A pair of baby \bunny{} matures after one month.

                Every subsequent month, a mature pair of \bunny{} produces a pair of baby
                \bunny{}.  

                The \bunny{} never \emoji{skull}.
            }%
            \only<2>{%
                Let $F_n$ denote the pairs of \bunny{} you have at month $n$.

                The sequence
                \begin{equation*}
                    \{F_n\}_{n \ge 0}
                    =
                    0, 1, 1, 2, 3, 5, 8, \dots
                \end{equation*}
                is called the \alert{Fibonacci sequence}.

                \think{}
                Can we find a recurrence relation for
                $F_n$? A closed-form?
            }%
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-never-dies.jpg}
                \caption{When the \bunny{} never dies}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{The Recurrence Relation for The Fibonacci Sequence}

    By observing the initial few numbers, 
    we can see that
    \begin{equation*}
        F_{n+2} = F_{n+1} + F_{n},\qquad  \text{for all } n \ge 0
    \end{equation*}

    \think{} How to prove this?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.55\textwidth]{rabbit.png}
        \caption{The recurrence relation for the Fibonacci sequence}
    \end{figure}
\end{frame}

\subsection{Recurrence For Strings}

\begin{frame}
    \frametitle{Example 9.3}
    
    Let $a_{n}$ be the number of binary strings in which no two consecutive characters are
    $1$'s.

    \think{} Can we find a recurrence relation for $a_{n}$?

    \hint{} Note that
    \begin{equation*}
        \{a_{n}\}_{n \ge 1}
        =
        2, 3, 5, \dots
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 9.4 (3.6): Ternary Strings}

    Let $t_{n}$ be the number of ternary string of length $n$
    with the alphabet \(\{\temoji{apple}, \temoji{banana}, \temoji{cheese}\}\)
    which does not contain the pattern \emoji{apple}\emoji{cheese}.

    \think{} Can we find a recurrence relation for $t_{n}$?
\end{frame}

\subsection{Lines and Regions}

\begin{frame}
    \frametitle{Review: Example 3.3}

    We draw some infinitely long lines to divide an infinitely
    large continent into a number kingdoms such that
    \begin{itemize}
        \item \emph{No} point on the continent belongs to more than two lines,
        \item and \emph{no} two lines are parallel.
    \end{itemize}

    \think{} Let $r_n$ be the number of kingdoms for $n$ lines. Can we find a recurrence
    relation for $r_n$?

    \begin{figure}[htpb]
        \centering
        \resizebox{4.5cm}{!}{\input{../tikz-snippet/kindom.tex}}
        \caption{Three lines divide the continent into seven kingdoms.}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Closed form vs recurrence}
%
%    The number of kingdoms we get is \( r_{0} = 1 \) and
%    \begin{equation*}
%        r_{n} = r_{n-1} + n, \qquad \text{for all } n \ge 1.
%    \end{equation*}
%    This is called an \alert{recurrence equation}.
%
%    \hint{} We can also consider the sequence \( (r_n)_{n \ge 0} \) as a
%    function \( r: \mathbb{N} \mapsto \mathbb{N} \) and write
%    \begin{equation*}
%        r(n) = 
%        \begin{cases}
%            1, & \text{if } n = 0, \\
%            r(n-1) + n, & \text{if } n \ge 1.
%        \end{cases}
%    \end{equation*}
%
%    \think{} Can we find a \emph{closed} formula for \( r_n \)?
%\end{frame}

\section{\acs{ac} 9.2 Linear Recurrence Equations}

\subsection{The Definition of a Linear Recurrence Equation}

\begin{frame}{Linear recurrence equations}
    A sequence \( \{a_{n}\}_{n\ge 0}\) satisfies a \alert{linear recurrence} if
    \[
        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
    \]
    where 
    \begin{itemize}
        \item \(k \ge 1\) is an integers,
        \item \(c_{0}, c_{1},\dots,c_{k}\)  are constants, 
        \item with \(c_{0}, c_{k} \ne 0\),  (if they are $0$, why not shorten \ac{lhs}
            \smiling{})
        \item and \(g\) is a function.
    \end{itemize}

    The number $k$ is called the \alert{order} of the recurrence.

    \pause{}

    \cake{} What are $k$, $c_{0}, \dots, c_k$ and $g(n)$ of this equation
    \begin{equation*}
        r_{n} = r_{n-1} + n,
        \qquad
        \text{for all } n \ge 1.
    \end{equation*}
\end{frame}

%\begin{frame}
%    \frametitle{\zany{} The constant \( c_0 \) and \( c_k \)}
%
%    Consider the general \( k \)-th order linear recurrence:
%    \[
%        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
%    \]
%    
%    When \( c_{0} = 0 \), the recurrence simplifies to:
%    \[
%        c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
%    \]
%    
%    Similarly, when \( c_{k} = 0 \), it becomes:
%    \[
%        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k-1} a_{n} = g(n),
%    \]
%
%    \hint{} To uniquely define an \( k \)-th order linear recurrence, both \( c_{0} \) and \( c_{k} \) must be nonzero.
%\end{frame}

\subsection{Homogeneous and Nonhomogeneous Recurrence}

\begin{frame}[c]
    \frametitle{\zany{} Homogeneity}
    
    \begin{columns}[c]
        % Column 1: Etymology
        \begin{column}{0.5\textwidth}
            The term \emph{homogeneous} originates from two \emoji{greece} words:
            \begin{itemize}
                \item \textbf{Homos:} Meaning "same" or "similar"
                \item \textbf{Genos:} Meaning "kind" or "type"
            \end{itemize}
            
            Thus \emph{homogeneous} means \emph{similar kind}.
        \end{column}
        
        % Column 2: Funny Bunny Picture
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{bunny-identical.jpg}
                \caption{Homogeneous \bunny{}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{homogeneous and Nonhomogeneous Recurrence}
    The recursion is \alert{homogeneous} if \(g(n)\) is always \(0\), like the
    Fibonacci sequence:
    \[
        f_{n+2} - f_{n+1} - f_n = 0
    \]
    Otherwise it is \alert{nonhomogeneous}, like the \emoji{crown} sequence:
    \[
        r_{n+1}-r_{n}=n+1
    \]

    \cake{} Is the following homogeneous or nonhomogeneous?
    \begin{equation*}
    t_{n+2} = 3 t_{n+1} - t_{n}
    \end{equation*}
\end{frame}

\subsection{Advancement operator}

\begin{frame}
    \frametitle{Advancement operators}

    An \alert{operator} is a function that maps a function to another function.

    The \alert{advancement operator} \( A \) is defined as:
    \begin{equation*}
        A f(n) = f(n + 1)
    \end{equation*}

    We can generalize this to \( A^{p} \):
    \begin{equation*}
        A^{p} f(n) = f(n + p)
    \end{equation*}

    \pause{}

    \cake{} Let \( f(n)  = n(n+1)\): What are \( A f \) and \( A^{2} f \)?
\end{frame}

\begin{frame}
    \frametitle{Write the Fibonacci Sequence with Advancement Operators}

    The advancement operator \( A \) allows us to write recurrence more succinctly.
    
    For example,
    \[
        f(n + 2) - f(n + 1) - f(n) = 0
    \]
    can be rewritten as
    \[
        A^{2} f(n) - A f(n) - A^{0} f(n) = 0
    \]
    or even more simpler
    \[
        (A^{2} - A - 1) f = 0
    \]
\end{frame}

\begin{frame}
    \frametitle{Write Linear recurrences with Advancement Operators}

    Similarly, the linear recurrence
    \[
        c_{0} f(n+k) + c_{1} f(n+k-1) + \cdots + c_{k} f(n) = g(n),
    \]
    can be succinctly expressed as
    \[
        p(A) f = g(n).
    \]
    where
    \begin{equation*}
        p(A) = c_{0} A^{k} + c_{1} A^{k-1} + \cdots + c_{k}
    \end{equation*}

    \pause{}

    \cake{}
    How to write
    \begin{equation*}
        f(n+3) = 2f(n+2) + 3f(n+1) - f(n)
    \end{equation*}
    with advancement operator \( A \)?
\end{frame}

\begin{frame}
    \frametitle{\puzzle{} Exercise}

    Consider the following linear recurrences:
    \begin{enumerate}
        \item \( g(n+1) = -g(n) \)
        \item \( h(n+2) = h(n+1) + 2h(n) + n^{2} \)
    \end{enumerate}

    Can you rewrite these equations using the advancement operator \( A \)?

    \pause{}

    Answer:
    \begin{enumerate}
        \item $(A + 1) g = 0$
        \item $(A^2 - A - 2) h = n^{2}$
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Example 9.8}
    Suppose that $\{s_{n}\}_{0}$ is a sequence 
    satisfies $s_{0}=3$ and $s_{n+1} = 2s_{n}$ for $n \ge 0$.

    \think{} How to find a closed (explicit) formula for $s_{n}$?

    \pause{}

    Answer: Let $f(n) = s_{n}$. Then
    \begin{equation*}
        (A - 2) f(n) = 0
    \end{equation*}
    \cake{} Can you think of a function that satisfies this?
\end{frame}

%\begin{frame}
%    \frametitle{A property advancement operator}
%
%    If $p(A) = q(A)$ holds as \emph{polynomials} of the variable $A$,
%    then $p(A)f = q(A)f$ as \emph{functions}.
%
%    \begin{example}
%        Let us verify this:
%        \begin{equation*}
%            (A-2)(A + 3) f = (A^{2} + A - 6)f.
%        \end{equation*}
%    \end{example}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Another property advancement operator}
%    
%    We also have $p(A)(f + g) = p(A) f + p(A) g$.
%    \begin{example}[]
%        Let us verify this:
%        \begin{equation*}
%            (A-2)(f + g) = (A-2) f + (A-2) g
%        \end{equation*}
%    \end{example}
%\end{frame}

\section{\acs{ac} 9.4 Solving Advancement Operator Equations}

\subsection{Homogeneous Equations}

\begin{frame}
    \frametitle{Example 9.9}
    \think{} How to find all functions $f$ that satisfy
    \begin{equation}
        \label{eq:homogeneous:1}
        (A^{2}+A-6) f 
        = 0
    \end{equation}

    \only<1,2>{%
        \hint{} Note we can factor polynomial like
        \begin{equation*}
            (A^{2}+A-6) 
            = 
            (A+3)(A-2)
            =
            (A-2)(A+3)
        \end{equation*}
        and that
        \begin{equation*}
            (A^{2}+A-6) f 
            =
            (A+3)(A-2)
            f
            =
            (A-2)(A+3)
            f
        \end{equation*}
    }%
    \only<2>{%
        \emoji{star} 
        If $p(A) = q(A)$ holds as \emph{polynomials} of the variable $A$,
        then $p(A)f = q(A)f$ as \emph{functions}.
    }%
\end{frame}

\begin{frame}
    \frametitle{Example 9.9: Guess an Answer}
    \cake{}
    Can you think of a function $f_{1}$ that satisfies
    \begin{equation*}
        (A+3)(A-2) f_{1} = 0
    \end{equation*}
    \pause{}%
    \cake{}
    Can you think of a function $f_{2}$ that satisfies
    \begin{equation*}
        (A-2)(A+3) f_{2} = 0
    \end{equation*}
    \pause{}%
    \think{} We happens when we replace $f$ by $f_{1} + f_{2}$ in the following
    \begin{equation*}
        A^{2} + A - 6 f
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.10}
    Consider the recurrence
    \begin{equation}
        \label{eg:9.10}
        (A^{2} - 3 A + 1) t = 0
    \end{equation}

    \pause{}

    Note that
    \begin{equation*}
        A^{2} - 3 A + 1
        =
        \left(A-\frac{3 + \sqrt{5}}{2}\right)\left(A-\frac{3 - \sqrt{5}}{2}\right)
    \end{equation*}

    \only<2>{%
        \hint{} We can factorize a quadratic polynomial using the follow formula
        \begin{equation*}
            x^{2} + b x + c
            =
            \left(x - \frac{-b + \sqrt{b^{2} - 4 c}}{2}\right)
            \left(x - \frac{-b - \sqrt{b^{2} - 4 c}}{2}\right)
        \end{equation*}
    }%

    \pause{}

    Thus, for any constant $c_{1}, c_{2}$ (two arbitrary numbers that does not depend on $n$),
    \begin{equation*}
        t(n) = 
        c_{1} \left(A-\frac{3 + \sqrt{5}}{2}\right)^{n}
        +
        c_{2}
        \left(A-\frac{3 - \sqrt{5}}{2}\right)^{n}
    \end{equation*}
    satisfies \eqref{eg:9.10}.

    \cake{} What if we also want $t(1) = 3$ and $t(2) = 8$?
\end{frame}

\begin{frame}
    \frametitle{General Solutions}

    \emoji{star}
    The functions $f$ satisfying a homogeneous linear recurrence
    \begin{equation*}
        p(A) f = 0
    \end{equation*}
    of order $k$ is always for the form
    \begin{equation*}
        c_{1} f_{1}(n) + c_{2} f_{2}(n) + \cdots + c_{k} f_{k}(n)
    \end{equation*}
    where $c_{1}, c_{2}, \cdots, c_{k}$ are constants and
    $f_{1}, f_{2}, \dots, f_{k}$ are \alert{linearly independent} functions.

    \pause{}
    
    The set of such function is called the \alert{general solution} of the linear recurrence.

    \hint{} The proof for this should be covered in a linear algebra course. We will just
    focus on finding the $f_{1}, f_{2}, \cdots, f_{k}$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.11}
    
    \cake{} Can you guess the general solution of
    \begin{equation*}
        (A+1)(A-6)(A+4)f = 0
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.12}
    
    \think{} How to find the general solution of
    \begin{equation*}
        (A-2)^{2} f = 0
    \end{equation*}
    \cake{} Would $c_{1} 2^{n} + c_{2} 2^{n}$ be the general solution?
\end{frame}

\begin{frame}
    \frametitle{Example 9.13}

    Let $\{f_{n}\}$ be the sequence which satisfies a linear recurrence
    \begin{equation*}
        f_{n+4} = −2 f_{n+3} + 12 f_{n+2} - 14 f_{n+1} + 5 f_{n}
    \end{equation*}
    and $f_{0} = 1, f_{1} = 2, f_{2} = 4, f_{3} = 4$.

    \think{} How can we find closed formula for $f_{n}$?

    \only<2>{%
        Step 1: 
        Find the \emph{general solution} of
        \begin{equation*}
            (A^{4} + 2 A^{3} - 12 A^{2} + 14 A - 5) f 
            =
            (A+5)(A-1)^{3} f
            = 0
        \end{equation*}
        Obviously, $f_{1}(n) = (-5)^{n}, f_{2}(n) = 1^{n}, f_{3}(n) = n \cdot 1^{n}$ all satisfy
        this.

        \cake{} Can you think of a $f_{4}(n)$ which also satisfies it?
    }%
    \only<3>{%
        Step 2: 
        Having found that the general solution,
        we know that
        \begin{equation*}
            \begin{aligned}
                f_0 & = c_{1} (-5)^{0} + c_{2} 1^{0} + c_{3} 0 \cdot 1^{0} + c_{4} 
                \blankshort{} \\
                f_1 & = c_{1} (-5)^{1} + c_{2} 1^{1} + c_{3} 1 \cdot 1^{1} + c_{4} 
                \blankshort{} \\
                f_2 & = c_{1} (-5)^{2} + c_{2} 1^{2} + c_{3} 2 \cdot 1^{2} + c_{4} 
                \blankshort{} \\
                f_3 & = c_{1} (-5)^{3} + c_{2} 1^{3} + c_{3} 3 \cdot 1^{3} + c_{4} 
                \blankshort{} \\
            \end{aligned}
        \end{equation*}
        Solving this linear system, we find that
        \begin{equation*}
            c_{1} = \blankveryshort{}, \qquad
            c_{2} = \blankveryshort{}, \qquad
            c_{3} = \blankveryshort{}, \qquad
            c_{4} = \blankveryshort{}
        \end{equation*}
    }%
\end{frame}

\end{document}
