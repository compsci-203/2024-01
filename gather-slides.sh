#!/bin/bash

# The slides
find \
    slides \
    -type f \
    \( -name "*.pdf" ! -path "*/images/*" \) \
    -printf '%p\n' \
    -exec cp {} pdf \;
